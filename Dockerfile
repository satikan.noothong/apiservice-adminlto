FROM node:19-bullseye-slim

# Expose the API Port
EXPOSE 9000

# Working Dir
WORKDIR /src/app
COPY . /src/app

# Install Files
# RUN npm install -g npm@9.2.0
RUN npm install

COPY . .

CMD [ "npm", "start" ]