const express = require("express");
const cors = require("cors");
const expressValidator = require("express-validator");
const httpContext = require('express-http-context');
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger-output.json");
const { createServer } = require('http');
const { Server } = require('socket.io');
const dotenv = require("dotenv");
const app = express();
app.use(httpContext.middleware);
const config = require("./src/config/config");
dotenv.config();
// import routes
const authentication_router = require("./src/routes/authentication.routes");
const master_router = require("./src/routes/master.routes");
const announce_router = require("./src/routes/announce.routes");
const agent_router = require("./src/routes/agent.routes");
const helper_router = require("./src/routes/helper.routes");
const bank_agent_router = require("./src/routes/bankAgent.routes");
const contact_router = require("./src/routes/contact.routes");
const popup_router = require("./src/routes/popup.routes");
const depositWithdraw_router = require("./src/routes/depositWithdraw.routes");
const settingDepositWithdraw_router = require("./src/routes/settingDepositWithdraw.routes");
const reportCredit_router = require("./src/routes/reportCredit.routes");
const inbox_router = require("./src/routes/inbox.routes");
const outbox_router = require("./src/routes/outbox.routes");
const listDepositWithdraw_router = require("./src/routes/listDepositWithdraw.routes");
const sms_transfer_router = require("./src/routes/smsTransfer.routes");
const sms_general_router = require("./src/routes/smsGeneral.routes");
const list_member_router = require("./src/routes/listMember.routes");
const login_router = require("./src/routes/login.routes");
const network_member_router = require("./src/routes/networkMember.routes");
const bank_router = require("./src/routes/bank.routes");
const menu_router = require("./src/routes/menu.routes");
const menu_group_router = require("./src/routes/menuGroup.routes");
const member_router = require("./src/routes/member.routes");
const ask_credit_router = require("./src/routes/askCredit.routes");
const listDirectDepositWithdraw_router = require("./src/routes/listDirectDepositWithdraw.routes");
const listHuay_router = require("./src/routes/listHuay.routes");
const commission_router = require("./src/routes/commission.routes");
const permission_router = require("./src/routes/permission.routes");
const listMemberNetwork_router = require("./src/routes/listMemberNetwork.routes");
const accountSummary_router = require("./src/routes/accountSummary.routes");
const dailySummary_router = require("./src/routes/dailySummary.routes");
const board_router = require("./src/routes/board.routes");
const create_file_router = require("./src/routes/createFile.routes");
const banner_router = require("./src/routes/banner.routes");
const profile_router = require("./src/routes/profile.routes");
const api_router = require("./src/routes/api.routes");
const { getDataLotto } = require("./src/utils/utilities");

app.use(function (req, res, next) {
    // 5 Min
    req.socket.setTimeout(300000);
    next();
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(expressValidator());

app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use("/api/authentication", authentication_router);
app.use("/api/master", master_router);
app.use("/api/banner", banner_router);
app.use("/api/announce", announce_router);
app.use("/api/agent", agent_router);
app.use("/api/helper", helper_router);
app.use("/api/bank_agent", bank_agent_router);
app.use("/api/contact", contact_router);
app.use("/api/popup", popup_router);
app.use("/api/depositWithdraw", depositWithdraw_router);
app.use("/api/settingDepositWithdraw", settingDepositWithdraw_router);
app.use("/api/reportCredit", reportCredit_router);
app.use("/api/inbox", inbox_router);
app.use("/api/outbox", outbox_router);
app.use("/api/listDepositWithdraw", listDepositWithdraw_router);
app.use("/api/smsTransfer", sms_transfer_router);
app.use("/api/smsGeneral", sms_general_router);
app.use("/api/listMember", list_member_router);
app.use("/api/login", login_router);
app.use("/api/networkMember", network_member_router);
app.use("/api/bank", bank_router);
app.use("/api/menu", menu_router);
app.use("/api/menuGroup", menu_group_router);
app.use("/api/member", member_router);
app.use("/api/askCredit", ask_credit_router);
app.use("/api/listDirectDepositWithdraw", listDirectDepositWithdraw_router);
app.use("/api/listHuay", listHuay_router);
app.use("/api/commission", commission_router);
app.use("/api/permission", permission_router);
app.use("/api/listMemberNetwork", listMemberNetwork_router);
app.use("/api/accountSummary", accountSummary_router);
app.use("/api/dailySummary", dailySummary_router);
app.use("/api/board", board_router);
app.use("/api", create_file_router);
app.use("/api/profile", profile_router);
app.use("/api", api_router);

// Start Modified by Noppanut Noothong 08/08/2023
// app.listen(config.port, async() => {
//   console.log(
//     `Server is running on port : ${config.port} for ${config.app_env}`
//   );

//   await getDataLotto();
// });

const httpServer = createServer(app);

const io = new Server(httpServer, {
    cors: {
      origin: [config.original_host, config.original_admin_host],
      methods: ['GET', 'POST'],
    }
});

global.io = io;

// รอการ connect จาก client
io.on('connection', async (client) => {
    console.log(`User connected : ${client.id}`);
  
    // เมื่อ Client ตัดการเชื่อมต่อ
    client.on('disconnect', () => {
        console.log(`User disconnected : ${client.id}`);
    });
});

httpServer.listen(config.port, async () => {
    console.log(`Server is running on port : ${config.port} for ${config.app_env}`);
    await getDataLotto();
});
// End Modified by Noppanut Noothong 08/08/2023