const keyHuaySelector = {
    top_3_number: 1,
    th_3_number: 2,
    front_3_number: 4,
    back_3_number: 5,
    top_2_number: 6,
    bottom_2_number: 7,
    top_1_number: 9,
    bottom_1_number: 10,
    top_4_number: 11,
    th_4_number: 12,
  };
  const lotto = {
    THAI: 1,
    YIKI: 2,
    HUAYHUNG: 3,
    HUAYCHUD: 4,
    HUAYCOUNTRY: 5
  };

  const lotto_status = `(7,8,9,10,12)`

  const whiteListSubLotto = ['1']

  const HUAYHUNG_THAI_PID_YEAN = '21';

  module.exports = {
    keyHuaySelector,
    lotto,
    lotto_status,
    whiteListSubLotto,
    HUAYHUNG_THAI_PID_YEAN
  }

  