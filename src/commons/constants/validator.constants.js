const msg_validate = {
    msg_required: "@fieldname is required",
    msg_date_format: "@fieldname is the incorrect date string format. It should be @formatdate",
    msg_not_number: "@fieldname is not a number",
    msg_not_decimal: "@fieldname is not a decimal",
    msg_not_string: "@fieldname is not a string",
    msg_must_be_length: "@fieldname must be @number chars long",
    msg_email: "Invalid email address"
}

module.exports = {
    msg_validate
}