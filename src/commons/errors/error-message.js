module.exports = {
    SUCCESS: {
        code: 200,
        message: 'Success'
    },
    INTERNAL_SERVER_ERROR: {
        code: 500,
        message: 'Error'
    },

    CLIENT_ERROR: {
        code: 400,
        message: 'Error'
    }
}