const { INTERNAL_SERVER_ERROR } = require("../errors/error-message");


class ApplicationError extends Error {
    constructor(error= {...INTERNAL_SERVER_ERROR}, result=undefined){
        super(error.message);
        this.code = error.code;
        this.message = error.message;
        this.error = result
    }
}

module.exports = ApplicationError;