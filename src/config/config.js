const dotenv = require( "dotenv" );
dotenv.config();

const env = process.env;

const config = {
    app_env: env.APP_ENV,
    port: parseInt(env.PORT),
    jwt_secret: env.ACCESS_TOKEN_SECRET,
    access_secret: env.ACCESS_TOKEN_SECRET,
    refresh_secret: env.REFRESH_TOKEN_SECRET,
    jwt_username: env.TOKEN_USERNAME,
    jwt_password: env.TOKEN_PASSWORD,
    jwt_expire: env.TOKEN_EXPIRE,
    refresh_token_expire: env.REFRESH_TOKEN_EXPIRE,
    swagger_host: env.SWAGGER_HOST,
    original_host: env.ORIGINAL_HOST,
    original_admin_host: env.ORIGINAL_ADMIN_HOST,
    pg_connection: {
        host: env.PG_HOST,
        user: env.PG_USER,
        password: env.PG_PASSWORD,
        database: env.PG_DATABASE,
        port: env.PG_PORT,
        // tz: env.PG_TZ,
        ssl: {
            rejectUnauthorized: false
        }
    },
    aws_s3: {
        bucket_name: env.AWS_BUCKET_NAME,
        access_key: env.AWS_ACCESS_KEY,
        access_secret: env.AWS_SECRET_KEY,
        region : env.AWS_BUCKET_REGION
    },
    sms:{
        api_key: env.SMSMKT_API_KEY,
        sender: env.SMSMKT_SENDER,
        secret_key: env.SMSMKT_SECRET_KEY,
        host: env.SMSMKT_HOST
    }
}

module.exports = config;