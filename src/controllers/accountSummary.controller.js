const db_access = require("../db_access");
const moment = require("moment/moment");
const { format } = require('date-fns');

// Retrieve AccountSummary.
exports.get_by_today = async (req, res) => {
  // #swagger.tags = ['AccountSummary']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/AccountSummary" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let one = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
      one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
      one += "from bank b;";
    } else if (typeAgent === 1) {
      one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
      one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
      one += "from bank b;";
    }else if (typeAgent === 2 || typeAgent === 3) {
      one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit,COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept,";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, COALESCE((select SUM(amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept,";
      one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and cast(current_date as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting from bank b;";
    }
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};



// Retrieve AccountSummary.
exports.get_by_date = async (req, res) => {
  // #swagger.tags = ['AccountSummary']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/AccountSummary" }} */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let one = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }

    const formattedStartDate = format(new Date(data.start_date), 'yyyy-MM-dd');
    const formattedEndDate = format(new Date(data.end_date), 'yyyy-MM-dd');
    if(data.current){
      if (typeAgent === 0) {
        one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1),0) as total_deposit, ";
        one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2),0) as total_withdraw, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
        one += "from bank b;";
      } else if (typeAgent === 1) {
        one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1),0) as total_deposit, ";
        one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2),0) as total_withdraw, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
        one += "from bank b;";
      } else if (typeAgent === 2 || typeAgent === 3) {
        one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1),0) as total_deposit,COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept,";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2),0) as total_withdraw, COALESCE((select SUM(amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept,";
        one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex where lhex.bank_id  = b.id and (CAST(lhex.created_date AS DATE) BETWEEN CURRENT_DATE - INTERVAL '8 days' AND CURRENT_DATE - INTERVAL '1 days') and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting from bank b;";
      }
    } else {
      if (formattedStartDate !== formattedEndDate && formattedEndDate !== formattedEndDate) {
        if (typeAgent === 0) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        } else if (typeAgent === 1) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        } else if (typeAgent === 2 || typeAgent === 3) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE)  and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE)  and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) < cast(lhex.created_date as DATE)  and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) < cast(lhex.created_date as DATE)  and cast('" + data.end_date + "' as DATE) > cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        }
      } else {
        if (typeAgent === 0) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        } else if (typeAgent === 1) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" and lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        } else if (typeAgent === 2 || typeAgent === 3) {
          one += "select b.bank_name , COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1),0) as total_deposit, ";
          one += "COALESCE((select SUM(amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 1 and lhex.status = 4),0) as total_deposit_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2),0) as total_withdraw, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "'as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 4),0) as total_withdraw_accept, ";
          one += "COALESCE((select SUM(lhex.amount) from deposit_withdraw lhex inner join members m on lhex.member_id = m.id where lhex.bank_id  = b.id and cast('" + data.start_date + "' as DATE) = cast(lhex.created_date as DATE) and lhex.trans_type_id = 2 and lhex.status = 3),0) as total_withdraw_waiting ";
          one += "from bank b;";
        }
      }
    }
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve AccountSummary.
exports.get_by_six_month = async (req, res) => {
  // #swagger.tags = ['AccountSummary']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/AccountSummary" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let one = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      one += "select TO_CHAR(DATE_TRUNC('month',dw.created_date), 'YYYY-MM') AS  create_date, COALESCE(SUM(dw.amount) filter (where trans_type_id = 2),0) as sum_withdraw ,";
      one += "COALESCE(SUM(dw.amount) filter (where trans_type_id = 1),0) as sum_deposit ,COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2 and dw.status =3),0) as summary";
      one += " from deposit_withdraw dw inner join members m on dw.member_id = m.id inner join agent a on a.agent_id = m.agent_id where m.agent_id ="+agentId+" and cast(dw.created_date+ INTERVAL '7 hours' as DATE) >= cast(CURRENT_DATE - INTERVAL '6 months' as DATE) group by  DATE_TRUNC('month',dw.created_date)  order by DATE_TRUNC('month',dw.created_date)  desc;";
    } else if (typeAgent === 1) {
      one += "select TO_CHAR(DATE_TRUNC('month',dw.created_date), 'YYYY-MM') AS  create_date, COALESCE(SUM(dw.amount) filter (where trans_type_id = 2),0) as sum_withdraw ,";
      one += "COALESCE(SUM(dw.amount) filter (where trans_type_id = 1),0) as sum_deposit ,COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2 and dw.status =3),0) as summary";
      one += " from deposit_withdraw dw inner join members m on dw.member_id = m.id inner join agent a on a.agent_id = m.agent_id where m.agent_id ="+agentNo+" and cast(dw.created_date+ INTERVAL '7 hours' as DATE) >= cast(CURRENT_DATE - INTERVAL '6 months' as DATE) group by  DATE_TRUNC('month',dw.created_date)  order by DATE_TRUNC('month',dw.created_date)  desc;";
    }else if (typeAgent === 2) {
      one += "select TO_CHAR(DATE_TRUNC('month',dw.created_date), 'YYYY-MM') AS  create_date, COALESCE(SUM(dw.amount) filter (where trans_type_id = 2),0) as sum_withdraw ,";
      one += "COALESCE(SUM(dw.amount) filter (where trans_type_id = 1),0) as sum_deposit ,COALESCE(SUM(dw.amount) filter (where  dw.trans_type_id = 2 and  dw.status =3),0) as summary";
      one += " from deposit_withdraw dw  where cast(dw.created_date + INTERVAL '7 hours' as DATE) >= cast(CURRENT_DATE - INTERVAL '6 months' as DATE) group by  DATE_TRUNC('month',dw.created_date)  order by DATE_TRUNC('month',dw.created_date)  desc;";
    }else if (typeAgent === 3) {
      one += "select TO_CHAR(DATE_TRUNC('month',dw.created_date), 'YYYY-MM') AS  create_date, COALESCE(SUM(dw.amount) filter (where trans_type_id = 2),0) as sum_withdraw ,";
      one += "COALESCE(SUM(dw.amount) filter (where trans_type_id = 1),0) as sum_deposit ,COALESCE(SUM(dw.amount) filter (where  dw.trans_type_id = 2 and  dw.status =3),0) as summary";
      one += " from deposit_withdraw dw  where cast(dw.created_date + INTERVAL '7 hours' as DATE) >= cast(CURRENT_DATE - INTERVAL '6 months' as DATE) group by  DATE_TRUNC('month',dw.created_date)  order by DATE_TRUNC('month',dw.created_date)  desc;";
    }
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
