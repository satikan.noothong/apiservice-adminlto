const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require("bcrypt");

// Retrieve agent.
exports.get_list_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */

  try {
    let agent = "";
    const listNote = [];
    let typeUser = "";
    const agentId = req.user.agent_id;
    const type = "select a.type_user from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let first = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeUser === 0) {
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +agentId + " order by a.updated_date desc;";
    } else if (typeUser === 1) {
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +agentNo + " order by a.updated_date desc;";
    } else if (typeUser === 2 || typeUser === 3) {
      first =  "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0  order by a.updated_date desc;";
    }
    let resu = await db_access.query(first);
    let resp = resu.rows;
    let resul = await db_access.query("select * from note_agent;");
    const respo = resul.rows;
    for (let i = 0; i < resp.length; i++) {
      const dataList = {
        agent_id: {},
        username: {},
        credit: {},
        phone: {},
        lastip: {},
        last_access: {},
        created_date: {},
        agentName: {},
        status_name_th: {},
        type_user: {},
        note_agent: {},
      };
      dataList.agent_id = resp[i].agent_id;
      dataList.username = resp[i].username;
      dataList.credit = resp[i].credit;
      dataList.phone = resp[i].phone;
      dataList.lastip = resp[i].lastip;
      dataList.last_access = resp[i].last_access;
      dataList.created_date = resp[i].created_date;
      dataList.agentName = resp[i].agentname;
      dataList.status_name_th = resp[i].status_name_th;
      dataList.type_user = resp[i].type_user;

      agent = dataList.agent_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {
        if (agent === respo[c].agent_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0) {
          dataList.note_agent = listNo;
        } else {
          dataList.note_agent = [];
        }
      }
      listNote.push(dataList);
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve agent.
exports.get_search_list_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */

  try {
    let agent = "";
    const listNote = [];
    const agentId = req.user.agent_id;
    const data = req.body;
    const type =
      "select a.type_user from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let first = "";
    let typeUser = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
    }
    if (data.text !== null || data.text !== "") {
      if (typeUser === 0) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +
          agentId +
          " and (cast(a.agent_id as text) ilike '%" +
          data.text +
          "%' or a.username ilike '%" +
          data.text +
          "%' or cast(a.phone as text) ilike '%" +
          data.text +
          "%' or cast(a.credit as text) ilike '%" +
          data.text +
          "%' or cast(a.lastip as text) ilike '%" +
          data.text +
          "%' or cast(a.last_access as text) ilike '%" +
          data.text +
          "%' or cast(a.created_date as text) ilike '%" +
          data.text +
          "%' or aa.username ilike '%" +
          data.text +
          "%' or s.status_name_th ilike '%" +
          data.text +
          "%' or cast(a.type_user as text) ilike '%" +
          data.text +
          "%') order by a.updated_date desc;";
      } else if (typeUser === 1) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +
          agentId +
          " and (cast(a.agent_id as text) ilike '%" +
          data.text +
          "%' or a.username ilike '%" +
          data.text +
          "%' or cast(a.phone as text) ilike '%" +
          data.text +
          "%' or cast(a.credit as text) ilike '%" +
          data.text +
          "%' or cast(a.lastip as text) ilike '%" +
          data.text +
          "%' or cast(a.last_access as text) ilike '%" +
          data.text +
          "%' or cast(a.created_date as text) ilike '%" +
          data.text +
          "%' or aa.username ilike '%" +
          data.text +
          "%' or s.status_name_th ilike '%" +
          data.text +
          "%' or cast(a.type_user as text) ilike '%" +
          data.text +
          "%') order by a.updated_date desc;";
      } else if (typeUser === 2) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and (cast(a.agent_id as text) like '%" +
          data.text +
          "%' or a.username ilike '%" +
          data.text +
          "%' or cast(a.phone as text) ilike '%" +
          data.text +
          "%' or cast(a.credit as text) ilike '%" +
          data.text +
          "%' or cast(a.lastip as text) ilike '%" +
          data.text +
          "%' or cast(a.last_access as text) ilike '%" +
          data.text +
          "%' or cast(a.created_date as text) ilike '%" +
          data.text +
          "%' or aa.username ilike '%" +
          data.text +
          "%' or s.status_name_th ilike '%" +
          data.text +
          "%' or cast(a.type_user as text) ilike '%" +
          data.text +
          "%')  order by a.updated_date desc;";
      } else {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and (cast(a.agent_id as text) like '%" +
          data.text +
          "%' or a.username ilike '%" +
          data.text +
          "%' or cast(a.phone as text) ilike '%" +
          data.text +
          "%' or cast(a.credit as text) ilike '%" +
          data.text +
          "%' or cast(a.lastip as text) ilike '%" +
          data.text +
          "%' or cast(a.last_access as text) ilike '%" +
          data.text +
          "%' or cast(a.created_date as text) ilike '%" +
          data.text +
          "%' or aa.username ilike '%" +
          data.text +
          "%' or s.status_name_th ilike '%" +
          data.text +
          "%' or cast(a.type_user as text) ilike '%" +
          data.text +
          "%')  order by a.updated_date desc;";
      }
    } else {
      if (resps[i].type_user === 0) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +
          agentId +
          " order by a.updated_date desc;";
      } else if (resps[i].type_user === 1) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0 and a.agent =" +
          agentId +
          " order by a.updated_date desc;";
      } else if (resps[i].type_user === 2) {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0  order by a.updated_date desc;";
      } else {
        first =
          "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 0  order by a.updated_date desc;";
      }
    }

    resu = await db_access.query(first);
    const resp = resu.rows;
    let resul = await db_access.query("select * from note_agent;");
    const respo = resul.rows;
    for (let i = 0; i < resp.length; i++) {
      const dataList = {
        agent_id: {},
        username: {},
        credit: {},
        phone: {},
        lastip: {},
        last_access: {},
        created_date: {},
        agentName: {},
        status_name_th: {},
        type_user: {},
        note_agent: {},
      };
      dataList.agent_id = resp[i].agent_id;
      dataList.username = resp[i].username;
      dataList.credit = resp[i].credit;
      dataList.phone = resp[i].phone;
      dataList.lastip = resp[i].lastip;
      dataList.last_access = resp[i].last_access;
      dataList.created_date = resp[i].created_date;
      dataList.agentName = resp[i].agentname;
      dataList.status_name_th = resp[i].status_name_th;
      dataList.type_user = resp[i].type_user;

      agent = dataList.agent_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {
        if (agent === respo[c].agent_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0) {
          dataList.note_agent = listNo;
        } else {
          dataList.note_agent = [];
        }
      }
      listNote.push(dataList);
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_agent_id = async (req, res) => {
  // #swagger.tags = ['Agent']
  // const data = req.body;
  const data = req.params.id;
  const listMenu = [];
  let agent_id = "";
  let username = "";
  let password = "";
  let confirm_password = "";
  let status = "";
  let reference = "";
  let phone = "";
  let email = "";
  let lastip = "";
  const dataList = {
    agent_id: {},
    username: {},
    password: {},
    confirm_password: {},
    status: {},
    reference: {},
    phone: {},
    email: {},
    lastip: {},
    menu: {},
  };
  try {
    let result = await db_access.query(
      "SELECT a.agent_id, a.username , a.password, a.confirm_password, a.status, a.reference, a.phone, a.email, a.lastip, a.uuid FROM public.agent a where a.agent_id =" +
        data +
        ";"
    );
    const resp = result.rows;
    let response = await db_access.query(
      "select  mam.menu_id from menu m inner join menu_group mg  on m.menu_group_id  = mg.id  inner join mapping_agent_menu mam on mam.menu_id = m.id inner join agent a on a.agent_id = mam.agent_id where mg.status = 1 and m.status =1 and a.agent_id =" +
        data +
        ";"
    );
    const response1 = response.rows;
    for (let i = 0; i < resp.length; i++) {
      agent_id = resp[i].agent_id;
      username = resp[i].username;
      password = resp[i].password;
      confirm_password = resp[i].confirm_password;
      status = resp[i].status;
      reference = resp[i].reference;
      phone = resp[i].phone;
      email = resp[i].email;
      lastip = resp[i].lastip;
      uuid = resp[i].uuid;
    }

    for (let i = 0; i < response1.length; i++) {
      const menu = response1[i].menu_id;
      listMenu.push(menu);
    }
    dataList.agent_id = agent_id;
    dataList.username = username;
    dataList.password = password;
    dataList.confirm_password = confirm_password;
    dataList.status = status;
    dataList.reference = reference;
    dataList.phone = phone;
    dataList.email = email;
    dataList.lastip = lastip;
    dataList.uuid = uuid;
    dataList.menu = listMenu;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: dataList,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */

  try {
    let result = await db_access.query(
      "SELECT * FROM public.agent where type_user = 0;"
    );
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_agent_by_login = async (req, res) => {
  // #swagger.tags = ['Agent']

  try {
    let result = await db_access.query(
      "SELECT agent_id, uuid, phone FROM public.agent where agent_id = $1;",
      [req.user.agent_id]
    );
    const resp = result?.rows?.[0];

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_agent_credit = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  try {
    const agentId = req.user.agent_id;
    let type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag  ="";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "" ;
    if (typeAgent === 1) {
      one += "SELECT aa.credit, a.username FROM public.agent a inner join agent aa on aa.agent_id = a.agent where a.agent_id=" +agentId +"";
    } else {
      one += "SELECT a.credit, a.username FROM public.agent a where a.agent_id=" +agentId +"";
    }

    let result = await db_access.query(one);
    const resp = result.rows;
    const data = Object.values(resp).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: data,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_map_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */

  try {
    let result = await db_access.query(
      "SELECT * FROM public.mapping_agent_id mai inner join menu m on m.id = mai.agent_id inner join menu_group mg on mg.id = m.menu_group_id;"
    );
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new agent.
exports.post_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Agent' }
  } */
  const data = req.body;
  const password = await bcrypt.hash(data.password, 10);
  let user = "";
  let agentId = req.user.agent_id;
  try {
    let result = await db_access.query("select count(a.username) as count from agent a  where a.username = '"+data.username+"';");
    const resp = result.rows;
    for (let i = 0; i < resp.length; i++ ){
      user = resp[i].count;
    }
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    if (user == 0 ) {
      const datas = await db_access.query(
          "CALL public.spinsertupdateagent($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);",
          [
            0,
            utilities.is_empty_string(data.username) ? null : data.username,
            utilities.is_empty_string(password) ? null : password,
            utilities.is_empty_string(password) ? null : password,
            utilities.is_empty_string(data.status) ? null : data.status,
            utilities.is_empty_string(data.reference) ? null : data.reference,
            utilities.is_empty_string(data.phone) ? null : data.phone,
            utilities.is_empty_string(data.email) ? null : data.email,
            utilities.is_empty_string(data.ipAddress) ? null : data.ipAddress,
            utilities.is_empty_string(agen) ? null : agen,
            utilities.is_empty_string(0) ? null : 0,
          ]
      );
      const agent = Object.values(datas.rows.find((a) => a.prm_agent_id))
          .values()
          .next().value;
      if (agent != null && data.menu.length > 0) {
        for (let i = 0; i < data.menu.length; i++) {
          await db_access.query("CALL public.spinsertupdatemapagent($1,$2,$3);", [
            0,
            utilities.is_empty_string(agent) ? null : agent,
            utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
          ]);
        }
      }
      res.status(200).json({
        code: 200,
        message: "Success",
      });
    } else{
      res.status(500).send({
        code: 500,
        message: "Same Username",
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new agent.
exports.put_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Agent' }
  } */
  let password = "";
  const data = req.body;
  try {
    if (data.password == null || data.password === "" || data.password === "") {
      await db_access.query(
        "CALL public.spupdateagentnopass($1,$2,$3,$4,$5,$6);",
        [
          utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
          utilities.is_empty_string(data.status) ? null : data.status,
          utilities.is_empty_string(data.reference) ? null : data.reference,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.ipAddress) ? null : data.ipAddress,
        ]
      );
    } else {
      password = await bcrypt.hash(data.password, 10);
      await db_access.query(
        "CALL public.spupdateagent($1,$2,$3,$4,$5,$6,$7,$8);",
        [
          utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
          utilities.is_empty_string(password) ? null : password,
          utilities.is_empty_string(password) ? null : password,
          utilities.is_empty_string(data.status) ? null : data.status,
          utilities.is_empty_string(data.reference) ? null : data.reference,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.ipAddress) ? null : data.ipAddress,
        ]
      );
    }
    if (data.menu.length > 0) {
      await db_access.query("CALL public.spdeletemapagent($1);", [
        utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      ]);
      for (let i = 0; i < data.menu.length; i++) {
        await db_access.query("CALL public.spinsertupdatemapagent($1,$2,$3);", [
          utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
          utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
          utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
        ]);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Change status user.
exports.put_status_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Agent' }
  } */
  const data = req.body;
  try {
    await db_access.query("CALL public.spupdatestatusagent($1,$2);", [
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.status) ? null : data.status,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete user.
exports.delete_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  const data = req.params.id;
  try {
    await db_access.query(
      "delete from public.agent  where agent_id  = " + data + ";"
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new NoteAgent.
exports.post_note_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Agent' }
  } */
  const data = req.body;
  try {
    if (data.note !== null || data.note !== "") {
      await db_access.query("CALL public.spinsertupdatenoteagent($1,$2,$3);", [
        0,
        utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
        utilities.is_empty_string(data.note) ? null : data.note,
      ]);

      res.status(200).json({
        code: 200,
        message: "Success",
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete user.
exports.delete_note_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  const data = req.params.id;
  try {
    await db_access.query(
      "delete from public.note_agent  where id  = " + data + ";"
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_note_agent = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  const data = req.params.id;
  const first =
    "select a.type_user , na.note , na.created_date, na.id  from agent a inner join note_agent na  on na.agent_id = a.agent_id where a.agent_id = ";
  const last = " and na.agent_id  = ";
  try {
    let result = await db_access.query(first + data + last + data + ";");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ListDirectDirectDepositWithdraw.
exports.get_listDirectDepositWithdrawById = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  const memberId = req.params.id;
  try {
    let result = await db_access.query("select CAST(created_date  AS DATE) as created_date , COALESCE(SUM(amount) filter (where trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(amount) filter (where trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw_agent dw  where dw.agent_id =" + memberId + " GROUP by CAST(created_date  AS DATE) order by CAST(updated_date  AS DATE) desc LIMIT 7;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ReportCredit.
exports.get_reportCreditById = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  try {
    const agentId = req.params.id;
    const first = "select dw.id , m.username as actor, a.username as member, dw.actor_id as sub_actor_id, an.username as agent, dw.amount as amount, dw.current_credit as balance, coalesce(dw.comment, dw.remark) as remark, dw.created_date + INTERVAL '7 hours' as created_date, dw.id,tt.transaction_type as status, dw.agent_id as agent_id FROM deposit_withdraw_agent dw inner join agent m on m.agent_id= dw.agent full outer join agent an on an.agent_id= dw.actor_id full outer join transaction_type tt on tt.id = dw.trans_type_id inner join agent a on a.agent_id= dw.agent_id where dw.agent_id ="+ agentId + " and dw.status = 4 order by dw.id desc;";
    let resu = await db_access.query(first);
    const respp = resu.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// create a new Member direct_withdraw.
exports.post_direct_withdraw = async (req, res) => {
  // #swagger.tags = ['Agent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Agent' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let types = "";
  let message = "";
  let name = "";
  let note = "";
  let ip = data.ip_address.ip;
  let currentCredit=0 ;
  let check = 0;
  try {
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    let agent = await db_access.query("select a.credit  from agent a  where a.agent_id  =" + data.agent_id + ";");
    let respp = agent.rows;
    for (let i = 0; i < respp.length; i++) {
       currentCredit = respp[i].credit;
    }
      let credit = parseFloat(currentCredit);
      let amount = parseFloat(data.amount);
      let currentCredits = parseFloat(data.current_credit);
      let sum = null;
      let total = null;
      if (data.trans_type_id === 1) {
        await db_access.query("CALL public.spinsertagentdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8);",
          [
              0,
            utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
            utilities.is_empty_string("3") ? null : "3",
            utilities.is_empty_string(amount) ? null : amount,
            utilities.is_empty_string(data.comment) ? null : data.comment,
            utilities.is_empty_string(currentCredits + amount) ? 0 : currentCredits + amount,
            utilities.is_empty_string(agen) ? null : agen,
            utilities.is_empty_string(ip) ? null :  ip,
          ]
        );
        sum = credit + amount;
        await db_access.query("UPDATE agent SET credit =" + sum + " WHERE agent_id =" + data.agent_id + ";");
        await db_access.query("UPDATE deposit_withdraw_agent SET status = 4 where agent_id = " + data.agent_id + ";");
        let creditMas = await db_access.query("select a.credit  from agent a  where a.type_user =2 limit 1;");
        const response = creditMas.rows;
        let cre = Object.values(response).values().next().value;
        let creditMaster = parseFloat(cre.credit);
        total = creditMaster - amount;
        await db_access.query("UPDATE agent SET credit =" + total + " WHERE type_user =2;");
        types = 3;
        name = "ฝากตรง";
        check = 1;
      } else if (data.trans_type_id === 2) {
        if (credit >= amount) {
          await db_access.query("CALL public.spinsertagentdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8);",
            [
              0,
              utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
              utilities.is_empty_string("4") ? null : "4",
              utilities.is_empty_string(amount) ? null : amount,
              utilities.is_empty_string(data.comment) ? null : data.comment,
              utilities.is_empty_string(currentCredits - amount) ? null : currentCredits - amount,
              utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id,
              utilities.is_empty_string(ip) ? null : ip,
            ]
          );
          sum = credit - amount;
          await db_access.query("UPDATE agent SET credit =" + sum + " WHERE agent_id =" + data.agent_id + ";");
          await db_access.query("UPDATE deposit_withdraw_agent SET status = 4 where agent_id = " + data.agent_id + ";");
          let creditMas = await db_access.query("select a.credit  from agent a  where a.type_user =2 limit 1;");
          const response = creditMas.rows;
          let cre = Object.values(response).values().next().value;
          let creditMaster = parseFloat(cre.credit);
          total = creditMaster + amount;
          await db_access.query("UPDATE agent SET credit =" + total + " WHERE type_user =2;");
          types = 4;
          name = "ถอนตรง";
          check = 1
        } else {
          types = "4";
          name = "ถอนตรงไม่สำเร็จ";
          note = " เนื่องจากเครดิตไม่เพียงพอ";
          check = 2
        }
      }

    if (check ===1) {
      await db_access.query("INSERT INTO ask_credit (agent_id, type_id, bank_id, status, total, current_credit, description) values ($1, $2, $3, $4, $5, $6, $7);",
          [data.agent_id, types, 0, 4, data.amount, currentCredits + amount, data.comment]
      );
      res.status(200).json({
        code: 200,
        message: "Success",
      });
    } else if (check === 2){
      await db_access.query("INSERT INTO ask_credit (agent_id, type_id, bank_id, status, total, current_credit) values ($1, $2, $3, $4, $5, $6);",
          [data.agent_id, types, 0, 5, data.amount, currentCredits - amount]
      );
      res.status(200).json({
        code: 200,
        message: "not enough credit",
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_agent_by_id = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  const data = req.params.id;
  try {
    const type = "select a.type_user, a.agent  from agent a inner join agent aa on aa.agent_id = a.agent where a.agent_id =" + data + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = null;
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one =
      "select a.agent_id , a.username ,a.phone , a.credit , a.reference, a.email,a.birthday ,a.created_date as account_create_date, a.last_access , a.lastip  as last_ip, aa.username as agent, a.type_user ,";
    one +=
      "a.vip, COALESCE(SUM(wa.amount) filter (where trans_type_id = 1),0) as sum_deposit,COALESCE(SUM(wa.amount) filter (where trans_type_id = 2),0) as sum_withdraw,";
    one +=
      "COALESCE((select dw2.created_date  from deposit_withdraw_agent dw2 inner join agent m2 on m2.agent_id  = dw2.agent_id  where dw2.agent_id  = a.agent_id  and dw2.trans_type_id =1 order by dw2.updated_date desc limit 1),null) as date_last_deposit,";
    one +=
      "(COALESCE(SUM(wa.amount) filter (where trans_type_id = 1),0) - COALESCE(SUM(wa.amount) filter (where trans_type_id = 2),0)) as diff,";
    one +=
      "COALESCE((select SUM(total_price) from lotto_header lhex inner join members mex on lhex.member_id = mex.id and mex.advisor_id = a.agent_id  ),0) as total_price_network, a.totalbet  as total_bet, a.credit as current_credit,";
    one +=
      "COALESCE((select SUM((total_price * c.commission_percentage)/100) from lotto_header lhex inner join members mex on lhex.member_id = mex.id and mex.advisor_id = a.agent_id inner join lotto l on l.id = lhex.lotto_id  inner join commissions c on c.id = l.commission_id where mex.advisor_id != 0 and lhex.status = 10 ),0) as cumulative_income,";
    one +=
      "a.recommen_id  as recommender,a.recommen  as recommender_name from agent a ";
    one += "inner join agent aa on aa.agent_id = a.agent  full outer join deposit_withdraw_agent wa on a.agent_id  = wa.agent_id ";
    one += "where a.agent_id  =" + data;
    one += " GROUP by a.agent_id , a.username ,a.phone , a.credit , a.reference, a.email,a.birthday ,a.created_date, a.last_access , a.lastip , aa.username,a.type_user  ,a.vip, a.totalbet ,a.recommen_id, a.recommen ";

    let result = await db_access.query(one);
    const resp = result.rows;
    const response = Object.values(resp).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_bankByAgentId = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  try {
    const agentId = req.params.id;
    const first =
      "select b.id as bank_id ,b.bank_name, ba.bank_name, ba.bank_account,ba.remark, t.title_name, t.id   from agent a  inner join bank_agent ba on ba.agent_id =a.agent_id inner join bank b on b.id = ba.bank_id inner join title t on t.id = ba.title_id where a.agent_id =" +
      agentId + ";";
    let resu = await db_access.query(first);
    const respp = resu.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve listMember.
exports.get_listMemberNetwork = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */

  try {
    const agentId = req.params.id;
    let one = "";
    one += "select m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date ,CAST(m.created_date  AS DATE) as created_date , COALESCE(SUM(dw.amount) filter (where trans_type_id = 2),0) as sum_withdraw ,COALESCE(SUM(dw.amount) filter (where trans_type_id = 1),0) as sum_deposit ,";
    one += "COALESCE((select SUM(total_price) from lotto_header lhex inner join members mex on lhex.member_id = mex.id where  lhex.member_id = m.id ),0) as total_bet, m.credit as current_credit,";
    one += "COALESCE((select SUM(total_price) from lotto_header lhex inner join members mex on lhex.member_id = mex.id and mex.advisor_id = m.id where mex.advisor_id != 0 ),0) as total_price_network, (select count(advisor_id) from members where advisor_id = m.id ) as total_member , m.advisor_id, ";
    one += "COALESCE((select SUM((total_price * c.commission_percentage)/100) from lotto_header lhex inner join members mex on lhex.member_id = mex.id and mex.advisor_id = m.id inner join lotto l on l.id = lhex.lotto_id  inner join commissions c on c.id = l.commission_id where mex.advisor_id != 0 and lhex.status = 10) ,0) as cumulative_income ";
    one += "from members m inner join status s   on s.id  = m.status  inner join agent a   on a.agent_id  = m.agent_id  full outer join deposit_withdraw dw  on dw.member_id = m.id where m.agent_id="+agentId+" GROUP by CAST(m.created_date  AS DATE), m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date  order by m.created_date desc;";
    let result = await db_access.query(one);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};