const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const AWS  = require('aws-sdk');
const fs = require("fs");
const path = require("path");
const moment = require("moment");

// Retrieve Announces.
exports.get_announce = async (req, res) => {
  // #swagger.tags = ['Announce']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Announce" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let result = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      result = await db_access.query("SELECT * FROM public.announce where agent_id="+agentId+" and status=1 order by created_date desc ;");
    } else if (typeAgent === 1) {
      result = await db_access.query("SELECT * FROM public.announce where agent_id="+agentNo+" and status=1 order by created_date desc ;");
    } else if (typeAgent === 2) {
      result = await db_access.query("SELECT * FROM public.announce where status=1 order by created_date desc;");
    } else if (typeAgent === 3) {
      result = await db_access.query("SELECT * FROM public.announce where status=1 order by created_date desc;");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_search_announce = async (req, res) => {
  // #swagger.tags = ['Announce']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Announce" }} */

  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.text !== null || data.text !== "") {
      if (typeAgent === 0) {
        result = await db_access.query("SELECT a.id,a.title,a.created_date,s.status_name_th, a.status FROM announce a inner join status s on s.id = a.status where agent_id=" + agentId + " and status=1 and (cast(a.id as text) ilike '%" + data.text + "%' or a.title ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%') order by created_date desc;");
      } else if (typeAgent === 1) {
        result = await db_access.query("SELECT a.id,a.title,a.created_date,s.status_name_th, a.status FROM announce a inner join status s on s.id = a.status where agent_id=" + agentNo + " and status=1 and (cast(a.id as text) ilike '%" + data.text + "%' or a.title ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%') order by created_date desc;");
      } else if (typeAgent === 2) {
        result = await db_access.query("SELECT a.id,a.title,a.created_date,s.status_name_th, a.status FROM announce a inner join status s on s.id = a.status where status=1 and (cast(a.id as text) ilike '%" + data.text + "%' or a.title ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%') order by created_date desc;");
      } else if (typeAgent === 3) {
        result = await db_access.query("SELECT a.id,a.title,a.created_date,s.status_name_th, a.status FROM announce a inner join status s on s.id = a.status where status=1 and (cast(a.id as text) ilike '%" + data.text + "%' or a.title ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%') order by created_date desc;");
      }
    } else{
      if (typeAgent === 0) {
        result = await db_access.query("SELECT * FROM public.announce where agent_id="+agentId+" and status=1 order by created_date desc ;");
      } else if (typeAgent === 1) {
        result = await db_access.query("SELECT * FROM public.announce where agent_id="+agentNo+" and status=1 order by created_date desc;");
      } else if (typeAgent === 2) {
        result = await db_access.query("SELECT * FROM public.announce where status=1 order by created_date desc;");
      } else if (typeAgent === 3) {
        result = await db_access.query("SELECT * FROM public.announce where status=1 order by created_date desc;");
      }
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve Announces.
exports.get_announce_by_id = async (req, res) => {
  // #swagger.tags = ['Announce']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Announce" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("SELECT * FROM public.announce where id="+data +";");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.post_announce = async (req, res) => {
  // #swagger.tags = ['Announce']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Announce' }
  } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    if (typeAgent === 1) {
      await db_access.query("CALL public.spinsertupdateannounce($1,$2,$3,$4,$5);", [
        0,
        utilities.is_empty_string(ag) ? 0 : ag,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.description) ? null : data.description,
        utilities.is_empty_string(data.status) ? 2 : data.status
      ]);
    } else{
      await db_access.query("CALL public.spinsertupdateannounce($1,$2,$3,$4,$5);", [
        0,
        utilities.is_empty_string(req.user.agent_id) ? 0 : req.user.agent_id,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.description) ? null : data.description,
        utilities.is_empty_string(data.status) ? 2 : data.status
      ]);
    }
      res.status(200).json({
        code: 200,
        message: "Success"
      });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.put_announce = async (req, res) => {
  // #swagger.tags = ['Announce']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Announce' }
  } */
  const data = req.body;
  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    if (typeAgent === 1) {
      await db_access.query("CALL public.spinsertupdateannounce($1,$2,$3,$4,$5);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(ag) ? 0 : ag,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.description) ? null : data.description,
        utilities.is_empty_string(data.status) ? 2 : data.status
      ]);
    } else{
      await db_access.query("CALL public.spinsertupdateannounce($1,$2,$3,$4,$5);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(ag) ? 0 : ag,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.description) ? null : data.description,
        utilities.is_empty_string(data.status) ? 2 : data.status
      ]);
    }
      res.status(200).json({
        code: 200,
        message: "Success"
      });

  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete announce.
exports.delete_announce = async (req, res) => {
  // #swagger.tags = ['announce']
  const data = req.params.id;
  try {
    await db_access.query("delete from public.announce  where id  = "+ data + ";");

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
