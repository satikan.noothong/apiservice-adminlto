const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const {sms} = require("../config/config");
const {post} = require("axios");
const moment = require("moment");

// Retrieve type.
exports.get_type = async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/api" }} */

  try {
    let result = await db_access.query("SELECT * FROM public.transaction_type ;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve type.
exports.get_agent = async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/api" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + " and a.status =1;";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser ="";
    let agentNo = "";
    let result = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeUser === 0 ) {
       result = await db_access.query("SELECT a.agent_id,a.username, ba.id as bank_agent_id, ba.bank_account FROM agent a inner join bank_agent ba on ba.agent_id = a.agent_id where a.agent_id =" + agentId + " and a.status =1;");
    } else if (typeUser === 1) {
      result = await db_access.query("SELECT a.agent_id,a.username, ba.id as bank_agent_id, ba.bank_account FROM agent a inner join bank_agent ba on ba.agent_id = a.agent_id where a.agent_id =" + agentNo + " and a.status =1;");
    } else{
      result = await db_access.query("SELECT a.agent_id,a.username, ba.id as bank_agent_id, ba.bank_account FROM agent a inner join bank_agent ba on ba.agent_id = a.agent_id where a.status =1;");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve type.
exports.get_list_agent = async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/api" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + " and a.status =1;";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser ="";
    let result = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "SELECT a.agent_id,a.username FROM agent a where a.status =1 ";
    if (typeUser === 0) {
      one += " AND a.agent =" + agentId
    } else if (typeUser === 1) {
      one += " AND a.agent =" + agentNo
    }
    result = await db_access.query(one);

    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve type.
exports.get_bank_agent = async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/api" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser ="";
    let agentNo = "";
    let result = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeUser === 0 ) {
      result = await db_access.query("SELECT a.id as bank_agent_id,a.bank_account, a.agent_id FROM bank_agent a where a.agent_id =" + agentId);
    } else if (typeUser === 1){
      result = await db_access.query("SELECT a.id as bank_agent_id,a.bank_account, a.agent_id FROM bank_agent a where a.agent_id =" + agentNo);
    } else{
      result = await db_access.query("SELECT a.id as bank_agent_id, a.bank_account, a.agent_id FROM bank_agent a ");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve type.
exports.get_status_sms= async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/api" }} */

  try {
    let result = await db_access.query("SELECT * FROM status  ;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve type.
exports.get_title= async (req, res) => {
  // #swagger.tags = ['api']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/api" }} */

  try {
    let result = await db_access.query("SELECT * FROM title where status =1 ;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve OTP from AWS SNS.
exports.get_send_otp = async (req, res) => {
  // #swagger.tags = ['Member']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ResultSendOTP" }} */
  const data = req.query;

  try {
    const res_otp = await sendOTP(data.mobile_no, data.source);

    res.status(200).json({
      code: 200,
      message: "Success",
      data: res_otp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

async function sendOTP(mobileNo, source) {
  var OTP = await generateRandomNumber(1000, 9999);

  let params = {
    message: `${source == 0 ? 'Register' : source == 1 ? 'Forgotpass' : 'Your'} code: ${OTP}. Valid for 5 minutes.`,
    phone: mobileNo,
    sender: sms.sender
  }

  let config_header = {
    headers: {
      'Content-Type': 'application/json',
      'api_key': sms.api_key,
      'secret_key': sms.secret_key
    }
  }

  console.log('Request send sms : ', params);
  const resp = await post(`${sms.host}/send-message`, params, config_header);
  console.log('Response send sms : ', resp?.data);

  let sent_otp = moment();
  let expire_otp = moment(sent_otp).add(5, "minutes");

  let objSendOTP = {
    mobile_no: mobileNo,
    otp: OTP,
    sent_otp: sent_otp.format("YYYY-MM-DD HH:mm:ss"),
    expire_otp: expire_otp.format("YYYY-MM-DD HH:mm:ss"),
  };

  return objSendOTP;
}

async function generateRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}