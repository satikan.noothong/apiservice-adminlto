const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve AskCredit.
exports.get_askCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/AskCredit" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      resu = await db_access.query("SELECT ac.current_credit  as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentId + " and ac.status != 5 order by ac.created_date desc;");
    } else if (typeAgent === 1) {
      resu = await db_access.query("SELECT ac.current_credit  as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentNo + " and ac.status != 5  order by ac.created_date desc;");
    } else if (typeAgent === 2) {
      resu = await db_access.query("SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where ac.status != 5  order by ac.created_date desc;");
    } else if (typeAgent === 3) {
      resu = await db_access.query("SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where ac.status != 5  order by ac.created_date desc;");
    }

    const resp = resu.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve AskCredit.
exports.get_search_askCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/AskCredit" }} */

  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "";
    if (data.text !== null || data.text !== "") {
      if (typeAgent === 0) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentId + " and (cast(a.credit as text) ilike '%"+data.text+"%' or tt.transaction_type ilike '%"+data.text+"%' or cast(ac.total as text) ilike '%"+data.text+"%' or ac.description ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or cast(ac.created_date as text) ilike '%"+data.text+"%' or cast(ac.id as text) ilike '%"+data.text+"%' or cast(CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as text) ilike '%"+data.text+"%') and ac.status in (3,4) order by ac.created_date desc;";
      } else if (typeAgent === 1) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentNo + " and (cast(a.credit as text) ilike '%"+data.text+"%' or tt.transaction_type ilike '%"+data.text+"%' or cast(ac.total as text) ilike '%"+data.text+"%' or ac.description ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or cast(ac.created_date as text) ilike '%"+data.text+"%' or cast(ac.id as text) ilike '%"+data.text+"%' or cast(CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as text) ilike '%"+data.text+"%') and ac.status in (3,4) order by ac.created_date desc;";
      } else if (typeAgent === 2) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where (cast(a.credit as text) ilike '%"+data.text+"%' or tt.transaction_type ilike '%"+data.text+"%' or cast(ac.total as text) ilike '%"+data.text+"%' or ac.description ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or cast(ac.created_date as text) ilike '%"+data.text+"%' or cast(ac.id as text) ilike '%"+data.text+"%' or cast(CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as text) ilike '%"+data.text+"%') and ac.status in (3,4) order by ac.created_date desc;";
      } else if (typeAgent === 3) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where (cast(a.credit as text) ilike '%"+data.text+"%' or tt.transaction_type ilike '%"+data.text+"%' or cast(ac.total as text) ilike '%"+data.text+"%' or ac.description ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or cast(ac.created_date as text) ilike '%"+data.text+"%' or cast(ac.id as text) ilike '%"+data.text+"%' or cast(CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END as text) ilike '%"+data.text+"%') and ac.status in (3,4) order by ac.created_date desc;";
      }
    } else{
      if (typeAgent === 0) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END  as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentId + " and ac.status != 5  order by ac.created_date desc;";
      } else if (typeAgent === 1) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END  as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where a.agent_id=" + agentNo + " and ac.status != 5  order by ac.created_date desc;";
      } else if (typeAgent === 2) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END  as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status where ac.status != 5 order by ac.created_date desc;";
      } else if (typeAgent === 3) {
        one ="SELECT ac.current_credit as current_credit, tt.transaction_type, ac.total, ac.description, s.status_name_th, ac.created_date + INTERVAL '7 hours' as created_date, ac.id, CASE WHEN a.type_user = 0  THEN a.username ELSE aa.username END  as agentName  FROM ask_credit ac inner join agent a on a.agent_id = ac.agent_id inner join agent aa on aa.agent_id = a.agent inner join transaction_type tt on tt.id = ac.type_id inner join status s on s.id = ac.status  where ac.status != 5 order by ac.created_date desc;";
      }
    }
    resu = await db_access.query(one);
    const resp = resu.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new AskCredit.
exports.post_askCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/AskCredit' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  const type = "select a.credit, a.type_user, a.agent_id, a.agent, aa.credit as agent_credit from agent a inner join agent aa on aa.agent_id = a.agent where a.agent_id =" + agentId + ";";
  let resupp = await db_access.query(type);
  const resps = resupp.rows;
  let current_credit = null;
  let typeUser = "";
  let agentNo = "";
  let agentCredit = null;
  let agent = "";
  let credit = null;
  let sumCredit;
  for (let i = 0; i < resps.length; i++) {
    current_credit = resps[i].credit;
    typeUser = resps[i].type_user;
    agentNo = resps[i].agent;
    agentCredit = resps[i].agent_credit;
  }
  try {
    if (typeUser === 1){
      agent = agentNo;
      credit = agentCredit;
    } else {
      agent = agentId;
      credit = current_credit;
    }
    let credits = parseFloat(credit)
    let total = parseFloat(data.total)
    if (data.type_id == "1"){
      sumCredit = credits + total;
    } else {
      sumCredit = credits - total;
    }
    const  datas = await db_access.query("CALL public.spinsertupdateaskcredit($1,$2,$3,$4,$5,$6,$7,$8);", [
      0,
      utilities.is_empty_string(agent) ? null : agent,
      utilities.is_empty_string(data.type_id) ? null : data.type_id,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.total) ? null : data.total,
      utilities.is_empty_string(data.description) ? null : data.description,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(sumCredit) ? null : sumCredit
    ]);
    const askId = Object.values(datas.rows.find(a => a.prm_id)).values().next().value;
    await db_access.query("INSERT INTO deposit_withdraw_agent (agent_id, current_credit, amount, bank_id, ip_address,trans_type_id,status,agent,actor_id,created_date,ask_id,comment) values (" + agent + "," +  sumCredit + "," + data.total + ",0 ,'" + data.ipAddress.ip + "'," + data.type_id +" , "+data.status+ " , "+agent+",0,now(),"+askId+",'"+data.description+"');");
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Update a new AskCredit.
exports.put_askCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/AskCredit' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  const type = "select a.credit, a.type_user, a.agent_id, a.agent, aa.credit as agentCredit from agent a inner join agent aa on aa.agent_id = a.agent where a.agent_id =" + agentId + ";";
  let resupp = await db_access.query(type);
  const resps = resupp.rows;
  let current_credit = null;
  let typeUser = "";
  let agentNo = "";
  let agentCredit = "";
  let agent = "";
  let credit = "";
  for (let i = 0; i < resps.length; i++) {
    current_credit = resps[i].credit;
    typeUser = resps[i].type_user;
    agentNo = resps[i].agent;
    agentCredit = resps[i].agentCredit;
  }
  try {
    if (typeUser === 1){
      agent = agentNo;
      credit =  agentCredit;
    } else {
      agent = agentId;
      credit = current_credit;
    }
    await db_access.query("CALL public.spinsertupdateaskcredit($1,$2,$3,$4,$5,$6,$7,$8);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(agent) ? null : agent,
      utilities.is_empty_string(data.type_id) ? null : data.type_id,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.total) ? null : data.total,
      utilities.is_empty_string(data.description) ? null : data.description,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(credit) ? null : credit,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a Update AskCredit.
exports.put_updateStatusAskCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/AskCredit' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  try {
    let ress = await db_access.query("SELECT ac.total, tt.id as tran_type_id, ac.agent_id, a.credit FROM ask_credit ac inner join transaction_type tt on tt.id = ac.type_id inner join agent a on a.agent_id = ac.agent_id where ac.id="+data.id+";");
    const respp = ress.rows;
    let total = "";
    let agentt = "";
    let type_id ="";
    const one = Object.values(respp).values().next().value;
    total = parseFloat(one.total);
    let current_credit = parseFloat(one.credit);
    agentt = one.agent_id;
    type_id = one.tran_type_id;
    let result = await db_access.query("SELECT a.credit FROM agent a where type_user = 2;");
    const resp = result.rows;
    const cred = Object.values(resp).values().next().value;
    const credit = parseFloat(cred.credit);
    let sum = null;
    let resultt = await db_access.query("SELECT ac.credit, a.agent FROM agent a inner join agent ac on ac.agent_id = a.agent where a.agent_id ="+agentt+";");
    const responseCredit = resultt.rows;
    let credits ="";
    let ag = "";
    const two = Object.values(responseCredit).values().next().value;
    credits = parseFloat(two.credit);
    ag = two.agent;
    let totalUser = null;
    let types = "";
    let message = "";
    let username = ""
    let description = "";
    let name = "";
    let first = "";
    let agent = "";
    if (data.status == '4') {
      first = "ดำเนินการแจ้ง";
      if (type_id === '1'){
        sum = current_credit + total;
        totalUser = credits - total;
        types = "1";
        name = "ฝาก";
      } else{
        sum = current_credit - total;
        totalUser = credits + total;
        types = "2";
        name = "ถอน";
      }
      let three = "UPDATE agent SET credit ="+sum+" where agent_id = "+agentt+";";
      await db_access.query(three);
      await db_access.query("UPDATE agent SET credit ="+sum+" where type_user =2 and agent="+agentt);
      await db_access.query("UPDATE agent SET credit ="+totalUser+" where agent_id= "+ag+";");
    } else{
      first = "ยกเลิกแจ้ง";
      if (type_id === '1') {
        types = "1";
        name = "ฝาก";
      } else{
        types = "2";
        name = "ถอน";
      }
      await db_access.query("delete from ask_credit where id ="+ data.id);
      await db_access.query("delete from deposit_withdraw_agent where ask_id='"+data.id+"'");
    }
    if (data.status == '4') {
      await db_access.query("CALL public.spinsertupdatestatusaskcredit($1,$2);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(data.status) ? null : data.status
      ]);
      await db_access.query("UPDATE deposit_withdraw_agent set status ="+data.status+" where ask_id= '"+data.id+"'");
    }
    let resul = await db_access.query("SELECT ac.agent_id,a.username, ac.description FROM ask_credit ac inner join agent a on a.agent_id= ac.agent_id where ac.agent_id ="+agentt+";");
    const responseAgent = resul.rows;
    for (let i = 0; i < responseAgent.length; i++) {
      username = responseAgent[i].username;
      description = responseAgent[i].description;
      agent = responseAgent[i].agent_id;
    }
    message = first+name+"จำนวน "+total+"เครดิต โดย"+username+" หมายเหตุ:"+ description;
    await db_access.query("INSERT INTO transaction_agent_member (agent_id, actor_id, type_log_transaction_id, detail, ip_address) values ("+agent+","+ agentId+"," +types+",'"+message+"','"+data.ipAddress+"');");

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete AskCredit.
// exports.delete_askCredit = async (req, res) => {
//   // #swagger.tags = ['AskCredit']
//   const data = req.query;
//   console.log("ddd : " + data.id)
//   try {
//     console.log("ddd : " + data.id)
//     await db_access.query("delete from public.ask_credit  where id  = "+ data.id + ";");
//
//     res.status(200).json({
//       code: 200,
//       message: "Success",
//     });
//   } catch (error) {
//     console.log("error : ", error);
//     res.status(500).send({
//       code: 500,
//       message: error,
//     });
//   }
// };

exports.delete_askCredit = async (req, res) => {
  // #swagger.tags = ['AskCredit']
  const data = req.params.id;
  try {
    await db_access.query("delete from public.ask_credit  where id  = "+ data + ";");

    res.status(200).json({
      code: 200,
      message: "Success"
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};



