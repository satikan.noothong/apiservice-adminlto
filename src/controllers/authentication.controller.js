const jwt = require("jsonwebtoken");
const config = require("../config/config");

exports.authentication = (req, res) => {
  // #swagger.tags = ['Authentication']
  const { username, password } = req.body;

  try {
    if (username === config.jwt_username && password === config.jwt_password) {
      const token = jwt.sign({ username: username }, config.jwt_secret, {
        expiresIn: config.jwt_expire,
        algorithm: "HS256",
      });

      res.status(200).json({
        code: 200,
        message: "Success",
        data: {
          access_token: token
        },
      });
    } else {
      return res.status(401).send({
        code: 401,
        message: "Unauthorized!",
      });
    }
  } catch (error) {
    console.log(error)
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.refresh_token = (req, res) => {
  // #swagger.tags = ['Authentication']

  try {
    const access_token = jwt.sign(req.user, config.access_secret, {
      expiresIn: config.jwt_expire,
      algorithm: "HS256",
    });

    const refresh_token = jwt.sign(req.user, config.refresh_secret, {
      expiresIn: config.refresh_token_expire,
      algorithm: "HS256",
    });

    res.status(200).json({
      code: 200,
      message: "Success",
      data: {
        access_token: access_token,
        refresh_token: refresh_token
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
