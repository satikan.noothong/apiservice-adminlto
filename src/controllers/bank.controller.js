const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');

// Retrieve Bank.
exports.get_bank = async (req, res) => {
  // #swagger.tags = ['Bank']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Bank" }} */

  try {
    let result = await db_access.query("SELECT * FROM public.bank where id != 10;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

