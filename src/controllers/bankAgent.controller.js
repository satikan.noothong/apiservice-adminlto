const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');

// Retrieve BankAgent.
exports.get_bank_agent = async (req, res) => {
  // #swagger.tags = ['BankAgent']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/BankAgent" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user,a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      result = await db_access.query("SELECT * FROM public.bank_agent where agent_id ="+agentId+" order by created_date desc;");
    } else if (typeAgent === 1) {
      result = await db_access.query("SELECT * FROM public.bank_agent where agent_id ="+agentNo+" order by created_date desc;");
    } else if (typeAgent === 2) {
      result = await db_access.query("SELECT ba.id,ba.agent_id, ba.bank_id, ba.bank_name,ba.bank_account,ba.weight,ba.remark,ba.status,ba.created_date,ba.updated_date,ba.title_id,a.username as agent_name FROM public.bank_agent ba inner join agent a on a.agent_id = ba.agent_id order by created_date desc;");
    } else if (typeAgent === 3) {
      result = await db_access.query("SELECT ba.id,ba.agent_id, ba.bank_id, ba.bank_name,ba.bank_account,ba.weight,ba.remark,ba.status,ba.created_date,ba.updated_date,ba.title_id,a.username as agent_name FROM public.bank_agent ba inner join agent a on a.agent_id = ba.agent_id order by created_date desc;");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve BankAgent.
exports.get_search_bank_agent = async (req, res) => {
  // #swagger.tags = ['BankAgent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/BankAgent" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = "";
    const data = req.body;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }

    if (data.text === null || data.text === "") {
      if (typeAgent === 0) {
        result = await db_access.query("SELECT * FROM public.bank_agent where agent_id =" + agentId + " order by created_date desc;");
      } else if (typeAgent === 1) {
        result = await db_access.query("SELECT * FROM public.bank_agent where agent_id =" + agentNo + " order by created_date desc;");
      } else if (typeAgent === 2 || typeAgent === 3) {
        result = await db_access.query("SELECT ba.id,ba.agent_id, ba.bank_id, ba.bank_name,ba.bank_account,ba.weight,ba.remark,ba.status,ba.created_date,ba.updated_date,ba.title_id,a.username as agent_name FROM public.bank_agent inner join agent a on a.agent_id = ba.agent_id order by created_date desc;");
      }
    } else{
      if (typeAgent === 0) {
        result = await db_access.query("SELECT ba.id , b.id as bank_id, b.bank_name , ba.bank_name, ba.bank_account, ba.weight, s.id as status, s.status_name_th, ba.remark, ba.created_date FROM public.bank_agent ba inner join bank b on b.id = ba.bank_id inner join status s on s.id = ba.status where agent_id =" + agentId + " and (cast(ba.id as text) ilike '%"+data.text+"%' or b.bank_name ilike '%"+data.text+"%' or ba.bank_name ilike '%"+data.text+"%' or cast(ba.bank_account as text) ilike '%"+data.text+"%' or cast(ba.weight as text) ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or ba.remark ilike '%"+data.text+"%' or cast(ba.created_date as text) ilike '%"+data.text+"%') order by created_date desc;");
      } else if (typeAgent === 1) {
        result = await db_access.query("SELECT ba.id , b.id as bank_id, b.bank_name , ba.bank_name, ba.bank_account, ba.weight, s.id as status, s.status_name_th, ba.remark, ba.created_date FROM public.bank_agent ba inner join bank b on b.id = ba.bank_id inner join status s on s.id = ba.status where agent_id =" + agentNo + " and (cast(ba.id as text) ilike '%"+data.text+"%' or b.bank_name ilike '%"+data.text+"%' or ba.bank_name ilike '%"+data.text+"%' or cast(ba.bank_account as text) ilike '%"+data.text+"%' or cast(ba.weight as text) ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or ba.remark ilike '%"+data.text+"%' or cast(ba.created_date as text) ilike '%"+data.text+"%') order by created_date desc;");
      }  else if (typeAgent === 2 || typeAgent === 3) {
        result = await db_access.query("SELECT ba.id, b.id as bank_id, b.bank_name , ba.bank_name, ba.bank_account, ba.weight, s.id as status, s.status_name_th, ba.remark, ba.created_date,a.username as agent_name FROM public.bank_agent ba inner join bank b on b.id = ba.bank_id inner join status s on s.id = ba.status inner join agent a on a.agent_id = ba.agent_id where cast(ba.id as text) ilike '%"+data.text+"%' or b.bank_name ilike '%"+data.text+"%' or ba.bank_name ilike '%"+data.text+"%' or cast(ba.bank_account as text) ilike '%"+data.text+"%' or cast(ba.weight as text) ilike '%"+data.text+"%' or s.status_name_th ilike '%"+data.text+"%' or ba.remark ilike '%"+data.text+"%' or cast(ba.created_date as text) ilike '%"+data.text+"%' or a.username ilike '%"+data.text+"%' order by created_date desc;");
      }
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new BankAgent.
exports.post_bank_agent = async (req, res) => {
  // #swagger.tags = ['BankAgent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/BankAgent' }
  } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    await db_access.query("CALL public.spinsertupdatebankagent($1,$2,$3,$4,$5,$6,$7,$8,$9);", [
      0,
      utilities.is_empty_string(ag) ? null : ag,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
      utilities.is_empty_string(data.bank_account) ? null : data.bank_account,
      utilities.is_empty_string(data.weight) ? null : data.weight,
      utilities.is_empty_string(data.remark) ? null : data.remark,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.title) ? null : data.title
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new BankAgent.
exports.put_bank_agent = async (req, res) => {
  // #swagger.tags = ['BankAgent']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/BankAgent' }
  } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    await db_access.query("CALL public.spinsertupdatebankagent($1,$2,$3,$4,$5,$6,$7,$8,$9);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(ag) ? null : ag,
        utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
        utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
        utilities.is_empty_string(data.bank_account) ? null : data.bank_account,
        utilities.is_empty_string(data.weight) ? null : data.weight,
        utilities.is_empty_string(data.remark) ? null : data.remark,
        utilities.is_empty_string(data.status) ? null : data.status,
        utilities.is_empty_string(data.title) ? null : data.title
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete BankAgent.
exports.delete_bank_agent = async (req, res) => {
  // #swagger.tags = ['BankAgent']
  const data = req.query;
  try {
    await db_access.query("CALL public.spdeletebankagent($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_bankById = async (req, res) => {
  // #swagger.tags = ['Agent']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Agent" }} */
  try {
    const agentId = req.params.id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    const first = "select b.id as bank_id ,b.bank_name, ba.bank_name, ba.bank_account,ba.remark, t.title_name, t.id as title_id  from agent a  inner join bank_agent ba on ba.agent_id =a.agent_id inner join bank b on b.id = ba.bank_id inner join title t on t.id = ba.title_id where ba.id =" +
        ag + ";";
    let resu = await db_access.query(first);
    const respp = resu.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};