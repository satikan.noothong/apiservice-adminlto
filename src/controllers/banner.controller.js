const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const AWS  = require('aws-sdk');
const fs = require("fs");
const path = require("path");
const moment = require("moment");

// Retrieve banners.
exports.get_banner = async (req, res) => {
  // #swagger.tags = ['Banner']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Banner" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let result = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      result = await db_access.query("SELECT * FROM public.banner where agent_id="+agentId+" order by id asc;");
    } else  if (typeAgent === 1) {
      result = await db_access.query("SELECT * FROM public.banner where agent_id="+agentNo+" order by id asc;");
    } else  if (typeAgent === 2) {
      result = await db_access.query("SELECT * FROM public.banner order by id asc;");
    } else  if (typeAgent === 3) {
      result = await db_access.query("SELECT * FROM public.banner  order by id asc;");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve banners.
exports.get_banner_by_id = async (req, res) => {
  // #swagger.tags = ['Banner']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Banner" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("SELECT * FROM public.banner where id="+data +";");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.post_banner = async (req, res) => {
  // #swagger.tags = ['Banner']
  // #swagger.consumes = ['multipart/form-data']
  /* #swagger.parameters['file'] = {
          in: 'formData',
          type: 'file',
          required: true
  } */
  /*  #swagger.parameters['id'] = {
           in: 'formData',
           type: 'number'
   } */
  /*  #swagger.parameters['links'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['title'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['detail'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['status'] = {
           in: 'formData',
           type: 'number'
   } */
   const message_file = req.file;
   const data = req.body;
   let res_upload_s3 = null;
   try {
     const agentId = req.user.agent_id;
     const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
     let resupp = await db_access.query(type);
     const resps = resupp.rows;
     let typeAgent = null;
     let agentNo = null;
     let ag = "";
     for (let i = 0; i < resps.length; i++) {
       typeAgent = resps[i].type_user;
       agentNo = resps[i].agent;
     }
     if (typeAgent === 1){
       ag = agentNo;
     } else{
       ag  =agentId;
     }
     const extension = path.extname(message_file.originalname);
     if (req.file) {
       const  datas =  await db_access.query("CALL public.spinsertupdatebanner($1,$2,$3,$4,$5,$6,$7);", [
         0,
         utilities.is_empty_string(ag) ? 0 : ag,
         utilities.is_empty_string(data.links) ? null : data.links,
         utilities.is_empty_string(data.title) ? null : data.title,
         utilities.is_empty_string(data.detail) ? null : data.detail,
         utilities.is_empty_string(null) ? null : null,
         utilities.is_empty_string(data.status) ? 2 : data.status,
       ]);
 
       const banner = Object.values(datas.rows.find(a => a.prm_banner_id)).values().next().value;
       const file_name = `banner/${banner}_${moment().format("YYYYMMDD_HHmmss")}${extension}`;
       const res_upload_file = await utilities.upload_file_s3(message_file, file_name);
       res_upload_s3 = res_upload_file.Location;
       await db_access.query("CALL public.spupdatebannerimg($1,$2);", [
         utilities.is_empty_string(banner) ? null : banner,
         utilities.is_empty_string(res_upload_s3) ? null : res_upload_s3
       ]);
 
       res.status(200).json({
         code: 200,
         message: "Success"
       });
     } else {
       let msg = "Not File Upload";
       console.log("error : ", msg);
       res.status(400).send({
         code: 400,
         message: msg,
       });
     }
     /* #swagger.responses[200] = {
       schema: { "$ref": "#/definitions/ResultSuccess" }} */
   } catch (error) {
     console.log("error : ", error);
     res.status(500).send({
       code: 500,
       message: error,
     });
   }
};

exports.put_banner = async (req, res) => {
  // #swagger.tags = ['Banner']
  // #swagger.consumes = ['multipart/form-data']
  /* #swagger.parameters['file'] = {
          in: 'formData',
          type: 'file'
  } */
  /*  #swagger.parameters['id'] = {
           in: 'formData',
           type: 'number'
   } */
  /*  #swagger.parameters['links'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['title'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['detail'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['status'] = {
           in: 'formData',
           type: 'number'
   } */
  const message_file = req.file;
  const data = req.body;
  let res_upload_s3 = null;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
    if (req.file) {
        let extension = path.extname(message_file.originalname);
        let file_name = `banner/${data.id}_${moment().format("YYYYMMDD_HHmmss")}${extension}`;
        let res_upload_file = await utilities.upload_file_s3(message_file, file_name);
        res_upload_s3 = res_upload_file.Location;
        await db_access.query("CALL public.spinsertupdatebanner($1,$2,$3,$4,$5,$6,$7);", [
          utilities.is_empty_string(data.id) ? null : data.id,
          utilities.is_empty_string(ag) ? 0 : ag,
          utilities.is_empty_string(data.links) ? null : data.links,
          utilities.is_empty_string(data.title) ? null : data.title,
          utilities.is_empty_string(data.detail) ? null : data.detail,
          utilities.is_empty_string(res_upload_s3) ? null : res_upload_s3,
          utilities.is_empty_string(data.status) ? 2 : data.status,
        ]);
      res.status(200).json({
        code: 200,
        message: "Success"
      });
    } else {
      await db_access.query("CALL public.spinsertupdatebannernoimg($1,$2,$3,$4,$5,$6);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(ag) ? 0 : ag,
        utilities.is_empty_string(data.links) ? null : data.links,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.detail) ? null : data.detail,
        utilities.is_empty_string(data.status) ? 2 : data.status,
      ]);

      res.status(200).json({
        code: 200,
        message: "Success"
      });
    }
/* #swagger.responses[200] = {
  schema: { "$ref": "#/definitions/ResultSuccess" }} */
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.put_banner_data = async (req, res) => {
  // #swagger.tags = ['Banner']
  // #swagger.consumes = ['multipart/form-data']
  /*  #swagger.parameters['id'] = {
           in: 'formData',
           type: 'number'
   } */
  /*  #swagger.parameters['links'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['title'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['detail'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['status'] = {
           in: 'formData',
           type: 'number'
   } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =agentId;
    }
      await db_access.query("CALL public.spinsertupdatebannernoimg($1,$2,$3,$4,$5,$6);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(ag) ? 0 : ag,
        utilities.is_empty_string(data.links) ? null : data.links,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.detail) ? null : data.detail,
        utilities.is_empty_string(data.status) ? 2 : data.status
      ]);

      res.status(200).json({
        code: 200,
        message: "Success"
      });
    /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ResultSuccess" }} */
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete Banner.
exports.delete_banner = async (req, res) => {
  // #swagger.tags = ['Banner']
  const data = req.params.id;
  try {
    await db_access.query("delete from public.banner  where id  = "+ data + ";");

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
