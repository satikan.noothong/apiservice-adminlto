const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
dayjs.extend(utc)
dayjs.extend(timezone)

// Retrieve Board.
exports.get_board = async (req, res) => {
  // #swagger.tags = ['Board']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Board" }} */
  const agentId = req.user.agent_id;
  try {
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId+ ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let two ="";
    let three = "";
    let agentNo = null;
    let typeUser = null;
    for (let i = 0; i<resps.length; i++ ) {
      agentNo = resps[i].agent;
      typeUser = resps[i].type_user;
    }
    let today = dayjs().tz("Asia/Bangkok").subtract(4, 'hours').format("YYYY-MM-DD");

//     let sql = `SELECT 
//     m.username, 
//     m.logged_date + INTERVAL '7 hours' AS logged_date, 
//     m.id AS member_id,
//     COALESCE(daily_deposit.amount, 0) AS daily_deposit,
//     COALESCE(daily_withdraw.amount, 0) AS daily_withdraw,
//     COALESCE(benefit_forever.total, 0) AS benefit_forever,
//     COALESCE(daily_income_withdraw.total, 0) AS daily_income_withdraw 
// FROM members m 
// INNER JOIN agent a ON m.agent_id = a.agent_id
// LEFT JOIN ( 
//     SELECT lhex.member_id, SUM(lhex.amount) AS amount 
//     FROM deposit_withdraw lhex 
//     INNER JOIN members l ON l.id = lhex.member_id 
//     WHERE lhex.status IN (4) 
//     AND lhex.trans_type_id = 1    
//      AND lhex.created_date + INTERVAL '7 hours' >= CURRENT_DATE + INTERVAL '4 hours'
//             AND lhex.created_date + INTERVAL '7 hours' < CURRENT_DATE + INTERVAL '1 day' + INTERVAL '1 hours'
//     GROUP BY lhex.member_id
// ) AS daily_deposit ON m.id = daily_deposit.member_id 
// LEFT JOIN (
//     SELECT lhex.member_id, SUM(lhex.amount) AS amount 
//     FROM deposit_withdraw lhex 
//     INNER JOIN members l ON l.id = lhex.member_id 
//     WHERE lhex.status IN (4) 
//       AND lhex.trans_type_id = 2 
//       AND lhex.is_withdraw_direct IS NOT TRUE  
//          AND lhex.created_date + INTERVAL '7 hours' >= CURRENT_DATE + INTERVAL '4 hours'
//             AND lhex.created_date + INTERVAL '7 hours' < CURRENT_DATE + INTERVAL '1 day' + INTERVAL '1 hours'
//     GROUP BY lhex.member_id
// ) AS daily_withdraw ON m.id = daily_withdraw.member_id 
// LEFT JOIN (
//     SELECT
//         mex.advisor_id, SUM((lh.total_price * c.commission_percentage) / 100) AS total
//     FROM lotto_header lh 
//     LEFT JOIN lotto l ON lh.lotto_id = l.id 
//     LEFT JOIN commissions c ON l.commission_id = c.id 
//     LEFT JOIN members mex ON lh.member_id = mex.id 
//     WHERE lh.status IN (8, 9, 10) 
//     GROUP BY mex.advisor_id
// ) AS benefit_forever ON m.id = benefit_forever.advisor_id 
// LEFT JOIN ( 
//     SELECT wa.member_id as advisor_id, sum(ABS(amount)) as total 
//     FROM withdraw_affiliate wa 
//   WHERE  wa.created_date + INTERVAL '7 hours' >= CURRENT_DATE + INTERVAL '4 hours'
//      AND wa.created_date + INTERVAL '7 hours' < CURRENT_DATE + INTERVAL '1 day' + INTERVAL '1 hours'
//     GROUP BY wa.member_id
// ) AS daily_income_withdraw ON m.id = daily_income_withdraw.advisor_id`;
    let sql = `SELECT 
        m.username, 
        m.logged_date + INTERVAL '7 hours' AS logged_date, 
        m.id AS member_id,
        COALESCE(daily_deposit.amount, 0) AS daily_deposit,
        COALESCE(daily_withdraw.amount, 0) AS daily_withdraw,
        COALESCE(benefit_forever.total, 0) AS benefit_forever,
        COALESCE(daily_income_withdraw.total, 0) AS daily_income_withdraw 
    FROM members m 
    INNER JOIN agent a ON m.agent_id = a.agent_id
    LEFT JOIN ( 
        SELECT lhex.member_id, SUM(lhex.amount) AS amount 
        FROM deposit_withdraw lhex 
        INNER JOIN members l ON l.id = lhex.member_id 
        WHERE lhex.status IN (4) 
        AND lhex.trans_type_id = 1    
        and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
        GROUP BY lhex.member_id
    ) AS daily_deposit ON m.id = daily_deposit.member_id 
    LEFT JOIN (
        SELECT lhex.member_id, SUM(lhex.amount) AS amount 
        FROM deposit_withdraw lhex 
        INNER JOIN members l ON l.id = lhex.member_id 
        WHERE lhex.status IN (4) 
          AND (lhex.trans_type_id = 2 or lhex.trans_type_id = 4)
          and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
        GROUP BY lhex.member_id
    ) AS daily_withdraw ON m.id = daily_withdraw.member_id 
    LEFT JOIN (
      select a.advisor_id, sum(total) as total from (
        SELECT mex.advisor_id, (((sum(lh.total_price) + COALESCE(mtp.total_price, 0)) * c.commission_percentage)/100) AS total
        FROM lotto_header lh 
        LEFT JOIN lotto l ON lh.lotto_id = l.id 
        LEFT JOIN members_total_price mtp ON lh.member_id = mtp.member_id
        LEFT JOIN commissions c ON l.commission_id = c.id 
        LEFT JOIN members mex ON lh.member_id = mex.id 
        WHERE lh.status IN (8, 9, 10) 
        GROUP BY mex.advisor_id, mtp.total_price, c.commission_percentage) as a
      group by a.advisor_id
    ) AS benefit_forever ON m.id = benefit_forever.advisor_id 
    LEFT JOIN ( 
        SELECT wa.member_id as advisor_id, sum(ABS(amount)) as total 
        FROM withdraw_affiliate wa 
      WHERE  wa.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
        GROUP BY wa.member_id
    ) AS daily_income_withdraw ON m.id = daily_income_withdraw.advisor_id`;
    if (typeUser === 0) {
      sql += ` where a.agent_id= ${agentId} `;
    } else if (typeUser === 1) {
      sql += ` where a.agent_id= ${agentNo}`;
    }

    let result = await db_access.query(sql);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_ref_board = async (req, res) => {
  // #swagger.tags = ['Board']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Board" }} */
  try {
    let resu = await db_access.query("select a.recommen_link, a.recommen_link_2  from agent a where a.agent_id ="+req.user.agent_id+";");
    const ress = resu.rows;
    const response = Object.values(ress).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

