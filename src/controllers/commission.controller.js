const db_access = require("../db_access");
const moment = require('moment');
const dayjs = require("dayjs");

// Retrieve Commission.
exports.get_commission = async (req, res) => {
  // #swagger.tags = ['Commission']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Commission" }} */

  try {
    let list = [];
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "with dates( id, idate ) as";
    one += "     ( select id, d::date";
    one += "        from ( select distinct  id ";
    one += "                from lotto_header";
    one += "             ) u ";
    one += "       join generate_series( current_date - interval '6 days' ";
    one += "                             , current_date  ";
    one += "                             , interval '1 day'";
    one += "                             ) gs(d)";
    one += "           on true  ";
    one += "      )";
    if (typeAgent === 0) {
      one += "select CAST(d.idate  AS DATE) as created_date ,  d.id as lotto_id,";
      one += "COALESCE((select count(lh.id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id ),0) as list_name,";
      one += "COALESCE((select count(DISTINCT lh.member_id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as total_player,";
      one += "COALESCE((select sum(lh.total_price) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE)  and lotto_id= d.id and lh.status in (8,9,10)),0) as total_price,";
      one += "COALESCE((select (SUM(lh.total_price) * 0.01) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as net_income ";
      one += "from dates d inner join lotto_header lh on (lh.id = d.id)  where  lh.status IN (8,9,10) ";
      one += "group by d.id , CAST(d.idate AS DATE), lh.id, CAST(lh.created_date AS DATE) ";
      one += "order by  d.id, CAST(lh.created_date AS DATE)  asc limit 7;";
    } else if (typeAgent === 1) {
      one += "select CAST(d.idate AS DATE) as created_date ,  d.id as lotto_id,";
      one += "COALESCE((select count(lh.id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id ),0) as list_name,";
      one += "COALESCE((select count(DISTINCT lh.member_id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as total_player,";
      one += "COALESCE((select sum(lh.total_price) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE)  and lotto_id= d.id and lh.status in (8,9,10)),0) as total_price,";
      one += "COALESCE((select (SUM(lh.total_price) * 0.01) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + " and CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as net_income ";
      one += "from dates d inner join lotto_header lh on (lh.id = d.id)  where  lh.status IN (8,9,10)";
      one += "group by d.id , CAST(d.idate AS DATE), lh.id, CAST(lh.created_date AS DATE) ";
      one += "order by  d.id, CAST(lh.created_date AS DATE)  asc limit 7;";
    } else if (typeAgent === 2 || typeAgent === 3) {
      one += "select CAST(d.idate AS DATE) as created_date ,  d.id as lotto_id,";
      one += "COALESCE((select count(lh.id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where CAST(lh.created_date AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id),0) as list_name,";
      one += "COALESCE((select count(DISTINCT lh.member_id) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as total_player,";
      one += "COALESCE((select sum(lh.total_price) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where CAST(lh.created_date AS DATE) + interval '7 hour' = CAST(d.idate AS DATE)  and lotto_id= d.id and lh.status in (8,9,10)),0) as total_price,";
      one += "COALESCE((select (SUM(lh.total_price) * 0.01) from lotto_header lh inner join members m on m.id = lh.member_id  inner join agent a on a.agent_id = m.agent_id where CAST(lh.created_date + interval '7 hour' AS DATE) = CAST(d.idate AS DATE) and lotto_id= d.id and lh.status in (8,9,10)),0) as net_income ";
      one += "from dates d inner join lotto_header lh on (lh.id = d.id)  where  lh.status IN (8,9,10)";
      one += "group by d.id , CAST(d.idate AS DATE), lh.id, CAST(lh.created_date AS DATE) ";
      one += "order by  d.id, CAST(lh.created_date AS DATE)  asc limit 7;";
    }

    let result = await db_access.query(one);
    const resp = result.rows;
    if (resp != null) {
      for (let i = 0; i < resp.length; i++){
        const data = {"created_date" :{}, "list" : {}, "total_player" : {}, "total_price" : {}, "net_income":{}};
        const date = resp[i].created_date;
        const listName = resp[i].list_name;
        const totalPlayer = resp[i].total_player;
        const totalPrice = resp[i].total_price;
        const netIncome = resp[i].net_income;
        let day = moment(date).locale("th").format('dddd');// พุธ
        const dayMonth = moment(date).locale("th").format("Do MMM");
        if ("จันทร์" === day){
          day = " (จ.)";
        } else if ("อังคาร" === day){
          day = " (อ.)";
        }else if ("พุธ" === day){
          day = " (พ.)";
        }else if ("พฤหัสบดี" === day){
          day = " (พฤ.)";
        }else if ("ศุกร์" === day){
          day = " (ศ.)";
        }else if ("เสาร์" === day){
          day = " (ส.)";
        }else if ("อาทิตย์" === day){
          day = " (อา.)";
        }
        const createDate = dayMonth +day;
        data.created_date = createDate;
        data.list = listName;
        data.total_player = totalPlayer;
        data.total_price = totalPrice;
        data.net_income = netIncome;
        list.push(data);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: list,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve Commission.
exports.get_commission_by_week = async (req, res) => {
  // #swagger.tags = ['Commission']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Commission" }} */

  try {
    const data = req.body;
    let list = [];
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }

// Convert year and week to corresponding dates
    let year = data.year;
    let week = data.week;

// Calculate the start date of the week
    let startDate = moment().isoWeekYear(year).isoWeek(week).startOf('isoWeek');
// Adjust the start date to be on Sunday
    if (startDate.day() !== 0) {
      startDate.day(0);
    }

    let endDate = startDate.clone().add(6, 'days'); // Calculate the end date as 6 days after the start date

    let startDay = startDate.format("YYYY/MM/DD"); // Adjusted date format
    let endDay = endDate.format("YYYY/MM/DD"); // Adjusted date format
    // let today = dayjs(data.startDate).tz("Asia/Bangkok").subtract(4, 'hours').format("YYYY-MM-DD");
    // let ee = dayjs(data.endDate).tz("Asia/Bangkok").subtract(4, 'hours').format("YYYY-MM-DD");
    let today = dayjs(data.startDate).tz("Asia/Bangkok").format("YYYY-MM-DD");
    let ee = dayjs(data.endDate).tz("Asia/Bangkok").format("YYYY-MM-DD");

    // let one = `WITH date_range AS ( 
    //   SELECT generate_series(
    //       '${today}',
    //       '${ee}',
    //       '1 day'::interval
    //   )::date AS idate
    // )
    // SELECT 
    // d.idate + INTERVAL '7 hours' AS created_date, 
    // COALESCE(COUNT(lh.id), 0) AS list_name, 
    // COALESCE(COUNT(DISTINCT lh.member_id), 0) AS total_player,
    // COALESCE(SUM(lh.total_win), 0) AS total_win,
    // COALESCE(SUM(lh.total_price), 0) AS total_price,
    // COALESCE(SUM(lh.total_price)* 0.01, 0) AS net_income,
    // lh.agent_id,
    // lh.username as agent_name
    // FROM date_range d 
    // left join (
    //   select aa.*, a.agent_id, a.username from lotto_header aa
    //   inner join public.members m on aa.member_id = m.id 
    //   inner join public.agent a on m.agent_id = a.agent_id 
    //   where aa.status IN (8, 9, 10) ${typeAgent == 0 ? ` and a.agent_id = ${agentId}` : typeAgent == 1 ? ` and a.agent_id = ${agentNo}` : ''}
    // ) lh on lh.created_date + INTERVAL '7 hours' between d.idate::date + INTERVAL '4 hours' and d.idate::date + interval '1 day' + INTERVAL '4 hours'
    // GROUP by d.idate + INTERVAL '7 hours', lh.agent_id, lh.username;`;

    let one = `WITH date_range AS ( 
      SELECT generate_series(
          '${today}',
          '${ee}',
          '1 day'::interval
      )::date AS idate
    )
    SELECT 
    d.idate + INTERVAL '7 hours' AS created_date, 
    COALESCE(COUNT(lh.id), 0) AS list_name, 
    COALESCE(COUNT(DISTINCT lh.member_id), 0) AS total_player,
    COALESCE(SUM(lh.total_win), 0) AS total_win,
    COALESCE(SUM(lh.total_price), 0) AS total_price,
    COALESCE(SUM(lh.total_price)* 0.01, 0) AS net_income,
    lh.agent_id,
    lh.username as agent_name
    FROM date_range d 
    left join (
      select aa.*, a.agent_id, a.username from lotto_header aa
      inner join public.members m on aa.member_id = m.id 
      inner join public.agent a on m.agent_id = a.agent_id 
      where aa.status IN (8, 9, 10) ${typeAgent == 0 ? ` and a.agent_id = ${agentId}` : typeAgent == 1 ? ` and a.agent_id = ${agentNo}` : ''}
    ) lh on lh.date_id = d.idate
    GROUP by d.idate + INTERVAL '7 hours', lh.agent_id, lh.username;`;

    let result = await db_access.query(one);
    const resp = result.rows;
    if (resp != null) {
      for (let i = 0; i < resp.length; i++){
        const data = {"created_date" :{}, "list" : {}, "total_player" : {}, "total_price" : {}, "net_income":{}};
        const date = resp[i].created_date;
        const agentId = resp[i].agent_id;
        const agentName = resp[i].agent_name;
        const listName = resp[i].list_name;
        const totalPlayer = resp[i].total_player;
        const totalWin = resp[i].total_win;
        const totalPrice = resp[i].total_price;
        const netIncome = resp[i].net_income;
        let day = moment(date).locale("th").format('dddd');// พุธ
        const dayMonth = moment(date).locale("th").format("Do MMM");
        if ("จันทร์" === day){
          day = " (จ.)";
        } else if ("อังคาร" === day){
          day = " (อ.)";
        }else if ("พุธ" === day){
          day = " (พ.)";
        }else if ("พฤหัสบดี" === day){
          day = " (พฤ.)";
        }else if ("ศุกร์" === day){
          day = " (ศ.)";
        }else if ("เสาร์" === day){
          day = " (ส.)";
        }else if ("อาทิตย์" === day){
          day = " (อา.)";
        }
        const createDate = dayMonth +day;
        data.agent_id = agentId;
        data.agent_name = agentName;
        data.created_date = createDate;
        data.list = listName;
        data.total_player = totalPlayer;
        data.total_win = totalWin;
        data.total_price = (Math.round(totalPrice * 100) / 100).toFixed(2);
        data.net_income = (Math.round(netIncome * 100) / 100).toFixed(2);
        list.push(data);
      }
    }

    res.status(200).json({
      code: 200,
      message: "Success",
      data: list,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
