const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve contacts.
exports.get_contact = async (req, res) => {
  // #swagger.tags = ['Contact']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Contact" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "SELECT * FROM public.contact ";
    if (typeAgent === 0) {
      one += " where agent_id =" + agentId + ";";
    } else if (typeAgent === 1) {
      one += "where agent_id ="+agentNo+";";
    }

    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve contacts.
exports.get_contact_by_id = async (req, res) => {
  // #swagger.tags = ['Contact']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Contact" }} */
  const agentId = req.user.agent_id;
  try {
    let result = await db_access.query("SELECT * FROM public.contact where agent_id ="+agentId+";");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new contact.
exports.post_contact = async (req, res) => {
  // #swagger.tags = ['Contact']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Contact' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatecontact($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
      0,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.phone1) ? null : data.phone1,
      utilities.is_empty_string(data.phone2) ? null : data.phone2,
      utilities.is_empty_string(data.phone3) ? null : data.phone3,
      utilities.is_empty_string(data.phone4) ? null : data.phone4,
      utilities.is_empty_string(data.phone5) ? null : data.phone5,
      utilities.is_empty_string(data.phone6) ? null : data.phone6,
      utilities.is_empty_string(data.phone7) ? null : data.phone7,
      utilities.is_empty_string(data.phone8) ? null : data.phone8,
      utilities.is_empty_string(data.phone9) ? null : data.phone9,
      utilities.is_empty_string(data.email) ? null : data.email,
      utilities.is_empty_string(data.line) ? null : data.line,
      utilities.is_empty_string(data.facebook) ? null : data.facebook,
      utilities.is_empty_string(data.group_facebook) ? null : data.group_facebook,
      utilities.is_empty_string(data.twitter) ? null : data.twitter,
      utilities.is_empty_string(data.website) ? null : data.website,
      utilities.is_empty_string(data.description) ? null : data.description,
      utilities.is_empty_string(data.status) ? null : data.status
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new contact.
exports.put_contact = async (req, res) => {
  // #swagger.tags = ['Contact']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Contact' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let agent = null;
  let type = "";
  let agentNo = null;
  try {
    let result = await db_access.query("SELECT a.agent,a.type_user FROM agent a where a.agent_id ="+agentId+";");
    const resp = result.rows;
      for (let i = 0; i < resp.length; i++) {
        agent = resp[i].agent;
        type = resp[i].type_user;
      }
      let one  ="SELECT * FROM contact c where c.agent_id=";
    if (type === 1) {
      one +=  agent;
    } else{
      one += agentId;
    }
    let resu = await db_access.query(one);
    const resppc = resu.rows;
    for (let i = 0; i < resppc.length; i++) {
      agentNo = resppc[i].agent;
    }
      if(agentNo != null || agentNo !== undefined) {
      if (type === 1) {
        await db_access.query("CALL public.spinsertupdatecontact($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
          utilities.is_empty_string(data.contact_id) ? null : data.contact_id,
          utilities.is_empty_string(agent) ? null : agent,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.phone1) ? null : data.phone1,
          utilities.is_empty_string(data.phone2) ? null : data.phone2,
          utilities.is_empty_string(data.phone3) ? null : data.phone3,
          utilities.is_empty_string(data.phone4) ? null : data.phone4,
          utilities.is_empty_string(data.phone5) ? null : data.phone5,
          utilities.is_empty_string(data.phone6) ? null : data.phone6,
          utilities.is_empty_string(data.phone7) ? null : data.phone7,
          utilities.is_empty_string(data.phone8) ? null : data.phone8,
          utilities.is_empty_string(data.phone9) ? null : data.phone9,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.line) ? null : data.line,
          utilities.is_empty_string(data.facebook) ? null : data.facebook,
          utilities.is_empty_string(data.group_facebook) ? null : data.group_facebook,
          utilities.is_empty_string(data.twitter) ? null : data.twitter,
          utilities.is_empty_string(data.website) ? null : data.website,
          utilities.is_empty_string(data.description) ? null : data.description,
          utilities.is_empty_string(data.status) ? null : data.status
        ]);
      } else {
        await db_access.query("CALL public.spinsertupdatecontact($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
          utilities.is_empty_string(data.contact_id) ? null : data.contact_id,
          utilities.is_empty_string(agentId) ? null : agentId,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.phone1) ? null : data.phone1,
          utilities.is_empty_string(data.phone2) ? null : data.phone2,
          utilities.is_empty_string(data.phone3) ? null : data.phone3,
          utilities.is_empty_string(data.phone4) ? null : data.phone4,
          utilities.is_empty_string(data.phone5) ? null : data.phone5,
          utilities.is_empty_string(data.phone6) ? null : data.phone6,
          utilities.is_empty_string(data.phone7) ? null : data.phone7,
          utilities.is_empty_string(data.phone8) ? null : data.phone8,
          utilities.is_empty_string(data.phone9) ? null : data.phone9,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.line) ? null : data.line,
          utilities.is_empty_string(data.facebook) ? null : data.facebook,
          utilities.is_empty_string(data.group_facebook) ? null : data.group_facebook,
          utilities.is_empty_string(data.twitter) ? null : data.twitter,
          utilities.is_empty_string(data.website) ? null : data.website,
          utilities.is_empty_string(data.description) ? null : data.description,
          utilities.is_empty_string(data.status) ? null : data.status
        ]);

      }
    } else{
      if (type === 1) {
        await db_access.query("CALL public.spinsertupdatecontact($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
          utilities.is_empty_string(data.contact_id) ? null : data.contact_id,
          utilities.is_empty_string(agent) ? null : agent,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.phone1) ? null : data.phone1,
          utilities.is_empty_string(data.phone2) ? null : data.phone2,
          utilities.is_empty_string(data.phone3) ? null : data.phone3,
          utilities.is_empty_string(data.phone4) ? null : data.phone4,
          utilities.is_empty_string(data.phone5) ? null : data.phone5,
          utilities.is_empty_string(data.phone6) ? null : data.phone6,
          utilities.is_empty_string(data.phone7) ? null : data.phone7,
          utilities.is_empty_string(data.phone8) ? null : data.phone8,
          utilities.is_empty_string(data.phone9) ? null : data.phone9,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.line) ? null : data.line,
          utilities.is_empty_string(data.facebook) ? null : data.facebook,
          utilities.is_empty_string(data.group_facebook) ? null : data.group_facebook,
          utilities.is_empty_string(data.twitter) ? null : data.twitter,
          utilities.is_empty_string(data.website) ? null : data.website,
          utilities.is_empty_string(data.description) ? null : data.description,
          utilities.is_empty_string(data.status) ? null : data.status
        ]);
      } else {

        await db_access.query("CALL public.spinsertupdatecontact($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
          0,
          utilities.is_empty_string(agentId) ? null : agentId,
          utilities.is_empty_string(data.phone) ? null : data.phone,
          utilities.is_empty_string(data.phone1) ? null : data.phone1,
          utilities.is_empty_string(data.phone2) ? null : data.phone2,
          utilities.is_empty_string(data.phone3) ? null : data.phone3,
          utilities.is_empty_string(data.phone4) ? null : data.phone4,
          utilities.is_empty_string(data.phone5) ? null : data.phone5,
          utilities.is_empty_string(data.phone6) ? null : data.phone6,
          utilities.is_empty_string(data.phone7) ? null : data.phone7,
          utilities.is_empty_string(data.phone8) ? null : data.phone8,
          utilities.is_empty_string(data.phone9) ? null : data.phone9,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.line) ? null : data.line,
          utilities.is_empty_string(data.facebook) ? null : data.facebook,
          utilities.is_empty_string(data.group_facebook) ? null : data.group_facebook,
          utilities.is_empty_string(data.twitter) ? null : data.twitter,
          utilities.is_empty_string(data.website) ? null : data.website,
          utilities.is_empty_string(data.description) ? null : data.description,
          utilities.is_empty_string(data.status) ? null : data.status
        ]);

      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete contact.
exports.delete_contact = async (req, res) => {
  // #swagger.tags = ['Contact']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletecontact($1);", [
      utilities.is_empty_string(data.contact_id) ? null : data.contact_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
