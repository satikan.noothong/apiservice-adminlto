const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const path = require("path");
const moment = require("moment");


// Update a new CreateLinkFile.
exports.post_create_filep = async (req, res) => {
  // #swagger.tags = ['LinkFile']
  // #swagger.consumes = ['multipart/form-data']
  /* #swagger.parameters['file'] = {
          in: 'formData',
          type: 'file'
  } */
  /*  #swagger.parameters['id'] = {
          in: 'formData',
          type: 'number'
  } */
  try {
    const message_file = req.file;
    const data = req.body;
    let res_upload_s3 = null;
    if (req.file) {
      const extension = path.extname(message_file.originalname);
      const file_name = `popup/${data.id}_${moment().format("YYYYMMDD_HHmmss")}${extension}`;
      const res_upload_file = await utilities.upload_file_s3(message_file, file_name);
      res_upload_s3 = res_upload_file.Location;
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: res_upload_s3
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete Popup.
exports.delete_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletepopup($1);", [
      utilities.is_empty_string(data.popup_id) ? null : data.popup_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
