const db_access = require("../db_access");
const dayjs = require("dayjs");
const {check_validator_basic, request_validator} = require("../validator/request.validator");
const {lotto} = require("../commons/constants/lotto.constants");
const ApplicationError = require("../commons/exceptions/application-error");

// Retrieve DailySummary.
exports.get_daily_summary = async (req, res) => {
  // #swagger.tags = ['DailySummary']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/DailySummary" }} */
  const data = req.body;
  const agentId = req.user.agent_id;
  try {

    let type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let date = data.date;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
let one = "";
    let two = "";
    one += "WITH lotto_each (id, type_id, type_name, order_seq) AS (";
    one += "  SELECT DISTINCT l.id AS lotto_id, nt.id AS number_type_id, nt.number_type_name AS number_type_name, nt.order_seq ";
    one += "  FROM lotto l ";
    one += "  CROSS JOIN number_type nt ";
    one += ")";
    one += "SELECT ";
    one += "  lt.id AS lotto_header_id,";
    one += "  lt.lotto_type_name AS lotto_header_name,";
    one += "  l.lotto_name,";
    one += "  le.type_name,";
    one += "  '"+data.date+"' AS created_date,";
    one += "  SUM(COALESCE(ld.price, 0)) AS total_bet,";
    one += "  SUM(COALESCE(ld.win_price, 0)) AS total_win,";
    one += "  SUM(COALESCE(ld.price, 0) - COALESCE(ld.win_price, 0)) AS diff ";
    one += "FROM lotto_header lh ";
    two += "LEFT JOIN lotto l ON l.id = lh.lotto_id ";
    two += "LEFT JOIN lotto_type lt ON lt.id = l.lotto_type_id ";
    two += "LEFT JOIN lotto_each le ON le.id = l.id ";
    two += "LEFT JOIN members mm on lh.member_id  =mm.id ";
    two += "LEFT JOIN agent a on mm.agent_id = a.agent_id ";
    two += "LEFT JOIN lotto_detail ld ON ld.lotto_header_id = lh.id ";
    two += "  AND ld.number_type_id = le.type_id ";
    // two += "  AND CAST('"+data.date+"' AS DATE) = CAST(ld.created_date + INTERVAL '7 hours' AS DATE) ";
    two += "  AND '"+data.date+"' = lh.date_id ";
    two += "  AND l.lotto_type_id = "+ data.lotto_header_id+" ";
    two += "  AND lh.status IN (8, 9, 10) ";
    two += "inner join (select distinct lotto_id, number_type_id from public.rate_limit rl) rl2 on le.type_id = rl2.number_type_id and l.id = rl2.lotto_id ";
    two += "WHERE l.lotto_type_id = "+ data.lotto_header_id+" ";
    if (typeAgent === 0) {
      two += " AND mm.agent_id="+agentId+" ";
    } else if  (typeAgent === 1){
       two += " AND mm.agent_id="+agentNo+" ";
    }
    two += "GROUP BY lt.lotto_type_name, l.lotto_name, le.type_name, l.id, lt.id, le.order_seq ";
    two += "ORDER BY l.id, le.order_seq; ";

    let result = await db_access.query(one+two);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_daily_summary_by_yiki = async (req, res) => {
    // #swagger.tags = ['DailySummary']
    /* #swagger.responses[200] = {
        schema: { "$ref": "#/definitions/DailySummary" }} */
    const data = req.body;
    const agentId = req.user.agent_id;
    try {

        let type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
        let resupp = await db_access.query(type);
        const resps = resupp.rows;
        let typeAgent = null;
        let agentNo = null;
        for (let i = 0; i < resps.length; i++) {
            typeAgent = resps[i].type_user;
            agentNo = resps[i].agent;
        }
        let one = "";
        let two = "";
        let today = dayjs(data.date).tz("Asia/Bangkok").format("YYYY-MM-DD");
        one += "WITH lotto_each (id, type_id, type_name, order_seq) AS (";
        one += "  SELECT DISTINCT l.id AS lotto_id, nt.id AS number_type_id, nt.number_type_name AS number_type_name, nt.order_seq ";
        one += "  FROM lotto l ";
        one += "  CROSS JOIN number_type nt ";
        one += ")";
        one += "SELECT ";
        one += "  lt.id AS lotto_header_id,";
        one += "  lt.lotto_type_name AS lotto_header_name,";
        one += "  l.lotto_name,";
        one += "  le.type_name,";
        one += "  '"+data.date+"' AS created_date,";
        one += "  SUM(COALESCE(ld.price, 0)) AS total_bet,";
        one += "  SUM(COALESCE(ld.win_price, 0)) AS total_win,";
        one += "  SUM(COALESCE(ld.price, 0) - COALESCE(ld.win_price, 0)) AS diff ";
        one += "FROM lotto_header lh ";
        two += "LEFT JOIN lotto l ON l.id = lh.lotto_id ";
        two += "LEFT JOIN lotto_type lt ON lt.id = l.lotto_type_id ";
        two += "LEFT JOIN lotto_each le ON le.id = l.id ";
        two += "LEFT JOIN members mm on lh.member_id  =mm.id ";
        two += "LEFT JOIN agent a on mm.agent_id = a.agent_id ";
        two += "LEFT JOIN lotto_detail ld ON ld.lotto_header_id = lh.id ";
        two += "  AND ld.number_type_id = le.type_id ";
        // two += "  AND CAST('"+data.date+"' AS DATE) = CAST(ld.created_date AS DATE) ";
        two += "  AND ld.created_date + INTERVAL '7 hours' >= '"+today+"' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'";
        two += "  AND l.lotto_type_id = "+ data.lotto_header_id+" ";
        two += "  AND lh.status IN (8, 9, 10) ";
        two += "inner join (select distinct lotto_id, number_type_id from public.rate_limit rl) rl2 on le.type_id = rl2.number_type_id and l.id = rl2.lotto_id ";
        two += "WHERE l.lotto_type_id = "+ data.lotto_header_id+"  ";
        if (typeAgent === 0) {
            two += " AND mm.agent_id="+agentId+" ";
        } else if  (typeAgent === 1){
            two += " AND mm.agent_id="+agentNo+" ";
        }
        two += "GROUP BY lt.lotto_type_name, l.lotto_name, le.type_name, l.id, lt.id, le.order_seq ";
        two += "ORDER BY l.id, le.order_seq; ";
        let result = await db_access.query(one+two);
        const resp = result.rows;

        res.status(200).json({
            code: 200,
            message: "Success",
            data: resp,
        });
    } catch (error) {
        console.log("error : ", error);
        res.status(500).send({
            code: 500,
            message: error,
        });
    }
};

exports.get_daily_summary_huay_all_day_by_yiki = async (req, res) => {
    // #swagger.tags = ['DailySummary']
    /* #swagger.responses[200] = {
        schema: { "$ref": "#/definitions/DailySummary" }} */
    const data = req.body;
    const agentId = req.user.agent_id;
    try {
        let type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
        let resupp = await db_access.query(type);
        const resps = resupp.rows;
        let typeAgent = null;
        let agentNo = null;
        for (let i = 0; i < resps.length; i++) {
            typeAgent = resps[i].type_user;
            agentNo = resps[i].agent;
        }
        let one = "";
        let two = "";
        let today = dayjs(data.date).tz("Asia/Bangkok").format("YYYY-MM-DD");
        one += "WITH lotto_each (id, type_id, type_name, order_seq) AS (";
        one += "  SELECT DISTINCT l.id AS lotto_id, nt.id AS number_type_id, nt.number_type_name AS number_type_name, nt.order_seq ";
        one += "  FROM lotto l ";
        one += "  CROSS JOIN number_type nt ";
        one += ")";
        one += "SELECT ";
        one += "  lt.id AS lotto_header_id,";
        one += "  lt.lotto_type_name AS lotto_header_name,";
        one += "  l.lotto_name,";
        one += "  le.type_name,";
        one += "  '"+data.date+"' AS created_date,";
        one += "  SUM(COALESCE(ld.price, 0)) AS total_bet,";
        one += "  SUM(COALESCE(ld.win_price, 0)) AS total_win,";
        one += "  SUM(COALESCE(ld.price, 0) - COALESCE(ld.win_price, 0)) AS diff ";
        one += "FROM lotto_header lh ";
        two += "LEFT JOIN lotto l ON l.id = lh.lotto_id ";
        two += "LEFT JOIN lotto_type lt ON lt.id = l.lotto_type_id ";
        two += "LEFT JOIN lotto_each le ON le.id = l.id ";
        two += "LEFT JOIN members mm on lh.member_id  =mm.id ";
        two += "LEFT JOIN agent a on mm.agent_id = a.agent_id ";
        two += "LEFT JOIN lotto_detail ld ON ld.lotto_header_id = lh.id ";
        two += "  AND ld.number_type_id = le.type_id ";
        // two += "  AND CAST('"+data.date+"' AS DATE) = CAST(ld.created_date AS DATE) ";
        two += "  AND ld.created_date + INTERVAL '7 hours' >= '"+today+"' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'";
        two += "  AND l.lotto_type_id = "+ data.lotto_header_id+" AND le.id = l.id ";
        two += "  AND lh.status IN (8, 9, 10) ";
        two += "inner join (select distinct lotto_id, number_type_id from public.rate_limit rl) rl2 on le.type_id = rl2.number_type_id and l.id = rl2.lotto_id ";
        two += "WHERE l.lotto_type_id = "+ data.lotto_header_id+" and le.id = "+data.id+" ";
        if (typeAgent === 0) {
            two += " AND mm.agent_id="+agentId+" ";
        } else if  (typeAgent === 1){
            two += " AND mm.agent_id="+agentNo+" ";
        }
        two += "GROUP BY lt.lotto_type_name, l.lotto_name, le.type_name, l.id, lt.id, le.order_seq ";
        two += "ORDER BY l.id, le.order_seq ; ";
        let result = await db_access.query(one+two);
        const resp = result.rows;


        res.status(200).json({
            code: 200,
            message: "Success",
            data: resp,
        });
    } catch (error) {
        console.log("error : ", error);
        res.status(500).send({
            code: 500,
            message: error,
        });
    }
};

exports.get_daily_summary_by_huay = async (req, res) => {
  // #swagger.tags = ['DailySummary']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/DailySummary" }} */
  try {
    // let one = "select lt.lotto_type_name, lt.id , coalesce(( select SUM(total_price) from lotto_header lhex inner join lotto l on l.lotto_type_id = lt.id where lhex.lotto_id = l.id and l.lotto_type_id = lt.id and cast(current_date + interval '7 hour' as DATE) = cast(lhex.created_date + interval '7 hour' as DATE) AND lhex.status IN (8, 9, 10)), 0) as total_bet,";
    // one += "COALESCE((select SUM(total_win) from lotto_header lhex inner join lotto l on l.lotto_type_id = lt.id  where lhex.lotto_id = l.id and l.lotto_type_id = lt.id and cast(current_date + interval '7 hour' as DATE) = cast(lhex.created_date + interval '7 hour' as DATE) AND lhex.status IN (8, 9, 10)),0) as total_win,";
    // one += "(COALESCE((select SUM(total_price) from lotto_header lhex inner join lotto l on l.lotto_type_id = lt.id  where lhex.lotto_id = l.id and l.lotto_type_id = lt.id and cast(current_date + interval '7 hour' as DATE) = cast(lhex.created_date + interval '7 hour' as DATE) AND lhex.status IN (8, 9, 10)),0) ";
    // one += "- COALESCE((select SUM(total_win) from lotto_header lhex inner join lotto l on l.lotto_type_id = lt.id  where lhex.lotto_id = l.id and l.lotto_type_id = lt.id and cast(current_date + interval '7 hour' as DATE) = cast(lhex.created_date + interval '7 hour' as DATE) AND lhex.status IN (8, 9, 10)),0)) as diff ";
    // one += "from lotto_type lt;";
      const data = req.body;
      const agentId = req.user.agent_id;
      let type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
      let resupp = await db_access.query(type);
      const resps = resupp.rows;
      let typeAgent = null;
      let agentNo = null;
      for (let i = 0; i < resps.length; i++) {
          typeAgent = resps[i].type_user;
          agentNo = resps[i].agent;
      }
    let today = dayjs(data.date).tz("Asia/Bangkok").format("YYYY-MM-DD");

    // let one  = `SELECT
    //     lt.lotto_type_name,
    //     lt.id,
    //     '${data.date}' as date,
    //     COALESCE((
    //         SELECT SUM(total_price)
    //         FROM lotto_header lhex
    //         INNER JOIN lotto l ON l.lotto_type_id = lt.id
    //         INNER JOIN members m ON m.id = lhex.member_id 
    //         WHERE lhex.lotto_id = l.id
    //             AND l.lotto_type_id = lt.id
    //             and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
    //             AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
    //     ), 0) AS total_bet,
    //     COALESCE((
    //         SELECT SUM(total_win)
    //         FROM lotto_header lhex
    //         INNER JOIN lotto l ON l.lotto_type_id = lt.id
    //         INNER JOIN members m ON m.id = lhex.member_id 
    //         WHERE lhex.lotto_id = l.id
    //             AND l.lotto_type_id = lt.id
    //             and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
    //             AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
    //     ), 0) AS total_win,
    //     COALESCE((
    //         SELECT SUM(total_price)
    //         FROM lotto_header lhex
    //         INNER JOIN lotto l ON l.lotto_type_id = lt.id
    //         INNER JOIN members m ON m.id = lhex.member_id 
    //         WHERE lhex.lotto_id = l.id
    //             AND l.lotto_type_id = lt.id
    //             and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
    //             AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
    //     ), 0) - COALESCE((
    //         SELECT SUM(total_win)
    //         FROM lotto_header lhex
    //         INNER JOIN lotto l ON l.lotto_type_id = lt.id
    //         INNER JOIN members m ON m.id = lhex.member_id 
    //         WHERE lhex.lotto_id = l.id
    //             AND l.lotto_type_id = lt.id
    //             and lhex.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
    //             AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
    //     ), 0) AS diff
    // FROM lotto_type lt `;

    let one  = `SELECT
        lt.lotto_type_name,
        lt.id,
        '${data.date}' as date,
        COALESCE((
            SELECT SUM(total_price)
            FROM lotto_header lhex
            INNER JOIN lotto l ON l.lotto_type_id = lt.id
            INNER JOIN members m ON m.id = lhex.member_id 
            WHERE lhex.lotto_id = l.id
                AND l.lotto_type_id = lt.id
                and lhex.date_id = '${today}'
                AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
        ), 0) AS total_bet,
        COALESCE((
            SELECT SUM(total_win)
            FROM lotto_header lhex
            INNER JOIN lotto l ON l.lotto_type_id = lt.id
            INNER JOIN members m ON m.id = lhex.member_id 
            WHERE lhex.lotto_id = l.id
                AND l.lotto_type_id = lt.id
                and lhex.date_id = '${today}'
                AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
        ), 0) AS total_win,
        COALESCE((
            SELECT SUM(total_price)
            FROM lotto_header lhex
            INNER JOIN lotto l ON l.lotto_type_id = lt.id
            INNER JOIN members m ON m.id = lhex.member_id 
            WHERE lhex.lotto_id = l.id
                AND l.lotto_type_id = lt.id
                and lhex.date_id = '${today}'
                AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
        ), 0) - COALESCE((
            SELECT SUM(total_win)
            FROM lotto_header lhex
            INNER JOIN lotto l ON l.lotto_type_id = lt.id
            INNER JOIN members m ON m.id = lhex.member_id 
            WHERE lhex.lotto_id = l.id
                AND l.lotto_type_id = lt.id
                and lhex.date_id = '${today}'
                AND lhex.status IN (8, 9, 10) ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : '' }
        ), 0) AS diff
    FROM lotto_type lt `;

    let result = await db_access.query(one);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve DailySummary.
exports.get_daily_summary_yiki = async (req, res) => {
    // #swagger.tags = ['DailySummary']
    /* #swagger.responses[200] = {
        schema: { "$ref": "#/definitions/DailySummary" }} */
    const data = req.body;
    const agentId = req.user.agent_id;
    try {
        let type = "select a.type_user, a.agent from agent a where a.agent_id = " + agentId + ";";
        let resupp = await db_access.query(type);
        const resps = resupp.rows;
        let typeAgent = null;
        let agentNo = null;

        for (let i = 0; i < resps.length; i++) {
            typeAgent = resps[i].type_user;
            agentNo = resps[i].agent;
        }
        // const originalDate = data.date;
        // const parsedDate = new Date(originalDate);
        // const formattedDate = parsedDate.toISOString().split('T')[0];
        let today = dayjs(data.date).tz("Asia/Bangkok").format("YYYY-MM-DD");
    
//         let query = `
//    select lt.id AS lotto_header_id,
//    lt.lotto_type_name AS lotto_header_name,
//    nt.number_type_name as type_name,
//    mc.category_name AS lotto_name,
//    mc.category_id,
//    case when lc.finished_date is not null then 'จ่ายเงินแล้ว' when lc.canceled_date is not null then 'ยกเลิกโพย' else 'รับแทง' end as status,
//    '${data.date}' AS created_date,
//  COALESCE(lh2.total_bet, 0) as total_bet,
//  COALESCE(lh2.total_win, 0) as total_win,
//  COALESCE(lh2.diff, 0) as diff
//    from number_type nt
//    cross join mapping_category mc
//    inner join lotto l on mc.lotto_id = l.id
//    inner join lotto_type lt on l.lotto_type_id = lt.id
//    inner join (select distinct number_type_id from public.rate_limit rl where rl.lotto_id = ${data.id}) rl2 on nt.id = rl2.number_type_id
//    left join (
//     select lh.lotto_id, lh.category_id, ld.number_type_id, SUM(COALESCE(ld.price, 0)) AS total_bet,
//       SUM(COALESCE(ld.win_price, 0)) AS total_win,
//       SUM(COALESCE(ld.price, 0) - COALESCE(ld.win_price, 0)) AS diff from lotto_header lh
//     left join lotto_detail ld on lh.id = ld.lotto_header_id
//     left join members m on lh.member_id = m.id
//     where lh.lotto_id = ${data.id} AND lh.status IN (8, 9, 10) AND CAST('${dayjs(data.date).tz("Asia/Bangkok").format("YYYY-MM-DD")}' AS DATE)= CAST(ld.created_date + INTERVAL '7 hours' AS DATE)
//           AND ld.created_date + INTERVAL '7 hours' >= CURRENT_DATE + INTERVAL '4 hours'
//           AND ld.created_date + INTERVAL '7 hours' < CURRENT_DATE + INTERVAL '1 day' + INTERVAL '4 hours'
//     `;
//     if (typeAgent === 0) {
//         query += `and m.agent_id =${agentId} `;
//     } else if (typeAgent === 1) {
//         query += ` AND m.agent_id = ${agentNo}`;
//     }
//     query += `  group by lh.lotto_id, lh.category_id, ld.number_type_id
//    ) lh2 on l.id = lh2.lotto_id and nt.id = lh2.number_type_id and lh2.category_id = mc.category_id
//    left join lotto_category lc on lc.date_id = '${formattedDate}' and mc.category_id = lc.category_id
//    where nt.is_number_set is true and lt.id = ${data.lotto_header_id} and l.id = ${data.id}
//    order by mc.category_id, nt.order_seq asc; `;

let query = `
   select lt.id AS lotto_header_id,
   lt.lotto_type_name AS lotto_header_name,
   nt.number_type_name as type_name,
   mc.category_name AS lotto_name,
   mc.category_id,
   case when lc.finished_date is not null then 'จ่ายเงินแล้ว' when lc.canceled_date is not null then 'ยกเลิกโพย' else 'รับแทง' end as status,
   '${data.date}' AS created_date,
 COALESCE(lh2.total_bet, 0) as total_bet,
 COALESCE(lh2.total_win, 0) as total_win,
 COALESCE(lh2.diff, 0) as diff
   from number_type nt
   cross join mapping_category mc
   inner join lotto l on mc.lotto_id = l.id
   inner join lotto_type lt on l.lotto_type_id = lt.id
   inner join (select distinct number_type_id from public.rate_limit rl where rl.lotto_id = ${data.id}) rl2 on nt.id = rl2.number_type_id
   left join (
    select lh.lotto_id, lh.category_id, ld.number_type_id, SUM(COALESCE(ld.price, 0)) AS total_bet,
      SUM(COALESCE(ld.win_price, 0)) AS total_win,
      SUM(COALESCE(ld.price, 0) - COALESCE(ld.win_price, 0)) AS diff from lotto_header lh
    left join lotto_detail ld on lh.id = ld.lotto_header_id
    left join members m on lh.member_id = m.id
    where lh.lotto_id = ${data.id} AND lh.status IN (8, 9, 10) 
    and ld.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' + INTERVAL '4 hours' - INTERVAL '7 hours'
    `;
    if (typeAgent === 0) {
        query += `and m.agent_id =${agentId} `;
    } else if (typeAgent === 1) {
        query += ` AND m.agent_id = ${agentNo}`;
    }
    query += `  group by lh.lotto_id, lh.category_id, ld.number_type_id
   ) lh2 on l.id = lh2.lotto_id and nt.id = lh2.number_type_id and lh2.category_id = mc.category_id
   left join lotto_category lc on lc.date_id = '${today}' and mc.category_id = lc.category_id
   where nt.is_number_set is true and lt.id = ${data.lotto_header_id} and l.id = ${data.id}
   order by mc.category_id, nt.order_seq asc; `;

        let result = await db_access.query(query);
        // let result = await db_access.query(
        //     "SELECT * FROM public.spget_daily_summary_yiki($1, $2, $3, $4)",
        //     [
        //         data.id,
        //         data.lotto_header_id,
        //         typeAgent === 0 ? agentId : typeAgent === 1 ? agentNo : 0,
        //         today
        //     ]
        //   );
        //   const resp = result.rows;
        const resp = result.rows;

        res.status(200).json({
            code: 200,
            message: "Success",
            data: resp,
        });
    } catch (error) {
        console.log("error : ", error);
        res.status(500).send({
            code: 500,
            message: error,
        });
    }
};





