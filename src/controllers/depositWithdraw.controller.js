const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve DepositWithdraw.
exports.get_depositwithdraw = async (req, res) => {
  // #swagger.tags = ['DepositWithdraw']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/DepositWithdraw" }} */

  try {
    let result = await db_access.query("SELECT * FROM public.deposit_withdraw;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new DepositWithdraw.
exports.post_depositwithdraw = async (req, res) => {
  // #swagger.tags = ['Depositwithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/DepositWithdraw' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatedepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23);", [
      0,
      utilities.is_empty_string(data.member_id) ? null : data.member_id,
      utilities.is_empty_string(data.current_credit) ? null : data.current_credit,
      utilities.is_empty_string(data.amount) ? null : data.amount,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
      utilities.is_empty_string(data.bank_account) ? null : data.bank_account,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.remark) ? null : data.remark,
      utilities.is_empty_string(data.transection_id) ? null : data.transection_id,
      utilities.is_empty_string(data.trans_type_id) ? null : data.trans_type_id,
      utilities.is_empty_string(data.channel) ? null : data.channel,
      utilities.is_empty_string(data.ref) ? null : data.ref,
      utilities.is_empty_string(data.url_image) ? null : data.url_image,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.comment) ? null : data.comment,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.actor_id) ? null : data.actor_id,
      utilities.is_empty_string(data.deposited_date) ? null : data.deposited_date,
      utilities.is_empty_string(data.approved_date) ? null : data.approved_date,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new DepositWithdraw.
exports.put_depositwithdraw = async (req, res) => {
  // #swagger.tags = ['DepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/DepositWithdraw' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatedepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.member_id) ? null : data.member_id,
      utilities.is_empty_string(data.current_credit) ? null : data.current_credit,
      utilities.is_empty_string(data.amount) ? null : data.amount,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
      utilities.is_empty_string(data.bank_account) ? null : data.bank_account,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.remark) ? null : data.remark,
      utilities.is_empty_string(data.transection_id) ? null : data.transection_id,
      utilities.is_empty_string(data.trans_type_id) ? null : data.trans_type_id,
      utilities.is_empty_string(data.channel) ? null : data.channel,
      utilities.is_empty_string(data.ref) ? null : data.ref,
      utilities.is_empty_string(data.url_image) ? null : data.url_image,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.comment) ? null : data.comment,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.actor_id) ? null : data.actor_id,
      utilities.is_empty_string(data.deposited_date) ? null : data.deposited_date,
      utilities.is_empty_string(data.approved_date) ? null : data.approved_date,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete DepositWithdraw.
exports.delete_depositwithdraw = async (req, res) => {
  // #swagger.tags = ['DepositWithdraw']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletedepositwithdraw($1);", [
      utilities.is_empty_string(data.depositwithdraw_id) ? null : data.depositwithdraw_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
