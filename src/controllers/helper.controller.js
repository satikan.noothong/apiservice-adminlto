const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');

// Retrieve Helper.
exports.get_helper = async (req, res) => {
  // #swagger.tags = ['Helper']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Helper" }} */

  try {
    let result = await db_access.query("SELECT * FROM public.agent where type_user = 1;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_list_helper = async (req, res) => {
  // #swagger.tags = ['Helper']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Helper" }} */

  try {
    let helper = "";
    const listNote = [];
    const helperId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + helperId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeUser = "";
    let first = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeUser === 0){
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + helperId + " order by a.created_date desc;";
    } else if (typeUser === 1){
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + agentNo + " order by a.created_date desc;";
    } else if (typeUser === 2) {
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id where a.type_user = 1 or a.type_user = 3 order by a.created_date desc;";
    } else {
      first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id where a.type_user = 1 or a.type_user = 3 order by a.created_date desc;";
    }
    resu = await db_access.query(first);
    const resp = resu.rows;
    let resul = await db_access.query("select * from note_agent;");
    const respo = resul.rows;
    for (let i = 0; i<resp.length; i++){
      const dataList = {"agent_id":{},"username":{},"credit":{},"phone":{},"lastip":{},"last_access":{},"created_date":{}, "agentName":{},
        "status_name_th":{},"type_user":{},"note_helper":{}};
      dataList.agent_id = resp[i].agent_id;
      dataList.username = resp[i].username;
      dataList.credit = resp[i].credit;
      dataList.phone = resp[i].phone;
      dataList.lastip = resp[i].lastip;
      dataList.last_access = resp[i].last_access;
      dataList.created_date = resp[i].created_date;
      dataList.agentName = resp[i].agentname;
      dataList.status_name_th = resp[i].status_name_th;
      dataList.type_user = resp[i].type_user;

      helper =  dataList.agent_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {

        if ( helper === respo[c].agent_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0){
          dataList.note_helper = listNo;
        } else{
          dataList.note_helper = [];
        }
      }
      listNote.push(dataList);
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_search_list_helper = async (req, res) => {
  // #swagger.tags = ['Helper']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Helper" }} */
  try {
    let helper = "";
    const listNote = [];
    const helperId = req.user.agent_id;
    const data = req.body;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + helperId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeUser = "";
    let first = "";
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.text !== null || data.text !== "") {
      if (typeUser === 0) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + helperId + " and (cast(a.agent_id as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(a.phone as text) ilike '%" + data.text + "%' or cast(a.credit as text) ilike '%" + data.text + "%' or cast(a.lastip as text) ilike '%" + data.text + "%' or cast(a.last_access as text) ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or aa.username ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%' or cast(a.type_user as text) ilike '%" + data.text + "%') order by a.updated_date desc;";
      } else if (typeUser === 1) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + agentNo + " and (cast(a.agent_id as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(a.phone as text) ilike '%" + data.text + "%' or cast(a.credit as text) ilike '%" + data.text + "%' or cast(a.lastip as text) ilike '%" + data.text + "%' or cast(a.last_access as text) ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or aa.username ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%' or cast(a.type_user as text) ilike '%" + data.text + "%') order by a.updated_date desc;";
      } else if (typeUser === 2) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and (cast(a.agent_id as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(a.phone as text) ilike '%" + data.text + "%' or cast(a.credit as text) ilike '%" + data.text + "%' or cast(a.lastip as text) ilike '%" + data.text + "%' or cast(a.last_access as text) ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or aa.username ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%' or cast(a.type_user as text) ilike '%" + data.text + "%')  order by a.updated_date desc;";
      } else {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and (cast(a.agent_id as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(a.phone as text) ilike '%" + data.text + "%' or cast(a.credit as text) ilike '%" + data.text + "%' or cast(a.lastip as text) ilike '%" + data.text + "%' or cast(a.last_access as text) ilike '%" + data.text + "%' or cast(a.created_date as text) ilike '%" + data.text + "%' or aa.username ilike '%" + data.text + "%' or s.status_name_th ilike '%" + data.text + "%' or cast(a.type_user as text) ilike '%" + data.text + "%')  order by a.updated_date desc;";
      }
    } else{
      if (typeUser === 0 ) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + helperId + " order by a.updated_date desc;";
      }else if (typeUser === 1 ) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1 and a.agent =" + agentNo + " order by a.updated_date desc;";
      } else if (typeUser === 2) {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1  order by a.updated_date desc;";
      } else {
        first = "select a.agent_id as agent_id, a.username, a.phone, a.credit, a.lastip, a.last_access, a.created_date, aa.username as agentName , s.status_name_th, a.type_user from agent a inner join agent aa on a.agent = aa.agent_id inner join status s on a.status = s.id  where a.type_user = 1  order by a.updated_date desc;";
      }
    }
    resu = await db_access.query(first);
    const resp = resu.rows;
    let resul = await db_access.query("select * from note_agent;");
    const respo = resul.rows;
    for (let i = 0; i<resp.length; i++){
      const dataList = {"agent_id":{},"username":{},"credit":{},"phone":{},"lastip":{},"last_access":{},"created_date":{}, "agentName":{},
        "status_name_th":{},"type_user":{},"note_helper":{}};
      dataList.agent_id = resp[i].agent_id;
      dataList.username = resp[i].username;
      dataList.credit = resp[i].credit;
      dataList.phone = resp[i].phone;
      dataList.lastip = resp[i].lastip;
      dataList.last_access = resp[i].last_access;
      dataList.created_date = resp[i].created_date;
      dataList.agentName = resp[i].agentname;
      dataList.status_name_th = resp[i].status_name_th;
      dataList.type_user = resp[i].type_user;

      helper =  dataList.agent_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {

        if ( helper === respo[c].agent_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0){
          dataList.note_helper = listNo;
        } else{
          dataList.note_helper = [];
        }
      }
      listNote.push(dataList);
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Create a new Helper.
exports.post_helper = async (req, res) => {
  // #swagger.tags = ['Helper']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Helper' }
  } */
  const data = req.body;
  const helperId = req.user.agent_id;
  try {
    const types = "select a.type_user, a.agent from agent a where a.agent_id =" + helperId + ";";
    let resu = await db_access.query(types);
    const resss = resu.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    let tt = 1;
    for (let i = 0; i < resss.length; i++) {
      typeAgent = resss[i].type_user;
      agentNo = resss[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag  =helperId;
    }
    if (typeAgent === 2){
      tt = 3;
    }
    let user = "";
    const password = await bcrypt.hash(data.password, 10);
    const type = "select a.credit from agent a where a.agent_id =" + ag + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let credit = Object.values(resps).values().next().value;
    let result = await db_access.query("select count(a.username) as count from agent a  where a.username = '"+data.username+"';");
    const resp = result.rows;
    for (let i = 0; i < resp.length; i++ ){
      user = resp[i].count;
    }
    
    if (user == 0 ) {
    const  datas = await db_access.query("CALL public.spinsertupdatehelper($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);", [
      0,
      utilities.is_empty_string(data.username) ? null : data.username,
      utilities.is_empty_string(password) ? null : password,
      utilities.is_empty_string(password) ? null : password,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.reference) ? null : data.reference,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.email) ? null : data.email,
      utilities.is_empty_string(data.ipAddress) ? null : data.ipAddress,
      utilities.is_empty_string(ag) ? null : ag,
      utilities.is_empty_string(tt) ? null : tt,
      utilities.is_empty_string(credit.credit) ? null : credit.credit
    ]);
    const helper = Object.values(datas.rows.find(a => a.prm_agent_id)).values().next().value;
    if (helper != null){
      for (let i = 0; i < data.menu.length; i++){
        await db_access.query("CALL public.spinsertupdatemapagent($1,$2,$3);", [
          0,
          utilities.is_empty_string(helper) ? null : helper,
          utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
        ]);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
    });
    } else{
      res.status(500).send({
        code: 500,
        message: "Same Username",
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new Helper.
exports.put_helper = async (req, res) => {
  // #swagger.tags = ['Helper']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Helper' }
  } */
  const data = req.body;
  const password = await bcrypt.hash(data.password, 10);
  try {
    await db_access.query("CALL public.spupdateagent($1,$2,$3,$4,$5,$6,$7,$8);", [
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(password) ? null : password,
      utilities.is_empty_string(password) ? null : password,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.reference) ? null : data.reference,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.email) ? null : data.email,
      utilities.is_empty_string(data.ipAddress) ? null : data.ipAddress,
    ]);
    if(data.menu.length > 0 ) {
      await db_access.query("CALL public.spdeletemapagent($1);", [
        utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      ]);
      for (let i = 0; i < data.menu.length; i++) {
        await db_access.query("CALL public.spinsertupdatemapagent($1,$2,$3);", [
          utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
          utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
          utilities.is_empty_string(data.menu[i]) ? null : data.menu[i],
        ]);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


