const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve Inbox.
exports.get_inbox = async (req, res) => {
  // #swagger.tags = ['Inbox']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Inbox" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date, i.attachments  FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentId+" order by i.created_date desc;");
    }else if (typeAgent === 1) {
      resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date, i.attachments FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentNo+" order by i.created_date desc;");
    }else if (typeAgent === 2) {
      resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date, i.attachments FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id order by i.created_date desc ;");
    } else if (typeAgent === 3) {
      resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date, i.attachments FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id order by i.created_date desc;");
    }

    const resp = resu.rows;
    
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new Inbox.
exports.post_inbox = async (req, res) => {
  // #swagger.tags = ['Inbox']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Inbox' }
  } */
  const data = req.body;

  try {
    const agentId = req.user.agent_id;
    const type = "select a.credit, a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let agentNo = "";
    let agent = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;

    }
    if (typeUser === 1){
      agent = agentNo;
    } else {
      agent = agentId;
    }
    await db_access.query("CALL public.spinsertupdateinbox($1,$2,$3,$4,$5,$6);", [
      0,
      utilities.is_empty_string(data.inbox_no) ? null : data.inbox_no,
      utilities.is_empty_string(data.send_from) ? null : data.send_from,
      utilities.is_empty_string(data.title) ? null : data.title,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(agent) ? null : agent
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new Inbox.
exports.put_inbox = async (req, res) => {
  // #swagger.tags = ['Inbox']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Inbox' }
  } */
  const data = req.body;

  try {
    const agentId = req.user.agent_id;
    const type = "select a.credit, a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let agentNo = "";
    let agent = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;

    }
    if (typeUser === 1){
      agent = agentNo;
    } else {
      agent = agentId;
    }
    await db_access.query("CALL public.spinsertupdateinbox($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);", [
      utilities.is_empty_string(data.inbox_id) ? null : data.inbox_id,
      utilities.is_empty_string(data.inbox_no) ? null : data.inbox_no,
      utilities.is_empty_string(data.send_from) ? null : data.send_from,
      utilities.is_empty_string(data.title) ? null : data.title,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(agent) ? null : agent,
      utilities.is_empty_string(data.created_by) ? null : data.created_by,
      utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);
    
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete Inbox.
exports.delete_inbox = async (req, res) => {
  // #swagger.tags = ['Inbox']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeleteinbox($1);", [
      utilities.is_empty_string(data.inbox_id) ? null : data.inbox_id,
    ]);
    
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve Inbox.
exports.get_search_inbox = async (req, res) => {
  // #swagger.tags = ['Inbox']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Inbox" }} */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.text !== null || data.text !== "") {
      if (typeAgent === 0) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentId+" and (m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%'or cast(i.inbox_id as TEXT) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%'  or i.message ilike '%" + data.text + "%'  or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%');");
      } else if (typeAgent === 1) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentNo+" and (m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%'or cast(i.inbox_id as TEXT) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%'  or i.message ilike '%" + data.text + "%'  or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%');");
      } else if (typeAgent === 2 || typeAgent === 3) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%'or cast(i.inbox_id as TEXT) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%'  or i.message ilike '%" + data.text + "%'  or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%';");
      }
    } else{
      if (typeAgent === 0) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentId+" order by i.created_date desc;");
      }else if (typeAgent === 1) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id where a.agent_id="+agentNo+" order by i.created_date desc;");
      }else if (typeAgent === 2) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id order by i.created_date desc;");
      } else if (typeAgent === 3) {
        resu = await db_access.query("SELECT m.username as send_from, a.username as sent_to, i.inbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.inbox i inner join members m on i.send_from = m.id inner join agent a on i.sent_to= a.agent_id order by i.created_date desc;");
      }
    }
    const resp = resu.rows;
    
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};