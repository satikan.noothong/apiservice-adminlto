const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const config = require("../config/config");
const AWS  = require('aws-sdk');
const {compare} = require("bcrypt");
const dayjs = require("dayjs");

// Retrieve ListDepositWithdraw.
exports.get_listDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ListDepositWithdraw" }} */

  try {
    const agentId = req.user.agent_id;
    const query = req.query;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId+ ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let agentNo = "";
    for (let i = 0; i<resps.length; i++ ) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let transection = "";
    let data = "";
    let pagination = '';

    if(query.limit && query.limit > 0 && query.page && query.page > 0){
      const page = parseInt(query.page)
      const offset = (page-1) * parseInt(query.limit);
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`
    }

    let today = dayjs().tz("Asia/Bangkok").format("YYYY-MM-DD");
    // let where = ` AND dw.created_date between CURRENT_DATE -1 + '00:00:00.0000000'::time and CURRENT_DATE + '23:59:59.999999'::time`
    // let where = ` AND dw.created_date + INTERVAL '7 hours' between (case when (CURRENT_DATE + LOCALTIME) < CURRENT_DATE + '04:00:00.0000000'::time then (CURRENT_DATE - 1) + '04:00:00.0000000'::time else CURRENT_DATE + '04:00:00.0000000'::time end) and (case when (CURRENT_DATE + LOCALTIME) < CURRENT_DATE + '04:00:00.0000000'::time then CURRENT_DATE + '03:59:59.999999'::time else (CURRENT_DATE + 1) + '03:59:59.999999'::time end)`;
    let where = ` AND dw.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' `;
    let orderByStatus = `CASE 
        WHEN dw.status = 3 THEN 1
        WHEN dw.status = 4 THEN 2
        ELSE 3
    END,
    dw.created_date DESC`;
    if (typeUser === 0) {
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id  inner join agent a2  on a2.agent_id  = m.agent_id where a2.agent_id='${agentId}' ${where} ORDER BY ${orderByStatus}`;
    } else if (typeUser === 1) {
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id  inner join agent a2  on a2.agent_id  = m.agent_id where a2.agent_id='${agentNo}' ${where} ORDER BY ${orderByStatus}`;
    } else if (typeUser === 2 || typeUser === 3) {
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id ${where} ORDER BY ${orderByStatus}`;
    }
    let result = await db_access.query(`SELECT COUNT(*) AS total_record FROM (${transection}) rs`);
    let total_record = 0;
    if(result.rowCount > 0){
      total_record = parseInt(result.rows[0].total_record)
    }
    let respp = await db_access.query(`SELECT * FROM (${transection}) rs ${pagination}`);
    const resppp = respp.rows;
    let listData = [];

// Build a comma-separated string of resppp IDs
    const respppIds = resppp.map(item => `'${item.id}'`).join(',');
    let one = "";
// Build the SQL query
    if (respppIds && respppIds.length > 0) {
       one += `
  SELECT
    CAST(coalesce(dw.id, '0') AS integer) as id,
    dw.member_id, m.credit as current_credit,
    dw.deposited_date + interval '7 hour' as deposited_date,
    dw.updated_date + interval '7 hour' as updated_date,
    dw.amount, dw.created_date + interval '7 hour' as created_date,
    dw.remark, dw.approved_date  + interval '7 hour' as approved_date,
    dw.rejected_date + interval '7 hour' as rejected_date,
    m.username, m.mobile_no, m.bank_firstname as bank_member_firstname,
    m.bank_lastname as bank_member_lastname, m.bank_account as bank_member_account,
    dw.bank_username  as bank_agent_fullname, dw.bank_account  as bank_agent_account,
    tt.transaction_type, dw.url_image,
    coalesce((SELECT id FROM bank b WHERE dw.bank_id=b.id), 0) as agent_bank_id,
    dw.is_deposit_direct, dw.is_withdraw_direct,
    coalesce((SELECT bank_name FROM bank b WHERE dw.bank_id=b.id), null) as agent_bank_name,
    coalesce((SELECT bank_url FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_url,
    coalesce((SELECT bank_color FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_color,
    coalesce((SELECT id FROM bank b WHERE m.bank_id=b.id), 0) as member_bank_id,
    coalesce((SELECT bank_name FROM bank b WHERE m.bank_id=b.id), null) as name_bank_member,
    coalesce((SELECT bank_url FROM bank b WHERE m.bank_id=b.id), null) as url_bank_member,
    coalesce((SELECT bank_color FROM bank b WHERE m.bank_id=b.id), null) as color_bank_member,
    coalesce((SELECT title_name FROM title t WHERE m.bank_title_id = t.id), null) as bank_member_title_name,
    coalesce((SELECT status_name_th FROM status s WHERE dw.status = s.id), null) as status_name_th,
    coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) as actor_id,
    CASE
      WHEN coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) = 0 AND dw.status = 4 THEN 'อัตโนมัติ'
      ELSE coalesce((SELECT username FROM agent a WHERE dw.actor_id = a.agent_id AND dw.status = 4), null)
    END AS actor_name,
    dw.bank_username as bank_agent_username, dw.bank_account as bank_agent_account
  FROM deposit_withdraw dw
  INNER JOIN members m ON m.id = dw.member_id
  INNER JOIN transaction_type tt ON tt.id = dw.trans_type_id
  INNER JOIN agent a2 ON a2.agent_id = m.agent_id
  WHERE dw.id IN (${respppIds})
`;

      if (typeUser === 0) {
        one += ` AND a2.agent_id = '${agentId}'`;
      } else if (typeUser === 1) {
        one += ` AND a2.agent_id = '${agentNo}'`;
      }

      one += `
  ORDER BY ${orderByStatus};
`;
    }
// Execute the query
    let resu = await db_access.query(one);
     listData = resu.rows;
     total_record = listData.length;

    res.status(200).json({
      code: 200,
      message: "Success",
      // data: listData.sort((a, b) => new Date(b.created_date) - new Date(a.created_date)),
      data: listData ,
      total_record: total_record
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ListDepositWithdraw.
exports.get_search_listDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListDepositWithdraw" }} */

  try {
    const agentId = req.user.agent_id;
    // const agentId = 96;
    const data = req.body;
    const query = req.query;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let text = "";
    let bank = "";
    let tranType = "";
    let agentName = "";
    let titleName = "";
    let bankAgent = "";
    let actor = "";
    let moreThanAmount = "";
    let lessAmount = "";
    let inerr ="";
    let datee = "";
    let transection = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }

    let groupBy = "";
    let groupBy1 = "";
    let pagination = '';

    if(query.limit && query.limit > 0 && query.page && query.page > 0){
      const page = parseInt(query.page)
      const offset = (page-1) * parseInt(query.limit);
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`
    }
    let today = dayjs().tz("Asia/Bangkok").format("YYYY-MM-DD");
    let day = dayjs().tz("Asia/Bangkok").subtract(8,'day').format("YYYY-MM-DD");
    // let where = ` AND dw.created_date between CURRENT_DATE -1 + '00:00:00.0000000'::time and CURRENT_DATE + '23:59:59.999999'::time`
    // let where = ` AND dw.created_date  + INTERVAL '7 hours' between (case when (CURRENT_DATE + LOCALTIME) < CURRENT_DATE + '04:00:00.0000000'::time then (CURRENT_DATE - 1) + '04:00:00.0000000'::time else CURRENT_DATE + '04:00:00.0000000'::time end) and (case when (CURRENT_DATE + LOCALTIME) < CURRENT_DATE + '04:00:00.0000000'::time then CURRENT_DATE + '03:59:59.999999'::time else (CURRENT_DATE + 1) + '03:59:59.999999'::time end)`;
    let where = ` `;
    if (data.date === "" || data.date === null){
      where = "";
    } else{
      if (data.date === "1"){
        where += ` AND dw.created_date + INTERVAL '7 hours' >= '${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' `;
      } else if (data.date === "0"){
        where += ` AND dw.created_date + INTERVAL '7 hours' >= '${day}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' AND dw.created_date + INTERVAL '7 hours' <= '${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' `;
      }
    }
    if (typeUser === 0) {
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id  inner join agent a2  on a2.agent_id  = m.agent_id where a2.agent_id='${agentId}' ${where} ORDER BY dw.created_date DESC`;
    } else if (typeUser === 1) {
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id  inner join agent a2  on a2.agent_id  = m.agent_id where a2.agent_id='${agentNo}' ${where} ORDER BY dw.created_date DESC`;
    }else if (typeUser === 2 || typeUser === 3){
      transection = `select tt.transaction_type, dw.id, dw.trans_type_id from deposit_withdraw dw inner join members m on m.id = dw.member_id inner join transaction_type tt on tt.id = dw.trans_type_id ${where} ORDER BY dw.created_date DESC`;
    }

    let result = await db_access.query(`SELECT COUNT(*) AS total_record FROM (${transection}) rs`);
    let total_record = 0;
    if(result.rowCount > 0){
      total_record = parseInt(result.rows[0].total_record)
    }
    let respp = await db_access.query(`SELECT * FROM (${transection}) rs ${pagination}`);
    const resppp = respp.rows;
    let listData = [];
    if (data.text === "" || data.text === null) {
      text = "";
    } else {
      // text += " and (cast(m.credit as text) ilike '%" + data.text + "%' or cast(dw.deposited_date as text) ilike '%" + data.text + "%' or cast(dw.updated_date as text) ilike '%" + data.text + "%'  or cast(dw.amount as text) ilike '%" + data.text + "%' or cast(dw.created_date as text) ilike '%" + data.text + "%' or dw.remark ilike '%" + data.text + "%' or cast(dw.approved_date as text) ilike '%" + data.text + "%' or cast(dw.rejected_date as text) ilike '%" + data.text + "%' or m.username ilike '%" + data.text + "%' or m.mobile_no ilike '%" + data.text + "%' or m.bank_firstname ilike '%" + data.text + "%' or cast(dw.is_deposit_direct as text) ilike '%" + data.text + "%' or  cast(dw.is_withdraw_direct as text) ilike '%" + data.text + "%' or m.bank_lastname ilike '%" + data.text + "%' or cast(m.bank_account as text) ilike '%" + data.text + "%' or dw.bank_username ilike '%" + data.text + "%' or dw.bank_account ilike '%" + data.text + "%' or tt.transaction_type ilike '%" + data.text + "%' or coalesce((select title_name  from title t  where m.bank_title_id =t.id),null) ilike '%" + data.text + "%' or coalesce((select bank_name  from bank b where m.bank_id=b.id),null) ilike '%" + data.text + "%' or coalesce((select bank_name  from bank b where dw.bank_id=b.id),null) ilike '%" + data.text + "%' or coalesce((select status_name_th  from status s where dw.status  =s.id),null) ilike '%" + data.text + "%' or dw.bank_username ilike '%" + data.text + "%' or CAST(dw.bank_account as text) ilike '%" + data.text + "%') ";
      text += " and (cast(m.credit as text) ilike '%" + data.text + "%' or cast(dw.deposited_date as text) ilike '%" + data.text + "%' or cast(dw.updated_date as text) ilike '%" + data.text + "%'  or cast(dw.amount as text) ilike '%" + data.text + "%' or cast(dw.created_date as text) ilike '%" + data.text + "%' or dw.remark ilike '%" + data.text + "%' or cast(dw.approved_date as text) ilike '%" + data.text + "%' or cast(dw.rejected_date as text) ilike '%" + data.text + "%' or m.username ilike '%" + data.text + "%' or m.mobile_no ilike '%" + data.text + "%' or m.bank_firstname ilike '%" + data.text + "%' or cast(dw.is_deposit_direct as text) ilike '%" + data.text + "%' or  cast(dw.is_withdraw_direct as text) ilike '%" + data.text + "%' or m.bank_lastname ilike '%" + data.text + "%' or cast(m.bank_account as text) ilike '%" + data.text + "%' or dw.bank_username ilike '%" + data.text + "%' or dw.bank_account ilike '%" + data.text + "%' or tt.transaction_type ilike '%" + data.text + "%' or coalesce((select title_name  from title t  where m.bank_title_id =t.id),null) ilike '%" + data.text + "%' or coalesce((select status_name_th  from status s where dw.status  =s.id),null) ilike '%" + data.text + "%' or dw.bank_username ilike '%" + data.text + "%' or CAST(dw.bank_account as text) ilike '%" + data.text + "%') ";
    }
    // if (data.bank === "" || data.bank === null || data.bank === undefined) {
    //   bank = "";
    // } else {
    //   bank += " and (coalesce((select bank_name  from bank b where m.bank_id=b.id),null) like '%" + data.bank + "%' "
    // }
    if (data.type === "" || data.type === null) {
      tranType = "";
    } else {
      if (data.type === "ฝาก"){
        tranType = " and (tt.transaction_type = '" + data.type + "' or tt.transaction_type = 'ฝากตรง')";
      } else {
        tranType = " and (tt.transaction_type = '" + data.type + "' or tt.transaction_type = 'ถอนตรง')";
      }
    }
    if (data.agent_name === "" || data.agent_name === null) {
      agentName = "";
    } else {
      inerr = " inner join agent a2  on a2.agent_id  = m.agent_id ";
      agentName = " and a2.username  ='" + data.agent_name + "' ";
    }
    if (data.bank_agent === "" || data.bank_agent === null) {
      bankAgent = "";
    } else {
      bankAgent = " and dw.bank_account ='" + data.bank_agent + "' ";
    }
    if (data.more_than_amount === "" || data.more_than_amount === null) {
      moreThanAmount = "";
    } else {
      moreThanAmount = " and dw.amount >=" + data.more_than_amount  +" " ;
    }
    if (data.less_amount === "" || data.less_amount === null) {
      lessAmount = "";
    } else {
      lessAmount = " and dw.amount <=" + data.less_amount + " ";
    }
    if (data.actor === "" || data.actor === null) {
      actor = "";
    } else {
      if (data.actor === 0 || data.actor === "0" ) {
        actor = "  and dw.actor_id =0 ";
      } else  if (data.actor === 1 || data.actor === "1" )  {
        actor = "  and dw.actor_id != 0 ";
      }
    }
    if (data.date === "" || data.date === null){
      datee = "";
    } else{
      if (data.date === "1"){
        // date = " and DATE(dw.created_date) = current_date ";
        datee = " AND dw.created_date + INTERVAL '7 hours' >= '"+today+"' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' ";
      } else if (data.date === "0"){
        datee = " AND dw.created_date + INTERVAL '7 hours' >= '"+day+"' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' AND dw.created_date + INTERVAL '7 hours' <= '"+today+"' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours'  ";
      }
    }
    groupBy1 = " group by dw.id, m.credit, dw.bank_account,dw.bank_username, dw.deposited_date, dw.updated_date, dw.amount, dw.created_date, dw.remark, dw.approved_date ,dw.rejected_date , m.username, m.bank_account, tt.transaction_type,dw.url_image,m.mobile_no, m.bank_firstname , m.bank_lastname, m.bank_id ,m.bank_title_id, dw.is_deposit_direct, dw.is_withdraw_direct ";
    const respppIds = resppp.map(item => `'${item.id}'`).join(',');
    let orderByStatus = `CASE 
        WHEN dw.status = 3 THEN 1
        WHEN dw.status = 4 THEN 2
        ELSE 3
    END,
    dw.created_date DESC`;
    let one = "";
    if ((data.text === "" || data.text === null) && (data.bank === "" || data.bank === null) && (data.type === "" || data.type === null)
        && (data.agent_name === "" || data.agent_name === null) && (data.more_than_amount === "" || data.more_than_amount === null)
        && (data.actor === "" || data.actor === null) && (data.bank_agent === "" || data.bank_agent === null) && (data.date === "" || data.date === null)) {
      if (respppIds && respppIds.length > 0) {
      one += `
  SELECT
    CAST(coalesce(dw.id, '0') AS integer) as id,
    dw.member_id, m.credit as current_credit,
    dw.deposited_date + interval '7 hour' as deposited_date,
    dw.updated_date + interval '7 hour' as updated_date,
    dw.amount, dw.created_date + interval '7 hour' as created_date,
    dw.remark, dw.approved_date  + interval '7 hour' as approved_date,
    dw.rejected_date + interval '7 hour' as rejected_date,
    m.username, m.mobile_no, m.bank_firstname as bank_member_firstname,
    m.bank_lastname as bank_member_lastname, m.bank_account as bank_member_account,
    dw.bank_username  as bank_agent_fullname, dw.bank_account  as bank_agent_account,
    tt.transaction_type, dw.url_image,
    coalesce((SELECT id FROM bank b WHERE dw.bank_id=b.id), 0) as agent_bank_id,
    dw.is_deposit_direct, dw.is_withdraw_direct,
    coalesce((SELECT bank_name FROM bank b WHERE dw.bank_id=b.id), null) as agent_bank_name,
    coalesce((SELECT bank_url FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_url,
    coalesce((SELECT bank_color FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_color,
    coalesce((SELECT id FROM bank b WHERE m.bank_id=b.id), 0) as member_bank_id,
    coalesce((SELECT bank_name FROM bank b WHERE m.bank_id=b.id), null) as name_bank_member,
    coalesce((SELECT bank_url FROM bank b WHERE m.bank_id=b.id), null) as url_bank_member,
    coalesce((SELECT bank_color FROM bank b WHERE m.bank_id=b.id), null) as color_bank_member,
    coalesce((SELECT title_name FROM title t WHERE m.bank_title_id = t.id), null) as bank_member_title_name,
    coalesce((SELECT status_name_th FROM status s WHERE dw.status = s.id), null) as status_name_th,
    coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) as actor_id,
    CASE
      WHEN coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) = 0 AND dw.status = 4 THEN 'อัตโนมัติ'
      ELSE coalesce((SELECT username FROM agent a WHERE dw.actor_id = a.agent_id AND dw.status = 4), null)
    END AS actor_name,
    dw.bank_username as bank_agent_username, dw.bank_account as bank_agent_account
  FROM deposit_withdraw dw
  INNER JOIN members m ON m.id = dw.member_id
  INNER JOIN transaction_type tt ON tt.id = dw.trans_type_id
  INNER JOIN agent a2 ON a2.agent_id = m.agent_id
  WHERE dw.id IN (${respppIds}) 
`;
      if (typeUser === 0) {
        one += ` AND a2.agent_id = '${agentId}'`;
      } else if (typeUser === 1) {
        one += ` AND a2.agent_id = '${agentNo}'`;
      }

      one += `ORDER BY ${orderByStatus}; `;
    }
    } else {
      let bank1 = "";
      if (data.bank === "" || data.bank === null || data.bank === undefined){
        bank1 = " ";
      } else{
        bank1 = " and coalesce((select bank_name  from bank b where dw.bank_id=b.id),null) like '%" + data.bank + "%' ";

      }
      if (respppIds && respppIds.length > 0) {
        one += `
  SELECT
    CAST(coalesce(dw.id, '0') AS integer) as id,
    dw.member_id, m.credit as current_credit,
    dw.deposited_date + interval '7 hour' as deposited_date,
    dw.updated_date + interval '7 hour' as updated_date,
    dw.amount, dw.created_date + interval '7 hour' as created_date,
    dw.remark, dw.approved_date  + interval '7 hour' as approved_date,
    dw.rejected_date + interval '7 hour' as rejected_date,
    m.username, m.mobile_no, m.bank_firstname as bank_member_firstname,
    m.bank_lastname as bank_member_lastname, m.bank_account as bank_member_account,
    dw.bank_username  as bank_agent_fullname, dw.bank_account  as bank_agent_account,
    tt.transaction_type, dw.url_image,
    coalesce((SELECT id FROM bank b WHERE dw.bank_id=b.id), 0) as agent_bank_id,
    dw.is_deposit_direct, dw.is_withdraw_direct,
    coalesce((SELECT bank_name FROM bank b WHERE dw.bank_id=b.id), null) as agent_bank_name,
    coalesce((SELECT bank_url FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_url,
    coalesce((SELECT bank_color FROM bank b WHERE dw.bank_id=b.id), null) as bank_agent_color,
    coalesce((SELECT id FROM bank b WHERE m.bank_id=b.id), 0) as member_bank_id,
    coalesce((SELECT bank_name FROM bank b WHERE m.bank_id=b.id), null) as name_bank_member,
    coalesce((SELECT bank_url FROM bank b WHERE m.bank_id=b.id), null) as url_bank_member,
    coalesce((SELECT bank_color FROM bank b WHERE m.bank_id=b.id), null) as color_bank_member,
    coalesce((SELECT title_name FROM title t WHERE m.bank_title_id = t.id), null) as bank_member_title_name,
    coalesce((SELECT status_name_th FROM status s WHERE dw.status = s.id), null) as status_name_th,
    coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) as actor_id,
    CASE
      WHEN coalesce((SELECT agent_id FROM agent a WHERE dw.actor_id = a.agent_id), 0) = 0 AND dw.status = 4 THEN 'อัตโนมัติ'
      ELSE coalesce((SELECT username FROM agent a WHERE dw.actor_id = a.agent_id AND dw.status = 4), null)
    END AS actor_name,
    dw.bank_username as bank_agent_username, dw.bank_account as bank_agent_account
  FROM deposit_withdraw dw
  INNER JOIN members m ON m.id = dw.member_id
  INNER JOIN transaction_type tt ON tt.id = dw.trans_type_id
  INNER JOIN agent a2 ON a2.agent_id = m.agent_id
  WHERE dw.id IN (${respppIds}) ${text} ${moreThanAmount} ${lessAmount} ${bank} ${bank1} ${actor} ${titleName} ${bankAgent} ${agentName} ${tranType} ${datee} ${groupBy1}
`;

        if (typeUser === 0) {
          one += ` AND a2.agent_id = '${agentId}'`;
        } else if (typeUser === 1) {
          one += ` AND a2.agent_id = '${agentNo}'`;
        }
        one += ` order by dw.created_date desc `;
// Execute the query

      }
    }
    let resu = await db_access.query(one);
    listData = resu.rows;
    total_record = listData.length;
    
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listData,
      total_record: total_record
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Create a new ListDepositWithdraw.
exports.post_listDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ListDepositWithdraw' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatelistDepositWithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);", [
      0,
      utilities.is_empty_string(data.date) ? null : data.date,
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.current_credit) ? null : data.current_credit,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.type) ? null : data.type,
      utilities.is_empty_string(data.bank_customer_id) ? null : data.bank_customer_id,
      utilities.is_empty_string(data.date_deposit) ? null : data.date_deposit,
      utilities.is_empty_string(data.bank_agent_id) ? null : data.bank_agent_id,
      utilities.is_empty_string(data.total) ? null : data.total,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.note) ? null : data.note,
      utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id
    ]);
    
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new ListDepositWithdraw.
exports.put_listDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ListDepositWithdraw' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatelistDepositWithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.date) ? null : data.date,
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.current_credit) ? null : data.current_credit,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.type) ? null : data.type,
      utilities.is_empty_string(data.bank_customer_id) ? null : data.bank_customer_id,
      utilities.is_empty_string(data.date_deposit) ? null : data.date_deposit,
      utilities.is_empty_string(data.bank_agent_id) ? null : data.bank_agent_id,
      utilities.is_empty_string(data.total) ? null : data.total,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.note) ? null : data.note,
      utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id
    ]);
    
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete ListDepositWithdraw.
exports.delete_listDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletelistDepositWithdraw($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);
    
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a Update AskCredit.
exports.put_updateStatusDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/AskCredit' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let creds = 0;
  let agen = "";
  try {
    let result = await db_access.query("select m.credit , dw.status, dw.amount, dw.trans_type_id, dw.member_id  from deposit_withdraw dw  inner join members m on m.id =dw.member_id  where dw.id ="+data.id+";");
    const resp = result.rows;
    for (let i = 0; i< resp.length; i++) {
      let status = resp[i].status;
      let credit_1 = resp[i].credit;
      let amount_1 = resp[i].amount;
      let credit = parseFloat(credit_1);
      let amount = parseFloat(amount_1);
      let trans_type_id = resp[i].trans_type_id;
      let memberId = resp[i].member_id;
      let sum = null;
      let resultt = await db_access.query("SELECT a.credit, a.agent_id FROM members m inner join agent a on a.agent_id = m.agent_id where m.id ="+memberId+";");
      const responseCredit = resultt.rows;
      for (let i = 0; i< responseCredit.length; i++){
        creds = responseCredit[i].credit;
        agen = responseCredit[i].agent_id;
      }
      let credits = parseFloat(creds);
      let totalUser = 0;
      let types = "";
      let message = "";
      let name = "";
      let note = "";
      let creditAgent = "";
      let agent_id = await db_access.query("select a.username  from agent a  where a.agent_id  =" + agentId + ";");
      let ag = "";
      const response = agent_id.rows;
      for (let i = 0; i < response.length; i++) {
        ag = response[i].username;
      }
      // console.log(data.status);
      if (status === 3) {
        if (trans_type_id === 1) {
          if (credits >= amount ) {
            await db_access.query("CALL public.spupdatestatusdepositwithdraw($1,$2,$3);", [
              utilities.is_empty_string(data.id) ? null : data.id,
              utilities.is_empty_string(data.status) ? null : data.status,
              utilities.is_empty_string(agentId) ? 0 : agentId
            ]);
            sum = credit + amount;
            totalUser = credits - amount;
            if (data.status === 4) {
              await db_access.query("UPDATE members  SET credit =" + sum + " WHERE id =" + memberId + ";");
              await db_access.query("UPDATE agent SET credit = " + totalUser + " where agent_id=" + agen + ";");

              global.io.sockets.emit('action_current_credit', {
                member_id: memberId,
                credit: sum
              });
            } else {
              types = "1";
              name = "ยกเลิกฝาก";
              message = name + " จำนวน" + amount + note + " โดย" + ag;
              await db_access.query("DELETE FROM deposit_withdraw WHERE id =" + data.id + ";");
              await db_access.query("INSERT INTO transaction_agent_member (member_id, actor_id, type_log_transaction_id, detail, ip_address) values (" + memberId + "," + agentId + "," + types + " ,'" + message + "','" + data.ip_address + "');");
            }
            res.status(200).json({
              code: 200,
              message: "Success",
            });
          } else if (data.status === 5){
            {
              types = "1";
              name = "ยกเลิกฝาก";
              message = name + " จำนวน" + amount + note + " โดย" + ag;
              await db_access.query("DELETE FROM deposit_withdraw WHERE id =" + data.id + ";");
              await db_access.query("INSERT INTO transaction_agent_member (member_id, actor_id, type_log_transaction_id, detail, ip_address) values (" + memberId + "," + agentId + "," + types + " ,'" + message + "','" + data.ip_address + "');");
            }
            res.status(200).json({
              code: 200,
              message: "Success",
            });
          }else {
            res.status(200).json({
              code: 200,
              message: "agent not enough credit",
            });
          }
        } else {
          if (data.status === 4) {
            if (credit >= amount) {
              await db_access.query("CALL public.spupdatestatusdepositwithdraw($1,$2,$3);", [
                utilities.is_empty_string(data.id) ? null : data.id,
                utilities.is_empty_string(data.status) ? null : data.status,
                utilities.is_empty_string(agentId) ? 0 : agentId
              ]);
              sum = credit - amount;
              totalUser = credits + amount;
              if (data.status === 4) {
                await db_access.query("UPDATE members  SET credit =" + sum + " WHERE id =" + memberId + ";");
                await db_access.query("UPDATE agent SET credit = " + totalUser + " where agent_id=" + agen + ";");

                global.io.sockets.emit('action_current_credit', {
                  member_id: memberId,
                  credit: sum
                });
              }
              res.status(200).json({
                code: 200,
                message: "Success",
              });
            } else {
              res.status(200).json({
                code: 200,
                message: "not enough credit",
              });
            }
          } else if (data.status === 5) {
            types = "2";
            name = "ยกเลิกถอน";
            message = name + " จำนวน" + amount + note + " โดย" + ag;
            await db_access.query("DELETE FROM deposit_withdraw WHERE id =" + data.id + ";");
            await db_access.query("INSERT INTO transaction_agent_member (member_id, actor_id, type_log_transaction_id, detail, ip_address) values (" + memberId + "," + agentId + "," + types + " ,'" + message + "','" + data.ip_address + "');");
            
            res.status(200).json({
              code: 200,
              message: "Success",
            });
          }
        }
      }else{
        res.status(200).json({
          code: 200,
          message: "Status has been updated",
        });
      }

      global.io.sockets.emit('action_deposit_withdraw', {
        member_id: resp[i].member_id,
        deposit_withdraw: true
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve AskCredit.
exports.get_slipDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDepositWithdraw']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListDepositWithdraw" }} */

  try {
    const data = req.body;
    let fileName = null;
    let result = await db_access.query("SELECT dw.url_image FROM deposit_withdraw dw where dw.member_id ="+data.member_id+" and dw.id = "+data.id+";");
    const resp = result.rows;
    for (let i = 0; i<resp.length; i++) {
      fileName =  resp[i];
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: fileName,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.post_listDetailHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListHuay" }} */

  try {
    const data = req.body;
    let one = '';
      one = await db_access.query(
          'SELECT lh.id,  lh.total_price,  lh.total_price as total , lh.total_win, lh.ip_address, lh.created_date + INTERVAL \'7 hours\' as created_date , s.status_name_th, m.username, mc.category_name as lotto_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where lh.member_id='+ data.member_id +' and lh.lotto_id ='+data.lotto_id+' order by lh.created_date desc;'
      );
    const resp = one.rows;

    res.status(200).json({
      code: 200,
      message: 'Success',
      data: resp,
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve GetDateail.
exports.post_listDetailHuay1 = async (req, res) => {
  // #swagger.tags = ['GetDateail']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/GetDateail" }} */
  const data = req.body;
  try {
    const sql = "select ld.lotto_header_id as id, m.username, nt.number_type_name, ld.rate  , ld.numbers , ld.price , ld.win_price , ld.result_number, s.status_name_th  from lotto_detail ld inner join lotto_header lh on lh.id = ld.lotto_header_id inner join members m on m.id = lh.member_id inner join number_type nt on nt.id = ld.number_type_id inner join status s  on s.id = ld.status where m.id  =";
    let result = await db_access.query(sql + data.member_id + "and lh.lotto_id ="+data.lotto_id+" order by ld.number_type_id; ");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: 'Success',
      data: resp,
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};