const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve ListDirectDirectDepositWithdraw.
exports.get_listDirectDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDirectDepositWithdraw']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ListDirectDepositWithdraw" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = null;
    let agentNo = null;
    let one ="";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    one += "WITH dates (idate ) AS (";
    one += "  SELECT generate_series(";
    one += "    CURRENT_DATE - INTERVAL '5 days',";
    one += "    CURRENT_DATE + INTERVAL '1 day',   ";
    one += "    INTERVAL '1 day'";
    one += "  )::date";
    one += ")";
    one += " SELECT CAST(d.idate AS DATE) AT TIME ZONE 'Asia/Bangkok' AS created_date,";
    if (typeAgent === 0){
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw INNER JOIN members m ON m.id = dw.member_id WHERE dw.trans_type_id = 4 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND m.agent_id ="+agentId+" AND dw.is_withdraw_direct = true),0) AS sum_withdraw,";
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw INNER JOIN members m ON m.id = dw.member_id WHERE dw.trans_type_id = 3 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND m.agent_id ="+agentId+" AND dw.is_deposit_direct = true), 0) AS sum_deposit ";
    } else if (typeAgent === 1){
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw INNER JOIN members m ON m.id = dw.member_id WHERE dw.trans_type_id = 4 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND m.agent_id ="+agentNo+" AND dw.is_withdraw_direct = true),0) AS sum_withdraw,";
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw INNER JOIN members m ON m.id = dw.member_id WHERE dw.trans_type_id = 3 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND m.agent_id ="+agentNo+" AND dw.is_deposit_direct = true),0) AS sum_deposit ";
    } else if (typeAgent === 2 || typeAgent === 3){
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw WHERE dw.trans_type_id = 4 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND dw.is_withdraw_direct = true),0) AS sum_withdraw,";
      one += " COALESCE((SELECT SUM(dw.amount) FROM deposit_withdraw dw WHERE dw.trans_type_id = 3 AND CAST(dw.created_date + INTERVAL '7 hours' AS DATE) = d.idate AND dw.is_deposit_direct = true),0) AS sum_deposit ";
    }
    one += "FROM dates d ORDER BY d.idate ASC;";

    result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve ListDirectDirectDepositWithdraw.
exports.get_search_listDirectDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['ListDirectDepositWithdraw']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListDirectDepositWithdraw" }} */

  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.text !== "" || data.text !== null) {
      if (typeAgent === 0) {
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date ,COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw inner join members m on m.id = dw.member_id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentId + "and (CAST(dw.created_date as text) ilike '%" + data.text + "%') GROUP by CAST(dw.created_date  AS DATE) having  (CAST(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)  as text) ilike '%" + data.text + "%' or cast(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as text) ilike '%" + data.text + "%' or cast((COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) -  COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)) as text) ilike '%" + data.text + "%')  order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      } else if (typeAgent === 1) {
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date ,COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw inner join members m on m.id = dw.member_id inner join agent a on a.agent_id = m.agent_id where a.agent_id =" + agentNo + "and (CAST(dw.created_date as text) ilike '%" + data.text + "%') GROUP by CAST(dw.created_date  AS DATE) having  (CAST(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)  as text) ilike '%" + data.text + "%' or cast(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as text) ilike '%" + data.text + "%' or cast((COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) -  COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)) as text) ilike '%" + data.text + "%')  order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      } else if (typeAgent === 2) {
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw where CAST(dw.created_date as text) ilike '%" + data.text + "%' GROUP by CAST(dw.created_date  AS DATE) having  CAST(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)  as text) ilike '%" + data.text + "%' or cast(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as text) ilike '%" + data.text + "%' or cast((COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) -  COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)) as text) ilike '%" + data.text + "%'  order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      } else if (typeAgent === 3) {
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw where CAST(dw.created_date as text) ilike '%" + data.text + "%' GROUP by CAST(dw.created_date  AS DATE) having  CAST(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)  as text) ilike '%" + data.text + "%' or cast(COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) as text) ilike '%" + data.text + "%' or cast((COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 1),0) -  COALESCE(SUM(dw.amount) filter (where dw.trans_type_id = 2),0)) as text) ilike '%" + data.text + "%'  order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      }
    } else{
      if (typeAgent === 0){
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date ,COALESCE(SUM(amount) filter (where trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(amount) filter (where trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw inner join members m on m.id = dw.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentId+" GROUP by CAST(dw.created_date  AS DATE) order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      } else if (typeAgent === 1){
        result = await db_access.query("select CAST(dw.created_date  AS DATE) as created_date ,COALESCE(SUM(amount) filter (where trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(amount) filter (where trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw inner join members m on m.id = dw.id inner join agent a on a.agent_id = m.agent_id where a.agent_id ="+agentNo+" GROUP by CAST(dw.created_date  AS DATE) order by CAST(dw.created_date  AS DATE) desc LIMIT 7;");
      }  else if (typeAgent === 2){
        result = await db_access.query("select CAST(created_date  AS DATE) as created_date , COALESCE(SUM(amount) filter (where trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(amount) filter (where trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw  GROUP by CAST(created_date  AS DATE) order by CAST(created_date  AS DATE) desc LIMIT 7;");
      } else if (typeAgent === 3){
        result = await db_access.query("select CAST(created_date  AS DATE) as created_date , COALESCE(SUM(amount) filter (where trans_type_id = 2),0) as sum_withdraw , COALESCE(SUM(amount) filter (where trans_type_id = 1),0) as sum_deposit FROM deposit_withdraw dw  GROUP by CAST(created_date  AS DATE) order by CAST(created_date  AS DATE) desc LIMIT 7;");
      }
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

