const dayjs = require("dayjs");
var utc = require('dayjs/plugin/utc');
var timezone = require('dayjs/plugin/timezone'); // dependent on utc plugin
const db_access = require("../db_access");
const utilities = require("../utils/utilities");
dayjs.extend(utc)
dayjs.extend(timezone)

const {
  request_validator,
  check_validator_basic,
} = require("../validator/request.validator");
const {
  lotto,
  keyHuaySelector,
  lotto_status,
  whiteListSubLotto,
  HUAYHUNG_THAI_PID_YEAN,
} = require("../commons/constants/lotto.constants");
const {
  response_error_builder,
  response_success_builder,
} = require("../utils/response-builder/response-utils");
const { manage_error } = require("../utils/exception-handler/manage-error");
const ApplicationError = require("../commons/exceptions/application-error");
const { CLIENT_ERROR } = require("../commons/errors/error-message");
const httpContext = require("express-http-context");
const lotto_type = httpContext.get("lotto_type");

const getHuayById = async () => {
  const reverseObj = {};
  for await (const item of Object.keys(keyHuaySelector)) {
    reverseObj[keyHuaySelector[item]] = item;
  }
  return reverseObj;
};

exports.get_numbersetfalse = async (req, res) => {

  try {
    let one  = `select * from public.number_type nt where nt.is_number_set is false;`;
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_numbersettrue = async (req, res) => {

  try {
    let one  = `select * from public.number_type nt where nt.is_number_set is true;`;
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_ratebynumber = async (req, res) => {

  try {
    const data = req.body;
    let one  = `select ld.numbers, ld.rate, count(lh.member_id) as total_member, CAST(sum(ld.price) AS INT) as total_price, nt.number_type_name as type from public.lotto_header lh 
    inner join public.lotto_detail ld on lh.id = ld.lotto_header_id
    inner join public.number_type nt on ld.number_type_id = nt.id
    where lh.lotto_id = '${data.lotto_id}' and lh.date_id = '${data.date}' and ld.numbers = '${data.lotto_number}' and nt.id = '${data.number_type}' and ld.status != 11
    group by ld.numbers, ld.rate, nt.number_type_name;`;
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve getMenuHuay.
exports.get_listMenuHuay = async (req, res) => {
  // #swagger.tags = ['GetMenuHuay']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/GetMenuHuay" }} */

  try {
    let result = await db_access.query(
      "SELECT l.id  , l.lotto_name  as lotto_name, lt.id as lotto_header_id, lt.lotto_type_name as lotto_header_name  FROM lotto l inner join lotto_type lt on lt.id = l.lotto_type_id order by l.id asc;"
    );
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ListHuay.
exports.get_listHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListHuay" }} */

  try {
    const agentId = req.user.agent_id;
    const type =
      "select a.type_user, a.agent from agent a where a.agent_id =" +
      agentId +
      ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let typeUser = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeUser === 0) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, l.lotto_name, mc.category_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where m.agent_id=" +
          agentId +
          "and DATE(lh.updated_date + INTERVAL '7 hours' ) = current_date  order by lh.updated_date desc;"
      );
    } else if (typeUser === 1) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, l.lotto_name, mc.category_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where m.agent_id=" +
          agentNo +
          " and DATE(lh.updated_date + INTERVAL '7 hours' ) = current_date order by lh.updated_date desc;"
      );
    } else if (typeUser === 2 || typeUser === 3) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, l.lotto_name, mc.category_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where DATE(lh.updated_date + INTERVAL '7 hours' ) = current_date  order by lh.updated_date desc;"
      );
    }
    const resp = one.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_search_listHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ListHuay" }} */

  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type =
      "select a.type_user, a.agent from agent a where a.agent_id =" +
      agentId +
      ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let typeUser = "";
    let orderNo = "";
    let datee = "";
    let roundNumber = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.order_no === "" || data.order_no === null) {
      orderNo = "";
    } else {
      if (typeUser === 0 || typeUser === 1) {
        orderNo = " and cast(lh.id as text) ilike '%" + data.order_no + "%' ";
      } else {
        orderNo = " where cast(lh.id as text) ilike '%" + data.order_no + "%' ";
      }
    }
    if (data.round_number === "" || data.round_number === null) {
      roundNumber = "";
    } else {
      if (typeUser === 0 || typeUser === 1) {
        roundNumber =
          " and cast(lh.updated_date as text) ilike '%" +
          data.round_number +
          "%' ";
      } else {
        if (data.order_no === "" || data.order_no === null) {
          roundNumber =
            " where cast(lh.updated_date as text) ilike '%" +
            data.round_number +
            "%' ";
        } else {
          roundNumber =
            " and cast(lh.updated_date as text) ilike '%" +
            data.round_number +
            "%' ";
        }
      }
    }
    if (data.date !== "" || data.date !== null) {
      if (
          (data.order_no === "" || data.order_no === null) &&
          (data.round_number === "" || data.round_number === null)
      ) {
        if (typeUser === 0 || typeUser === 1) {
          if (data.date === "1" || data.date === 1) {
            datee = " and DATE(lh.updated_date + INTERVAL '7 hours' ) = current_date ";
          } else if (data.date === "0" || data.date === 0 ) {
            datee = " and DATE(lh.updated_date + INTERVAL '7 hours') > current_date - INTERVAL '8 DAY' and DATE(lh.updated_date + INTERVAL '7 hours') != current_date ";
          }
        } else {
          if (data.date === "1" || data.date === 1) {
            datee = " where DATE(lh.updated_date + INTERVAL '7 hours' ) = current_date ";
          } else if (data.date === "0" || data.date === 0) {
            datee = " where DATE(lh.updated_date + INTERVAL '7 hours') > current_date - INTERVAL '8 DAY' and DATE(lh.updated_date + INTERVAL '7 hours') != current_date ";
          }
        }
      } else {
        if (data.date === "1"  || data.date === 1) {
          datee = " and DATE(lh.updated_date+ INTERVAL '7 hours') = current_date ";
        } else if (data.date === "0" || data.date === 0) {
          datee = " AND DATE(lh.updated_date + INTERVAL '7 hours') > current_date - INTERVAL '8 DAY' and DATE(lh.updated_date + INTERVAL '7 hours') != current_date ";
        }
      }
    } else {
      datee = "";
    }
    if (typeUser === 0) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, mc.category_name, l.lotto_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id  inner join mapping_category mc on mc.category_id = lh.category_id where m.agent_id=" +
          agentId +
          orderNo +
          roundNumber +
          datee +
          " order by lh.updated_date desc;"
      );
    } else if (typeUser === 1) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, mc.category_name, l.lotto_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where m.agent_id=" +
          agentNo +
          orderNo +
          roundNumber +
          datee +
          " order by lh.updated_date desc;"
      );
    } else if (typeUser === 2 || typeUser === 3) {
      one = await db_access.query(
        "SELECT lh.id,  lh.total_price , lh.total_win, lh.ip_address, lh.updated_date  + INTERVAL '7 hours' as created_date, s.status_name_th, m.username, mc.category_name, l.lotto_name FROM lotto_header lh inner join status s on s.id = lh.status inner join members m on m.id = lh.member_id  inner join mapping_category mc on mc.category_id = lh.category_id inner join lotto l on l.id = lh.lotto_id " +
          orderNo +
          roundNumber +
          datee +
          " order by lh.updated_date desc;"
      );
    }
    const resp = one.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve GetDateail.
exports.post_listDetailHuay = async (req, res) => {
  // #swagger.tags = ['GetDateail']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/GetDateail" }} */
  const data = req.body;
  try {
    const sql =
      "select m.username, nt.number_type_name, ld.rate  , ld.numbers , ld.price , ld.win_price , ld.result_number, s.status_name_th  from lotto_detail ld inner join lotto_header lh on lh.id = ld.lotto_header_id inner join members m on m.id = lh.member_id inner join number_type nt on nt.id = ld.number_type_id inner join status s  on s.id = ld.status where lh.id  =";
    let result = await db_access.query(
      sql + data.id + " order by ld.number_type_id; "
    );
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new ListHuay.
exports.post_listHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ListHuay' }
  } */
  const data = req.body;

  try {
    await db_access.query(
      "CALL public.spinsertupdatelistHuay($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);",
      [
        0,
        utilities.is_empty_string(data.date) ? null : data.date,
        utilities.is_empty_string(data.user_id) ? null : data.user_id,
        utilities.is_empty_string(data.current_credit)
          ? null
          : data.current_credit,
        utilities.is_empty_string(data.phone) ? null : data.phone,
        utilities.is_empty_string(data.type) ? null : data.type,
        utilities.is_empty_string(data.bank_customer_id)
          ? null
          : data.bank_customer_id,
        utilities.is_empty_string(data.date_deposit) ? null : data.date_deposit,
        utilities.is_empty_string(data.bank_agent_id)
          ? null
          : data.bank_agent_id,
        utilities.is_empty_string(data.total) ? null : data.total,
        utilities.is_empty_string(data.status) ? null : data.status,
        utilities.is_empty_string(data.note) ? null : data.note,
        utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id,
      ]
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new ListHuay.
exports.put_listHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ListHuay' }
  } */
  const data = req.body;

  try {
    await db_access.query(
      "CALL public.spinsertupdatelistHuay($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);",
      [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(data.date) ? null : data.date,
        utilities.is_empty_string(data.user_id) ? null : data.user_id,
        utilities.is_empty_string(data.current_credit)
          ? null
          : data.current_credit,
        utilities.is_empty_string(data.phone) ? null : data.phone,
        utilities.is_empty_string(data.type) ? null : data.type,
        utilities.is_empty_string(data.bank_customer_id)
          ? null
          : data.bank_customer_id,
        utilities.is_empty_string(data.date_deposit) ? null : data.date_deposit,
        utilities.is_empty_string(data.bank_agent_id)
          ? null
          : data.bank_agent_id,
        utilities.is_empty_string(data.total) ? null : data.total,
        utilities.is_empty_string(data.status) ? null : data.status,
        utilities.is_empty_string(data.note) ? null : data.note,
        utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id,
      ]
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete ListHuay.
exports.delete_listHuay = async (req, res) => {
  // #swagger.tags = ['ListHuay']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletelistHuay($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Get Lotto Type
exports.get_list_header_huay = async (req, res) => {
  try {
    // const sql = `SELECT lotto_type.id, lotto_type.lotto_type_name, lotto_type.status, lotto_type.created_date, 
    // lotto_type.updated_date, lotto_type.order_result,
    // l.id as lotto_id, l.lotto_name
    // FROM public.lotto_type 
    // inner join lotto l on l.lotto_type_id = lotto_type.id
    // where l.status = 1 and lotto_type.status = 1
    // order by lotto_type.id asc , l.id asc`;
    const sql = `SELECT lotto_type.id, lotto_type.lotto_type_name, lotto_type.status, lotto_type.created_date, 
    lotto_type.updated_date, lotto_type.order_result,
    l.id as lotto_id, l.lotto_name, l.status as lotto_status
    FROM public.lotto_type 
    inner join lotto l on l.lotto_type_id = lotto_type.id
    order by lotto_type.id asc , l.id asc`;
    const result = await db_access.query(sql);
    const lotto_types = [];
    const l_types = {};
    for await (const lotto_type of result.rows) {
      if (l_types[`${lotto_type.id}`] === undefined) {
        l_types[`${lotto_type.id}`] = {
          id: lotto_type.id,
          lotto_type_name: lotto_type.lotto_type_name,
          created_date: lotto_type.created_date,
          updated_date: lotto_type.updated_date,
          status: lotto_type.status,
          order_result: lotto_type.order_result,
          lottos: [],
        };
      }

      if (!whiteListSubLotto.includes(`${lotto_type.id}`))
        if (lotto_type.lotto_status === 1) {
          l_types[`${lotto_type.id}`].lottos.push({
            lotto_id: lotto_type.lotto_id,
            lotto_name: lotto_type.lotto_name,
          });
        }
    }

    for await (const id of Object.keys(l_types)) {
      lotto_types.push(l_types[id]);
    }
    res.status(200).send({
      code: 200,
      message: "Success",
      data: lotto_types,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Get Config Huay
exports.get_config_huay = async (req, res) => {
  try {
    const lotto_type = req.params.id;
    if (!lotto_type) {
      res.status(400).send({
        code: 400,
        message: "lotto_type_id is require filed",
      });
    }

    const limit = req.query.limit || -1;
    const page = req.query.page || 1;
    const search = req.query.search || "";
    let pagination = "";
    let whereClause = "";

    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (search) {
      whereClause += ` AND (l.lotto_name ilike '%${search}%' OR lt.lotto_type_name ilike '%${search}%')`;
    }

    let sql = `SELECT rl.lotto_id, rl.number_type_id, nt.number_type_name , rl.rate, rl.win_price, rl.is_default, 
    rl.order_seq, l.status, rl.created_date, rl.updated_date, 
    l.id as lotto_id, l.lotto_name,
    lt.id, lt.lotto_type_name, 
    s.status_name, s.status_name_th, lt.lotto_type_name
    FROM public.rate_limit rl
    inner join number_type nt on nt.id = rl.number_type_id
    inner join lotto l on l.id = rl.lotto_id
    inner join lotto_type lt on lt.id = l.lotto_type_id 
    inner join status s on s.id = rl.status
    where rl.is_default = 1 and lt.id = $1 and rl.status=1 and l.id IN (SELECT l.id as TOTAL_RECORD FROM lotto l
    inner join lotto_type lt on lt.id = l.lotto_type_id where lt.id=$1 order by l.id asc ${pagination}) ${whereClause}
    order by rl.lotto_id , nt.id asc , rl.win_price asc , rl.order_seq asc 
    `;

    if (["3", "4", "5"].includes(`${lotto_type}`)) {
      sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name , rl.rate, rl.win_price, rl.is_default, 
      rl.order_seq, l.status, rl.created_date, rl.updated_date, 
      l.id as lotto_id, l.lotto_name,
      lt.id, lt.lotto_type_name, 
      s.status_name, s.status_name_th, lt.lotto_type_name,
      case when mc.default_close_weekday <> '' then mc.default_close_weekday else mc.default_close_extra  end as close_dates
      from mapping_category mc 
      inner join lotto l on l.id = mc.lotto_id 
      inner join lotto_type lt  on lt.id = l.lotto_type_id 
      inner join rate_limit rl on rl.lotto_id = l.id 
      inner join number_type nt on nt.id = rl.number_type_id
      inner join status s on s.id = rl.status
      where rl.is_default = 1 and lt.id = $1 and 
      rl.status=1 and l.id IN (SELECT l.id as TOTAL_RECORD FROM lotto l
      inner join lotto_type lt on lt.id = l.lotto_type_id where lt.id=$1 order by l.id asc ${pagination}) ${whereClause}
      order by rl.lotto_id , nt.id asc , rl.win_price asc , rl.order_seq asc `;
    }
    const result = await db_access.query(sql, [lotto_type]);
    const sql_count = `
    SELECT COUNT(*) as TOTAL_RECORD FROM lotto l
    inner join lotto_type lt on lt.id = l.lotto_type_id where lt.id=$1 ${whereClause}`;
    const result_count = await db_access.query(sql_count, [lotto_type]);
    const count_data = result_count.rows;
    let count = 0;
    if (count_data.length > 0) {
      count = count_data[0].total_record;
    }

    const rows = result.rows;
    const group = {};
    const lotto_type_item = {};
    const reverseObj = await getHuayById();

    for await (const row of rows) {
      lotto_type_item["lotto_type_id"] = row.id;
      lotto_type_item["lotto_type_name"] = row.lotto_type_name;
      if (!group[`${row.lotto_id}`]) {
        group[`${row.lotto_id}`] = {
          lotto_id: row.lotto_id,
          lotto_name: row.lotto_name,
        };
      }
      group[`${row.lotto_id}`] = {
        ...group[`${row.lotto_id}`],
        [reverseObj[row.number_type_id]]: parseFloat(row.rate),
      };
      group[`${row.lotto_id}`].status = row.status == 1 ? true : false;
      group[`${row.lotto_id}`].close_dates = row.close_dates;
    }

    const data = Object.keys(group).map((item) => ({ ...group[item] }));
    res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        count: parseInt(count),
        ...lotto_type_item,
        lottos: data,
      },
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Get Config Huay Info
exports.get_config_huay_info = async (req, res) => {
  try {
    const lotto_id = req.params.id;
    if (!lotto_id) {
      res.status(400).send({
        code: 400,
        message: "lotto_id is require filed",
      });
    }

    const sql = `SELECT rl.lotto_id, rl.number_type_id, nt.number_type_name , rl.rate, rl.win_price, rl.is_default, 
    rl.order_seq, l.status, rl.created_date, rl.updated_date, 
    l.id as lotto_id, l.lotto_name, 
    lt.id, lt.lotto_type_name, 
    s.status_name, s.status_name_th, lt.lotto_type_name, mc.default_open_time , mc.default_close_time , mc.default_open_before,
    case when mc.default_close_weekday <> '' then mc.default_close_weekday else mc.default_close_extra  end as close_dates,
    l.minutes_per_round, l.round_per_day
    FROM public.rate_limit rl
    inner join number_type nt on nt.id = rl.number_type_id
    inner join lotto l on l.id = rl.lotto_id
    inner join lotto_type lt on lt.id = l.lotto_type_id 
    inner join mapping_category mc on mc.lotto_id = l.id 
    inner join status s on s.id = rl.status
    where rl.is_default = 1 and l.id = $1 and rl.status=1
    order by rl.lotto_id , nt.id asc , rl.win_price asc , rl.order_seq asc , mc.category_id asc
    `;
    const result = await db_access.query(sql, [lotto_id]);

    const rows = result.rows;
    const group = {};
    const lotto_type_item = {};
    const reverseObj = await getHuayById();

    for await (const row of rows) {
      lotto_type_item["lotto_type_id"] = row.id;
      lotto_type_item["lotto_type_name"] = row.lotto_type_name;
      if (!group[`${row.lotto_id}`]) {
        group[`${row.lotto_id}`] = {
          lotto_id: row.lotto_id,
          lotto_name: row.lotto_name,
          open_time: row.default_open_time,
          close_time: row.default_close_time,
          round_per_day: row.round_per_day,
          minutes_per_round: row.minutes_per_round,
          close_dates: row.close_dates,
          default_open_before: row.default_open_before,
        };
      }
      group[`${row.lotto_id}`] = {
        ...group[`${row.lotto_id}`],
        [reverseObj[row.number_type_id]]: parseFloat(row.rate),
      };
      group[`${row.lotto_id}`].status = row.status == 1 ? true : false;
    }

    const data = Object.keys(group).map((item) => ({ ...group[item] }));
    res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        ...lotto_type_item,
        lotto: data.length > 0 ? data[0] : {},
      },
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update Config Huay
exports.update_huay_config = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const body = req.body;
    const updateHuay = {
      [lotto.THAI]: updateHuayThai,
      [lotto.YIKI]: updateHauyYiki,
      [lotto.HUAYHUNG]: updateHauyHung,
      [lotto.HUAYCOUNTRY]: updateHuayCountry,
      [lotto.HUAYCHUD]: updateHuayChud,
    };
    return await updateHuay[lotto_type_id](res, lotto_type_id, body);
  } catch (error) {
    console.log("error : ", error);
    response_error_builder(res, error);
  }
};

const updateHuayChud = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = [
      "lotto_id",
      "lotto_name",
      "top_3_number",
      "th_3_number",
      "top_2_number",
      "bottom_2_number",
      "top_4_number",
      "th_4_number",
      "open_time",
      "close_time",
      "default_open_before",
      "status",
      "close_dates",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }

    const lotto_id = body.lotto_id;
    const open_time = body.open_time;
    const close_time = body.close_time;
    const close_dates = body.close_dates;
    const default_open_before = body.default_open_before;

    let sql = `SELECT rt.lotto_id, rt.number_type_id, nt.number_type_name, rt.rate
    FROM public.rate_limit rt
    inner join number_type nt on nt.id =rt.number_type_id 
    where rt.is_default=1 AND rt.lotto_id='${lotto_id}'
    ORDER BY number_type_id ASC`;
    const result = await db_access.query(sql);
    const rows = result.rows;

    const reverseObj = await getHuayById();

    sql = "";
    for await (const item of rows) {
      const key = reverseObj[item.number_type_id];
      sql += `UPDATE rate_limit SET rate=${body[key]} , updated_date=now()
      where is_default=1 AND status=1 AND lotto_id='${lotto_id}' and number_type_id='${item.number_type_id}';`;
    }
    await db_access.query(sql);

    sql = `SELECT mc.category_id, mc.lotto_id 
    from mapping_category mc
    inner join lotto l on l.id = mc.lotto_id
    WHERE l.id='${lotto_id}' AND l.status=1 order by mc.category_id asc`;
    const mc_result = await db_access.query(sql);
    const mc_data = mc_result.rows;
    sql = "";
    for await (const row of mc_data) {
      sql += `UPDATE mapping_category SET default_open_time='${open_time}' , default_open_before='${default_open_before}',
      default_close_time='${close_time}' , updated_date=now(), default_close_weekday='${close_dates}'
      WHERE category_id='${row.category_id}' AND lotto_id='${row.lotto_id}';`;
    }

    // sql = `UPDATE mapping_category SET default_open_time='${body.open_time}', default_close_time='${body.close_time}'
    // WHERE lotto_id='${lotto_id}'`;
    await db_access.query(sql);

    sql = `UPDATE lotto SET status=${body.status ? 1 : 2}, lotto_name='${
      body.lotto_name
    }' , updated_date=now() WHERE id='${lotto_id}'`;
    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    manage_error(error);
  }
};

const updateHuayCountry = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = [
      "lotto_id",
      "lotto_name",
      "top_3_number",
      "th_3_number",
      "top_2_number",
      "bottom_2_number",
      "top_1_number",
      "bottom_1_number",
      "default_open_before",
      "open_time",
      "close_time",
      "close_dates",
      "status",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    const close_dates = body.close_dates.toString();
    const lotto_id = body.lotto_id;
    const open_time = body.open_time;
    const close_time = body.close_time;
    const default_open_before = body.default_open_before;

    // if (lotto_id == 24) {
    //   if (!check_validator_basic(body, ["top_4_number"])) {
    //     return request_validator(res, body, ["top_4_number"]);
    //   }
    // }
    let sql = `SELECT rt.lotto_id, rt.number_type_id, nt.number_type_name, rt.rate
    FROM public.rate_limit rt
    inner join number_type nt on nt.id =rt.number_type_id 
    where rt.is_default=1 AND rt.lotto_id='${lotto_id}'
    ORDER BY number_type_id ASC`;
    const result = await db_access.query(sql);
    const rows = result.rows;

    const reverseObj = await getHuayById();

    sql = "";
    for await (const item of rows) {
      const key = reverseObj[item.number_type_id];
      if (!body[key]) {
        sql += `DELETE FROM rate_limit WHERE is_default=1 AND status=1 AND lotto_id='${lotto_id}' 
        and number_type_id='${item.number_type_id}';`;
      } else {
        sql += `UPDATE rate_limit SET rate=${
          body[key] || 0
        } , updated_date=now()
        where is_default=1 AND status=1 AND lotto_id='${lotto_id}' and number_type_id='${
          item.number_type_id
        }';`;
      }
    }
    await db_access.query(sql);

    sql = `SELECT mc.category_id, mc.lotto_id 
    from mapping_category mc
    inner join lotto l on l.id = mc.lotto_id
    WHERE l.id='${lotto_id}' AND l.status=1 order by mc.category_id asc`;
    const mc_result = await db_access.query(sql);
    const mc_data = mc_result.rows;
    sql = "";
    for await (const row of mc_data) {
      sql += `UPDATE mapping_category SET default_close_weekday='${close_dates}', default_open_time='${open_time}' ,
      default_open_before='${default_open_before}',
      default_close_time='${close_time}' , updated_date=now()
      WHERE category_id='${row.category_id}' AND lotto_id='${row.lotto_id}';`;
    }

    // sql = `UPDATE mapping_category SET default_open_time='${body.open_time}', default_close_time='${body.close_time}'
    // WHERE lotto_id='${lotto_id}'`;
    await db_access.query(sql);

    sql = `UPDATE lotto SET status=${body.status ? 1 : 2}, lotto_name='${
      body.lotto_name
    }' , updated_date=now() WHERE id='${lotto_id}'`;
    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    manage_error(error);
  }
};

const updateHauyHung = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = [
      "lotto_id",
      "lotto_name",
      "top_3_number",
      "th_3_number",
      "top_2_number",
      "bottom_2_number",
      "top_1_number",
      "bottom_1_number",
      "open_time",
      "close_time",
      "close_dates",
      "default_open_before",
      "status",
    ];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    const close_dates = body.close_dates.toString();
    const default_open_before = body.default_open_before;
    const lotto_id = body.lotto_id;
    const open_time = body.open_time;
    const close_time = body.close_time;
    let sql = `SELECT rt.lotto_id, rt.number_type_id, nt.number_type_name, rt.rate
    FROM public.rate_limit rt
    inner join number_type nt on nt.id =rt.number_type_id 
    where rt.is_default=1 AND rt.lotto_id='${lotto_id}'
    ORDER BY number_type_id ASC`;
    const result = await db_access.query(sql);
    const rows = result.rows;

    const reverseObj = await getHuayById();

    sql = "";
    for await (const item of rows) {
      const key = reverseObj[item.number_type_id];
      sql += `UPDATE rate_limit SET rate=${body[key]} , updated_date=now()
      where is_default=1 AND status=1 AND lotto_id='${lotto_id}' and number_type_id='${item.number_type_id}';`;
    }
    await db_access.query(sql);

    sql = `SELECT mc.category_id, mc.lotto_id 
    from mapping_category mc
    inner join lotto l on l.id = mc.lotto_id
    WHERE l.id='${lotto_id}' AND l.status=1 order by mc.category_id asc`;
    const mc_result = await db_access.query(sql);
    const mc_data = mc_result.rows;
    sql = "";
    for await (const row of mc_data) {
      let close_date_field = "default_close_extra";
      
      if ([HUAYHUNG_THAI_PID_YEAN].includes(row.lotto_id.toString())) {
        close_date_field = `default_close_weekday`;
      }
      
      sql += `UPDATE mapping_category SET ${close_date_field}='${close_dates}', default_open_time='${open_time}' ,
      default_open_before='${default_open_before}',
      default_close_time='${close_time}' , updated_date=now()
      WHERE category_id='${row.category_id}' AND lotto_id='${row.lotto_id}';`;
    }

    // sql = `UPDATE mapping_category SET default_open_time='${body.open_time}', default_close_time='${body.close_time}'
    // WHERE lotto_id='${lotto_id}'`;
    await db_access.query(sql);

    sql = `UPDATE lotto SET status=${body.status ? 1 : 2}, lotto_name='${
      body.lotto_name
    }' , updated_date=now() WHERE id='${lotto_id}'`;
    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    manage_error(error);
  }
};

const updateHauyYiki = async (res, lotto_type_id, body) => {
  try {
    const filedVaildator = [
      "lotto_id",
      "lotto_name",
      "top_3_number",
      "th_3_number",
      "top_2_number",
      "bottom_2_number",
      "top_1_number",
      "bottom_1_number",
      "first_close_time",
      "minutes_per_round",
      "round_per_day",
      "status",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return request_validator(res, body, filedVaildator);
    }

    let lotto_id = body.lotto_id;
    let sql = `SELECT rt.lotto_id, rt.number_type_id, nt.number_type_name, rt.rate
    FROM public.rate_limit rt
    inner join number_type nt on nt.id =rt.number_type_id 
    where rt.is_default=1 AND rt.lotto_id='${lotto_id}'
    ORDER BY number_type_id ASC`;
    const result = await db_access.query(sql);
    const rows = result.rows;

    const reverseObj = await getHuayById();

    sql = "";
    for await (const item of rows) {
      const key = reverseObj[item.number_type_id];
      sql += `UPDATE rate_limit SET rate=${body[key]} , updated_date=now()
      where is_default=1 AND status=1 AND lotto_id='${lotto_id}' and number_type_id='${item.number_type_id}';`;
    }
    await db_access.query(sql);

    sql = `SELECT mc.category_id, mc.lotto_id 
    from mapping_category mc
    inner join lotto l on l.id = mc.lotto_id
    WHERE l.id='${lotto_id}' AND l.status=1 order by mc.category_id asc`;
    const mc_result = await db_access.query(sql);
    const mc_data = mc_result.rows;

    sql = "";
    let close_time = body.first_close_time;
    for await (const row of mc_data) {
      sql += `UPDATE mapping_category SET default_close_time='${close_time}', updated_date=now() WHERE category_id='${row.category_id}' AND lotto_id='${row.lotto_id}';`;
      close_time = getNextTime(close_time, body.minutes_per_round);
    }

    // sql = `UPDATE mapping_category SET default_open_time='${body.open_time}', default_close_time='${body.close_time}'
    // WHERE lotto_id='${lotto_id}'`;
    await db_access.query(sql);

    sql = `UPDATE lotto SET status=${body.status ? 1 : 2}, lotto_name='${
      body.lotto_name
    }', minutes_per_round='${body.minutes_per_round}', round_per_day='${
      body.round_per_day
    }', updated_date=now() WHERE id='${lotto_id}'`;
    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    manage_error(error);
  }
};

const getNextTime = (old_time, plus_minute) => {
  const times = old_time.split(":");
  const h = parseInt(times[0]);
  const m = parseInt(times[1]);
  const new_m = m + parseInt(plus_minute);
  const plus_h = parseInt(new_m / 60);
  const mm = new_m % 60;
  const mm_format = mm < 10 ? `0${mm}` : mm;
  const hh =
    (h + plus_h) % 24 < 10 ? `0${(h + plus_h) % 24}` : (h + plus_h) % 24;
  return `${hh}:${mm_format}`;
};

const updateHuayThai = async (res, lotto_id, body) => {
  try {
    if (
      !(
        body.lotto_name &&
        body.top_3_number &&
        body.th_3_number &&
        body.front_3_number &&
        body.back_3_number &&
        body.top_2_number &&
        body.bottom_2_number &&
        body.top_1_number &&
        body.bottom_1_number &&
        body.open_time &&
        body.close_time &&
        body.close_dates &&
        body.default_open_before !== undefined &&
        body.default_open_before !== ""
      )
    ) {
      return { code: 400, message: "ข้อมูลไม่ถูกต้อง" };
    }
    let sql = `SELECT rt.lotto_id, rt.number_type_id, nt.number_type_name, rt.rate
    FROM public.rate_limit rt
    inner join number_type nt on nt.id =rt.number_type_id 
    where rt.is_default=1 AND rt.lotto_id='${lotto_id}'
    ORDER BY number_type_id ASC`;
    const result = await db_access.query(sql);
    const rows = result.rows;

    const reverseObj = await getHuayById();

    sql = "";
    for await (const item of rows) {
      const key = reverseObj[item.number_type_id];
      sql += `UPDATE rate_limit SET rate=${body[key]} , updated_date=now()
      where is_default=1 AND status=1 AND lotto_id='${lotto_id}' and number_type_id='${item.number_type_id}';`;
    }
    await db_access.query(sql);

    sql = `UPDATE mapping_category SET default_open_time='${body.open_time}', default_close_time='${body.close_time}',
    default_open_before='${body.default_open_before}',
    updated_date=now(), default_close_extra = '${body.close_dates}'
    WHERE lotto_id='${lotto_id}'`;
    await db_access.query(sql);

    sql = `UPDATE lotto SET status=${body.status ? 1 : 2}, lotto_name='${
      body.lotto_name
    }' , updated_date=now() WHERE id='${lotto_id}'`;
    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    console.error(`UPDATE HUAY THAI ERROR : `, error);
    throw error;
  }
};

exports.get_huay_week = async (req, res) => {
  try {
    const get_huay = {
      [lotto["THAI"]]: get_huay_week_thai,
      [lotto["YIKI"]]: get_huay_week_yiki,
      [lotto["HUAYHUNG"]]: get_huay_week_huay_hung,
      [lotto.HUAYCOUNTRY]: get_huay_week_huay_country,
      [lotto.HUAYCHUD]: get_huay_week_huay_chud,
    };

    return await get_huay[req.params.id](res, req);
  } catch (error) {
    response_error_builder(res, error);
  }
};

const get_huay_week_thai = async (res, req) => {
  try {
    const status = req.query.status;
    const date = req.query.date;
    const lotto_id = lotto["THAI"];
    const limit = req.query.limit || 10;
    const page = req.query.page || 1;
    let pagination = "";
    if (limit > 0) {
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }

    let whereClause = "";
    if (status) {
      const checkStatus = {
        [0]: "0",
        [1]: "1",
        [2]: "2",
      };
      if (!checkStatus[status]) {
        throw new ApplicationError({
          code: 400,
          message:
            "filed 'status' not valid. expected result[0,1] (0='รับแทง', 1='จ่ายเงินแล้ว', 2='ยกเลิก')",
        });
      }
      if (checkStatus[status] === "2") {
        whereClause += ` AND lc.is_cancel='1'`;
      } else {
        whereClause += ` AND lc.is_finished='${status}' AND lc.is_cancel='0'`;
      }
    }

    // Start Modified by Noppanut.Noo 26/09/2023
    // if (date) {
    //   const day = dayjs(date).format("YYYY-MM-DD");
    //   whereClause += ` AND TO_CHAR(days.days::DATE AT TIME ZONE 'UTC+7', 'yyyy-MM-dd') = '${day}'`;
    // }

    // const sql = `SELECT 
    // lc.date_id ,
    // mc.lotto_id , 
    // mc.category_id ,
    // mc.category_name , 
    // lc.six_top , 
    // lc.four_top ,
    // lc.three_top ,
    // lc.two_top ,
    // lc.two_under ,
    // lc.three_front ,
    // lc.three_back ,
    // lc.is_hold ,
    // lc.is_cancel ,
    // lc.is_finished , 
    // days.days,
    // concat(to_char((days.days::DATE - mc.default_open_before), 'dd/MM/yyyy'), '') as open_date  ,
    // mc.default_open_time as open_time,
    // concat(to_char(days.days::DATE, 'dd/MM/yyyy'),'') as close_date,
    // mc.default_close_time as close_time, 
    // lc.result_date
    // from mapping_category mc
    // inner join lotto_category lc on lc.category_id = mc.category_id
    // inner join (select mc2.lotto_id, unnest(string_to_array( mc2.default_close_extra, ',')) as days from mapping_category mc2
    // where mc2.default_close_extra is not null and mc2.default_close_extra <> '' and mc2.lotto_id='${lotto_id}') days 
    // on days.lotto_id = mc.lotto_id and lc.date_id = days.days
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // order by mc.lotto_id asc, days.days desc `;

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(days.days::DATE AT TIME ZONE 'UTC+7', 'yyyy-MM-dd') = '${day}'`;
    }

    const sql = `select b.days as date_id,
    b.lotto_id, 
    b.category_id,
    b.category_name, 
    lc.six_top, 
    lc.four_top,
    lc.three_top,
    lc.two_top,
    lc.two_under,
    lc.three_front,
    lc.three_back,
    lc.is_hold,
    lc.is_cancel,
    lc.is_finished, 
    b.days,
    b.open_date,
    b.default_open_time as open_time,
    b.close_date,
    b.default_close_time as close_time, 
    b.close_date as result_date
    from (
	    select concat(to_char((a.days::DATE - a.default_open_before), 'yyyy-MM-dd'), '') as open_date, concat(to_char(a.days::DATE, 'dd/MM/yyyy'),'') as close_date, * from (
		    select mc.lotto_id, mc.category_id, mc.category_name, default_open_before, default_open_time, default_close_time, unnest(string_to_array( mc.default_close_extra, ',')) as days
		    from mapping_category mc
		    where mc.default_close_extra is not null and mc.default_close_extra <> '' and mc.lotto_id='${lotto_id}'
	    ) as a
    ) as b
    left join lotto_category lc on lc.category_id = b.category_id and b.days = lc.date_id
    where b.days::date >= (current_date - '30 day'::interval) and b.open_date::date <= current_date ${whereClause}
    order by b.days desc`;
    // End Modified by Noppanut.Noo 26/09/2023

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );
    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = count_result.rows[0].total_record;
    }
    const rows = result.rows.map((item) => ({
      ...item,
      result_date: item.result_date
        ? item.result_date
        : `${dayjs(item.default_close_extra).format("DD/MM/YYYY")} 00:00:00`,
      status: {
        id: item.is_cancel ? 2 : item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    return response_success_builder(res, {
      rows: rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const get_huay_week_yiki = async (res, req) => {
  try {
    const filedVaildator = ["lotto_id"];
    if (!check_validator_basic(req.query, filedVaildator)) {
      return request_validator(res, req.query, filedVaildator);
    }
    const status = req.query.status;
    const date = req.query.date;
    const lotto_type_id = lotto["YIKI"];
    const lotto_id = req.query.lotto_id;
    const limit = req.query.limit || 10;
    const page = req.query.page || 1;
    let pagination = "";
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }

    let whereClause = "";
    let whereClause2 = "";
    if (status) {
      const checkStatus = {
        [0]: "0",
        [1]: "1",
        [2]: "2",
      };
      if (!checkStatus[status]) {
        throw new ApplicationError({
          code: 400,
          message:
            "filed 'status' not valid. expected result[0,1] (0='รับแทง', 1='จ่ายเงินแล้ว', 2='ยกเลิก')",
        });
      }
      if (checkStatus[status] === "2") {
        whereClause += ` AND lc.is_cancel='1'`;
        whereClause2 += ` AND lc2.is_cancel='1'`;
      } else {
        whereClause += ` AND lc.is_finished='${status}' AND lc.is_cancel='0'`;
        whereClause2 += ` AND lc2.is_finished='${status}' AND lc2.is_cancel='0'`;
      }
    }

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(lc.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
      whereClause2 += ` AND TO_CHAR(lc2.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    }

    // let sql = `
    // WITH rs_mapping_category AS (
    //   select lotto_id, category_id, category_name, default_open_time ,
    //   default_close_time, default_result_time, default_close_weekday
    //   from mapping_category where lotto_id = '${lotto_id}'
    // ), rs_lotto_category as (
    // 	select * from lotto_category lc
    // 	where category_id in (select category_id from rs_mapping_category)
    //   and date_id::date > '${dayjs().format(
    //     "YYYY-MM-DD"
    //   )}'::date - interval '7 days'
    //    ${whereClause}
    // )
    // select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_time::time <= mc.default_open_time::time 
    //   then lc.date_id::date+ interval '1 days'
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR(lc.result_date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.three_top , lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from rs_mapping_category mc 
    // inner join rs_lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
    //       when mc.default_close_time::time <= mc.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
    //       else lc.date_id::date + mc.default_close_time::time
    //     end) or mc.category_id = (
    //     select mc2.category_id from rs_mapping_category mc2 
    //     inner join rs_lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     where mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
    //       when mc2.default_close_time::time <= mc2.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
    //       else lc.date_id::date + mc2.default_close_time::time
    //     end) order by mc2.category_id asc limit 1
    //     )
    // order by lc.date_id desc, mc.category_id desc `;

    let sql = `
    WITH rs_mapping_category AS (
      select lotto_id, category_id, category_name, default_open_time ,
      default_close_time, default_result_time, default_close_weekday
      from mapping_category where lotto_id = '${lotto_id}'
    ), rs_lotto_category as (
    	select * from lotto_category lc
    	where category_id in (select category_id from rs_mapping_category)
      and date_id::date > '${dayjs().format(
        "YYYY-MM-DD"
      )}'::date - interval '7 days'
       ${whereClause}
    )
    select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as result_date,
    mc.default_result_time ,
    lc.three_top , lc.two_under , 
    lc.is_finished , lc.is_cancel , lc.is_hold ,
    mc.default_close_time as first_close_time,
    l.minutes_per_round,
    l.round_per_day
    from rs_mapping_category mc 
    inner join rs_lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    where mc.lotto_id = '${lotto_id}' ${whereClause}
    and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time 
    -- and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= case 
    --  when mc.default_close_time::time <= mc.default_open_time::time 
    --  then lc.date_id::date+ interval '1 days'
    --  else lc.date_id::date
    -- end::date + mc.default_close_time::time
    order by lc.date_id desc, mc.category_id desc `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );
    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = count_result.rows[0].total_record;
    }

    const rows = result.rows.map((item) => ({
      ...item,
      result_date: item.result_date
        ? item.result_date
        : `${dayjs(item.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: item.is_cancel ? 2 : item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    return response_success_builder(res, {
      rows: rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    throw manage_error(error);
  }
};

const get_huay_week_huay_hung = async (res, req) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(req.query, filedValidator)) {
      return request_validator(res, req.query, filedValidator);
    }
    console.log("dddß")
    const status = req.query.status;
    const date = req.query.date;
    const lotto_type_id = lotto["HUAYHUNG"];
    const lotto_id = req.query.lotto_id;
    const limit = req.query.limit || 10;
    const page = req.query.page || 1;
    let pagination = "";
    let whereClauseCloseWeekly = '';
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }

    let whereClause = "";
    let whereClause2 = "";
    if (status) {
      const checkStatus = {
        [0]: "0",
        [1]: "1",
        [2]: "2",
      };
      if (!checkStatus[status]) {
        throw new ApplicationError({
          code: 400,
          message:
            "filed 'status' not valid. expected result[0,1] (0='รับแทง', 1='จ่ายเงินแล้ว', 2='ยกเลิก')",
        });
      }
      if (checkStatus[status] === "2") {
        whereClause += ` AND lc.is_cancel='1'`;
        whereClause2 += ` AND lc2.is_cancel='1'`;
      } else {
        whereClause += ` AND lc.is_finished='${status}' AND lc.is_cancel='0'`;
        whereClause2 += ` AND lc2.is_finished='${status}' AND lc2.is_cancel='0'`;
      }
    }

    // Start Modified by Noppanut.Noo 28/09/2023
    // if(lotto_id.includes('21')){
    //   whereClauseCloseWeekly += `and (mc.default_close_weekday <> '' and extract('DOW' from lc.date_id::date)::text = any(string_to_array(mc.default_close_weekday, ',')))`
    // }else{
    //   whereClauseCloseWeekly += `and (mc.default_close_extra <> '' and lc.date_id = any(string_to_array(mc.default_close_extra, ',')))`
    // }

    // if (date) {
    //   const day = dayjs(date).format("YYYY-MM-DD");
    //   whereClause += ` AND TO_CHAR(lc.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    //   whereClause2 += ` AND TO_CHAR(lc2.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    // }

    // let sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_time::time <= mc.default_open_time::time 
    //   then lc.date_id::date+ interval '1 days'
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR(lc.result_date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.three_top , lc.two_under , lc.two_top ,
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // ${whereClauseCloseWeekly}
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
    //       when mc.default_close_time::time <= mc.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
    //       else lc.date_id::date + mc.default_close_time::time
    //     end) or mc.category_id = (
    //     select mc2.category_id from mapping_category mc2 
    //     inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     where mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
    //       when mc2.default_close_time::time <= mc2.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
    //       else lc.date_id::date + mc2.default_close_time::time
    //     end) order by mc2.category_id asc limit 1
    //     )
    // order by lc.date_id desc, mc.category_id desc `;

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(lc.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
      whereClause2 += ` AND TO_CHAR(lc2.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    }

    let sql;
    if (lotto_id.includes('21')) {
      whereClauseCloseWeekly += `and (mc.default_close_weekday <> '' and extract('DOW' from lc.date_id::date)::text = any(string_to_array(mc.default_close_weekday, ',')))`
      sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
      TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
      mc.default_open_time as open_time , 
      TO_CHAR((case 
        when mc.default_close_time::time <= mc.default_open_time::time 
        then lc.date_id::date+ interval '1 days'
        else lc.date_id::date
      end)::date, 'dd/MM/yyyy') as close_date,
      mc.default_close_time as close_time ,
      TO_CHAR((case 
        when mc.default_close_time::time <= mc.default_open_time::time 
        then lc.date_id::date+ interval '1 days'
        else lc.date_id::date
      end)::date, 'dd/MM/yyyy') as result_date,
      mc.default_result_time ,
      lc.three_top , lc.two_under , lc.two_top ,
      lc.is_finished , lc.is_cancel , lc.is_hold ,
      mc.default_close_time as first_close_time,
      l.minutes_per_round,
      l.round_per_day
      from mapping_category mc 
      inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
      inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
      where mc.lotto_id = '${lotto_id}' ${whereClause}
      ${whereClauseCloseWeekly}
      and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time
      order by lc.date_id desc, mc.category_id desc `;
    } else {
      sql = `select b.days as date_id,b.category_id, b.lotto_id, b.category_name, TO_CHAR(b.open_date::date, 'dd/MM/yyyy') as open_date, b.default_open_time as open_time, 
      b.close_date, b.default_close_time as close_time, b.close_date as result_date, b.default_result_time, lc.three_top, lc.two_under, 
      lc.two_top, lc.is_finished, lc.is_cancel, lc.is_hold, b.default_close_time as first_close_time, 0 as minutes_per_round, 0 as round_per_day
      from (
        select concat(to_char((a.days::DATE - a.default_open_before), 'yyyy-MM-dd'), '') as open_date, concat(to_char(a.days::DATE, 'dd/MM/yyyy'),'') as close_date, * from (
          select mc.lotto_id, mc.category_id, mc.category_name, default_open_before, default_open_time, default_close_time, default_result_time, unnest(string_to_array( mc.default_close_extra, ',')) as days
          from mapping_category mc
          where mc.default_close_extra is not null and mc.default_close_extra <> '' and mc.lotto_id='${lotto_id}'
        ) as a
      ) as b
      left join lotto_category lc on lc.category_id = b.category_id and b.days = lc.date_id
      where b.days::date >= (current_date - '30 day'::interval) and b.open_date::date <= current_date ${whereClause}
      order by b.days desc`;
    }
    // End Modified by Noppanut.Noo 28/09/2023
    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );
    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = count_result.rows[0].total_record;
    }
    const rows = result.rows.map((item) => ({
      ...item,
      result_date: item.result_date
        ? item.result_date
        : `${dayjs(item.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: item.is_cancel ? 2 : item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    return response_success_builder(res, {
      rows: rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const get_huay_week_huay_country = async (res, req) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(req.query, filedValidator)) {
      return request_validator(res, req.query, filedValidator);
    }
    const status = req.query.status;
    const date = req.query.date;
    const lotto_type_id = lotto.HUAYCOUNTRY;
    const lotto_id = req.query.lotto_id;
    const limit = req.query.limit || 10;
    const page = req.query.page || 1;
    let pagination = "";
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }

    let whereClause = "";
    let whereClause2 = "";
    if (status) {
      const checkStatus = {
        [0]: "0",
        [1]: "1",
        [2]: "2",
      };
      if (!checkStatus[status]) {
        throw new ApplicationError({
          code: 400,
          message:
            "filed 'status' not valid. expected result[0,1] (0='รับแทง', 1='จ่ายเงินแล้ว', 2='ยกเลิก')",
        });
      }
      if (checkStatus[status] === "2") {
        whereClause += ` AND lc.is_cancel='1'`;
        whereClause2 += ` AND lc2.is_cancel='1'`;
      } else {
        whereClause += ` AND lc.is_finished='${status}' AND lc.is_cancel='0'`;
        whereClause2 += ` AND lc2.is_finished='${status}' AND lc2.is_cancel='0'`;
      }
    }

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(lc.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
      whereClause2 += ` AND TO_CHAR(lc2.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    }

    // let sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_time::time <= mc.default_open_time::time 
    //   then lc.date_id::date+ interval '1 days'
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR(lc.result_date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.four_top,
    // lc.three_top , lc.two_top, lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and (mc.default_close_weekday <> '' and extract('DOW' from lc.date_id::date)::text = any(string_to_array(mc.default_close_weekday, ',')))
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
    //       when mc.default_close_time::time <= mc.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
    //       else lc.date_id::date + mc.default_close_time::time
    //     end) or mc.category_id = (
    //     select mc2.category_id from mapping_category mc2 
    //     inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     where mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
    //       when mc2.default_close_time::time <= mc2.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
    //       else lc.date_id::date + mc2.default_close_time::time
    //     end) order by mc2.category_id asc limit 1
    //     ) 
    // order by lc.date_id desc, mc.category_id desc `;

    // let sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.four_top,
    // lc.three_top , lc.two_top, lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and (mc.default_close_weekday <> '' and extract('ISODOW' from lc.date_id::date)::int = any(array(select case when (t.val::int - mc.default_open_before) = 0 then 7 else (t.val::int - mc.default_open_before) end from unnest(string_to_array(mc.default_close_weekday, ',')) as t(val))))
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time
    // order by lc.date_id desc, mc.category_id desc `;

    // let sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR((case 
    //   when mc.default_open_before > 0
    //   then lc.date_id::date - mc.default_open_before
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.four_top,
    // lc.three_top , lc.two_top, lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and (mc.default_close_weekday <> '' and extract('ISODOW' from lc.date_id::date)::text = any(string_to_array(mc.default_close_weekday, ',')))
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time
    // order by lc.date_id desc, mc.category_id desc `;

    // let sql = `select lc.date_id, mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR((case 
    //   when mc.default_open_before > 0
    //   then lc.date_id::date - mc.default_open_before
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR((case 
    //   when mc.default_close_after > 0
    //   then lc.date_id::date + mc.default_close_after
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.four_top,
    // lc.three_top , lc.two_top, lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and (mc.default_close_weekday <> '' and extract('ISODOW' from (lc.date_id::date - mc.default_open_before)::date)::int = any(array(select case when (t.val::int - mc.default_open_before) = 0 then 7 else (t.val::int - mc.default_open_before) end from unnest(string_to_array(mc.default_close_weekday, ',')) as t(val))))
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time
    // order by lc.date_id desc, mc.category_id desc `;

    let sql = `select a.*, 
    lc2.four_top,
    lc2.three_top , lc2.two_top, lc2.two_under , 
    lc2.is_finished , lc2.is_cancel , lc2.is_hold from (
      select TO_CHAR((case 
      when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      else lc.date_id::date
    end)::date, 'yyyy-MM-dd') as date_id, mc.category_id, mc.lotto_id, mc.category_name , 
    TO_CHAR((lc.date_id::date)::date, 'dd/MM/yyyy') as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
	  when mc.default_open_before > 0 and mc.default_close_after > 0
	  then lc.date_id::date + mc.default_open_before + mc.default_close_after
	  when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      when mc.default_close_after > 0
      then lc.date_id::date + mc.default_close_after
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    TO_CHAR((case 
	  when mc.default_open_before > 0 and mc.default_close_after > 0
	  then lc.date_id::date + mc.default_open_before + mc.default_close_after
	  when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      when mc.default_close_after > 0
      then lc.date_id::date + mc.default_close_after
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as result_date,
    mc.default_result_time ,
    mc.default_close_time as first_close_time,
    l.minutes_per_round,
    l.round_per_day
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    where mc.lotto_id = '${lotto_id}' ${whereClause}
    and (mc.default_close_weekday <> '' and extract('ISODOW' from lc.date_id::date)::int = any(array(select case when (t.val::int - mc.default_open_before) = 0 then 7 else (t.val::int - mc.default_open_before) end from unnest(string_to_array(mc.default_close_weekday, ',')) as t(val))))
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time) as a
    left join lotto_category lc2 on lc2.category_id = a.category_id and lc2.date_id = a.date_id
    order by lc2.date_id desc, a.category_id desc `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );
    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = count_result.rows[0].total_record;
    }
    const rows = result.rows.map((item) => ({
      ...item,
      // result_date: item.result_date
      //   ? dayjs(item.result_date).format("DD/MM/YYYY")
      //   : `${dayjs(item.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: item.is_cancel ? 2 : item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    return response_success_builder(res, {
      rows: rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const get_huay_week_huay_chud = async (res, req) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(req.query, filedValidator)) {
      return request_validator(res, req.query, filedValidator);
    }
    const status = req.query.status;
    const date = req.query.date;
    const lotto_type_id = lotto.HUAYCHUD;
    const lotto_id = req.query.lotto_id;
    const limit = req.query.limit || 10;
    const page = req.query.page || 1;
    let pagination = "";
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }

    let whereClause = "";
    let whereClause2 = "";
    if (status) {
      const checkStatus = {
        [0]: "0",
        [1]: "1",
        [2]: "2",
      };
      if (!checkStatus[status]) {
        throw new ApplicationError({
          code: 400,
          message:
            "filed 'status' not valid. expected result[0,1] (0='รับแทง', 1='จ่ายเงินแล้ว', 2='ยกเลิก')",
        });
      }
      if (checkStatus[status] === "2") {
        whereClause += ` AND lc.is_cancel='1'`;
        whereClause2 += ` AND lc2.is_cancel='1'`;
      } else {
        whereClause += ` AND lc.is_finished='${status}' AND lc.is_cancel='0'`;
        whereClause2 += ` AND lc2.is_finished='${status}' AND lc2.is_cancel='0'`;
      }
    }

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(lc.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
      whereClause2 += ` AND TO_CHAR(lc2.date_id::DATE, 'yyyy-MM-dd') = '${day}'`;
    }

    // let sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    // TO_CHAR(lc.date_id::date, 'dd/MM/yyyy') as open_date,
    // mc.default_open_time as open_time , 
    // TO_CHAR((case 
    //   when mc.default_close_time::time <= mc.default_open_time::time 
    //   then lc.date_id::date+ interval '1 days'
    //   else lc.date_id::date
    // end)::date, 'dd/MM/yyyy') as close_date,
    // mc.default_close_time as close_time ,
    // TO_CHAR(lc.result_date, 'dd/MM/yyyy') as result_date,
    // mc.default_result_time ,
    // lc.four_top,
    // lc.three_top , lc.two_top ,lc.two_under , 
    // lc.is_finished , lc.is_cancel , lc.is_hold ,
    // mc.default_close_time as first_close_time,
    // l.minutes_per_round,
    // l.round_per_day
    // from mapping_category mc 
    // inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    // inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    // where mc.lotto_id = '${lotto_id}' ${whereClause}
    // and extract('DOW' from date_id::date)::text = any(string_to_array(default_close_weekday, ','))
    // and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
    //       when mc.default_close_time::time <= mc.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
    //       else lc.date_id::date + mc.default_close_time::time
    //     end) or mc.category_id = (
    //     select mc2.category_id from mapping_category mc2 
    //     inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     where mc2.lotto_id = '${lotto_id}' ${whereClause2}
    //     and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
    //       when mc2.default_close_time::time <= mc2.default_open_time::time 
    //       then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
    //       else lc.date_id::date + mc2.default_close_time::time
    //     end) order by mc2.category_id asc limit 1
    //     )
    // order by lc.date_id desc, mc.category_id desc `;

    let sql = `select a.*, 
    lc2.four_top,
    lc2.three_top , lc2.two_top, lc2.two_under , 
    lc2.is_finished , lc2.is_cancel , lc2.is_hold from (
      select TO_CHAR((case 
      when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      else lc.date_id::date
    end)::date, 'yyyy-MM-dd') as date_id, mc.category_id, mc.lotto_id, mc.category_name , 
    TO_CHAR((lc.date_id::date)::date, 'dd/MM/yyyy') as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
	  when mc.default_open_before > 0 and mc.default_close_after > 0
	  then lc.date_id::date + mc.default_open_before + mc.default_close_after
	  when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      when mc.default_close_after > 0
      then lc.date_id::date + mc.default_close_after
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    TO_CHAR((case 
	  when mc.default_open_before > 0 and mc.default_close_after > 0
	  then lc.date_id::date + mc.default_open_before + mc.default_close_after
	  when mc.default_open_before > 0
      then lc.date_id::date + mc.default_open_before
      when mc.default_close_after > 0
      then lc.date_id::date + mc.default_close_after
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as result_date,
    mc.default_result_time ,
    mc.default_close_time as first_close_time,
    l.minutes_per_round,
    l.round_per_day
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}' ${whereClause}
    inner join lotto l on l.id = mc.lotto_id and l.id='${lotto_id}'
    where mc.lotto_id = '${lotto_id}' ${whereClause}
    and (mc.default_close_weekday <> '' and extract('ISODOW' from lc.date_id::date)::int = any(array(select case when (t.val::int - mc.default_open_before) = 0 then 7 else (t.val::int - mc.default_open_before) end from unnest(string_to_array(mc.default_close_weekday, ',')) as t(val))))
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= lc.date_id::date + mc.default_open_time::time) as a
    left join lotto_category lc2 on lc2.category_id = a.category_id and lc2.date_id = a.date_id
    order by lc2.date_id desc, a.category_id desc `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );
    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = count_result.rows[0].total_record;
    }
    const rows = result.rows.map((item) => ({
      ...item,
      // result_date: item.result_date
      //   ? dayjs(item.result_date).format("DD/MM/YYYY")
      //   : `${dayjs(item.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: item.is_cancel ? 2 : item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    return response_success_builder(res, {
      rows: rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.get_huay_week_info = async (req, res) => {
  try {
    const get_info = {
      [lotto["THAI"]]: get_huay_week_thai_info,
      [lotto["YIKI"]]: get_huay_week_yiki_info,
      [lotto["HUAYHUNG"]]: get_huay_week_huay_hung_info,
      [lotto.HUAYCOUNTRY]: get_huay_week_huay_country_info,
      [lotto.HUAYCHUD]: get_huay_week_huay_chud_info,
    };

    return await get_info[req.params.id](req, res);
  } catch (error) {
    return await response_error_builder(res, error);
  }
};

const get_huay_week_thai_info = async (req, res) => {
  try {
    const date = req.query.date || dayjs().format("YYYY-MM-DD");
    const lotto_id = req.params.id;
    let whereClause = "";

    if (date) {
      const day = dayjs(date).format("YYYY-MM-DD");
      whereClause += ` AND TO_CHAR(days.days::DATE, 'yyyy-MM-dd') = '${day}'`;
    }

    const sql = `SELECT 
    lc.date_id ,
    mc.lotto_id , 
    mc.category_id ,
    mc.category_name , 
    lc.six_top , 
    lc.four_top ,
    lc.three_top ,
    lc.two_top ,
    lc.two_under ,
    lc.three_front ,
    lc.three_back ,
    lc.is_hold ,
    lc.is_cancel ,
    lc.is_finished , 
    days.days,
    concat(to_char((days.days::DATE - mc.default_open_before), 'dd/MM/yyyy'), '') as open_date  ,
    mc.default_open_time as open_time,
    concat(to_char(days.days::DATE, 'dd/MM/yyyy'),'') as close_date,
    mc.default_close_time as close_time, 
    lc.result_date
    from mapping_category mc
    inner join lotto_category lc on lc.category_id = mc.category_id
    inner join (select mc2.lotto_id, unnest(string_to_array( mc2.default_close_extra, ',')) as days from mapping_category mc2
    where mc2.default_close_extra is not null and mc2.default_close_extra <> '') days 
    on days.lotto_id = mc.lotto_id and lc.date_id = days.days
    where mc.lotto_id = '${lotto_id}' ${whereClause}
    order by mc.lotto_id asc, days.days desc `;
    const result = await db_access.query(sql);
    const rows = result.rows.map((item) => ({
      ...item,
      result_date: item.result_date
        ? item.result_date
        : `${dayjs(item.default_close_extra).format("DD/MM/YYYY")} 00:00:00`,
      status: {
        id: item.is_finished,
        value: item.is_cancel
          ? "ยกเลิก"
          : item.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    }));

    let row = {};
    if (rows.length > 0) {
      row = rows[0];
    }
    return response_success_builder(res, { ...row });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const get_huay_week_yiki_info = async (req, res) => {
  try {
    const query = req.query;
    const filedValidator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }
    const date = query.date;
    const lotto_id = query.lotto_id;
    const lotto_type_id = query.lotto_type_id;
    const category_id = query.category_id;

    const sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    lc.date_id as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    lc.three_top , lc.two_under , 
    lc.is_finished , lc.is_cancel , lc.is_hold ,
    lc.result_date , mc.default_result_time 
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}'
    and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    where mc.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
          when mc.default_close_time::time <= mc.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
          else lc.date_id::date + mc.default_close_time::time
        end) or mc.category_id = (
        select mc2.category_id from mapping_category mc2 
        inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}'
        and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        where mc2.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
          when mc2.default_close_time::time <= mc2.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
          else lc.date_id::date + mc2.default_close_time::time
        end) order by mc2.category_id asc limit 1
        )
    order by lc.date_id desc, mc.category_id desc `;

    const result = await db_access.query(sql);
    let row = {};
    if (result.rowCount > 0) {
      row = result.rows[0];
      const sumSql = `select sn.lotto_id, sn.category_id, sum(number::int8) from public.shootnumber sn
      left join public.members m on sn.member_id=m.id and sn.is_bot=0
      left join public.bot_members bm on sn.member_id=bm.id and sn.is_bot=1
      inner join public.mapping_category mc on sn.category_id=mc.category_id
      where sn.date_id=TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and sn.lotto_id='${lotto_id}'
      and sn.category_id='${category_id}'
      group by sn.lotto_id, sn.category_id`;

      const sqlDetailNumber = `select sn.lotto_id, sn.category_id, category_name, case when sn.is_bot=0 then CONCAT(LEFT(m.username,2), 'xxxx', RIGHT(m.username,2)) else bm.username end as username, sn.number, sn.created_date from public.shootnumber sn
      left join public.members m on sn.member_id=m.id and sn.is_bot=0
      left join public.bot_members bm on sn.member_id=bm.id and sn.is_bot=1
      inner join public.mapping_category mc on sn.category_id=mc.category_id
      where sn.date_id=TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and sn.lotto_id='${lotto_id}' and sn.category_id='${category_id}' and sn.created_date < concat('${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DD")}', ' ', mc.default_result_time)::timestamp
      order by sn.created_date desc, sn.is_bot desc;`;

      let number_result = "";
      let number_16th = "";
      let number_sum = "";
      let query_result_sum = await db_access.query(sumSql);
      if (query_result_sum.rowCount > 0) {
        number_result = query_result_sum.rows[0].sum;
        const query_result = await db_access.query(sqlDetailNumber);
        if (query_result.rowCount > 0) {
          number_16th = query_result.rows[15].number;
          number_sum = number_result - number_16th;
        }
      }
      row.number_result = number_result;
      row.number_16th = number_16th;
      row.number_sum = number_sum;
    }

    return response_success_builder(res, {
      ...row,
      result_date: row.result_date
        ? dayjs(row.result_date).format("DD/MM/YYYY")
        : `${dayjs(row.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: row.is_cancel ? 2 : row.is_finished,
        value: row.is_cancel
          ? "ยกเลิก"
          : row.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    });
  } catch (error) {
    console.error(error);
    throw manage_error(error);
  }
};

const get_huay_week_huay_hung_info = async (req, res) => {
  try {
    const query = req.query;
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return request_validator(res, query, filedVaildator);
    }
    const date = query.date || "";
    const lotto_id = query.lotto_id;
    const lotto_type_id = query.lotto_type_id;
    const category_id = query.category_id;

    const sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    lc.date_id as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    lc.three_top , lc.two_under , lc.two_top,
    lc.is_finished , lc.is_cancel , lc.is_hold ,
    lc.result_date , mc.default_result_time 
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}'
    and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    where mc.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
          when mc.default_close_time::time <= mc.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
          else lc.date_id::date + mc.default_close_time::time
        end) or mc.category_id = (
        select mc2.category_id from mapping_category mc2 
        inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}'
        and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        where mc2.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
          when mc2.default_close_time::time <= mc2.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
          else lc.date_id::date + mc2.default_close_time::time
        end) order by mc2.category_id asc limit 1
        )
    order by lc.date_id desc, mc.category_id desc `;

    const result = await db_access.query(sql);
    let row = {};
    if (result.rowCount > 0) {
      row = result.rows[0];
    }

    return response_success_builder(res, {
      ...row,
      result_date: row.result_date
        ? dayjs(row.result_date).format("DD/MM/YYYY")
        : `${dayjs(row.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: row.is_cancel ? 2 : row.is_finished,
        value: row.is_cancel
          ? "ยกเลิก"
          : row.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const get_huay_week_huay_country_info = async (req, res) => {
  try {
    const query = req.query;
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return request_validator(res, query, filedVaildator);
    }
    const date = query.date || "";
    const lotto_id = query.lotto_id;
    const lotto_type_id = query.lotto_type_id;
    const category_id = query.category_id;

    const sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    lc.date_id as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    lc.four_top,
    lc.three_top , lc.two_top, lc.two_under , 
    lc.is_finished , lc.is_cancel , lc.is_hold ,
    lc.result_date , mc.default_result_time 
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}'
    and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    where mc.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
          when mc.default_close_time::time <= mc.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
          else lc.date_id::date + mc.default_close_time::time
        end) or mc.category_id = (
        select mc2.category_id from mapping_category mc2 
        inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}'
        and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        where mc2.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
          when mc2.default_close_time::time <= mc2.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
          else lc.date_id::date + mc2.default_close_time::time
        end) order by mc2.category_id asc limit 1
        )
    order by lc.date_id desc, mc.category_id desc `;

    const result = await db_access.query(sql);
    let row = {};
    if (result.rowCount > 0) {
      row = result.rows[0];
    }

    return response_success_builder(res, {
      ...row,
      result_date: row.result_date
        ? dayjs(row.result_date).format("DD/MM/YYYY")
        : `${dayjs(row.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: row.is_cancel ? 2 : row.is_finished,
        value: row.is_cancel
          ? "ยกเลิก"
          : row.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const get_huay_week_huay_chud_info = async (req, res) => {
  try {
    const query = req.query;
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return request_validator(res, query, filedVaildator);
    }
    const date = query.date || "";
    const lotto_id = query.lotto_id;
    const lotto_type_id = query.lotto_type_id;
    const category_id = query.category_id;

    const sql = `select lc.date_id ,mc.category_id, mc.lotto_id, mc.category_name , 
    lc.date_id as open_date,
    mc.default_open_time as open_time , 
    TO_CHAR((case 
      when mc.default_close_time::time <= mc.default_open_time::time 
      then lc.date_id::date+ interval '1 days'
      else lc.date_id::date
    end)::date, 'dd/MM/yyyy') as close_date,
    mc.default_close_time as close_time ,
    lc.four_top,
    lc.three_top , lc.two_top ,lc.two_under , 
    lc.is_finished , lc.is_cancel , lc.is_hold ,
    lc.result_date , mc.default_result_time 
    from mapping_category mc 
    inner join lotto_category lc on lc.category_id = mc.category_id and mc.lotto_id = '${lotto_id}'
    and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    where mc.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
    and  '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' >= (case 
          when mc.default_close_time::time <= mc.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc.default_close_time::time
          else lc.date_id::date + mc.default_close_time::time
        end) or mc.category_id = (
        select mc2.category_id from mapping_category mc2 
        inner join lotto_category lc2 on lc2.category_id = mc2.category_id and mc2.lotto_id = '${lotto_id}'
        and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        where mc2.lotto_id = '${lotto_id}' and lc.date_id = TO_CHAR('${date}'::DATE, 'yyyy-MM-dd') and lc.category_id='${category_id}'
        and '${dayjs().tz("Asia/Bangkok").format("YYYY-MM-DDTHH:mm")}' < (case 
          when mc2.default_close_time::time <= mc2.default_open_time::time 
          then lc.date_id::date+ interval '1 days' + mc2.default_close_time::time
          else lc.date_id::date + mc2.default_close_time::time
        end) order by mc2.category_id asc limit 1
        )
    order by lc.date_id desc, mc.category_id desc `;

    const result = await db_access.query(sql);
    let row = {};
    if (result.rowCount > 0) {
      row = result.rows[0];
    }

    return response_success_builder(res, {
      ...row,
      result_date: row.result_date
        ? dayjs(row.result_date).format("DD/MM/YYYY")
        : `${dayjs(row.date_id).format("DD/MM/YYYY")}`,
      status: {
        id: row.is_cancel ? 2 : row.is_finished,
        value: row.is_cancel
          ? "ยกเลิก"
          : row.is_finished
          ? "จ่ายเงินแล้ว"
          : "รับแทง",
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _updateHuayWeekThai = async (req, res) => {
  try {
    const lotto_id = req.params.id;
    const body = req.body;
    const filedVaildator = [
      "date",
      "six_top",
      "two_under",
      "three_front_1",
      "three_front_2",
      "three_back_1",
      "three_back_2",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const date_id = body.date;
    let sql = `SELECT category_id FROM mapping_category WHERE lotto_id='${lotto_id}' `;

    const status = body.status;
    let updateStatus = "";
    if (status !== undefined && status !== "" && status !== null) {
      if (status === 0 || status === 1) {
        updateStatus += ` , is_finished='${status}', is_cancel=0`;
      }
      if (status === 2) {
        updateStatus += `, is_cancel='1', is_finished=0`;
      }
    }
    const result = await db_access.query(sql);
    if (result.rowCount > 0) {
      const category_id = result.rows[0].category_id;
      sql = `UPDATE lotto_category SET six_top='${body.six_top}', 
      three_top='${body.six_top.slice(3, 6)}',
      two_top='${body.six_top.slice(4, 6)}',
      two_under='${body.two_under}', 
      three_front='${body.three_front_1},${body.three_front_2}' ,
      three_back='${body.three_back_1},${body.three_back_2}',
      updated_date=now() ${updateStatus}
      WHERE date_id = '${date_id}' AND category_id='${category_id}';`;
      await db_access.query(sql);
    }
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

const _updateHuayWeekYiki = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const body = req.body;
    const filedVaildator = ["date", "two_under", "three_top", "category_id"];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const date_id = body.date;
    const two_under = body.two_under;
    const three_top = body.three_top;
    const category_id = body.category_id;
    const status = body.status;
    let updateStatus = "";
    if (status !== undefined && status !== "" && status !== null) {
      if (status === 0 || status === 1) {
        updateStatus += ` , is_finished='${status}' , is_cancel=0`;
      }
      if (status === 2) {
        updateStatus += `, is_cancel='1', is_finished=0`;
      }
    }

    const sql = `UPDATE lotto_category SET 
      two_under='${two_under}', 
      three_top='${three_top}',
      updated_date=now() ${updateStatus}
      WHERE date_id = '${date_id}' AND category_id='${category_id}';`;
    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

const _updateHuayWeekHuayHung = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const body = req.body;
    const filedVaildator = ["date", "two_under", "three_top", "category_id"];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }
    const date_id = body.date;
    const two_under = body.two_under;
    const two_top = body.two_top;
    const three_top = body.three_top;
    const category_id = body.category_id;
    const status = body.status;
    let updateStatus = "";
    if (status !== undefined && status !== "" && status !== null) {
      if (status === 0 || status === 1) {
        updateStatus += ` , is_finished='${status}' , is_cancel=0`;
      }
      if (status === 2) {
        updateStatus += `, is_cancel='1', is_finished=0`;
      }
    }

    const sql = `UPDATE lotto_category SET 
      two_under='${two_under}', 
      two_top='${two_top}',
      three_top='${three_top}',
      updated_date=now() ${updateStatus}
      WHERE date_id = '${date_id}' AND category_id='${category_id}';`;
    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

const _updateHuayWeekHuayCountry = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const body = req.body;
    const filedVaildator = ["date", "two_under", "two_top","three_top", "category_id"];
    let specificText = "";
    if (["3"].includes(`${body.category_id}`)) {
      // filedVaildator.push("four_top")
      specificText += ` four_top='${body.four_top || ""}',`;
    }

    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const date_id = body.date;
    const two_top = body.two_top;
    const two_under = body.two_under;
    const three_top = body.three_top;
    const category_id = body.category_id;
    const status = body.status;
    let updateStatus = "";
    if (status !== undefined && status !== "" && status !== null) {
      if (status === 0 || status === 1) {
        updateStatus += ` , is_finished='${status}' , is_cancel=0`;
      }
      if (status === 2) {
        updateStatus += `, is_cancel='1', is_finished=0`;
      }
    }

    const sql = `UPDATE lotto_category SET 
      ${specificText}
      two_top = '${two_top}',
      two_under='${two_under}', 
      three_top='${three_top}',
      updated_date=now() ${updateStatus}
      WHERE date_id = '${date_id}' AND category_id='${category_id}';`;
    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

const _updateHuayWeekHuayChud = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const body = req.body;
    const filedVaildator = ["date", "four_top", "category_id"];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }
    const date_id = body.date;
    const four_top = body.four_top;
    const category_id = body.category_id;
    const status = body.status;
    let updateStatus = "";
    if (status !== undefined && status !== "" && status !== null) {
      if (status === 0 || status === 1) {
        updateStatus += ` , is_finished='${status}' , is_cancel=0`;
      }
      if (status === 2) {
        updateStatus += `, is_cancel='1', is_finished=0`;
      }
    }

    const pattern = /^\d{4}$/;

    if (!pattern.test(four_top)) {
      return res.status(400).send({
        code: 400,
        message: "four_top should be 4 digit",
      });
    }

    let threeTop = four_top.slice(1, 4);
    let two_top = four_top.slice(0,2);
    let two_under = four_top.slice(2,4);

    const m_sql = `SELECT lotto_id from mapping_category mc WHERE mc.category_id = '${category_id}'`
    const mc_result = await db_access.query(m_sql);
    let lotto_id = '';
    if(mc_result.rowCount > 0){
      lotto_id = mc_result.rows[0].lotto_id;
    }

    if(lotto_id === 23){
      two_top = two_under;
      two_under = four_top.slice(0,2);
    }

    const sql = `UPDATE lotto_category 
    SET 
      four_top= '${four_top}',
      three_top='${threeTop}',
      two_top='${two_top}',
      two_under='${two_under}',
      updated_date=now() ${updateStatus}
      WHERE date_id = '${date_id}' AND category_id='${category_id}';`;
    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

exports.update_huay_week = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const update = {
      [lotto["THAI"]]: _updateHuayWeekThai,
      [lotto["YIKI"]]: _updateHuayWeekYiki,
      [lotto["HUAYHUNG"]]: _updateHuayWeekHuayHung,
      [lotto.HUAYCOUNTRY]: _updateHuayWeekHuayCountry,
      [lotto.HUAYCHUD]: _updateHuayWeekHuayChud,
    };
    return await update[lotto_type_id](req, res, next);
  } catch (error) {
    console.error(error);
    return response_error_builder(res, error);
  }
};

const _list_huay_thai_rate_limit = async (res, lotto_id, query) => {
  try {
    const limit = query.limit || 10;
    const page = query.page || 1;
    let pagination = "";
    const number_type_id = query.number_type_id;
    const search = query.search;
    let whereClause = "";
    if (limit && limit > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (number_type_id) {
      whereClause += ` AND rl.number_type_id = '${number_type_id}'`;
    }

    if (search) {
      whereClause += ` AND (CONCAT(rl.number_type_id) = '%${search}%' OR CONCAT(nt.number_type_name) ilike '%${search}%' OR CONCAT(rl.win_price) ilike '%${search}%' OR CONCAT(rl.rate) ilike '%${search}%')`;
    }
    const sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name, rl.rate, rl.win_price, rl.order_seq
    from rate_limit rl 
    inner join number_type nt on nt.id = rl.number_type_id
    where rl.lotto_id='${lotto_id}' ${whereClause}
    order by rl.lotto_id asc , rl.number_type_id asc, rl.rate desc`;
    const result = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );

    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = parseInt(count_result.rows[0].total_record);
    }

    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: result.rows,
        total_record,
      },
    });
  } catch (error) {
    throw error;
  }
};

const _list_huay_yiki_rate_limit = async (res, lotto_type_id, query) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }

    const limit = query.limit || 10;
    const page = query.page || 1;
    let pagination = "";
    const number_type_id = query.number_type_id;
    const search = query.search;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    if (limit && limit > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (number_type_id) {
      whereClause += ` AND rl.number_type_id = '${number_type_id}'`;
    }

    if (search) {
      whereClause += ` AND (CONCAT(rl.number_type_id) = '%${search}%' OR CONCAT(nt.number_type_name) ilike '%${search}%' OR CONCAT(rl.win_price) ilike '%${search}%' OR CONCAT(rl.rate) ilike '%${search}%')`;
    }
    const sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name, rl.rate, rl.win_price, rl.order_seq
    from rate_limit rl 
    inner join number_type nt on nt.id = rl.number_type_id
    where rl.lotto_id='${lotto_id}' ${whereClause}
    order by rl.lotto_id asc , rl.number_type_id asc, rl.rate desc`;
    const result = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );

    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = parseInt(count_result.rows[0].total_record);
    }

    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: result.rows,
        total_record,
      },
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_huay_huay_hung_rate_limit = async (res, lotto_type_id, query) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }

    const limit = query.limit || 10;
    const page = query.page || 1;
    let pagination = "";
    const number_type_id = query.number_type_id;
    const search = query.search;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    if (limit && limit > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (number_type_id) {
      whereClause += ` AND rl.number_type_id = '${number_type_id}'`;
    }

    if (search) {
      whereClause += ` AND (CONCAT(rl.number_type_id) = '%${search}%' OR CONCAT(nt.number_type_name) ilike '%${search}%' OR CONCAT(rl.win_price) ilike '%${search}%' OR CONCAT(rl.rate) ilike '%${search}%')`;
    }
    const sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name, rl.rate, rl.win_price, rl.order_seq
    from rate_limit rl 
    inner join number_type nt on nt.id = rl.number_type_id
    where rl.lotto_id='${lotto_id}' ${whereClause}
    order by rl.lotto_id asc , rl.number_type_id asc, rl.rate desc`;
    const result = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );

    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = parseInt(count_result.rows[0].total_record);
    }

    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: result.rows,
        total_record,
      },
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_huay_huay_country_rate_limit = async (
  res,
  lotto_type_id,
  query
) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }

    const limit = query.limit || 10;
    const page = query.page || 1;
    let pagination = "";
    const number_type_id = query.number_type_id;
    const search = query.search;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    if (limit && limit > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (number_type_id) {
      whereClause += ` AND rl.number_type_id = '${number_type_id}'`;
    }

    if (search) {
      whereClause += ` AND (CONCAT(rl.number_type_id) = '%${search}%' OR CONCAT(nt.number_type_name) ilike '%${search}%' OR CONCAT(rl.win_price) ilike '%${search}%' OR CONCAT(rl.rate) ilike '%${search}%')`;
    }
    const sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name, rl.rate, rl.win_price, rl.order_seq
    from rate_limit rl 
    inner join number_type nt on nt.id = rl.number_type_id
    where rl.lotto_id='${lotto_id}' ${whereClause}
    order by rl.lotto_id asc , rl.number_type_id asc, rl.rate desc`;
    const result = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );

    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = parseInt(count_result.rows[0].total_record);
    }

    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: result.rows,
        total_record,
      },
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_huay_huay_chud_rate_limit = async (res, lotto_type_id, query) => {
  try {
    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }

    const limit = query.limit || 10;
    const page = query.page || 1;
    let pagination = "";
    const number_type_id = query.number_type_id;
    const search = query.search;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    if (limit && limit > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }

    if (number_type_id) {
      whereClause += ` AND rl.number_type_id = '${number_type_id}'`;
    }

    if (search) {
      whereClause += ` AND (CONCAT(rl.number_type_id) = '%${search}%' OR CONCAT(nt.number_type_name) ilike '%${search}%' OR CONCAT(rl.win_price) ilike '%${search}%' OR CONCAT(rl.rate) ilike '%${search}%')`;
    }
    const sql = `select rl.lotto_id, rl.number_type_id, nt.number_type_name, rl.rate, rl.win_price, rl.order_seq
    from rate_limit rl 
    inner join number_type nt on nt.id = rl.number_type_id
    where rl.lotto_id='${lotto_id}' ${whereClause}
    order by rl.lotto_id asc , rl.number_type_id asc, rl.rate desc`;
    const result = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) rs`
    );

    let total_record = 0;
    if (count_result.rowCount > 0) {
      total_record = parseInt(count_result.rows[0].total_record);
    }

    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: result.rows,
        total_record,
      },
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.list_huay_rate_limit = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const getFunction = {
      [lotto["THAI"]]: _list_huay_thai_rate_limit,
      [lotto["YIKI"]]: _list_huay_yiki_rate_limit,
      [lotto["HUAYHUNG"]]: _list_huay_huay_hung_rate_limit,
      [lotto.HUAYCOUNTRY]: _list_huay_huay_country_rate_limit,
      [lotto.HUAYCHUD]: _list_huay_huay_chud_rate_limit,
    };

    return await getFunction[lotto_type_id](res, lotto_type_id, req.query);
  } catch (error) {
    console.error(error);
    response_error_builder(res, error);
  }
};

const _updateHuayCountryRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedVaildator = [
      "number_type_id",
      "order_seq",
      "win_price",
      "rate",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const lotto_id = body.lotto_id;
    let sql = `UPDATE rate_limit SET win_price='${body.win_price}', rate='${body.rate}', updated_date=now()
                WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}' AND order_seq='${body.order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _updateHuayChudRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedVaildator = [
      "number_type_id",
      "order_seq",
      "win_price",
      "rate",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const lotto_id = body.lotto_id;
    let sql = `UPDATE rate_limit SET win_price='${body.win_price}', rate='${body.rate}', updated_date=now()
                WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}' AND order_seq='${body.order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _updateHuayHungRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedVaildator = [
      "number_type_id",
      "order_seq",
      "win_price",
      "rate",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const lotto_id = body.lotto_id;
    let sql = `UPDATE rate_limit SET win_price='${body.win_price}', rate='${body.rate}', updated_date=now()
                WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}' AND order_seq='${body.order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _updateHuayYikiRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedVaildator = [
      "number_type_id",
      "order_seq",
      "win_price",
      "rate",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const lotto_id = body.lotto_id;
    let sql = `UPDATE rate_limit SET win_price='${body.win_price}', rate='${body.rate}', updated_date=now()
                WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}' AND order_seq='${body.order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _updateHuayRateLimit = async (res, lotto_id, body) => {
  try {
    const filedValidator = ["number_type_id", "order_seq", "win_price", "rate"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    await request_validator(res, body);
    let sql = `UPDATE rate_limit SET win_price='${body.win_price}', rate='${body.rate}', updated_date=now()
                WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}' AND order_seq='${body.order_seq}' `;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

exports.update_huay_rate_limit = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const update = {
      [lotto["THAI"]]: _updateHuayRateLimit,
      [lotto["YIKI"]]: _updateHuayYikiRateLimit,
      [lotto["HUAYHUNG"]]: _updateHuayHungRateLimit,
      [lotto.HUAYCOUNTRY]: _updateHuayCountryRateLimit,
      [lotto.HUAYCHUD]: _updateHuayChudRateLimit,
    };
    await update[lotto_type_id](res, lotto_type_id, req.body);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    return response_error_builder(res, error);
  }
};

const _addHuayThaiRateLimit = async (res, lotto_id, body) => {
  try {
    const filedVaildator = ["number_type_id", "rate", "win_price"];
    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    let sql = `INSERT INTO rate_limit (lotto_id, number_type_id, rate, win_price, is_default, order_seq, status)
    values('${lotto_id}', '${body.number_type_id}', '${body.rate}', '${body.win_price}', '0', '2', '1')`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _addHuayYikiRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "win_price", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }

    const lotto_id = body.lotto_id;
    let sql = `INSERT INTO rate_limit (lotto_id, number_type_id, rate, win_price, is_default, order_seq, status)
    values('${lotto_id}', '${body.number_type_id}', '${body.rate}', '${body.win_price}', '0', '2', '1')`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _addHuayHungRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "win_price", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }

    const lotto_id = body.lotto_id;
    let sql = `INSERT INTO rate_limit (lotto_id, number_type_id, rate, win_price, is_default, order_seq, status)
    values('${lotto_id}', '${body.number_type_id}', '${body.rate}', '${body.win_price}', '0', '2', '1')`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
    // return response_success_builder(res)
  } catch (error) {
    throw error;
  }
};

const _addHuayCountryRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "win_price", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }

    const lotto_id = body.lotto_id;
    let sql = `INSERT INTO rate_limit (lotto_id, number_type_id, rate, win_price, is_default, order_seq, status)
    values('${lotto_id}', '${body.number_type_id}', '${body.rate}', '${body.win_price}', '0', '2', '1')`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
    // return response_success_builder(res)
  } catch (error) {
    throw error;
  }
};

const _addHuayChudRateLimit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "win_price", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }

    const lotto_id = body.lotto_id;
    let sql = `INSERT INTO rate_limit (lotto_id, number_type_id, rate, win_price, is_default, order_seq, status)
    values('${lotto_id}', '${body.number_type_id}', '${body.rate}', '${body.win_price}', '0', '2', '1')`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
    // return response_success_builder(res)
  } catch (error) {
    throw error;
  }
};

exports.add_huay_rate_limit = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const add = {
      [lotto["THAI"]]: _addHuayThaiRateLimit,
      [lotto["YIKI"]]: _addHuayYikiRateLimit,
      [lotto["HUAYHUNG"]]: _addHuayHungRateLimit,
      [lotto.HUAYCOUNTRY]: _addHuayCountryRateLimit,
      [lotto.HUAYCHUD]: _addHuayChudRateLimit,
    };

    await add[lotto_type_id](res, lotto_type_id, req.body);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    return response_error_builder(res, error);
  }
};

const _delete_huay_thai_rate_limit = async (res, lotto_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    let sql = `DELETE FROM rate_limit 
    WHERE lotto_id = '${lotto_id}' AND number_type_id= '${body.number_type_id}' AND rate='${body.rate}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _delete_huay_yiki_rate_limit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "order_seq", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const lotto_id = body.lotto_id;
    const order_seq = body.order_seq;
    let sql = `DELETE FROM rate_limit 
    WHERE lotto_id = '${lotto_id}' AND number_type_id= '${body.number_type_id}' AND rate='${body.rate}' AND order_seq='${order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _delete_huay_hung_rate_limit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "order_seq", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const lotto_id = body.lotto_id;
    const order_seq = body.order_seq;
    let sql = `DELETE FROM rate_limit 
    WHERE lotto_id = '${lotto_id}' AND number_type_id= '${body.number_type_id}' AND rate='${body.rate}' AND order_seq='${order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _delete_huay_country_rate_limit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "order_seq", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const lotto_id = body.lotto_id;
    const order_seq = body.order_seq;
    let sql = `DELETE FROM rate_limit 
    WHERE lotto_id = '${lotto_id}' AND number_type_id= '${body.number_type_id}' AND rate='${body.rate}' AND order_seq='${order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};

const _delete_huay_chud_rate_limit = async (res, lotto_type_id, body) => {
  try {
    const filedValidator = ["number_type_id", "rate", "order_seq", "lotto_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const lotto_id = body.lotto_id;
    const order_seq = body.order_seq;
    let sql = `DELETE FROM rate_limit 
    WHERE lotto_id = '${lotto_id}' AND number_type_id= '${body.number_type_id}' AND rate='${body.rate}' AND order_seq='${order_seq}'`;
    await db_access.query(sql);

    sql = `SELECT rate, number_type_id FROM rate_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${body.number_type_id}'
    AND is_default=0
    order by lotto_id asc , number_type_id asc, rate desc`;
    const result = await db_access.query(sql);
    let seq = 2;
    sql = "";
    for await (const row of result.rows) {
      sql += `UPDATE rate_limit SET order_seq='${seq}' WHERE lotto_id='${lotto_id}' AND number_type_id='${row.number_type_id}' AND rate='${row.rate}';`;
      seq++;
    }
    await db_access.query(sql);
  } catch (error) {
    throw error;
  }
};
exports.delete_huay_rate_limit = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const deleteHuayRateLimit = {
      [lotto["THAI"]]: _delete_huay_thai_rate_limit,
      [lotto["YIKI"]]: _delete_huay_yiki_rate_limit,
      [lotto["HUAYHUNG"]]: _delete_huay_hung_rate_limit,
      [lotto.HUAYCOUNTRY]: _delete_huay_country_rate_limit,
      [lotto.HUAYCHUD]: _delete_huay_chud_rate_limit,
    };

    await deleteHuayRateLimit[lotto_type_id](res, lotto_type_id, req.body);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    return res.status(500).send({
      code: 500,
      message: "Success",
      errors: error,
    });
  }
};

const _list_number_limit_huay_thai = async (res, query) => {
  try {
    if (!query.date) {
      return res.status(400).send({
        code: 400,
        message: "date is required",
      });
    }
    let whereClause = "";
    if (query.number_type_id) {
      whereClause += " AND nt.id=" + query.number_type_id;
    }

    if (query.search) {
      whereClause += ` AND (CONCAT(nt.number_type_name,'') ilike '%${query.search}%' OR CONCAT(nl.rate,'') ilike '%${query.search}%' OR CONCAT(nl.numbers,'') ilike '%${query.search}%')`;
    }

    let pagination = "";
    if (query.limit && query.limit > 0) {
      const offset = (query.page - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset >= 0 ? offset : 0}`;
    }
    const sql = `select nt.id as number_type_id , nt.number_type_name , nl.date_id ,nl.category_id , nl.numbers , nl.rate , nl.status  from number_limit nl
    left join number_type nt on nt.id = nl.number_type_id 
    where nl.date_id = '${query.date}' AND category_id='1'  ${whereClause}
    order by nt.id, nl.order_seq `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    const total_record =
      count_result.rowCount > 0 ? count_result.rows[0]["total_record"] : 0;
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: { rows: result.rows, total_record: parseInt(total_record) },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _list_number_limit_huay_yiki = async (res, query) => {
  try {
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return await request_validator(res, query, filedVaildator);
    }
    const category_id = query.category_id;
    let whereClause = "";
    if (query.number_type_id) {
      whereClause += " AND nt.id=" + query.number_type_id;
    }

    if (query.search) {
      whereClause += ` AND (CONCAT(nt.number_type_name,'') ilike '%${query.search}%' OR CONCAT(nl.rate,'') ilike '%${query.search}%' OR CONCAT(nl.numbers,'') ilike '%${query.search}%')`;
    }

    let pagination = "";
    if (query.limit && query.limit > 0) {
      const offset = (query.page - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset >= 0 ? offset : 0}`;
    }
    const sql = `select nt.id as number_type_id , nt.number_type_name , nl.date_id ,nl.category_id , 
    nl.numbers , nl.rate , nl.status  from number_limit nl
    left join number_type nt on nt.id = nl.number_type_id 
    where nl.date_id = '${query.date}' AND category_id='${category_id}' ${whereClause}
    order by nt.id, nl.order_seq `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    const total_record =
      count_result.rowCount > 0 ? count_result.rows[0]["total_record"] : 0;

    response_success_builder(res, {
      rows: result.rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_number_limit_huay_hung = async (res, query) => {
  try {
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return await request_validator(res, query, filedVaildator);
    }
    const category_id = query.category_id;
    let whereClause = "";
    if (query.number_type_id) {
      whereClause += " AND nt.id=" + query.number_type_id;
    }

    if (query.search) {
      whereClause += ` AND (CONCAT(nt.number_type_name,'') ilike '%${query.search}%' OR CONCAT(nl.rate,'') ilike '%${query.search}%' OR CONCAT(nl.numbers,'') ilike '%${query.search}%')`;
    }

    let pagination = "";
    if (query.limit && query.limit > 0) {
      const offset = (query.page - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset >= 0 ? offset : 0}`;
    }
    const sql = `select nt.id as number_type_id , nt.number_type_name , nl.date_id ,nl.category_id , 
    nl.numbers , nl.rate , nl.status  from number_limit nl
    left join number_type nt on nt.id = nl.number_type_id 
    where nl.date_id = '${query.date}' AND category_id='${category_id}' ${whereClause}
    order by nt.id, nl.order_seq `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    const total_record =
      count_result.rowCount > 0 ? count_result.rows[0]["total_record"] : 0;

    response_success_builder(res, {
      rows: result.rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_number_limit_huay_country = async (res, query) => {
  try {
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return await request_validator(res, query, filedVaildator);
    }
    const category_id = query.category_id;
    let whereClause = "";
    if (query.number_type_id) {
      whereClause += " AND nt.id=" + query.number_type_id;
    }

    if (query.search) {
      whereClause += ` AND (CONCAT(nt.number_type_name,'') ilike '%${query.search}%' OR CONCAT(nl.rate,'') ilike '%${query.search}%' OR CONCAT(nl.numbers,'') ilike '%${query.search}%')`;
    }

    let pagination = "";
    if (query.limit && query.limit > 0) {
      const offset = (query.page - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset >= 0 ? offset : 0}`;
    }
    const sql = `select nt.id as number_type_id , nt.number_type_name , nl.date_id ,nl.category_id , 
    nl.numbers , nl.rate , nl.status  from number_limit nl
    left join number_type nt on nt.id = nl.number_type_id 
    where nl.date_id = '${query.date}' AND category_id='${category_id}' ${whereClause}
    order by nt.id, nl.order_seq `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    const total_record =
      count_result.rowCount > 0 ? count_result.rows[0]["total_record"] : 0;

    response_success_builder(res, {
      rows: result.rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _list_number_limit_huay_chud = async (res, query) => {
  try {
    const filedVaildator = ["date", "lotto_id", "category_id"];
    if (!check_validator_basic(query, filedVaildator)) {
      return await request_validator(res, query, filedVaildator);
    }
    const category_id = query.category_id;
    let whereClause = "";
    if (query.number_type_id) {
      whereClause += " AND nt.id=" + query.number_type_id;
    }

    if (query.search) {
      whereClause += ` AND (CONCAT(nt.number_type_name,'') ilike '%${query.search}%' OR CONCAT(nl.rate,'') ilike '%${query.search}%' OR CONCAT(nl.numbers,'') ilike '%${query.search}%')`;
    }

    let pagination = "";
    if (query.limit && query.limit > 0) {
      const offset = (query.page - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset >= 0 ? offset : 0}`;
    }
    const sql = `select nt.id as number_type_id , nt.number_type_name , nl.date_id ,nl.category_id , 
    nl.numbers , nl.rate , nl.status  from number_limit nl
    left join number_type nt on nt.id = nl.number_type_id 
    where nl.date_id = '${query.date}' AND category_id='${category_id}' ${whereClause}
    order by nt.id, nl.order_seq `;

    const result = await db_access.query(sql + pagination);
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    const total_record =
      count_result.rowCount > 0 ? count_result.rows[0]["total_record"] : 0;

    response_success_builder(res, {
      rows: result.rows,
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.list_number_limit = async (req, res, next) => {
  try {
    const lotto_type_id = req.params.id;
    const listNumberLimitFunc = {
      [lotto["THAI"]]: _list_number_limit_huay_thai,
      [lotto["YIKI"]]: _list_number_limit_huay_yiki,
      [lotto["HUAYHUNG"]]: _list_number_limit_huay_hung,
      [lotto.HUAYCOUNTRY]: _list_number_limit_huay_country,
      [lotto.HUAYCHUD]: _list_number_limit_huay_chud,
    };
    return await listNumberLimitFunc[lotto_type_id](res, req.query);
  } catch (error) {
    response_error_builder(res, error);
  }
};


const _update_number_limit_huay_thai_old = async (res, body) => {
  try {
    const lotto_id = lotto["THAI"];
    const category_id = 1;
    const filedValidator = ["number_type_id", "rate", "number_limit", "date"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);
    let num =
        result.rowCount > 0 && result.rows[0]["num"] > 0
            ? parseInt(result.rows[0]["num"])
            : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers = '${number_limit}' AND date_id='${date}'; `;

    result = await db_access.query(sql);
    if (
        (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
        result.rowCount <= 0
    ) {
        sql = ` INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
        VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
            num + 1
        }', '${rate}')`;
    } else {
        sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
    }
    await db_access.query(sql);

    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};


const _update_number_limit_huay_thai = async (res, body) => {
  try {
    const lotto_id = lotto["THAI"];
    const category_id = 1;
    const filedValidator = ["number_type_id", "rate", "number_limit", "date"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    const type = body.sub_number_type_id;
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);
    let values = [];
    if (type === "3" || type === 3 || type === "8" || type === 8) {
      function generatePermutations(current, remaining) {
        if (remaining.length === 0) {
          values.push(current);
          return;
        }

        for (let i = 0; i < remaining.length; i++) {
          const newCurrent = current + remaining[i];
          const newRemaining = remaining.slice(0, i) + remaining.slice(i + 1);

          // Include permutations starting with '0'
          generatePermutations(newCurrent, newRemaining);

          // Add leading zeros for non-empty current string
          if (current === '' || (current !== '' && remaining[i] !== '0')) {
            generatePermutations(current + remaining[i], newRemaining);
          }
        }
      }
      generatePermutations('', number_limit.toString());
    }

    values = [...new Set(values)]
    let num =
      result.rowCount > 0 && result.rows[0]["num"] > 0
        ? parseInt(result.rows[0]["num"])
        : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers = '${number_limit}' AND date_id='${date}'; `;

    result = await db_access.query(sql);
    if (
      (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
      result.rowCount <= 0
    ) {
      if (type === "3" || type === 3 || type === "8" || type === 8){
        for (let i =0; i< values.length; i++) {
            sql += ` INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
        VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${values[i]}', '${num + 1}', '${rate}'); `;
        }
      } else {
          sql = ` INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
        VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
          num + 1
        }', '${rate}')`;
      }
    } else {
      if (type === "3" || type === 3 || type === "4" || type === 4){
        for (let i =0; i< values.length; i++) {
          sql += `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${values[i]}'; `;
        }
      } else {
        sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
      }
    }
    await db_access.query(sql);

    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _update_number_limit_huay_yiki = async (res, body) => {
  try {
    const lotto_type_id = lotto["YIKI"];

    const filedValidator = [
      "number_type_id",
      "rate",
      "number_limit",
      "date",
      "category_id",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const category_id = body.category_id;
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);

    let num =
      result.rowCount > 0 && result.rows[0]["num"] > 0
        ? parseInt(result.rows[0]["num"])
        : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers='${number_limit}' AND date_id='${date}'`;

    result = await db_access.query(sql);
    if (
      (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
      result.rowCount <= 0
    ) {
      sql = `INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
      VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
        num + 1
      }', '${rate}')`;
    } else {
      sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
    }

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _update_number_limit_huay_hung_yiki = async (res, body) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];

    const filedValidator = [
      "number_type_id",
      "rate",
      "number_limit",
      "date",
      "category_id",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const category_id = body.category_id;
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);

    let num =
      result.rowCount > 0 && result.rows[0]["num"] > 0
        ? parseInt(result.rows[0]["num"])
        : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers='${number_limit}' AND date_id='${date}'`;

    result = await db_access.query(sql);
    if (
      (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
      result.rowCount <= 0
    ) {
      sql = `INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
      VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
        num + 1
      }', '${rate}')`;
    } else {
      sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
    }

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _update_number_limit_huay_hung_country = async (res, body) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];

    const filedValidator = [
      "number_type_id",
      "rate",
      "number_limit",
      "date",
      "category_id",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const category_id = body.category_id;
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);

    let num =
      result.rowCount > 0 && result.rows[0]["num"] > 0
        ? parseInt(result.rows[0]["num"])
        : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers='${number_limit}' AND date_id='${date}'`;

    result = await db_access.query(sql);
    if (
      (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
      result.rowCount <= 0
    ) {
      sql = `INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
      VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
        num + 1
      }', '${rate}')`;
    } else {
      sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
    }

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _update_number_limit_huay_hung_chud = async (res, body) => {
  try {
    const lotto_type_id = lotto.HUAYCHUD;

    const filedValidator = [
      "number_type_id",
      "rate",
      "number_limit",
      "date",
      "category_id",
      "lotto_id",
    ];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const category_id = body.category_id;
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const rate = body.rate;
    const number_limit = body.number_limit;
    const date = body.date;
    let sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND date_id='${date}'`;
    let result = await db_access.query(sql);

    let num =
      result.rowCount > 0 && result.rows[0]["num"] > 0
        ? parseInt(result.rows[0]["num"])
        : 0;

    sql = `SELECT COUNT(*) AS num FROM number_limit nl WHERE nl.lotto_id='${lotto_id}' AND category_id = '${category_id}' 
    AND number_type_id='${number_type_id}' AND numbers='${number_limit}' AND date_id='${date}'`;

    result = await db_access.query(sql);
    if (
      (result.rowCount > 0 && parseInt(result.rows[0]["num"]) === 0) ||
      result.rowCount <= 0
    ) {
      sql = `INSERT INTO number_limit (date_id, lotto_id, category_id, number_type_id, numbers, order_seq, rate)
      VALUES('${date}', '${lotto_id}', '${category_id}', '${number_type_id}', '${number_limit}', '${
        num + 1
      }', '${rate}')`;
    } else {
      sql = `UPDATE number_limit SET rate='${rate}', updated_date=now() WHERE number_type_id='${number_type_id}' 
      AND lotto_id='${lotto_id}' AND category_id='${category_id}' AND date_id='${date}' AND numbers='${number_limit}'`;
    }

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.update_number_limit = async (req, res) => {
  try {
    const lotto_id = req.params.id;
    const updateNumberLimitFunction = {
      [lotto["THAI"]]: _update_number_limit_huay_thai,
      [lotto["YIKI"]]: _update_number_limit_huay_yiki,
      [lotto["HUAYHUNG"]]: _update_number_limit_huay_hung_yiki,
      [lotto.HUAYCOUNTRY]: _update_number_limit_huay_hung_country,
      [lotto.HUAYCHUD]: _update_number_limit_huay_hung_chud,
    };

    return await updateNumberLimitFunction[lotto_id](res, req.body);
  } catch (error) {
    return response_error_builder(res, error);
  }
};

const _deleteNumberLimitHuayThai = async (res, body) => {
  try {
    const filedValidator = ["date", "number_type_id", "number_limit"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const lotto_id = lotto["THAI"];
    const category_id = 1;
    const date = body.date;
    const number_type_id = body.number_type_id;
    const number_limit = body.number_limit;

    const sql = `DELETE FROM number_limit WHERE lotto_id='${lotto_id}' AND category_id='${category_id}'
                AND number_type_id='${number_type_id}' AND date_id='${date}' AND numbers='${number_limit}'`;

    await db_access.query(sql);

    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    throw error;
  }
};

const _deleteNumberLimitHuayYiki = async (res, body) => {
  try {
    const filedValidator = [
      "date",
      "number_type_id",
      "number_limit",
      "category_id",
      "lotto_id",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );

    const lotto_id = body.lotto_id;
    const category_id = body.category_id;
    const date = body.date;
    const number_type_id = body.number_type_id;
    const number_limit = body.number_limit;

    const sql = `DELETE FROM number_limit WHERE lotto_id='${lotto_id}' AND category_id='${category_id}'
                AND number_type_id='${number_type_id}' AND date_id='${date}' AND numbers='${number_limit}'`;

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _deleteNumberLimitHuayHung = async (res, body) => {
  try {
    const filedValidator = [
      "date",
      "number_type_id",
      "number_limit",
      "category_id",
      "lotto_id",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );

    const lotto_id = body.lotto_id;
    const category_id = body.category_id;
    const date = body.date;
    const number_type_id = body.number_type_id;
    const number_limit = body.number_limit;

    const sql = `DELETE FROM number_limit WHERE lotto_id='${lotto_id}' AND category_id='${category_id}'
                AND number_type_id='${number_type_id}' AND date_id='${date}' AND numbers='${number_limit}'`;

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _deleteNumberLimitHuayCountry = async (res, body) => {
  try {
    const filedValidator = [
      "date",
      "number_type_id",
      "number_limit",
      "category_id",
      "lotto_id",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );

    const lotto_id = body.lotto_id;
    const category_id = body.category_id;
    const date = body.date;
    const number_type_id = body.number_type_id;
    const number_limit = body.number_limit;

    const sql = `DELETE FROM number_limit WHERE lotto_id='${lotto_id}' AND category_id='${category_id}'
                AND number_type_id='${number_type_id}' AND date_id='${date}' AND numbers='${number_limit}'`;

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _deleteNumberLimitHuayChud = async (res, body) => {
  try {
    const filedValidator = [
      "date",
      "number_type_id",
      "number_limit",
      "category_id",
      "lotto_id",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );

    const lotto_id = body.lotto_id;
    const category_id = body.category_id;
    const date = body.date;
    const number_type_id = body.number_type_id;
    const number_limit = body.number_limit;

    const sql = `DELETE FROM number_limit WHERE lotto_id='${lotto_id}' AND category_id='${category_id}'
                AND number_type_id='${number_type_id}' AND date_id='${date}' AND numbers='${number_limit}'`;

    await db_access.query(sql);

    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.delete_number_limit = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const deleteNumberLimitFunction = {
      [lotto["THAI"]]: _deleteNumberLimitHuayThai,
      [lotto["YIKI"]]: _deleteNumberLimitHuayYiki,
      [lotto["HUAYHUNG"]]: _deleteNumberLimitHuayHung,
      [lotto.HUAYCOUNTRY]: _deleteNumberLimitHuayCountry,
      [lotto.HUAYCHUD]: _deleteNumberLimitHuayChud,
    };

    return await deleteNumberLimitFunction[lotto_type_id](res, req.body);
  } catch (error) {
    return response_error_builder(res, error);
  }
};

const _list_user_limit_huay_thai = async (res, query) => {
  try {
    const lotto_id = lotto["THAI"];

    let whereClause = "";
    let pagination = "";
    const type = query.number_type_id;
    const search = query.search;

    if (type) {
      whereClause += ` AND nt.id='${type}'`;
    }

    if (search) {
      whereClause += ` AND ( nt.number_type_name ilike '%${search}%' 
      OR CONCAT(ul.min,'') ilike '%${search}%' 
      OR CONCAT(ul.max,'') ilike '%${search}%' 
      OR CONCAT(ul.user_max, '') ilike '%${search}%')`;
    }

    if (query.limit && query.limit > 0) {
      const page = parseInt(query.page);
      const limit = parseInt(query.limit);
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }
    const sql = `SELECT ul.lotto_id, 
    ul.number_type_id, 
    nt.number_type_name ,
    ul.min, ul.max, 
    ul.user_max, ul."close", ul.status, s.status_name, s.status_name_th,
    ul.created_date, ul.updated_date
    FROM public.user_limit ul
    inner join number_type nt on nt.id = ul.number_type_id 
    inner join status s on s.id = ul.status
    where ul.lotto_id = '${lotto_id}' ${whereClause}
    order by nt.id asc, ul.min asc
    `;

    const data = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: data.rows,
        total_record:
          count_result.rowCount > 0
            ? parseInt(count_result.rows[0].total_record)
            : 0,
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _list_user_limit_huay_yiki = async (res, query) => {
  try {
    const lotto_type_id = lotto["YIKI"];

    let whereClause = "";
    let pagination = "";
    const type = query.number_type_id;
    const search = query.search;

    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query);
    }
    const lotto_id = query.lotto_id;

    if (type) {
      whereClause += ` AND nt.id='${type}'`;
    }

    if (search) {
      whereClause += ` AND ( nt.number_type_name ilike '%${search}%' 
      OR CONCAT(ul.min,'') ilike '%${search}%' 
      OR CONCAT(ul.max,'') ilike '%${search}%' 
      OR CONCAT(ul.user_max, '') ilike '%${search}%')`;
    }

    if (query.limit && query.limit > 0) {
      const page = parseInt(query.page);
      const limit = parseInt(query.limit);
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }
    const sql = `SELECT ul.lotto_id, 
    ul.number_type_id, 
    nt.number_type_name ,
    ul.min, ul.max, 
    ul.user_max, ul."close", ul.status, s.status_name, s.status_name_th,
    ul.created_date, ul.updated_date
    FROM public.user_limit ul
    inner join number_type nt on nt.id = ul.number_type_id 
    inner join status s on s.id = ul.status
    where ul.lotto_id = '${lotto_id}' ${whereClause}
    order by nt.id asc, ul.min asc
    `;

    const data = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: data.rows,
        total_record:
          count_result.rowCount > 0
            ? parseInt(count_result.rows[0].total_record)
            : 0,
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _list_user_limit_huay_hung = async (res, query) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];

    let whereClause = "";
    let pagination = "";
    const type = query.number_type_id;
    const search = query.search;

    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query);
    }
    const lotto_id = query.lotto_id;

    if (type) {
      whereClause += ` AND nt.id='${type}'`;
    }

    if (search) {
      whereClause += ` AND ( nt.number_type_name ilike '%${search}%' 
      OR CONCAT(ul.min,'') ilike '%${search}%' 
      OR CONCAT(ul.max,'') ilike '%${search}%' 
      OR CONCAT(ul.user_max, '') ilike '%${search}%')`;
    }

    if (query.limit && query.limit > 0) {
      const page = parseInt(query.page);
      const limit = parseInt(query.limit);
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }
    const sql = `SELECT ul.lotto_id, 
    ul.number_type_id, 
    nt.number_type_name ,
    ul.min, ul.max, 
    ul.user_max, ul."close", ul.status, s.status_name, s.status_name_th,
    ul.created_date, ul.updated_date
    FROM public.user_limit ul
    inner join number_type nt on nt.id = ul.number_type_id 
    inner join status s on s.id = ul.status
    where ul.lotto_id = '${lotto_id}' ${whereClause}
    order by nt.id asc, ul.min asc
    `;

    const data = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: data.rows,
        total_record:
          count_result.rowCount > 0
            ? parseInt(count_result.rows[0].total_record)
            : 0,
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _list_user_limit_huay_country = async (res, query) => {
  try {
    const lotto_type_id = lotto.HUAYCOUNTRY;

    let whereClause = "";
    let pagination = "";
    const type = query.number_type_id;
    const search = query.search;

    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query);
    }
    const lotto_id = query.lotto_id;

    if (type) {
      whereClause += ` AND nt.id='${type}'`;
    }

    if (search) {
      whereClause += ` AND ( nt.number_type_name ilike '%${search}%' 
      OR CONCAT(ul.min,'') ilike '%${search}%' 
      OR CONCAT(ul.max,'') ilike '%${search}%' 
      OR CONCAT(ul.user_max, '') ilike '%${search}%')`;
    }

    if (query.limit && query.limit > 0) {
      const page = parseInt(query.page);
      const limit = parseInt(query.limit);
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }
    const sql = `SELECT ul.lotto_id, 
    ul.number_type_id, 
    nt.number_type_name ,
    ul.min, ul.max, 
    ul.user_max, ul."close", ul.status, s.status_name, s.status_name_th,
    ul.created_date, ul.updated_date
    FROM public.user_limit ul
    inner join number_type nt on nt.id = ul.number_type_id 
    inner join status s on s.id = ul.status
    where ul.lotto_id = '${lotto_id}' ${whereClause}
    order by nt.id asc, ul.min asc
    `;

    const data = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: data.rows,
        total_record:
          count_result.rowCount > 0
            ? parseInt(count_result.rows[0].total_record)
            : 0,
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _list_user_limit_huay_chud = async (res, query) => {
  try {
    const lotto_type_id = lotto.HUAYCHUD;

    let whereClause = "";
    let pagination = "";
    const type = query.number_type_id;
    const search = query.search;

    const filedValidator = ["lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query);
    }
    const lotto_id = query.lotto_id;

    if (type) {
      whereClause += ` AND nt.id='${type}'`;
    }

    if (search) {
      whereClause += ` AND ( nt.number_type_name ilike '%${search}%' 
      OR CONCAT(ul.min,'') ilike '%${search}%' 
      OR CONCAT(ul.max,'') ilike '%${search}%' 
      OR CONCAT(ul.user_max, '') ilike '%${search}%')`;
    }

    if (query.limit && query.limit > 0) {
      const page = parseInt(query.page);
      const limit = parseInt(query.limit);
      const offset = (page - 1) * limit;
      pagination = ` LIMIT ${limit} OFFSET ${offset}`;
    }
    const sql = `SELECT ul.lotto_id, 
    ul.number_type_id, 
    nt.number_type_name ,
    ul.min, ul.max, 
    ul.user_max, ul."close", ul.status, s.status_name, s.status_name_th,
    ul.created_date, ul.updated_date
    FROM public.user_limit ul
    inner join number_type nt on nt.id = ul.number_type_id 
    inner join status s on s.id = ul.status
    where ul.lotto_id = '${lotto_id}' ${whereClause}
    order by nt.id asc, ul.min asc
    `;

    const data = await db_access.query(
      `SELECT * FROM (${sql}) rs ${pagination}`
    );
    const count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    return res.status(200).send({
      code: 200,
      message: "Success",
      data: {
        rows: data.rows,
        total_record:
          count_result.rowCount > 0
            ? parseInt(count_result.rows[0].total_record)
            : 0,
      },
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

exports.list_user_limit = async (req, res, next) => {
  try {
    const lotto_id = req.params.id;
    const listUserLimitFunction = {
      [lotto["THAI"]]: _list_user_limit_huay_thai,
      [lotto["YIKI"]]: _list_user_limit_huay_yiki,
      [lotto["HUAYHUNG"]]: _list_user_limit_huay_hung,
      [lotto.HUAYCOUNTRY]: _list_user_limit_huay_country,
      [lotto.HUAYCHUD]: _list_user_limit_huay_chud,
    };

    return await listUserLimitFunction[lotto_id](res, req.query);
  } catch (error) {
    return res.status(500).send({
      code: 500,
      message: "Error",
      errors: error,
    });
  }
};

const _upsertUserLimitHuayThai = async (res, body) => {
  try {
    const lotto_id = lotto["THAI"];
    const filedVaildator = ["number_type_id", "min", "max", "user_max"];

    if (!check_validator_basic(body, filedVaildator)) {
      return await request_validator(res, body, filedVaildator);
    }

    const number_type_id = body.number_type_id;
    const min = body.min;
    const max = body.max;
    const user_max = body.user_max;

    let sql = `SELECT COUNT(*) as num FROM user_limit ul 
    WHERE ul.lotto_id='${lotto_id}' AND ul.number_type_id='${number_type_id}'`;
    let result = await db_access.query(sql);
    if (
      result.rowCount === 0 ||
      (result.rowCount > 0 && result.rows[0].num == 0)
    ) {
      sql = `INSERT INTO user_limit (lotto_id, number_type_id, min, max, user_max) VALUES('${lotto_id}', '${number_type_id}', '${min}', '${max}', '${user_max}')`;
    } else {
      sql = `UPDATE user_limit SET min='${min}', max='${max}', user_max='${user_max}', updated_date=now()
            WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;
    }

    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};
const _upsertUserLimitHuayYiki = async (res, body) => {
  try {
    const lotto_type_id = lotto["YIKI"];
    const filedValidator = [
      "lotto_id",
      "number_type_id",
      "min",
      "max",
      "user_max",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const min = body.min;
    const max = body.max;
    const user_max = body.user_max;

    let sql = `SELECT COUNT(*) as num FROM user_limit ul 
    WHERE ul.lotto_id='${lotto_id}' AND ul.number_type_id='${number_type_id}'`;
    let result = await db_access.query(sql);
    if (
      result.rowCount === 0 ||
      (result.rowCount > 0 && result.rows[0].num == 0)
    ) {
      sql = `INSERT INTO user_limit (lotto_id, number_type_id, min, max, user_max) VALUES('${lotto_id}', '${number_type_id}', '${min}', '${max}', '${user_max}')`;
    } else {
      sql = `UPDATE user_limit SET min='${min}', max='${max}', user_max='${user_max}', updated_date=now()
            WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;
    }

    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _upsertUserLimitHuayHung = async (res, body) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];
    const filedValidator = [
      "lotto_id",
      "number_type_id",
      "min",
      "max",
      "user_max",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const min = body.min;
    const max = body.max;
    const user_max = body.user_max;

    let sql = `SELECT COUNT(*) as num FROM user_limit ul 
    WHERE ul.lotto_id='${lotto_id}' AND ul.number_type_id='${number_type_id}'`;
    let result = await db_access.query(sql);
    if (
      result.rowCount === 0 ||
      (result.rowCount > 0 && result.rows[0].num == 0)
    ) {
      sql = `INSERT INTO user_limit (lotto_id, number_type_id, min, max, user_max) VALUES('${lotto_id}', '${number_type_id}', '${min}', '${max}', '${user_max}')`;
    } else {
      sql = `UPDATE user_limit SET min='${min}', max='${max}', user_max='${user_max}', updated_date=now()
            WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;
    }

    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _upsertUserLimitHuayCountry = async (res, body) => {
  try {
    const lotto_type_id = lotto.HUAYCOUNTRY;
    const filedValidator = [
      "lotto_id",
      "number_type_id",
      "min",
      "max",
      "user_max",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const min = body.min;
    const max = body.max;
    const user_max = body.user_max;

    let sql = `SELECT COUNT(*) as num FROM user_limit ul 
    WHERE ul.lotto_id='${lotto_id}' AND ul.number_type_id='${number_type_id}'`;
    let result = await db_access.query(sql);
    if (
      result.rowCount === 0 ||
      (result.rowCount > 0 && result.rows[0].num == 0)
    ) {
      sql = `INSERT INTO user_limit (lotto_id, number_type_id, min, max, user_max) VALUES('${lotto_id}', '${number_type_id}', '${min}', '${max}', '${user_max}')`;
    } else {
      sql = `UPDATE user_limit SET min='${min}', max='${max}', user_max='${user_max}', updated_date=now()
            WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;
    }

    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

const _upsertUserLimitHuayChud = async (res, body) => {
  try {
    const lotto_type_id = lotto.HUAYCHUD;
    const filedValidator = [
      "lotto_id",
      "number_type_id",
      "min",
      "max",
      "user_max",
    ];

    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const lotto_id = body.lotto_id;
    const number_type_id = body.number_type_id;
    const min = body.min;
    const max = body.max;
    const user_max = body.user_max;

    let sql = `SELECT COUNT(*) as num FROM user_limit ul 
    WHERE ul.lotto_id='${lotto_id}' AND ul.number_type_id='${number_type_id}'`;
    let result = await db_access.query(sql);
    if (
      result.rowCount === 0 ||
      (result.rowCount > 0 && result.rows[0].num == 0)
    ) {
      sql = `INSERT INTO user_limit (lotto_id, number_type_id, min, max, user_max) VALUES('${lotto_id}', '${number_type_id}', '${min}', '${max}', '${user_max}')`;
    } else {
      sql = `UPDATE user_limit SET min='${min}', max='${max}', user_max='${user_max}', updated_date=now()
            WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;
    }

    await db_access.query(sql);
    return res.status(200).send({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.error(error);
    manage_error(error);
  }
};

exports.upsert_user_limit = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const upsertUserLimitFunction = {
      [lotto["THAI"]]: _upsertUserLimitHuayThai,
      [lotto["YIKI"]]: _upsertUserLimitHuayYiki,
      [lotto["HUAYHUNG"]]: _upsertUserLimitHuayHung,
      [lotto.HUAYCOUNTRY]: _upsertUserLimitHuayCountry,
      [lotto.HUAYCHUD]: _upsertUserLimitHuayChud,
    };

    return await upsertUserLimitFunction[lotto_type_id](res, req.body);
  } catch (error) {
    return res.status(500).send({
      code: 500,
      message: "Error",
      errors: error,
    });
  }
};

const _deleteUserLimitHuayThai = async (res, body) => {
  try {
    const lotto_id = lotto["THAI"];
    const filedValidator = ["number_type_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const number_type_id = body.number_type_id;

    const sql = `DELETE FROM user_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;

    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    throw error;
  }
};
const _deleteUserLimitHuayYiki = async (res, body) => {
  try {
    const lotto_id = body.lotto_id;

    const filedValidator = ["number_type_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const number_type_id = body.number_type_id;

    const sql = `DELETE FROM user_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;

    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _deleteUserLimitHuayCountry = async (res, body) => {
  try {
    const lotto_id = body.lotto_id;

    const filedValidator = ["number_type_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const number_type_id = body.number_type_id;

    const sql = `DELETE FROM user_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;

    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _deleteUserLimitHuayHung = async (res, body) => {
  try {
    const lotto_id = body.lotto_id;

    const filedValidator = ["number_type_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const number_type_id = body.number_type_id;

    const sql = `DELETE FROM user_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;

    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const _deleteUserLimitHuayChud = async (res, body) => {
  try {
    const lotto_id = body.lotto_id;

    const filedValidator = ["number_type_id"];
    if (!check_validator_basic(body, filedValidator)) {
      return await request_validator(res, body, filedValidator);
    }
    // await request_validator(res, body, );
    const number_type_id = body.number_type_id;

    const sql = `DELETE FROM user_limit WHERE lotto_id='${lotto_id}' AND number_type_id='${number_type_id}'`;

    await db_access.query(sql);
    return response_success_builder(res);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

exports.delete_user_limit = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const deleteUserLimitFunction = {
      [lotto["THAI"]]: _deleteUserLimitHuayThai,
      [lotto["YIKI"]]: _deleteUserLimitHuayYiki,
      [lotto["HUAYHUNG"]]: _deleteUserLimitHuayHung,
      [lotto.HUAYCOUNTRY]: _deleteUserLimitHuayCountry,
      [lotto.HUAYCHUD]: _deleteUserLimitHuayChud,
    };

    return await deleteUserLimitFunction[lotto_type_id](res, req.body);
  } catch (error) {
    return response_error_builder(res, error);
  }
};

const _getResultHuayThai = async (res, query) => {
  try {
    const lotto_id = lotto["THAI"];

    const filedValidator = ["date"];
    if (!check_validator_basic(query, filedValidator)) {
    }
    // await request_validator(res, query, );
    if (!dayjs(query.date).isValid()) {
      throw new ApplicationError(CLIENT_ERROR, {
        message: `date format invalid`,
      });
    }
    const date = query.date;
    const search = query.search;
    const number_type_id = query.number_type_id;
    const category_id = query.category_id;
    let whereClause = "";
    let pagination = "";
    if (search) {
      whereClause += ` AND (nt.number_type_name ilike '%${search}%' OR CONCAT(ld.numbers, '') ilike '%${search}%')`;
    }

    if (number_type_id) {
      whereClause += ` AND nt.id = '${number_type_id}'`;
    }

    if (query.limit && query.limit > 0) {
      const offset = (parseInt(query.page) - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`;
    }

    let sql = `select l.id as lotto_id, lh.date_id, nt.id as number_type_id, 
    nt.number_type_name , ld.numbers  , count(ld.numbers) as num_player,
    sum(ld.price) as total_price, 
    sum(
      case 
        when ld.status in (8) then ld.win_price 
        else 0
      end
    ) as total_pay
    from lotto_detail ld 
    inner join lotto_header lh on lh.id = ld.lotto_header_id 
    inner join lotto l on l.id = lh.lotto_id 
    inner join number_type nt on nt.id = ld.number_type_id 
    where l.id = '${lotto_id}' and lh.category_id = ${category_id} and lh.date_id = '${date}' AND ld.status IN ${lotto_status} ${whereClause}
    group by l.id, lh.date_id , nt.id, nt.number_type_name , ld.numbers
    order by l.id asc, lh.date_id desc, nt.id asc, ld.numbers asc`;

    // let result = await db_access.query(
    //   `SELECT * FROM (${sql}) rs ${pagination}`
    // );
    let result = await db_access.query(
      `SELECT * FROM (${sql}) rs`
    );
    let count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    let total_result =
      await db_access.query(`SELECT SUM(rs.num_player) AS  total_num_player, 
    SUM(rs.total_price) as total_all_price, SUM(rs.total_pay) as total_all_pay FROM (${sql}) rs`);
    const total = {
      total_num_player: 0,
      total_all_price: 0.0,
      total_all_pay: 0.0,
    };
    if (total_result.rowCount > 0) {
      total.total_num_player = parseInt(
        utilities.defaultValue(() => total_result.rows[0].total_num_player, 0.0)
      );
      total.total_all_price = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_price, 0.0)
      );
      total.total_all_pay = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_pay, 0.0)
      );
    }
    response_success_builder(res, {
      rows: result.rows,
      total: total,
      total_record: parseInt(
        utilities.defaultValue(() => count_result.rows[0].total_record, 0)
      ),
    });
  } catch (error) {
    manage_error(error);
  }
};
const _getResultHuayYiki = async (res, query) => {
  try {
    const lotto_type_id = lotto["YIKI"];

    const filedValidator = ["date", "category_id", "lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }
    // await request_validator(res, query, );

    const date = query.date;
    const search = query.search;
    const number_type_id = query.number_type_id;
    const category_id = query.category_id;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    let pagination = "";
    if (search) {
      whereClause += ` AND (nt.number_type_name ilike '%${search}%' OR CONCAT(ld.numbers, '') ilike '%${search}%')`;
    }

    if (number_type_id) {
      whereClause += ` AND nt.id = '${number_type_id}'`;
    }

    if (query.limit && query.limit > 0) {
      const offset = (parseInt(query.page) - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`;
    }

    let sql = `select l.id as lotto_id, lh.date_id, nt.id as number_type_id, 
    nt.number_type_name , ld.numbers  , count(ld.numbers) as num_player,
    sum(ld.price) as total_price, 
    sum(
      case 
        when ld.status in (8) then ld.win_price 
        else 0
      end
    ) as total_pay
    from lotto_detail ld 
    inner join lotto_header lh on lh.id = ld.lotto_header_id 
    inner join lotto l on l.id = lh.lotto_id 
    inner join number_type nt on nt.id = ld.number_type_id 
    where l.id = '${lotto_id}' and lh.category_id = ${category_id} and lh.date_id = '${date}' AND ld.status IN ${lotto_status} ${whereClause}
    group by l.id, lh.date_id , nt.id, nt.number_type_name , ld.numbers
    order by l.id asc, lh.date_id desc, nt.id asc, ld.numbers asc`;

    // let result = await db_access.query(
    //   `SELECT * FROM (${sql}) rs ${pagination}`
    // );
    let result = await db_access.query(
      `SELECT * FROM (${sql}) rs`
    );
    let count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    let total_result =
      await db_access.query(`SELECT SUM(rs.num_player) AS  total_num_player, 
    SUM(rs.total_price) as total_all_price, SUM(rs.total_pay) as total_all_pay FROM (${sql}) rs`);
    const total = {
      total_num_player: 0,
      total_all_price: 0.0,
      total_all_pay: 0.0,
    };
    if (total_result.rowCount > 0) {
      total.total_num_player = parseInt(
        utilities.defaultValue(
          () => total_result.rows[0].total_num_player,
          0.0
        ) ?? 0.0
      );
      total.total_all_price = parseFloat(
        utilities.defaultValue(
          () => total_result.rows[0].total_all_price,
          0.0
        ) ?? 0.0
      );
      total.total_all_pay = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_pay, 0.0) ??
          0.0
      );
    }
    response_success_builder(res, {
      rows: result.rows,
      total: total,
      total_record: parseInt(
        utilities.defaultValue(() => count_result.rows[0].total_record, 0)
      ),
    });
  } catch (error) {
    manage_error(error);
  }
};

const _getResultHuayHung = async (res, query) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];

    const filedValidator = ["date", "category_id", "lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }
    // await request_validator(res, query, );

    const date = query.date;
    const search = query.search;
    const number_type_id = query.number_type_id;
    const category_id = query.category_id;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    let pagination = "";
    if (search) {
      whereClause += ` AND (nt.number_type_name ilike '%${search}%' OR CONCAT(ld.numbers, '') ilike '%${search}%')`;
    }

    if (number_type_id) {
      whereClause += ` AND nt.id = '${number_type_id}'`;
    }

    if (query.limit && query.limit > 0) {
      const offset = (parseInt(query.page) - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`;
    }

    let sql = `select l.id as lotto_id, lh.date_id, nt.id as number_type_id, 
    nt.number_type_name , ld.numbers  , count(ld.numbers) as num_player,
    sum(ld.price) as total_price, 
    sum(
      case 
        when ld.status in (8) then ld.win_price 
        else 0
      end
    ) as total_pay
    from lotto_detail ld 
    inner join lotto_header lh on lh.id = ld.lotto_header_id 
    inner join lotto l on l.id = lh.lotto_id 
    inner join number_type nt on nt.id = ld.number_type_id 
    where l.id = '${lotto_id}' and lh.category_id = ${category_id} and lh.date_id = '${date}' AND ld.status IN ${lotto_status} ${whereClause}
    group by l.id, lh.date_id , nt.id, nt.number_type_name , ld.numbers
    order by l.id asc, lh.date_id desc, nt.id asc, ld.numbers asc`;

    // let result = await db_access.query(
    //   `SELECT * FROM (${sql}) rs ${pagination}`
    // );
    let result = await db_access.query(
      `SELECT * FROM (${sql}) rs`
    );
    let count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    let total_result =
      await db_access.query(`SELECT SUM(rs.num_player) AS  total_num_player, 
    SUM(rs.total_price) as total_all_price, SUM(rs.total_pay) as total_all_pay FROM (${sql}) rs`);
    const total = {
      total_num_player: 0,
      total_all_price: 0.0,
      total_all_pay: 0.0,
    };
    if (total_result.rowCount > 0) {
      total.total_num_player = parseInt(
        utilities.defaultValue(
          () => total_result.rows[0].total_num_player,
          0.0
        ) ?? 0.0
      );
      total.total_all_price = parseFloat(
        utilities.defaultValue(
          () => total_result.rows[0].total_all_price,
          0.0
        ) ?? 0.0
      );
      total.total_all_pay = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_pay, 0.0) ??
          0.0
      );
    }
    response_success_builder(res, {
      rows: result.rows,
      total: total,
      total_record: parseInt(
        utilities.defaultValue(() => count_result.rows[0].total_record, 0)
      ),
    });
  } catch (error) {
    manage_error(error);
  }
};

const _getResultHuayCountry = async (res, query) => {
  try {
    const lotto_type_id = lotto["HUAYHUNG"];

    const filedValidator = ["date", "category_id", "lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }
    // await request_validator(res, query, );

    const date = query.date;
    const search = query.search;
    const number_type_id = query.number_type_id;
    const category_id = query.category_id;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    let pagination = "";
    if (search) {
      whereClause += ` AND (nt.number_type_name ilike '%${search}%' OR CONCAT(ld.numbers, '') ilike '%${search}%')`;
    }

    if (number_type_id) {
      whereClause += ` AND nt.id = '${number_type_id}'`;
    }

    if (query.limit && query.limit > 0) {
      const offset = (parseInt(query.page) - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`;
    }

    let sql = `select l.id as lotto_id, lh.date_id, nt.id as number_type_id, 
    nt.number_type_name , ld.numbers  , count(ld.numbers) as num_player,
    sum(ld.price) as total_price, 
    sum(
      case 
        when ld.status in (8) then ld.win_price 
        else 0
      end
    ) as total_pay
    from lotto_detail ld 
    inner join lotto_header lh on lh.id = ld.lotto_header_id 
    inner join lotto l on l.id = lh.lotto_id 
    inner join number_type nt on nt.id = ld.number_type_id 
    where l.id = '${lotto_id}' and lh.category_id = ${category_id} and lh.date_id = '${date}' AND ld.status IN ${lotto_status} ${whereClause}
    group by l.id, lh.date_id , nt.id, nt.number_type_name , ld.numbers
    order by l.id asc, lh.date_id desc, nt.id asc, ld.numbers asc`;

    // let result = await db_access.query(
    //   `SELECT * FROM (${sql}) rs ${pagination}`
    // );
    let result = await db_access.query(
      `SELECT * FROM (${sql}) rs`
    );
    let count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    let total_result =
      await db_access.query(`SELECT SUM(rs.num_player) AS  total_num_player, 
    SUM(rs.total_price) as total_all_price, SUM(rs.total_pay) as total_all_pay FROM (${sql}) rs`);
    const total = {
      total_num_player: 0,
      total_all_price: 0.0,
      total_all_pay: 0.0,
    };
    if (total_result.rowCount > 0) {
      total.total_num_player = parseInt(
        utilities.defaultValue(
          () => total_result.rows[0].total_num_player,
          0.0
        ) ?? 0.0
      );
      total.total_all_price = parseFloat(
        utilities.defaultValue(
          () => total_result.rows[0].total_all_price,
          0.0
        ) ?? 0.0
      );
      total.total_all_pay = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_pay, 0.0) ??
          0.0
      );
    }
    response_success_builder(res, {
      rows: result.rows,
      total: total,
      total_record: parseInt(
        utilities.defaultValue(() => count_result.rows[0].total_record, 0)
      ),
    });
  } catch (error) {
    manage_error(error);
  }
};

const _getResultHuayChud = async (res, query) => {
  try {
    const lotto_type_id = lotto.HUAYCHUD;

    const filedValidator = ["date", "category_id", "lotto_id"];
    if (!check_validator_basic(query, filedValidator)) {
      return await request_validator(res, query, filedValidator);
    }
    // await request_validator(res, query, );

    const date = query.date;
    const search = query.search;
    const number_type_id = query.number_type_id;
    const category_id = query.category_id;
    const lotto_id = query.lotto_id;
    let whereClause = "";
    let pagination = "";
    if (search) {
      whereClause += ` AND (nt.number_type_name ilike '%${search}%' OR CONCAT(ld.numbers, '') ilike '%${search}%')`;
    }

    if (number_type_id) {
      whereClause += ` AND nt.id = '${number_type_id}'`;
    }

    if (query.limit && query.limit > 0) {
      const offset = (parseInt(query.page) - 1) * query.limit;
      pagination += ` LIMIT ${query.limit} OFFSET ${offset}`;
    }

    let sql = `select l.id as lotto_id, lh.date_id, nt.id as number_type_id, 
    nt.number_type_name , ld.numbers  , count(ld.numbers) as num_player,
    sum(ld.price) as total_price, 
    sum(
      case 
        when ld.status in (8) then ld.win_price 
        else 0
      end
    ) as total_pay
    from lotto_detail ld 
    inner join lotto_header lh on lh.id = ld.lotto_header_id 
    inner join lotto l on l.id = lh.lotto_id 
    inner join number_type nt on nt.id = ld.number_type_id 
    where l.id = '${lotto_id}' and lh.category_id = ${category_id} and lh.date_id = '${date}' AND ld.status IN ${lotto_status} ${whereClause}
    group by l.id, lh.date_id , nt.id, nt.number_type_name , ld.numbers
    order by l.id asc, lh.date_id desc, nt.id asc, ld.numbers asc`;

    // let result = await db_access.query(
    //   `SELECT * FROM (${sql}) rs ${pagination}`
    // );
    let result = await db_access.query(
      `SELECT * FROM (${sql}) rs`
    );
    let count_result = await db_access.query(
      `SELECT COUNT(*) AS total_record FROM (${sql}) rs`
    );
    let total_result =
      await db_access.query(`SELECT SUM(rs.num_player) AS  total_num_player, 
    SUM(rs.total_price) as total_all_price, SUM(rs.total_pay) as total_all_pay FROM (${sql}) rs`);
    const total = {
      total_num_player: 0,
      total_all_price: 0.0,
      total_all_pay: 0.0,
    };
    if (total_result.rowCount > 0) {
      total.total_num_player = parseInt(
        utilities.defaultValue(
          () => total_result.rows[0].total_num_player,
          0.0
        ) ?? 0.0
      );
      total.total_all_price = parseFloat(
        utilities.defaultValue(
          () => total_result.rows[0].total_all_price,
          0.0
        ) ?? 0.0
      );
      total.total_all_pay = parseFloat(
        utilities.defaultValue(() => total_result.rows[0].total_all_pay, 0.0) ??
          0.0
      );
    }
    response_success_builder(res, {
      rows: result.rows,
      total: total,
      total_record: parseInt(
        utilities.defaultValue(() => count_result.rows[0].total_record, 0)
      ),
    });
  } catch (error) {
    manage_error(error);
  }
};

exports.get_result_huay = async (req, res) => {
  try {
    const lotto_type_id = req.params.id;
    const getResultHuay = {
      [lotto["THAI"]]: _getResultHuayThai,
      [lotto["YIKI"]]: _getResultHuayYiki,
      [lotto["HUAYHUNG"]]: _getResultHuayHung,
      [lotto.HUAYCOUNTRY]: _getResultHuayCountry,
      [lotto.HUAYCHUD]: _getResultHuayChud,
    };

    return await getResultHuay[lotto_type_id](res, req.query);
  } catch (error) {
    return response_error_builder(res, error);
  }
};
