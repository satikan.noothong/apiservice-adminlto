const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require("bcrypt");
const moment = require("moment/moment");
const dayjs = require("dayjs");

// Retrieve listMember.
exports.get_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/listMember" }} */
  const listNote = [];
  const agentId = req.user.agent_id;
  try {
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let agentNo = "";
    let typeUser = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one ="";
    let two = "";
    // one += "SELECT";
    // one += "    m.id AS member_id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account,";
    // one += "    t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address, m.created_date, s.status_name_th,";
    // one += "    m.logged_date + INTERVAL '7 hours' AS logged_date,";
    // one += "    (SELECT COALESCE(SUM(dwa.amount), 0) FROM deposit_withdraw dwa WHERE dwa.member_id = m.id AND (dwa.trans_type_id = 2 or dwa.trans_type_id = 4) ) AS sum_withdraw,";
    // one += "    (SELECT COALESCE(SUM(dwa.amount), 0) FROM deposit_withdraw dwa WHERE dwa.member_id = m.id AND (dwa.trans_type_id = 1 or dwa.trans_type_id = 3) ) AS sum_deposit,";
    // one += "    m.advisor_id, COALESCE((SELECT SUM(lh.total_price) FROM public.lotto_header lh WHERE member_id = m.id AND lh.status IN (8,9,10)), 0) AS total_bet,";
    // one += "    m.status FROM members m FULL OUTER JOIN bank b ON m.bank_id = b.id FULL OUTER JOIN title t ON m.bank_title_id = t.id ";
    // one += "    FULL OUTER JOIN note_member nm ON nm.member_id = m.id INNER JOIN status s ON m.status = s.id ";
    // if (typeUser === 0 ) {
    //   one += "WHERE m.agent_id = " + agentId + " ";
    // } else if (typeUser === 1){
    //   one += "WHERE m.agent_id = " + agentNo + " ";
    // }
    // two += " GROUP BY m.id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account,";
    // two += " t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address, m.created_date, s.status_name_th, m.logged_date, m.advisor_id, m.status ";
    // two += " ORDER BY m.id DESC;";

    one += "SELECT";
    one += "    m.id AS member_id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account,";
    one += "    t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address, m.created_date, s.status_name_th,";
    one += "    m.logged_date + INTERVAL '7 hours' AS logged_date, COALESCE(dwa1.sum_withdraw, 0) as sum_withdraw, COALESCE(dwa2.sum_deposit, 0) as sum_deposit,";
    one += "    m.advisor_id, COALESCE(lh.sum_total_price, 0) AS total_bet,";
    one += "    m.status FROM members m FULL OUTER JOIN bank b ON m.bank_id = b.id FULL OUTER JOIN title t ON m.bank_title_id = t.id ";
    one += "    FULL OUTER JOIN note_member nm ON nm.member_id = m.id INNER JOIN status s ON m.status = s.id ";
    one += `    LEFT JOIN (
                  SELECT member_id, SUM(amount) as sum_withdraw FROM deposit_withdraw WHERE trans_type_id = 2 or trans_type_id = 4
                  group by member_id
                ) dwa1 ON dwa1.member_id = m.id
                LEFT JOIN (
                  SELECT member_id, SUM(amount) as sum_deposit FROM deposit_withdraw WHERE trans_type_id = 1 or trans_type_id = 3
                  group by member_id
                ) dwa2 ON dwa2.member_id = m.id
                LEFT JOIN (
                  SELECT lh.member_id, SUM(lh.total_price) + COALESCE(mtp.total_price, 0) as sum_total_price FROM public.lotto_header lh
                  left join members_total_price mtp on lh.member_id = mtp.member_id
                  WHERE lh.status IN (8,9,10)
                  group by lh.member_id, mtp.total_price
                ) lh ON lh.member_id = m.id `;
    if (typeUser === 0 ) {
      one += "WHERE m.agent_id = " + agentId + " ";
    } else if (typeUser === 1){
      one += "WHERE m.agent_id = " + agentNo + " ";
    }
    two += " GROUP BY m.id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account,";
    two += " t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address, m.created_date, s.status_name_th, m.logged_date, dwa1.sum_withdraw, dwa2.sum_deposit, m.advisor_id, lh.sum_total_price, m.status ";
    two += " ORDER BY m.id DESC; select * from note_member;";

    resu = await db_access.query(one + two);
    const resp = resu[0].rows;
    let member = "";
    // let resul = await db_access.query("select * from note_member;");
    // const respo = resul.rows;
    const respo = resu[1].rows;
    for (let i = 0; i<resp.length; i++){
      const dataList = {"member_id":{},"username":{},"mobile_no":{},"credit":{},"bank_name":{},"bank_url":{}, "bank_color":{},
        "bank_account":{},"title_name":{},"bank_firstname":{},"bank_lastname":{},"logged_address":{},"created_date":{}
        ,"status_name":{},"logged_date":{},"sum_withdraw":{},"sum_deposit":{},"advisor_id":{},"total_bet":{},"note_member":{}};
      dataList.member_id = resp[i].member_id;
      dataList.username = resp[i].username;
      dataList.mobile_no = resp[i].mobile_no;
      dataList.credit = resp[i].credit;
      dataList.bank_name = resp[i].bank_name;
      dataList.bank_url = resp[i].bank_url;
      dataList.bank_color = resp[i].bank_color;
      dataList.bank_account = resp[i].bank_account;
      dataList.title_name = resp[i].title_name;
      dataList.bank_firstname = resp[i].bank_firstname;
      dataList.bank_lastname = resp[i].bank_lastname;
      dataList.logged_address = resp[i].logged_address;
      dataList.created_date = resp[i].created_date;
      dataList.status_name = resp[i].status_name_th;
      dataList.logged_date = resp[i].logged_date;
      dataList.sum_withdraw = resp[i].sum_withdraw;
      dataList.sum_deposit = resp[i].sum_deposit;
      dataList.advisor_id = resp[i].advisor_id;
      dataList.total_bet = resp[i].total_bet;

      member =  dataList.member_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {

        if ( member === respo[c].member_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0){
          dataList.note_member = listNo;
        } else{
          dataList.note_member = [];
        }
      }
      listNote.push(dataList);
    }
    
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


function isNumeric(value) {
  return /^-?\d+$/.test(value);
}
// Retrieve listMember.
exports.get_search_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  const listNote = [];
  const agentId = req.user.agent_id;
  const data = req.body;
  try {
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let type_user = "";
    let message = false;
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      type_user = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let test = "มีผู้แนะนำ";
    let s = "";
    let one = "";
    let two = "";
    let orderBy = "";
    if (data.text === "" || data.text === null){
      // one += "SELECT m.id as member_id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account, t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address , m.created_date+ INTERVAL '7 hours' AS logged_date, s.status_name_th, m.logged_date + INTERVAL '7 hours' AS logged_date,COALESCE(SUM(wa.amount) filter (where trans_type_id = 2 oe trans_type_id = 4),0) as sum_withdraw , COALESCE(SUM(wa.amount) filter (where trans_type_id = 1 or trans_type_id = 3),0) as sum_deposit , m.advisor_id, COALESCE(SUM(wa.amount) filter (where trans_type_id = 1),0) as sum_deposit , m.advisor_id,COALESCE((select sum(lh.total_price) as total_bet from public.lotto_header lh where member_id = m.id and lh.status in (8,9,10)),0) as total_bet FROM members m full outer join bank b on m.bank_id = b.id full outer join title t on m.bank_title_id = t.id full outer join note_member nm  on nm.member_id  = m.id inner join status s on m.status = s.id full outer join deposit_withdraw wa on m.id = wa.member_id ";
      one += `SELECT m.id as member_id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account, t.title_name, 
      m.bank_firstname, m.bank_lastname, m.logged_address , m.created_date+ INTERVAL '7 hours' AS logged_date, s.status_name_th, 
      m.logged_date + INTERVAL '7 hours' AS logged_date,COALESCE(dwa1.sum_withdraw, 0) AS sum_withdraw, 
      COALESCE(dwa2.sum_deposit, 0) AS sum_deposit, m.advisor_id,
      COALESCE(lh.sum_total_price,0) as total_bet
      FROM members m 
      full outer join bank b on m.bank_id = b.id 
      full outer join title t on m.bank_title_id = t.id 
      full outer join note_member nm  on nm.member_id  = m.id 
      inner join status s on m.status = s.id 
      LEFT JOIN (
              SELECT member_id, SUM(amount) as sum_withdraw FROM deposit_withdraw WHERE trans_type_id = 2 or trans_type_id = 4
              group by member_id
            ) dwa1 ON dwa1.member_id = m.id
            LEFT JOIN (
              SELECT member_id, SUM(amount) as sum_deposit FROM deposit_withdraw WHERE trans_type_id = 1 or trans_type_id = 3
              group by member_id
            ) dwa2 ON dwa2.member_id = m.id
            LEFT JOIN (
              SELECT lh.member_id, SUM(lh.total_price) + COALESCE(mtp.total_price, 0) as sum_total_price FROM public.lotto_header lh
              left join members_total_price mtp on lh.member_id = mtp.member_id
              WHERE lh.status IN (8,9,10)
              group by lh.member_id, mtp.total_price
            ) lh ON lh.member_id = m.id `;
      if (type_user === 0) {
        one += " where m.agent_id=" + agentId ;
      } else if (type_user === 1) {
        one += " where m.agent_id=" + agentNo ;
      }
      one += " GROUP by m.id, m.username, m.mobile_no, m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account, t.title_name, m.bank_firstname, m.bank_lastname, m.logged_address, m.created_date, s.status_name_th, m.logged_date, dwa1.sum_withdraw, dwa2.sum_deposit, m.advisor_id, lh.sum_total_price order by m.created_date desc; select * from note_member;";

    } else {
      const substr = data.text.split("").sort();
      let len = data.text.length;
      for (let i = 0; i < test.length; i++) {
        s = test.substring(i, len).toString();
        const ss = Array.from(s).sort();
        for (let c = 0; c < ss.length; c++) {
          for (let j = 0; j < substr.length; j++) {
            if (ss.join("") === substr.join("")) {
              message = true;
              break;
            }
          }
          if (len < test.length) {
            len++;
          }
        }
      }
      if (message === true) {
        if (data.text === "มีผู้แนะนำ") {
          two += ` m.advisor_id = 1  `;
        } else {
          two += ` ( CAST(m.id AS text) ilike '%${data.text}%' OR m.username ilike '%${data.text}%' OR m.mobile_no ilike '%${data.text}%' OR CAST(m.credit AS text) ilike '%${data.text}%' OR b.bank_name ilike '%${data.text}%'
      OR m.bank_account ilike '%${data.text}%' OR t.title_name ilike '%${data.text}%' OR m.bank_firstname ilike '%${data.text}%' OR m.bank_lastname ilike '%${data.text}%' OR CAST(m.logged_address AS text) ilike '%${data.text}%'
      OR CAST(m.created_date AS text) ilike '%${data.text}%' OR s.status_name_th ilike '%${data.text}%' OR CAST(m.logged_date AS text) ilike '%${data.text}%' OR CAST(sv.sum_withdraw AS text) ilike '%${data.text}%'
      OR CAST(sv.sum_deposit AS text) ilike '%${data.text}%' OR CAST(sv.total_bet AS text) ilike '%${data.text}%' OR CONCAT(t.title_name, m.bank_firstname, m.bank_lastname) ilike CONCAT('%', REPLACE('${data.text}', ' ', '%'), '%') AND m.advisor_id = 0) `;
        }
      } else {
        two += ` ( CAST(m.id AS text) ilike '%${data.text}%' OR m.username ilike '%${data.text}%' OR m.mobile_no ilike '%${data.text}%' OR CAST(m.credit AS text) ilike '%${data.text}%' OR b.bank_name ilike '%${data.text}%'
      OR m.bank_account ilike '%${data.text}%' OR t.title_name ilike '%${data.text}%' OR m.bank_firstname ilike '%${data.text}%' OR m.bank_lastname ilike '%${data.text}%' OR CAST(m.logged_address AS text) ilike '%${data.text}%'
      OR CAST(m.created_date AS text) ilike '%${data.text}%' OR s.status_name_th ilike '%${data.text}%' OR CAST(m.logged_date AS text) ilike '%${data.text}%' OR CAST(sv.sum_withdraw AS text) ilike '%${data.text}%'
      OR CAST(sv.sum_deposit AS text) ilike '%${data.text}%' OR CAST(sv.total_bet AS text) ilike '%${data.text}%' OR CONCAT(t.title_name, m.bank_firstname, m.bank_lastname) ilike CONCAT('%', REPLACE('${data.text}', ' ', '%'), '%') ) `;
      }
      let type_userCondition = '';

      if (type_user === 0) {
        type_userCondition = `m.agent_id = '${agentId}' AND ${two}`;
      } else if (type_user === 1) {
        type_userCondition = `m.agent_id = '${agentNo}' AND ${two}`;
      } else {
        type_userCondition = two;
      }
      // one += `WITH sum_values AS ( 
      //     SELECT
      //     m.id AS member_id,COALESCE(SUM(wa.amount) FILTER (WHERE trans_type_id = 2 or trans_type_id = 4), 0) AS sum_withdraw, 
      //     COALESCE(SUM(wa.amount) FILTER (WHERE trans_type_id = 1 or trans_type_id = 3), 0) AS sum_deposit, 
      //     COALESCE((select sum(lh.total_price) as total_bet from public.lotto_header lh where member_id = m.id and lh.status in (8,9,10)),0) as total_bet
      //     FROM 
      //     members m 
      //     FULL OUTER JOIN deposit_withdraw wa ON m.id = wa.member_id 
      //     GROUP BY m.id 
      // ) 
      // SELECT DISTINCT m.id AS member_id, m.username, m.mobile_no,m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account, t.title_name, m.bank_firstname, m.bank_lastname, 
      // m.logged_address, m.created_date, s.status_name_th, m.logged_date + INTERVAL '7 hours' AS logged_date, m.advisor_id, sv.sum_withdraw, sv.sum_deposit, sv.total_bet 
      // FROM members m FULL OUTER JOIN bank b ON m.bank_id = b.id FULL OUTER JOIN title t ON m.bank_title_id = t.id FULL OUTER JOIN note_member nm ON nm.member_id = m.id 
      // INNER JOIN status s ON m.status = s.id INNER JOIN sum_values sv ON m.id = sv.member_id where ${type_userCondition} ORDER BY m.id DESC; `;

      one += `WITH sum_values AS ( 
        SELECT m.id AS member_id,COALESCE(dwa1.sum_withdraw, 0) AS sum_withdraw, 
        COALESCE(dwa2.sum_deposit, 0) AS sum_deposit, 
        COALESCE(lh.sum_total_price,0) as total_bet
        FROM 
        members m 
        LEFT JOIN (
                SELECT member_id, SUM(amount) as sum_withdraw FROM deposit_withdraw WHERE trans_type_id = 2 or trans_type_id = 4
                group by member_id
              ) dwa1 ON dwa1.member_id = m.id
              LEFT JOIN (
                SELECT member_id, SUM(amount) as sum_deposit FROM deposit_withdraw WHERE trans_type_id = 1 or trans_type_id = 3
                group by member_id
              ) dwa2 ON dwa2.member_id = m.id
              LEFT JOIN (
                SELECT lh.member_id, SUM(lh.total_price) + COALESCE(mtp.total_price, 0) as sum_total_price FROM public.lotto_header lh
                left join members_total_price mtp on lh.member_id = mtp.member_id
                WHERE lh.status IN (8,9,10)
                group by lh.member_id, mtp.total_price
              ) lh ON lh.member_id = m.id
        GROUP BY m.id, dwa1.sum_withdraw, dwa2.sum_deposit, lh.sum_total_price
    ) 
    SELECT DISTINCT m.id AS member_id, m.username, m.mobile_no,m.credit, b.bank_name, b.bank_url, b.bank_color, m.bank_account, t.title_name, m.bank_firstname, m.bank_lastname, 
    m.logged_address, m.created_date, s.status_name_th, m.logged_date + INTERVAL '7 hours' AS logged_date, m.advisor_id, sv.sum_withdraw, sv.sum_deposit, sv.total_bet 
    FROM members m FULL OUTER JOIN bank b ON m.bank_id = b.id FULL OUTER JOIN title t ON m.bank_title_id = t.id FULL OUTER JOIN note_member nm ON nm.member_id = m.id 
    INNER JOIN status s ON m.status = s.id INNER JOIN sum_values sv ON m.id = sv.member_id where ${type_userCondition} ORDER BY m.id DESC; select * from note_member;`;
    }
    resu = await db_access.query(one);
    const resp = resu[0].rows;
    let member = "";
    // let resul = await db_access.query("select * from note_member;");
    // const respo = resul.rows;
    const respo = resu[1].rows;
    for (let i = 0; i<resp.length; i++){
      const dataList = {"member_id":{},"username":{},"mobile_no":{},"credit":{},"bank_name":{},"bank_url":{}, "bank_color":{},
        "bank_account":{},"title_name":{},"bank_firstname":{},"bank_lastname":{},"logged_address":{},"created_date":{}
        ,"status_name":{},"logged_date":{},"sum_withdraw":{},"sum_deposit":{},"advisor_id":{},"total_bet":{},"note_member":{}};
      dataList.member_id = resp[i].member_id;
      dataList.username = resp[i].username;
      dataList.mobile_no = resp[i].mobile_no;
      dataList.credit = resp[i].credit;
      dataList.bank_name = resp[i].bank_name;
      dataList.bank_url = resp[i].bank_url;
      dataList.bank_color = resp[i].bank_color;
      dataList.bank_account = resp[i].bank_account;
      dataList.title_name = resp[i].title_name;
      dataList.bank_firstname = resp[i].bank_firstname;
      dataList.bank_lastname = resp[i].bank_lastname;
      dataList.logged_address = resp[i].logged_address;
      dataList.created_date = resp[i].created_date;
      dataList.status_name = resp[i].status_name_th;
      dataList.logged_date = resp[i].logged_date;
      dataList.sum_withdraw = resp[i].sum_withdraw;
      dataList.sum_deposit = resp[i].sum_deposit;
      dataList.advisor_id = resp[i].advisor_id;
      dataList.total_bet = resp[i].total_bet;

      member =  dataList.member_id;

      let listNo = [];
      for (let c = 0; c < respo.length; c++) {

        if ( member === respo[c].member_id) {
          listNo.push(respo[c]);
        }
        if (listNo.length > 0){
          dataList.note_member = listNo;
        } else{
          dataList.note_member = [];
        }
      }
      listNote.push(dataList);
    }
    
    res.status(200).json({
      code: 200,
      message: "Success",
      data: listNote,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};



exports.get_list_member_by_id = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("select m.username,m.password, m.ref_username,m.email,m.mobile_no,m.birthday from members m where id ="+data+";");
    const resp = result.rows;
    const response = Object.values(resp).values().next().value;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Create a new listMember.
exports.post_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  let user = "";
  try {
    let age = "";
    const agentId = req.user.agent_id;
    let agen = "";
    let typUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    let result = await db_access.query("select count(a.username) as count from members a  where a.username = '"+data.username+"';");
    const resp = result.rows;
    for (let i = 0; i < resp.length; i++ ){
      user = resp[i].count;
    }
    if (user == 0 ) {
      if (data.password === data.confirm_password) {
        const password = await bcrypt.hash(data.password, 10);
        await db_access.query("CALL public.spinsertupdatemember($1,$2,$3,$4,$5,$6,$7,$8);", [
          0,
          utilities.is_empty_string(data.username) ? null : data.username,
          utilities.is_empty_string(data.ref_username) ? null : data.ref_username,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.mobile_no) ? null : data.mobile_no,
          utilities.is_empty_string(data.birthday) ? null : data.birthday,
          utilities.is_empty_string(password) ? null : password,
          utilities.is_empty_string(agen) ? null : agen
        ]);
        
        res.status(200).json({
          code: 200,
          message: "Success",
        });
      } else {
        console.log("error : ", "Password Not Match");
        res.status(500).send({
          code: 500,
          message: "Password Not Match",
        });
      }
    } else{
      res.status(500).send({
        code: 500,
        message: "Same Username",
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new listMember.
exports.put_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let mess = "";
  let ref_username = "";
  let pass = "";
  let email = "";
  let mobile_no = "";
  let birthday = "";
  try {
    let age = "";
    let agen = "";
    let typUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    if (data.password === null || data.password === "" || data.password === '' ) {
      await db_access.query("CALL public.spupdatemembernopass($1,$2,$3,$4,$5);", [
        utilities.is_empty_string(data.id) ? null : data.id,
        utilities.is_empty_string(data.ref_username) ? null : data.ref_username,
        utilities.is_empty_string(data.email) ? null : data.email,
        utilities.is_empty_string(data.mobile_no) ? null : data.mobile_no,
        utilities.is_empty_string(data.birthday) ? null : data.birthday
      ]);
    }else{
      if (data.password === data.confirm_password) {
        const password = await bcrypt.hash(data.password, 10);
        await db_access.query("CALL public.spupdatemember($1,$2,$3,$4,$5,$6);", [
          utilities.is_empty_string(data.id) ? null : data.id,
          utilities.is_empty_string(data.ref_username) ? null : data.ref_username,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.mobile_no) ? null : data.mobile_no,
          utilities.is_empty_string(data.birthday) ? null : data.birthday,
          utilities.is_empty_string(password) ? null : password
        ]);
        pass = "และเปลี่ยน Password";
      } else {
        console.log("error : ","Password Not Match" );
        res.status(500).send({
          code: 500,
          message: "Password Not Match",
        });
      }
    }
    let agent_id = await db_access.query("select a.username  from agent a  where a.agent_id  =" + agen + ";");
    let ag = "";
    const response = agent_id.rows;
    for (let i = 0; i < response.length; i++) {
        ag = response[i].username;
    }
    let member_id = await db_access.query("select *  from members a  where a.id  =" + data.id + ";");
    const responseMember = member_id.rows;
    for (let i = 0; i < responseMember.length; i++) {
        ref_username = responseMember[i].ref_username;
        email = responseMember[i].email;
        mobile_no = responseMember[i].mobile_no;
        birthday = responseMember[i].birthday;
       // message = "เปลี่ยนข้อมูลจาก (ชื่ออ้างอิง "+responseMember[i].ref_username+" อีเมล "+ responseMember[i].email + " เบอร์มือถือ "+responseMember[i].mobile_no + " วันเกิด "+ responseMember[i].birthday+ ") เป็น (ชื่ออ้างอิง "+data.ref_username+" อีเมล "+ data.email + " เบอร์มือถือ "+data.mobile_no + " วันเกิด "+ data.birthday+ pass+ ") โดย"+ ag;

    }
    let newDay = moment(birthday).locale("th").format("MM/DD/YYYY");
    let oldDay = moment(data.birthday).locale("th").format("MM/DD/YYYY");
    mess += "แก้ไขข้อมูลส่วนตัวจาก (" + "ชื่ออ้างอิง:" + ref_username + ", อีเมล:" + email + ", เบอร์มือถือ:" + mobile_no + ", วันเกิด:" + newDay + ")";
    mess += " เป็น (" + "ชื่ออ้างอิง:" + data.ref_username + ", อีเมล:" + data.email + ", เบอร์มือถือ:" + data.mobile_no + ", วันเกิด:" + oldDay + ")";
    let query = "INSERT INTO transaction_agent_member (member_id, agent_id, actor_id, type_log_transaction_id, detail, ip_address) VALUES (" + data.id + ", " + agen + ","+agentId+", 10, '" + mess + "', '" + data.ipAddress + "')";
    await db_access.query(query);
    
    res.status(200).json({
        code: 200,
        message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// create a new Member direct_withdraw.
exports.post_direct_withdraw = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let ip = data.ip_address.ip;
  try {
    let creds = "";
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    let text = "SELECT a.credit FROM agent a where a.agent_id ="+agen+";";
    let resultt = await db_access.query(text);
    const responseCredit = resultt.rows;
    for (let i = 0; i < responseCredit.length; i++){
       creds = responseCredit[i].credit;
    }
    let text2 = "select a.username from agent a where a.agent_id  =" + agen + ";";
    let agent_id = await db_access.query(text2);
    let ag = "";
    const response = agent_id.rows;
    for (let i = 0; i < response.length; i++) {
      ag = response[i].username;
    }
    const credits = parseFloat(creds);
    let totalUser = 0;
    let types = "";
    let message = "";
    let name = "";
    let note = "";
    let currentCredit = "";
    let credit = "";
    let amount = parseFloat(data.amount);
    let sum = null;
    let agent = await db_access.query("select m.credit from members m where m.id ="+data.member_id+";");
    let respp = agent.rows;
    let check = 0;
    for (let i = 0; i<respp.length; i++) {
      currentCredit = respp[i].credit;
      credit = parseFloat(currentCredit);
    }
      if (data.trans_type_id === 1) {
        if (credits >= amount) {
          const datas = await db_access.query("CALL public.spinsertmemberdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);", [
            0,
            utilities.is_empty_string(data.member_id) ? null : data.member_id,
            utilities.is_empty_string("3") ? null : "3",
            utilities.is_empty_string(amount) ? null : amount,
            utilities.is_empty_string(data.comment) ? null : data.comment,
            utilities.is_empty_string(currentCredit) ? null : currentCredit,
            utilities.is_empty_string(agen) ? 0 : agen,
            utilities.is_empty_string(false),
            utilities.is_empty_string(true),
            utilities.is_empty_string(agentId) ? 0 : agentId,
            utilities.is_empty_string(ip) ? null : ip,
          ]);
          const id = Object.values(datas.rows.find((a) => a.prm_id))
              .values()
              .next().value;
          sum = credit + amount;
          totalUser = credits - amount;
          await db_access.query("UPDATE members SET credit =" + sum + " WHERE id =" + data.member_id + ";");
          await db_access.query("UPDATE deposit_withdraw SET status = 4, bank_id =10 where id = " + id + ";");
          await db_access.query("UPDATE agent SET credit = " + totalUser + " where agent_id=" + agen + ";");
          types = "6";
          name = "ฝากตรง";
          check =1;

          global.io.sockets.emit('action_current_credit', {
            member_id: data.member_id,
            credit: sum
          });
        }else {
          types = "6";
          name = "ฝากตรงไม่สำเร็จ";
          note = " เนื่องจากเครดิตเอเย่นไม่เพียงพอ";
          check = 2;
        }
      } else if (data.trans_type_id === 2) {
        if (credit >= amount) {
          const datas = await db_access.query("CALL public.spinsertmemberdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);", [
            0,
            utilities.is_empty_string(data.member_id) ? null : data.member_id,
            utilities.is_empty_string("4") ? null : "4",
            utilities.is_empty_string(amount) ? null : amount,
            utilities.is_empty_string(data.comment) ? null : data.comment,
            utilities.is_empty_string(currentCredit) ? null : currentCredit,
            utilities.is_empty_string(agen) ? 0 : agen,
            utilities.is_empty_string(true),
            utilities.is_empty_string(false),
            utilities.is_empty_string(agentId) ? 0 : agentId,
            utilities.is_empty_string(ip) ? null : ip,
          ]);
          const id = Object.values(datas.rows.find((a) => a.prm_id))
              .values()
              .next().value;
          sum = credit - data.amount;
          totalUser = credits + amount;
          await db_access.query("UPDATE members SET credit =" + sum + " WHERE id =" + data.member_id + ";");
          await db_access.query("UPDATE deposit_withdraw SET status = 4, bank_id=10 where id = " + id + ";");
          await db_access.query("UPDATE agent SET credit = "+totalUser+" where agent_id="+agen+";");
          types = "7";
          name = "ถอนตรง";
          check = 1;

          global.io.sockets.emit('action_current_credit', {
            member_id: data.member_id,
            credit: sum
          });
        } else {
          types = "7";
          name = "ถอนตรงไม่สำเร็จ";
          note = "เนื่องจากเครดิตไม่เพียงพอ";
          check = 3;
        }
      }
      message = name+" จำนวน"+amount+ note + " โดย"+ ag;
      await db_access.query("INSERT INTO transaction_agent_member (member_id, agent_id, actor_id, type_log_transaction_id, detail, ip_address) values ("+data.member_id+","+agen+","+ agentId+","+types+" ,'"+message+"','"+ip+"');");
      if (check ===1){
        res.status(200).json({
          code: 200,
          message: "Success",
        });
      } else if (check === 2){
        res.status(200).json({
          code: 200,
          message: "agent not enough credit",
        });
      } else if (check === 3){
        res.status(200).json({
          code: 200,
          message: "not enough credit",
        });
      }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Delete listMember.
exports.delete_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletelistmember($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_note_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  const first = `select na.note , na.created_date, na.id  from members a inner join note_member na  on na.member_id = a.id where na.member_id  ='${data}' order by na.created_date asc ;`;
  try {
    let result = await db_access.query(first);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete user.
exports.delete_note_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  const data = req.params.id;
  try {
    await db_access.query("delete from public.note_member  where id  = "+ data + ";");
    res.status(200).json({
      code: 200,
      message: "Success"
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new NoteAgent.
exports.post_note_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let message ="";
  try {
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    if (data.note !== null || data.note !== "") {
      await db_access.query("CALL public.spinsertupdatenotemember($1,$2,$3);", [
        0,
        utilities.is_empty_string(data.member_id) ? null : data.member_id,
        utilities.is_empty_string(data.note) ? null : data.note
      ]);
    }
      let agent_id = await db_access.query("select a.username  from agent a  where a.agent_id  =" + agen + ";");
      let ag = "";
      const response = agent_id.rows;
      for (let i = 0; i < response.length; i++) {
          ag = response[i].username;
      }
    let member_id = await db_access.query("select a.username  from members a  where a.id  =" + data.member_id + ";");
    let member = "";
    const responseMember = member_id.rows;
    for (let i = 0; i < responseMember.length; i++) {
      member = responseMember[i].username;
    }
    message = "จดบันทึกให้ username:" + member + " ข้อความ: " + data.note + " โดย " + ag;
    let query = "INSERT INTO transaction_agent_member (member_id, agent_id, actor_id, type_log_transaction_id, detail, ip_address) VALUES (" + data.member_id + ","+agen+", " + agentId + ", 10, '" + message + "', '" + data.ipAddress + "')";
    await db_access.query(query);

      res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Change status user.
exports.put_status_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  const agentId = req.user.agent_id;
  let message = "";
  try {
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    await db_access.query("CALL public.spupdatestatusmember($1,$2);", [
      utilities.is_empty_string(data.member_id) ? null : data.member_id,
      utilities.is_empty_string(data.status) ? null : data.status,
    ]);
      let agent_id = await db_access.query("select a.username  from agent a  where a.agent_id  =" + agen + ";");
      let ag = "";
      const response = agent_id.rows;
      for (let i = 0; i < response.length; i++) {
          ag = response[i].username;
      }
    let member_id = await db_access.query("select a.username, logged_address as ip_address, uuid from members a  where a.id  =" + data.member_id + ";");
    let member = "";
    const responseMember = member_id.rows;
    for (let i = 0; i < responseMember.length; i++) {
      member = responseMember[i].username;
    }
    message = "ระงับ username:"+member + " โดย " + ag;
    let query = "INSERT INTO transaction_agent_member (member_id, actor_id, type_log_transaction_id, detail, ip_address) VALUES (" + data.member_id + ", " + agentId + ", 10, '" + message + "', '" + data.ipAddress + "')";
    await db_access.query(query);

    if (data.status == 2) {
      global.io.sockets.emit('action_logout', {
        member_id: data.member_id,
        ip_address: responseMember[0].ip_address,
        uuid: responseMember[0].uuid
      });
    }

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ReportCredit.
exports.get_reportCreditById = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  try {
    
    const memberId = req.params.id;
    const text = "SELECT * FROM public.spget_report_transaction_member($1,$2)";
    const values = [memberId,3]
    const respo = await db_access.query(text, values);
    const respp = respo.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


//ต้องแก้
exports.get_member_by_id = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  const id = req.params.id;
  // let one = "";
  // let two = "";
  try {
    // one += "select m.id AS member_id, m.username, m.mobile_no, m.credit, m.ref_username, m.email, m.birthday, m.created_date AS account_create_date, m.logged_date + INTERVAL '7 hours' as logged_date, m.logged_address AS last_ip, a.username AS agent,";
    // one += "COALESCE(va.vip, 0) AS vip, COALESCE(SUM(CASE WHEN wa.trans_type_id = 1 THEN wa.amount ELSE 0 END), 0) AS sum_deposit,";
    // one += "COALESCE(SUM(CASE WHEN wa.trans_type_id = 2 THEN wa.amount ELSE 0 END), 0) AS sum_withdraw,";
    // one += "MAX(dw.created_date) AS date_last_deposit,"
    // one += "COALESCE((select sum(lh.total_price) as total_bet from public.lotto_header lh where member_id = m.id and lh.status in (8,9,10)),0) as total_bet,"
    // one += "COALESCE((select sum(lh.total_price) as total_bet_network from public.lotto_header lh where member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)), 0) AS total_price_network,"
    // one += "COALESCE((select sum(b.total_commission) total_commission from public.commissions a ";
    // one += "left join ( ";
    // one += " select c.id as commission_id, ((sum(lh.total_price)*c.commission_percentage)/100) as total_commission from public.lotto_header lh ";
    // one += "    left join public.lotto l on lh.lotto_id = l.id ";
    // one += "    left join public.commissions c on l.commission_id = c.id ";
    // one += "    where member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)";
    // one += "    group by c.id ";
    // one += ") as b on a.id = b.commission_id ";
    // one += "where a.status = 1";
    // one += "), 0 ) AS cumulative_income,";
    // two += "COALESCE((select credit as current_revenue from public.members_affiliate ma where ma.member_id = m.id), 0) AS current_credit,"
    // two += "COALESCE(m2.id, 0) AS recommender, COALESCE(m2.username, null) AS recommender_name ";
    // two += "FROM members m INNER JOIN agent a ON a.agent_id = m.agent_id left join members m2 on m2.id = m.advisor_id  ";
    // two += "LEFT JOIN ( SELECT member_id, MAX(created_date) AS created_date FROM deposit_withdraw WHERE trans_type_id = 1 GROUP BY member_id) dw ON dw.member_id = m.id ";
    // two += "LEFT JOIN vip_accepted va ON va.member_id = m.id LEFT JOIN deposit_withdraw wa ON wa.member_id = m.id LEFT JOIN lotto_header lhex ON lhex.member_id = m.id ";
    // two += "LEFT JOIN members mex ON lhex.member_id = mex.id AND mex.advisor_id = m.id ";
    // two += "LEFT JOIN lotto l ON l.id = lhex.lotto_id LEFT JOIN commissions c ON c.id = l.commission_id LEFT JOIN agent ON agent.agent_id = m.advisor_id ";
    // two += "WHERE m.id ="+id+" GROUP by  m.id,m.username,m2.id,m2.username ,m.mobile_no, m.credit, m.ref_username, m.email, m.birthday, m.created_date,m.logged_date,m.logged_address, a.username, va.vip,agent.agent_id, agent.username order by va.vip desc;";   
    // let result = await db_access.query(one + two) ;

    // Updated by Nueng 02/08/2023
    let str_query = `select m.id AS member_id, m.username, m.mobile_no, m.credit, m.ref_username, m.email, m.birthday, m.created_date AS account_create_date, m.logged_date + INTERVAL '7 hours' as logged_date, m.logged_address AS last_ip, a.username AS agent,
    COALESCE((select vip from vip_accepted where member_id = m.id order by vip desc limit 1), 0) AS vip, COALESCE(SUM(CASE WHEN wa.trans_type_id = 1 or wa.trans_type_id = 3 THEN wa.amount ELSE 0 END), 0) AS sum_deposit,
    COALESCE(SUM(CASE WHEN wa.trans_type_id = 2 OR wa.trans_type_id = 4 THEN wa.amount ELSE 0 END), 0) AS sum_withdraw,
    dw.created_date  + INTERVAL '7 hours' AS date_last_deposit,
    COALESCE((select sum(lh.total_price) + COALESCE(mtp.total_price, 0) as total_bet from public.lotto_header lh left join public.members_total_price mtp on mtp.member_id = m.id where lh.member_id = m.id and lh.status in (8,9,10) group by mtp.total_price),0) as total_bet,
    COALESCE((
    select sum(total_bet_network) as total_bet_network from (
	   select sum(lh.total_price) + coalesce(mtp.total_price, 0) as total_bet_network from public.lotto_header lh left join public.members_total_price mtp on mtp.member_id = lh.member_id 
	   where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10) group by mtp.total_price)
	as a
	), 0) AS total_price_network,
    COALESCE((select sum(b.total_commission) total_commission from public.commissions a 
    left join ( 
     select cc.commission_id, sum(total_commission) as total_commission from (
     	select c.id as commission_id, (((sum(lh.total_price) + coalesce(mtp.total_price, 0)) * c.commission_percentage)/100) as total_commission from public.lotto_header lh 
        left join public.lotto l on lh.lotto_id = l.id 
        left join public.members_total_price mtp on lh.member_id = mtp.member_id
        left join public.commissions c on l.commission_id = c.id 
        where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)
        group by c.id, mtp.total_price)
        as cc
        group by cc.commission_id
    ) as b on a.id = b.commission_id 
    where a.status = 1
    ), 0 ) AS cumulative_income,
    COALESCE((select credit as current_revenue from public.members_affiliate ma where ma.member_id = m.id), 0) AS current_credit,
    COALESCE(m2.id, 0) AS recommender, COALESCE(m2.username, null) AS recommender_name 
    FROM members m 
    INNER JOIN agent a ON a.agent_id = m.agent_id 
    left join members m2 on m2.id = m.advisor_id  
    LEFT JOIN ( SELECT member_id, created_date AS created_date FROM deposit_withdraw WHERE trans_type_id = 1 GROUP BY member_id, created_date ORDER BY created_date desc limit 1 ) dw ON dw.member_id = m.id 
    LEFT JOIN deposit_withdraw wa ON wa.member_id = m.id 
    LEFT JOIN agent ON agent.agent_id = m.advisor_id 
    WHERE m.id = $1 
    GROUP by  m.id,m.username,m2.id,m2.username ,m.mobile_no, m.credit, m.ref_username, m.email, m.birthday, m.created_date,m.logged_date,m.logged_address, a.username,agent.agent_id, agent.username,dw.created_date;`;
    let result = await db_access.query(str_query, [utilities.is_empty_string(id) ? 0 : id]) ;
    const resp = result.rows;
    const data = Object.values(resp).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: data,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_vipById = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  try {
    const memberId = req.params.id;
    const first = `select va.accepted_date, vr.vip from members m inner join vip_accepted va on va.member_id = m.id inner join vip_rang vr on vr.id =va.vip where m.id ='${memberId}' order by va.accepted_date asc;`;
    let resu = await db_access.query(first);
    const respp = resu.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_bankById = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  try {
    const memberId = req.params.id;
    const first = `select m.id ,m.bank_id as bank_id, m.bank_account, b.bank_name, t.title_name, m.bank_firstname, m.bank_lastname, m.remark as note from members m inner join bank b on b.id = m.bank_id inner join title t on t.id = m.bank_title_id  where m.id ='${memberId}';`;
    let resu = await db_access.query(first);
    const respp = resu.rows;
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: respp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// กำลังทำ
// Update a new listMember.
exports.put_bank_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;
  // str.substring(0, str.indexOf(' ')); // หน้า
  // str.substring(str.indexOf(' ') + 1); // หลัง
  try {
    const agentId = req.user.agent_id;
    let title = data.name.substring(0, data.name.indexOf(' '));
    let ff = data.name.substring(data.name.indexOf(' ') +1);
    let firstName = ff.substring(0,ff.indexOf(' '));
    let lastName = ff.substring(ff.indexOf(' ') + 1);
    let bank_title_id = "";
    let bank_name = ""
    let message = "";
    let bank_account = "";
    let bank_firstname = "";
    let bank_lastname = "";
    let title_name = "";
    let bank = "";
    let agent = "";
    if ("นาย" === title){
      bank_title_id = 1;
    } else if( "นาง" === title){
      bank_title_id = 2;
    } else{
      bank_title_id = 3;
    }
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    let resul = await db_access.query("SELECT b.bank_name, m.bank_account, m.bank_firstname, m.bank_lastname, t.title_name FROM members m  inner join bank b on b.id= m.bank_id inner join title t on t.id = m.bank_title_id where m.id ="+data.id+";");
    const responseMember = resul.rows;
    for (let i = 0; i < responseMember.length; i++) {
      bank_name = responseMember[i].bank_name;
      bank_account = responseMember[i].bank_account;
      bank_firstname = responseMember[i].bank_firstname;
      bank_lastname = responseMember[i].bank_lastname;
      title_name = responseMember[i].title_name;
    }
    let resulAg = await db_access.query("SELECT a.username FROM agent a where a.agent_id ="+agen+";");
    const responseAgent = resulAg.rows;
    for (let i = 0; i < responseAgent.length; i++) {
      agent = responseAgent[i].username
    }
    await db_access.query("CALL public.spupdatebankmembers($1,$2,$3,$4,$5,$6,$7);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(bank_title_id) ? null : bank_title_id,
      utilities.is_empty_string(firstName) ? null : firstName,
      utilities.is_empty_string(lastName) ? null : lastName,
      utilities.is_empty_string(data.bank_account) ? null : data.bank_account,
      utilities.is_empty_string(data.note) ? null : data.note,
    ]);

    await db_access.query("CALL public.spinsertupdatenotemember($1,$2,$3);", [
      0,
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.note) ? null : data.note
    ]);
    if (data.bank_id === 1){
      bank = "ไทยพาณิชย์";
    } else if (data.bank_id === 2){
      bank = "กสิกรไทย";
    } else if (data.bank_id === 3){
      bank = "ทีเอ็มบีธนชาต";
    } else if (data.bank_id === 4){
      bank = "กรุงศรี";
    } else if (data.bank_id === 5){
      bank = "กรุงไทย";
    } else if (data.bank_id === 6){
      bank = "กรุงเทพ";
    } else if (data.bank_id === 7){
      bank = "ยูโอบี";
    } else if (data.bank_id === 8){
      bank = "ออมสิน";
    } else if (data.bank_id === 9){
      bank = "ธกส";
    }
    message = "แก้ไขบัญชีธนาคารจาก ("+title_name +" "+bank_firstname + " "+ bank_lastname+" ธนาคาร"+ bank_name+" เลขที่บัญชี"+ bank_account+") เป็น ("+title +" "+firstName + " "+ lastName+" ธนาคาร"+ bank+" เลขที่บัญชี"+ data.bank_account+") โดย"+agentId +" หมายเหตุ"+data.note;
    await db_access.query("INSERT INTO transaction_agent_member (agent_id,actor_id, member_id, type_log_transaction_id, detail, ip_address) values ("+agen+","+agentId+","+ data.id+", 3,'"+message+"','"+data.ipAddress+"');");

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve ListDepositWithdraw.
exports.get_list_lotto = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  try {
    let list = [];
    let one = "";
    let today = dayjs().tz("Asia/Bangkok").format("YYYY-MM-DD");
    // one += `with dates( id, idate ) as 
    //      ( select id, d::date
    //         from ( select distinct  id 
    //                 from lotto
    //              ) u 
    //          join generate_series('${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' - INTERVAL '6 days' 
    //                              ,'${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours'    
    //                              , interval '1 day'
    //                              ) gs(d)
    //            on true  
    //       )
    // select COALESCE((select SUM(total_price) from lotto_header where member_id ='${data}' and CAST(created_date + INTERVAL '7 hours' AS DATE) = CAST(d.idate AS DATE) and lotto_id = d.id and status !=11),0) as cost,
    // d.id as lotto_id , l.lotto_name as lotto_name ,${data} as member_id , CAST(d.idate AS DATE) as created_date 
    // from dates d left join lotto l on (l.id = d.id) group by d.id , CAST(d.idate AS DATE) , l.lotto_name order by CAST(d.idate AS DATE) desc , d.id ;`;

    one += `with dates( id, idate ) as 
         ( select id, d::date
            from ( select distinct  id 
                    from lotto
                 ) u 
             join generate_series('${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours' - INTERVAL '6 days' 
                                 ,'${today}' AT TIME ZONE 'Asia/Bangkok' - INTERVAL '7 hours'    
                                 , interval '1 day'
                                 ) gs(d)
               on true  
          )
    select COALESCE(lh.sum_total_price,0) as cost,
    d.id as lotto_id , l.lotto_name as lotto_name ,${data} as member_id , CAST(d.idate AS DATE) as created_date 
    from dates d 
    left join lotto l on (l.id = d.id) 
    LEFT JOIN (
    	SELECT lotto_id, CAST(created_date + INTERVAL '7 hours' AS DATE) as created_date, SUM(total_price) as sum_total_price FROM public.lotto_header
    	where member_id = ${data} and status != 11
    	group by lotto_id, CAST(created_date + INTERVAL '7 hours' AS DATE)
    ) lh ON lh.created_date = CAST(d.idate AS DATE) and lh.lotto_id = d.id
    group by lh.sum_total_price, d.id , CAST(d.idate AS DATE) , l.lotto_name order by CAST(d.idate AS DATE) desc , d.id ;`;
    let result = await db_access.query(one);
    const resp = result.rows;
    if (resp != null) {
      for (let i = 0; i < resp.length; i++) {
        const data = {"cost": {}, "lotto_id": {}, "lotto_name": {}, "member_id": {}, "date": {}};
        const date = resp[i].created_date;
        const cost = resp[i].cost;
        const lotto_id = resp[i].lotto_id;
        const lotto_name = resp[i].lotto_name;
        const member_id = resp[i].member_id;
        let day = moment(date).locale("th").format('dddd');// พุธ
        const dayMonth = moment(date).locale("th").format("Do MMM");
        if ("จันทร์" === day){
          day = " (จ.)";
        } else if ("อังคาร" === day){
          day = " (อ.)";
        }else if ("พุธ" === day){
          day = " (พ.)";
        }else if ("พฤหัสบดี" === day){
          day = " (พฤ.)";
        }else if ("ศุกร์" === day){
          day = " (ศ.)";
        }else if ("เสาร์" === day){
          day = " (ส.)";
        }else if ("อาทิตย์" === day){
          day = " (อา.)";
        }
        const createDate = dayMonth +day;
        data.date = createDate;
        data.cost = cost;
        data.lotto_id = lotto_id;
        data.lotto_name = lotto_name;
        data.member_id = member_id;
        list.push(data);
      }
      res.status(200).json({
        code: 200,
        message: "Success",
        data: list,
      });
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// get history member.
exports.get_history_action = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("select ta.id , mt.type_transaction_name as transaction_type , ta.detail as comment ,ta.ip_address , ta.created_date  from transaction_agent_member ta inner join members m on m.id =ta.member_id  inner join master_type_transaction mt on mt.id = ta.type_log_transaction_id  where ta.member_id ="+data+" order by ta.created_date desc;");
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// get history member.
exports.get_list_huay_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  try {
    let one = ` select lh.id, m.username, (select SUM(lds.price) from lotto_detail lds inner join lotto_header mex  on lds.lotto_header_id  = mex.id where lds.lotto_header_id  = lh.id and lh.member_id  = mex.member_id) as total,
    lh.total_win, s.status_name_th, mc.category_name as lotto_name, l.id as round_number , lh.updated_date + INTERVAL '7 hours' as created_date , lh.ip_address from lotto_header lh 
    inner join members m on m.id =lh .member_id inner join status s on s.id = lh.status inner join lotto l on l.id = lh.lotto_id inner join mapping_category mc on mc.category_id = lh.category_id where lh.member_id='${data}'
    group by lh.id, m.username ,lh.total_win, s.status_name_th, l.lotto_name, l.id , lh.updated_date, mc.category_name order by lh.updated_date DESC `;
    let result = await db_access.query(one);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// get history member.
exports.get_list_huay_member_by_agent = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.body;
  const lottoId = data.lotto_id;
  const dateId = data.date_id;
  const categoryId = data.category_id;
  try {
    const date = " and DATE(lh.date_id) ='"+dateId+"' ";
      const two = `SELECT lh.id, m.username, 
                    SUM(lds.price) AS total, 
                    lh.total_win, 
                    s.status_name_th, 
                    l.lotto_name, 
                    l.id AS round_number, 
                    lh.updated_date + INTERVAL '7 hours' as created_date, 
                    lh.ip_address 
            FROM lotto_header lh 
            INNER JOIN members m ON m.id = lh.member_id 
            INNER JOIN status s ON s.id = lh.status 
            INNER JOIN lotto l ON l.id = lh.lotto_id 
            INNER JOIN agent a ON a.agent_id = m.agent_id 
            INNER JOIN lotto_detail lds ON lds.lotto_header_id = lh.id
            WHERE lh.lotto_id = $1 ${date} AND lh.category_id = $2
            GROUP BY lh.id, m.username, lh.total_win, s.status_name_th, l.lotto_name, l.id, lh.updated_date 
            ORDER BY lh.updated_date DESC`;

      const result = await db_access.query(two, [ lottoId,categoryId]);
      const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp
    });

  } catch (error) {
    console.error("An error occurred:", error);
    res.status(500).json({
      code: 500,
      message: "Internal Server Error",
      error: error.message
    });
  }
};

// get history member.
exports.get_search_list_huay_member_by_agent = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.body;
  const list = [];
  const lottoId = data.lotto_id;
  const categoryId = data.category_id;
  try {
    let date = "";
    if (data.date === "" || data.date === null || data.date === undefined){
      date = "";
    } else {
      if (data.date === "1") {
        date = " and DATE(lh.date_id) = current_date ";
      } else if (data.date === "0") {
        date = " and DATE(lh.date_id) > (NOW() - INTERVAL '7 DAY') ";
      }
    }
      const two = `SELECT lh.id, m.username, 
                    SUM(lds.price) AS total, 
                    lh.total_win, 
                    s.status_name_th, 
                    l.lotto_name, 
                    l.id AS round_number, 
                    lh.updated_date + INTERVAL '7 hours' as created_date, 
                    lh.ip_address 
            FROM lotto_header lh 
            INNER JOIN members m ON m.id = lh.member_id 
            INNER JOIN status s ON s.id = lh.status 
            INNER JOIN lotto l ON l.id = lh.lotto_id 
            INNER JOIN agent a ON a.agent_id = m.agent_id 
            INNER JOIN lotto_detail lds ON lds.lotto_header_id = lh.id
            WHERE lh.lotto_id = $1 ${date} AND lh.category_id = $2
            GROUP BY lh.id, m.username, lh.total_win, s.status_name_th, l.lotto_name, l.id, lh.updated_date 
            ORDER BY lh.updated_date DESC`;

      const result = await db_access.query(two, [ lottoId,categoryId]);
      const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};



// get history member.
exports.get_search_list_huay_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const memberId = req.params.id;
  const data = req.body;
  let one = "";
  let lotto = "";
  let date = "";
  try {

    if (data.lotto === "" || data.lotto === null) {
      lotto = "";
    } else {
      const two = `select l.id from lotto l where l.lotto_name='${data.lotto}' `;
      const lottoName = await db_access.query(two);
      const responseLottoId = lottoName.rows.map(item => item.id);
      lotto += ` and l.id IN (${responseLottoId.join(', ')}) `;
    }

    if (data.date === "" || data.date === null) {
      date += "";
    } else {
      let today = dayjs().tz("Asia/Bangkok").format("YYYY-MM-DD");
      if (data.date === "1" || data.date === 1) {
        date += `  and DATE(lh.updated_date + INTERVAL '7 hours') = CURRENT_DATE `;
      } else if (data.date === "0" || data.date === 0) {
        date += `  AND DATE(lh.updated_date + INTERVAL '7 hours') > CURRENT_DATE - INTERVAL '8 DAY' AND DATE(lh.updated_date + INTERVAL '7 hours') < CURRENT_DATE `;
      }
    }

    const conditions = (data.lotto === "" || data.lotto === null) && (data.date === "" || data.date === null)
        ? `where lh.member_id='${memberId}'`
        : `where lh.member_id='${memberId}' ${lotto} ${date}`;

    const one = `select lh.id, m.username, (select SUM(lds.price) from lotto_detail lds inner join lotto_header mex  on lds.lotto_header_id  = mex.id where lds.lotto_header_id  = lh.id and lh.member_id  = mex.member_id) as total,
    lh.total_win, s.status_name_th, mc.category_name as lotto_name, l.id as round_number , lh.updated_date + INTERVAL '7 hours' as created_date , lh.ip_address from lotto_header lh 
    inner join members m on m.id =lh .member_id inner join status s on s.id = lh.status 
    inner join lotto l on l.id = lh.lotto_id 
    inner join mapping_category mc on mc.category_id = lh.category_id ${conditions}
    group by lh.id, m.username ,lh.total_win, s.status_name_th, l.lotto_name, l.id , lh.updated_date, mc.category_name order by lh.updated_date DESC`;

    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// get history member.
exports.get_detail_huay_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
    schema: { "$ref": "#/definitions/listMember" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("select ld.result_number, nt.number_type_name, ld.numbers , ld.price , ld.rate, ld.win_price  from lotto_header lh inner join lotto_detail ld on ld.lotto_header_id = lh.id inner join number_type nt on nt.id  = ld.number_type_id  where lh.id  ="+ data + ";");
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve ReportCredit.
exports.get_reportHistoryCreditById = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  try {
    const data = req.body;
    // const first = "SELECT m.username as dealWith, a.username as actor, dw.actor_id as sub_actor_id, an.username as sub_actor, dw.amount as total, m.credit as current_credit, dw.remark, dw.comment, dw.created_date, dw.id,tt.transaction_type, dw.member_id  FROM deposit_withdraw dw inner join agent  m on m.agent_id = dw.agent_id  full outer join agent an on an.agent_id  = dw.actor_id full outer join transaction_type tt on tt.id = dw.trans_type_id inner join members a on a.id = dw.member_id  where dw.member_id =";
    // let resu = await db_access.query(first+ memberId+ " order by dw.created_date desc limit 7;");
    const text = "SELECT * FROM public.spget_report_transaction_member($1,$2)";
    const values = [data.member_id,data.type]
    const respo = await db_access.query(text, values);
    const response = respo.rows;
    if (response.length > 0) {
      response.forEach(element => {
        element.created_date = moment(element.created_date).format();
      });
    }
    // const resp = result;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve listMember.
exports.get_listMemberNetwork = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */
  try {
    const memberId = req.params.id;
    let one = `select m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date + INTERVAL '7 hours' as logged_date ,CAST(m.created_date + INTERVAL '7 hours' AS DATE) as created_date , COALESCE(SUM(CASE WHEN dw.trans_type_id = 2 OR dw.trans_type_id = 4 THEN dw.amount ELSE 0 END), 0) as sum_withdraw ,COALESCE(SUM(CASE WHEN dw.trans_type_id = 1 or dw.trans_type_id = 3 THEN dw.amount ELSE 0 END), 0) as sum_deposit ,
    COALESCE((
      select sum(lh.total_price) + coalesce(mtp.total_price, 0) as total_bet from public.lotto_header lh 
      left join public.members_total_price mtp on lh.member_id = mtp.member_id
      where lh.member_id = m.id and lh.status in (8,9,10)
      group by mtp.total_price)
      ,0) as total_bet, m.credit as current_credit,
      COALESCE((
        select sum(total_price_network) as total_price_network from (
        select SUM(lhex.total_price) + coalesce(mtp.total_price, 0) as total_price_network from lotto_header lhex 
        left join public.members_total_price mtp on lhex.member_id = mtp.member_id
        inner join members mex on lhex.member_id = mex.id and mex.advisor_id = m.id 
        where mex.advisor_id != 0 and lhex.status in (8,9,10)
        group by mtp.total_price) as aa
        ),0) as total_price_network, (select count(advisor_id) from members where advisor_id = m.id ) as total_member , m.advisor_id, 
        COALESCE((
          select sum(total_commission) as total_commission from (
        select (((sum(lhex.total_price) + coalesce(mtp.total_price, 0)) * c.commission_percentage)/100) as total_commission from lotto_header lhex 
        left join members_total_price mtp on lhex.member_id = mtp.member_id
          inner join members mex on lhex.member_id = mex.id and mex.advisor_id = m.id  
          inner join lotto l on l.id = lhex.lotto_id  
          inner join commissions c on c.id = l.commission_id 
          where mex.advisor_id != 0 and lhex.status in (8,9,10)
          group by mtp.total_price, c.commission_percentage) as cc
          ) ,0) as cumulative_income 
    from members m inner join status s   on s.id  = m.status  inner join agent a   on a.agent_id  = m.agent_id  full outer join deposit_withdraw dw  on dw.member_id = m.id where m.advisor_id='${memberId}' GROUP by CAST(m.created_date  AS DATE), m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date  order by m.created_date desc;`;
    let result = await db_access.query(one);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};




