const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve listMember.
exports.get_listMemberNetwork = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/listMember" }} */

  try {
    const agentId = req.user.agent_id;
    const type = ` select a.type_user, a.agent from agent a where a.agent_id ='${agentId}' `;
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let typeAgent = "";
    let agentNo = "";
    const limit = req.query.limit || -1;
    const page = req.query.page || 1;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }

    let pagination = "";
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset}`;
    }


    one += `
      SELECT m.id, m.username, m.mobile_no, m.credit, m.logged_address, s.status_name_th, m.logged_date + INTERVAL '7 hours' as logged_date, CAST(m.created_date AS DATE) as created_date,
      (SELECT COALESCE(SUM(dw.amount), 0) FROM deposit_withdraw dw WHERE dw.member_id = m.id AND (dw.trans_type_id = 2 or dw.trans_type_id = 4)) as sum_withdraw,
      (SELECT COALESCE(SUM(dwa.amount), 0) FROM deposit_withdraw dwa WHERE dwa.member_id = m.id AND (dwa.trans_type_id = 1 or dwa.trans_type_id = 3)) as sum_deposit,
      COALESCE((SELECT SUM(total_bet_network) FROM (
          SELECT SUM(lh.total_price) + COALESCE(mtp.total_price, 0) as total_bet_network FROM public.lotto_header lh 
          LEFT JOIN public.members_total_price mtp ON lh.member_id = mtp.member_id
          WHERE lh.member_id IN (SELECT id FROM public.members WHERE advisor_id = m.id) AND lh.status IN (8, 9, 10)
          GROUP BY mtp.total_price) as aa
        ), 0) AS total_price_network,
      COALESCE((SELECT SUM(b.total_commission) total_commission FROM public.commissions a 
          LEFT JOIN ( 
            SELECT cc.commission_id, SUM(total_commission) as total_commission FROM (
              SELECT c.id as commission_id, (((SUM(lh.total_price) + COALESCE(mtp.total_price, 0)) * c.commission_percentage)/100) as total_commission FROM public.lotto_header lh
              LEFT JOIN public.lotto l ON lh.lotto_id = l.id
              LEFT JOIN public.members_total_price mtp ON lh.member_id = mtp.member_id
              LEFT JOIN public.commissions c ON l.commission_id = c.id
              WHERE lh.member_id IN (SELECT id FROM public.members WHERE advisor_id = m.id) AND lh.status IN (8, 9, 10)
              GROUP BY c.id, mtp.total_price) as cc
            GROUP BY cc.commission_id
          ) as b ON a.id = b.commission_id 
          WHERE a.status = 1
        ), 0) AS cumulative_income,
      COALESCE((SELECT credit as current_revenue FROM public.members_affiliate ma WHERE ma.member_id = m.id), 0) AS current_credit,
      (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = m.id) as total_member, m.advisor_id 
      FROM members m 
      INNER JOIN status s ON s.id = m.status 
      INNER JOIN agent a ON a.agent_id = m.agent_id 
      FULL OUTER JOIN deposit_withdraw dw ON dw.member_id = m.id 
      ${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : ''}
      GROUP BY CAST(m.created_date AS DATE), m.id, m.username, m.mobile_no, m.credit, m.logged_address, s.status_name_th, m.logged_date 
      HAVING (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = m.id) > 0 
      ORDER BY m.created_date DESC ${pagination};`;

    let result = await db_access.query(one);
    const resp = result.rows;
    let totalCountQuery = `
     SELECT COUNT(*) AS total_records
FROM members m 
INNER JOIN status s ON s.id = m.status 
INNER JOIN agent a ON a.agent_id = m.agent_id 
FULL OUTER JOIN deposit_withdraw dw ON dw.member_id = m.id 
WHERE (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = m.id) > 0
${typeAgent === 0 ? `AND m.agent_id = ${agentId}` : typeAgent === 1 ? `AND m.agent_id = ${agentNo}` : ''};

`;

    let result_count = await db_access.query(totalCountQuery);
    const count_data = result_count.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data:resp,
      count: count_data[0].total_records
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

function isNumeric(value) {
  return /^-?\d+$/.test(value);
}

// Retrieve listMember.
exports.get_search_listMemberNetwork = async (req, res) => {
  // #swagger.tags = ['listMember']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/listMember" }} */

  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let two = "";
    let orderBy ="";
    let typeUser = "";
    let type_user = "";
    let message = false;
    let advisor = "";
    let having = "";
    let agentNo = "";
    const limit = req.query.limit || -1;
    const page = req.query.page || 1;
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let pagination = "";
    if (limit > 0 && page > 0) {
      const offset = (page - 1) * limit;
      pagination += ` LIMIT ${limit} OFFSET ${offset} `;
    }

    let test = "มีผู้แนะนำ";
    let s = "";
    if (data.text === "" || data.text === null){
      one += `
      select m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date + INTERVAL '7 hours' as logged_date,CAST(m.created_date  AS DATE) as created_date , (SELECT COALESCE(SUM(dw.amount), 0) FROM deposit_withdraw dw WHERE dw.member_id = m.id AND (dw.trans_type_id = 2 or dw.trans_type_id = 4) ) as sum_withdraw ,(SELECT COALESCE(SUM(dwa.amount), 0) FROM deposit_withdraw dwa WHERE dwa.member_id = m.id AND (dwa.trans_type_id = 1 or dwa.trans_type_id = 3) ) as sum_deposit ,
      COALESCE((
        select sum(total_bet_network) from (
        select sum(lh.total_price) + coalesce(mtp.total_price, 0) as total_bet_network from public.lotto_header lh 
        left join public.members_total_price mtp on lh.member_id = mtp.member_id
        where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)
        group by mtp.total_price) as aa
        ), 0) AS total_price_network,
      COALESCE(( 
        select sum(b.total_commission) total_commission from public.commissions a 
          left join ( 
          select cc.commission_id, sum(total_commission) as total_commission from (
            select c.id as commission_id, (((sum(lh.total_price) + coalesce(mtp.total_price, 0)) * c.commission_percentage)/100) as total_commission from public.lotto_header lh
            left join public.lotto l on lh.lotto_id = l.id
            left join public.members_total_price mtp on lh.member_id = mtp.member_id
            left join public.commissions c on l.commission_id = c.id
            where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)
            group by c.id, mtp.total_price)
          as cc
          group by cc.commission_id
          ) as b on a.id = b.commission_id 
          where a.status = 1
        ), 0 ) AS cumulative_income,
      COALESCE((select credit as current_revenue from public.members_affiliate ma where ma.member_id = m.id), 0) AS current_credit,
      (select count(advisor_id) from members where advisor_id = m.id ) as total_member , m.advisor_id 
      from members m inner join status s   on s.id  = m.status  inner join agent a   on a.agent_id  = m.agent_id  full outer join deposit_withdraw dw  on dw.member_id = m.id `;
      if (typeUser === 0) {
        one += ` where m.agent_id='${agentId}' `;
      } else  if (typeUser === 1) {
        one += ` where m.agent_id='${agentNo}' `;
      }
      one += ` GROUP by CAST(m.created_date  AS DATE), m.id,m.username , m.mobile_no , m.credit, m.logged_address, s.status_name_th, m.logged_date HAVING (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = m.id) > 0 order by m.created_date desc ${pagination}; `;
    } else {
      const substr = data.text.split("").sort();
      let len = data.text.length;
      for (let i = 0; i < test.length; i++) {
        s = test.substring(i, len).toString();
        const ss = Array.from(s).sort();
        for (let c = 0; c < ss.length; c++) {
          for (let j = 0; j < substr.length; j++) {
            if (ss.join("") === substr.join("")) {
              message = true;
              break;
            }
          }
          if (len < test.length) {
            len++;
          }
        }
      }
      if (message === true) {
        if (data.text === "มีผู้แนะนำ") {
          advisor +=  ` and subquery.advisor_id = 1 `;
        } else {
          advisor +=  ` and subquery.advisor_id = 0 `;
        }
      } else {
          advisor += ` `;
      }
      one += ` SELECT * 
      FROM ( 
      SELECT m.id, m.agent_id, m.username, m.mobile_no, m.credit, m.logged_address, s.status_name_th, m.logged_date + INTERVAL '7 hours' as logged_date, 
            CAST(m.created_date AS DATE) AS created_date, 
            (SELECT COALESCE(SUM(dw.amount), 0) FROM deposit_withdraw dw WHERE dw.member_id = m.id AND (dw.trans_type_id = 2 or dw.trans_type_id = 4) ) as sum_withdraw ,(SELECT COALESCE(SUM(dwa.amount), 0) FROM deposit_withdraw dwa WHERE dwa.member_id = m.id AND (dwa.trans_type_id = 1 or dwa.trans_type_id = 3) ) as sum_deposit , 
            COALESCE((select credit as current_revenue from public.members_affiliate ma where ma.member_id = m.id), 0) AS current_credit, 
            COALESCE((
              select sum(total_bet_network) from (
              select sum(lh.total_price) + coalesce(mtp.total_price, 0) as total_bet_network from public.lotto_header lh 
              left join public.members_total_price mtp on lh.member_id = mtp.member_id
              where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)
              group by mtp.total_price) as aa
              ), 0) AS total_price_network, 
            (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = m.id) AS total_member,
            m.advisor_id, 
            COALESCE(( 
              select sum(b.total_commission) total_commission from public.commissions a 
                left join ( 
                select cc.commission_id, sum(total_commission) as total_commission from (
                  select c.id as commission_id, (((sum(lh.total_price) + coalesce(mtp.total_price, 0)) * c.commission_percentage)/100) as total_commission from public.lotto_header lh
                  left join public.lotto l on lh.lotto_id = l.id
                  left join public.members_total_price mtp on lh.member_id = mtp.member_id
                  left join public.commissions c on l.commission_id = c.id
                  where lh.member_id in (select id from public.members where advisor_id = m.id) and lh.status in (8,9,10)
                  group by c.id, mtp.total_price)
                as cc
                group by cc.commission_id
                ) as b on a.id = b.commission_id 
                where a.status = 1
              ), 0 ) AS cumulative_income
        FROM members m 
        INNER JOIN status s ON s.id = m.status INNER JOIN agent a ON a.agent_id = m.agent_id FULL OUTER JOIN deposit_withdraw dw ON dw.member_id = m.id 
        GROUP BY CAST(m.created_date AS DATE), m.id, m.agent_id, m.username, m.mobile_no, m.credit, m.logged_address, s.status_name_th, m.logged_date 
      ) AS subquery `;
      two += `  ( CAST(subquery.id AS TEXT) ilike '%${data.text}%' 
          OR CAST(subquery.username AS TEXT) ilike '%${data.text}%' 
          OR CAST(subquery.mobile_no AS TEXT) ilike '%${data.text}%' 
          OR CAST(subquery.credit AS TEXT) ilike '%${data.text}%' 
          OR CAST(subquery.logged_address AS TEXT) ilike '%${data.text}%'
          OR subquery.status_name_th ilike '%${data.text}%'
          OR CAST(subquery.logged_date AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.created_date AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.advisor_id AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.sum_withdraw AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.sum_deposit AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.total_price_network AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.total_member AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.current_credit AS TEXT) ilike '%${data.text}%'
          OR CAST(subquery.cumulative_income AS TEXT) ilike '%${data.text}%' ) 
          AND (SELECT COUNT(advisor_id) FROM members WHERE advisor_id = subquery.id) > 0 `;
      orderBy += " ORDER BY subquery.created_date DESC ";
      if (typeUser === 0) {
        one += ` where subquery.agent_id ='${agentId}' and ${two} ${advisor} ${orderBy} ${pagination}`;
      } else if (typeUser === 1) {
        one += ` where subquery.agent_id ='${agentNo}' and  ${two} ${advisor} ${orderBy} ${pagination}`;
      } else if (typeUser === 2 || typeUser === 3) {
        one +=  ` where ${two} ${advisor} ${orderBy} ${pagination} `;
      }
    }
    let result = await db_access.query(one);
    const resp = result.rows;
    let totalCountQuery = `
SELECT COUNT(*) as total_records FROM (
    ${one.replace(pagination, '')}
  ) AS count_subquery
`;

    let result_count = await db_access.query(totalCountQuery);
    const count_data = result_count.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
      count: count_data[0].total_records
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new listMember.
exports.post_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatelistmember($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$21);", [
      0,
      utilities.is_empty_string(data.list_member_no) ? null : data.list_member_no,
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.agent_name) ? null : data.agent_name,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.credit) ? null : data.credit,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
      utilities.is_empty_string(data.deposit) ? null : data.deposit,
      utilities.is_empty_string(data.withdraw) ? null : data.withdraw,
      utilities.is_empty_string(data.total_bet) ? null : data.total_bet,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.last_access) ? null : data.last_access,
      utilities.is_empty_string(data.create_date) ? null : data.create_date,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.created_by) ? null : data.created_by,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new listMember.
exports.put_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/listMember' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatelistmember($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.list_member_no) ? null : data.list_member_no,
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.agent_name) ? null : data.agent_name,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.credit) ? null : data.credit,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(data.bank_name) ? null : data.bank_name,
      utilities.is_empty_string(data.deposit) ? null : data.deposit,
      utilities.is_empty_string(data.withdraw) ? null : data.withdraw,
      utilities.is_empty_string(data.total_bet) ? null : data.total_bet,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.last_access) ? null : data.last_access,
      utilities.is_empty_string(data.create_date) ? null : data.create_date,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.created_by) ? null : data.created_by,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete listMember.
exports.delete_list_member = async (req, res) => {
  // #swagger.tags = ['listMember']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletelistmember($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
