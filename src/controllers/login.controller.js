const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const crypto = require("node:crypto");
const { v4: uuidv4 } = require('uuid');
const config = require("../config/config");

// Login
exports.post_login = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Login' }
  } */
  const data = req.body;
  let status_login = false;
  let access_token;
  let refresh_token;

  try {
    let result = await db_access.query(
      "SELECT agent_id, username, password FROM public.agent WHERE status=1 AND lower(username)=lower($1);",
      [utilities.is_empty_string(data.username) ? null : data.username]
    );

    if (result.rows.length > 0) {
      status_login = await comparePassword(
        data.password,
        result.rows[0].password
      );
      if (!status_login) throw "รหัสผ่านไม่ถูกต้อง";
      else {
        const uuid = uuidv4();
        await db_access.query(
          "CALL public.spUpdateIPAddressAgent($1,$2,$3);",
          [
            utilities.is_empty_string(result.rows[0].agent_id) ? 0 : result.rows[0].agent_id,
            utilities.is_empty_string(data.ip_address?.ip) ? null : data.ip_address?.ip,
            uuid
          ]
        );

        access_token = await jwtAccessTokenGenerate(
          result.rows[0].username,
          result.rows[0].agent_id,
          uuid
        );

        refresh_token = await jwtRefreshTokenGenerate(
          result.rows[0].username,
          result.rows[0].agent_id,
          uuid
        );
      }
    } else throw "ไม่พบบัญชีผู้ใช้งานในระบบ";

    /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ResultLogin" }} */
    res.status(200).json({
      code: 200,
      message: "Success",
      data: {
        access_token: access_token,
        refresh_token: refresh_token,
      },
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve users.
exports.get_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/User" }} */
  let result = await db_access.query("SELECT * FROM public.agent;");
  const { username, password } = req.body;
  try {
    if (!username || !password) {
      return res.render('register', { message: 'Please try again' });
    }

    const user = await User.findOne({
      username
    });

    if (user) {
      const isCorrect = bcrypt.compareSync(password, user.password);

      if (isCorrect) {
        return res.render('index', { user });
      } else {
        return res.render('login', { message: 'Username or Password incorrect' });
      }
    } else {
      return res.render('login', { message: 'Username does not exist.' });
    }

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new user.
exports.post_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/User' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdateuser($1,$2,$3);", [
      0,
      utilities.is_empty_string(data.firstname) ? null : data.firstname,
      utilities.is_empty_string(data.lastname) ? null : data.lastname,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new user.
exports.put_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/User' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdateuser($1,$2,$3);", [
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.firstname) ? null : data.firstname,
      utilities.is_empty_string(data.lastname) ? null : data.lastname,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete user.
exports.delete_user = async (req, res) => {
  // #swagger.tags = ['Master']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeleteuser($1);", [
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

async function comparePassword(plaintextPassword, hash) {
  const result = await bcrypt.compare(plaintextPassword, hash);
  return result;
}

const jwtAccessTokenGenerate = async (username, agent_id, uuid) => {
  const accessToken = jwt.sign(
    { username: username, agent_id: agent_id, uuid: uuid },
    config.access_secret,
    { expiresIn: config.jwt_expire, algorithm: "HS256" }
  );

  return accessToken;
};

const jwtRefreshTokenGenerate = async (username, agent_id, uuid) => {
  const refreshToken = jwt.sign(
    { username: username, agent_id: agent_id, uuid: uuid },
    config.refresh_secret,
    { expiresIn: config.refresh_token_expire, algorithm: "HS256" }
  );

  return refreshToken;
};
