const db_access = require('../db_access');
const { response_error_builder, response_success_builder } = require('../utils/response-builder/response-utils');
const utilities = require('../utils/utilities');

// Retrieve users.
exports.get_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/User" }} */

  try {
    let result = await db_access.query('SELECT * FROM public.users;');
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: 'Success',
      data: resp,
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new user.
exports.post_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/User' }
  } */
  const data = req.body;

  try {
    await db_access.query('CALL public.spinsertupdateuser($1,$2,$3);', [0, utilities.is_empty_string(data.firstname) ? null : data.firstname, utilities.is_empty_string(data.lastname) ? null : data.lastname]);

    res.status(200).json({
      code: 200,
      message: 'Success',
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new user.
exports.put_user = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/User' }
  } */
  const data = req.body;

  try {
    await db_access.query('CALL public.spinsertupdateuser($1,$2,$3);', [
      utilities.is_empty_string(data.user_id) ? null : data.user_id,
      utilities.is_empty_string(data.firstname) ? null : data.firstname,
      utilities.is_empty_string(data.lastname) ? null : data.lastname,
    ]);

    res.status(200).json({
      code: 200,
      message: 'Success',
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete user.
exports.delete_user = async (req, res) => {
  // #swagger.tags = ['Master']
  const data = req.query;

  try {
    await db_access.query('CALL public.spdeleteuser($1);', [utilities.is_empty_string(data.user_id) ? null : data.user_id]);

    res.status(200).json({
      code: 200,
      message: 'Success',
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

//Get Commission Of Agent
exports.get_commission = async (req, res) => {
  try {
    const user = req.user;
    const page = req.query.page || 1;
    const limit = req.query.limit;
    if (req.query.week === undefined || req.query.week === null || req.query.week === '') {
      res.status(400).send({
        code: 400,
        message: 'รูปแบบสัปดาห์ไม่ถูกต้อง',
      });
    }
    const week = req.query.week;
    const start_date = req.query.start_date;
    const end_date = req.query.end_date;
    const master_id = user.agent_id;
    let whereClause = '';
    let limitQuery = '';

    if (limit !== undefined && limit !== '' && limit !== null && limit > 0 && page > 0) {
      const offset = (page-1)*limit;
      limitQuery = `LIMIT ${limit} OFFSET ${offset}`;
    }

    if (req.query.agent) {
      whereClause += ` AND b.username ilike '%${req.query.agent}%'`;
    }
    // const sql = `
    // SELECT a.username , SUM(lh.total_price) as total_price , COUNT(lh.lotto_id) as num_lotto , 
    // (select COUNT(*) from (
    //   SELECT lh2.member_id , m2.username 
    //   FROM public.lotto_header lh2
    //   inner join members m2 on m2.id = lh2.member_id 
    //   inner join agent a2 on a2.agent_id = m2.agent_id 
    //   where  a2.agent_id = a.agent_id and lh2.status IN (8,9,10)
    //   group by lh2.member_id , m2.username 
    // ) rs ) as num_member ,
    // SUM(lh.total_price * 0.01) as total_profit,
    // SUM(lh.total_win) as total_win,
    // concat(date_part('week', lh.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', lh.created_date AT TIME ZONE 'UTC+7')) as week_lotto
    // FROM public.lotto_header lh
    // inner join members m on m.id = lh.member_id 
    // inner join agent a on a.agent_id = m.agent_id 
    // inner join agent master on master.agent_id = a.agent
    // where master.agent_id = '${master_id}' AND concat(date_part('week', lh.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', lh.created_date AT TIME ZONE 'UTC+7')) = '${week}' 
    // and lh.status IN (8,9,10)
    // ${whereClause}
    // group by a.agent_id , a.username, date_part('year', lh.created_date AT TIME ZONE 'UTC+7'), date_part('week', lh.created_date AT TIME ZONE 'UTC+7')
    // order by a.agent_id asc, date_part('year', lh.created_date AT TIME ZONE 'UTC+7') desc, date_part('week', lh.created_date AT TIME ZONE 'UTC+7') DESC
    // `;

    const sql = `
    select b.username, COALESCE(SUM(b.total_price), 0) AS total_price, COALESCE(SUM(b.list_name), 0) as num_lotto, COALESCE(SUM(b.total_player), 0) as num_member,
    (COALESCE(SUM(b.total_price), 0) * 0.01) AS total_profit, COALESCE(SUM(b.total_win), 0) AS total_win from (
    SELECT a.username, CAST(lh.created_date AS DATE) AS created_date, COALESCE(COUNT(lh.id), 0) AS list_name, COALESCE(COUNT(DISTINCT lh.member_id), 0) AS total_player,    
    COALESCE(SUM(lh.total_price), 0) AS total_price, COALESCE(SUM(lh.total_price) * 0.01, 0) AS net_income, COALESCE(SUM(lh.total_win), 0) AS total_win FROM lotto_header lh 
    INNER JOIN members m ON lh.member_id = m.id 
    INNER JOIN agent a ON a.agent_id = m.agent_id   
    WHERE CAST(lh.created_date AS DATE) >= '${start_date}' AND CAST(lh.created_date AS DATE) <= '${end_date}' AND lh.status IN (8, 9, 10) ${whereClause}
    GROUP BY a.username, CAST(lh.created_date AS DATE)) as b
    group by b.username
    `;

    const result = await db_access.query(sql + limitQuery);
    const response = result.rows;

    const sqlCount = `
    SELECT COUNT(*) AS TOTAL_RECORD FROM (${sql}) result`;

    const resultCount = await db_access.query(sqlCount);
    const responseCount = resultCount.rows;
    let total_record = 0;
    if (responseCount.length > 0) {
      total_record = responseCount[0].total_record;
    }
    res.status(200).json({
      code: 200,
      message: 'Success',
      data: response.map((item) => {
        item.num_lotto = parseFloat(item.num_lotto);
        item.num_member = parseFloat(item.num_member);
        return item;
      }),
      total_record: parseInt(total_record),
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

//Get Flash News
exports.get_flash_news = async (req, res) => {
  try {
    const sql = `SELECT id, message, status, created_date, updated_date FROM flashnews ORDER BY updated_date DESC`;
    const result = await db_access.query(sql);
    const rows = result.rows;
    let row = {};
    if (result.rowCount > 0) {
      row = rows[0];
    }
    res.status(200).json({
      code: 200,
      message: 'Success',
      data: row,
    });
  } catch (error) {
    console.error('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

//Update FlashNews
exports.update_flash_news = async (req, res) => {
  try {
    const body = req.body;
    if (!utilities.validate_value_require(body.message)) {
      res.status(400).send({
        code: 400,
        message: 'message is require filed',
      });
    }
    const sql = `
    truncate table public.flashnews restart identity cascade;

    insert into flashnews (message) values ('${body.message}')
    returning id, message, updated_date;
	`;

    await db_access.query(sql);

    res.status(200).json({
      code: 200,
      message: 'Success'
    });
  } catch (error) {
    console.log('error : ', error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_number_types = async (req, res) => {
  try {
    const sql = `SELECT id as number_type_id, number_type_name FROM number_type
    where status = 1`;
    const result = await db_access.query(sql);
    return response_success_builder(res, result.rows)
  } catch (error) {
    return response_error_builder(res, error)
  }
}

exports.get_report_credit = async (req, res) => {
  try {
    const user = req.user;
    const sql = `select report_credit_id , report_no, detail , doer , deal_with, credit , date_time , note 
    from report_credit rc where agent_id = '${user.agent_id}'`;
    const result = await db_access.query(sql);
    return response_success_builder(res, result.rows)
  } catch (error) {
    return response_error_builder(res, error)
  }
}

// Cancel stake by category
exports.post_cancel_stake_category = async (req, res) => {
  // #swagger.tags = ['Stake']
  /*  #swagger.parameters['obj'] = {
        in: 'body',
        schema: { $ref: '#/definitions/PostCancelStakeCategory' }
  } */
  const data = req.body;
  
  try {
    let result_lc = await db_access.query(
      `select is_cancel, is_finished from public.lotto_category lc
      where lc.category_id=$1 and lc.date_id=$2;`, [
        utilities.is_empty_string(data.category_id) ? 0 : data.category_id,
        utilities.is_empty_string(data.date_id) ? null : data.date_id
      ]);
    const resp_lc = result_lc.rows;

    if (resp_lc[0].is_cancel == 1 || resp_lc[0].is_finished == 1 ) throw `ไม่สามารถยกเลิกโพยหวยได้`;
    else {
      await db_access.query("CALL public.spcancelstakecategory($1,$2,$3);", [
        utilities.is_empty_string(data.category_id) ? 0 : data.category_id,
        utilities.is_empty_string(data.date_id) ? null : data.date_id,
        utilities.is_empty_string(req.user.agent_id) ? 0 : req.user.agent_id
      ]);
      
      let result_member = await db_access.query(
        `select distinct lh.member_id, m.credit from public.lotto_header lh
        inner join public.members m on lh.member_id = m.id
        where lh.category_id=$1 and lh.date_id=$2::date and lh.status=11;`, [
          utilities.is_empty_string(data.category_id) ? 0 : data.category_id,
          utilities.is_empty_string(data.date_id) ? null : data.date_id
        ]);
      const resp_member = result_member.rows;
  
      if (resp_member.length > 0) {
        resp_member.forEach(element => {
          global.io.sockets.emit('action_current_credit', {
            member_id: element.member_id,
            credit: element.credit
          });
        });
      }
    }

    /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ResultSuccess" }} */
    res.status(200).json({
      code: 200,
      message: "Success"
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Post reward result huay by category
exports.post_reward_result_huay = async (req, res) => {
  // #swagger.tags = ['Stake']
  /*  #swagger.parameters['obj'] = {
        in: 'body',
        schema: { $ref: '#/definitions/PostRewardResultHuay' }
  } */
  const data = req.body;
  console.log("Start post_reward_result_huay : ", data);
  
  try {
    let result_lc = await db_access.query(
      `select is_cancel, is_finished from public.lotto_category lc
      where lc.category_id=$1 and lc.date_id=$2;`, [
        utilities.is_empty_string(data.category_id) ? 0 : data.category_id,
        utilities.is_empty_string(data.date_id) ? null : data.date_id
      ]);
    const resp_lc = result_lc.rows;

    if (resp_lc[0].is_cancel == 1 || resp_lc[0].is_finished == 1 ) throw `ไม่สามารถออกรางวัลได้`;
    else {
      let new_result_permute_four_top = [];
      if (!utilities.is_empty_string(data.four_top)) {
        const result_permute_four_top = await permute(data.four_top);
        if (result_permute_four_top.length > 0) {
          result_permute_four_top.forEach(element => {
            new_result_permute_four_top.push({results: element});
          });
        }
      }
        
      let new_result_permute_three_top = [];
      if (!utilities.is_empty_string(data.three_top)) {
        const result_permute_three_top = await permute(data.three_top);
        if (result_permute_three_top.length > 0) {
          result_permute_three_top.forEach(element => {
            new_result_permute_three_top.push({results: element});
          });
        }
      }

      reward_huay(req.user.agent_id, data, new_result_permute_four_top, new_result_permute_three_top);
      console.log("Response post_reward_result_huay");
    }

    /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ResultSuccess" }} */
    res.status(200).json({
      code: 200,
      message: "Success"
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_config_result_tong = async (req, res) => {
  // #swagger.tags = ['Master']

  try {
    let result = await db_access.query(
      `select crt.lotto_id, l.lotto_name, crt.start_date + INTERVAL '7 hours' as start_date, crt.end_date + INTERVAL '7 hours' as end_date, crt.max_tong, crt.max_double, crt.list_huay, crt.is_check_tong as status from public.config_result_tong crt 
      inner join public.lotto l on crt.lotto_id = l.id and l.status = 1
      order by l.id;`);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.put_config_result_tong = async (req, res) => {
  // #swagger.tags = ['Master']
  /*  #swagger.parameters['obj'] = {
            in: 'body',
            schema: { $ref: '#/definitions/PutConfigResultTong' }
      } */
  const data = req.body;

  try {
    await db_access.query(
      "CALL public.spupdateconfigresulttong($1,$2,$3,$4,$5,$6,$7);",
      [
        utilities.is_empty_string(data.lotto_id) ? 0 : data.lotto_id,
        utilities.is_empty_string(data.start_date) ? null : data.start_date,
        utilities.is_empty_string(data.end_date) ? null : data.end_date,
        utilities.is_empty_string(data.max_tong) ? 0 : data.max_tong,
        utilities.is_empty_string(data.max_double) ? 0 : data.max_double,
        utilities.is_empty_string(data.list_huay) ? null : data.list_huay,
        utilities.is_empty_string(data.status) ? 0 : data.status
      ]
    );

    /* #swagger.responses[200] = { 
          schema: { "$ref": "#/definitions/ResultSuccess" }} */
    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

let permute = (string) => {
  try{
    if (!string || typeof string !== "string"){
      return "Please enter a string";
    } else if (string.length < 2 ){
      return string;
    }
  
    let permutationsArray = [];
     
    for (let i = 0; i < string.length; i++){
      let char = string[i];
  
      if (string.indexOf(char) != i)
      continue;
  
      let remainingChars = string.slice(0, i) + string.slice(i + 1, string.length);
  
      for (let permutation of permute(remainingChars)){
        permutationsArray.push(char + permutation) }
    }
    return permutationsArray;
  } catch (error) {
    throw error;
  }
}

let reward_huay = async (agent_id, data, new_result_permute_four_top, new_result_permute_three_top) => {
  try{
    await db_access.query("CALL public.spupdateresulthuay($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14);", [
      utilities.is_empty_string(agent_id) ? 0 : agent_id,
      utilities.is_empty_string(data.category_id) ? 0 : data.category_id,
      utilities.is_empty_string(data.date_id) ? null : data.date_id,
      utilities.is_empty_string(data.six_top) ? null : data.six_top,
      utilities.is_empty_string(data.four_top) ? null : data.four_top,
      utilities.is_empty_string(data.three_top) ? null : data.three_top,
      utilities.is_empty_string(data.two_top) ? null : data.two_top,
      utilities.is_empty_string(data.two_under) ? null : data.two_under,
      utilities.is_empty_string(data.three_front1) ? null : data.three_front1,
      utilities.is_empty_string(data.three_front2) ? null : data.three_front2,
      utilities.is_empty_string(data.three_back1) ? null : data.three_back1,
      utilities.is_empty_string(data.three_back2) ? null : data.three_back2,
      new_result_permute_four_top.length > 0 ? JSON.stringify(new_result_permute_four_top) : null,
      new_result_permute_three_top.length > 0 ? JSON.stringify(new_result_permute_three_top) : null
    ]);

    let result_member = await db_access.query(
      `select distinct member_id, m.credit from public.lotto_header lh
      inner join public.members m on lh.member_id = m.id 
      where lh.date_id = $1::date and lh.category_id = $2 and lh.total_win > 0 and lh.status in (8,10);`, [
        utilities.is_empty_string(data.date_id) ? null : data.date_id,
        utilities.is_empty_string(data.category_id) ? 0 : data.category_id
      ]);
    const resp_member = result_member.rows;
    console.log("Finished post_reward_result_huay : ", resp_member);

    if (resp_member.length > 0) {
      resp_member.forEach(element => {
        global.io.sockets.emit('action_current_credit', {
          member_id: element.member_id,
          credit: element.credit
        });
      });
    }
  } catch (error) {
    throw error;
  }
}