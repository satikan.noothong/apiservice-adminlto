const db_access = require("../db_access");

// Retrieve Member.
exports.get_member = async (req, res) => {
  // #swagger.tags = ['Member']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Member" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let one = "";
    let typeUser = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    one += "select * from members m where m.status = '1' ";
    if (typeUser === 0) {
      one += ` and  m.agent_id='${agentId}' `;
    } else  if (typeUser === 1) {
      one += ` and m.agent_id='${agentNo}' `;
    }
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
