const db_access = require("../db_access");

// Retrieve Menu.
exports.get_menu = async (req, res) => {
  // #swagger.tags = ['Menu']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Menu" }} */
  let listData = [];
  try {
    let result = await db_access.query("select m.menu_name as menu_name , m.id ,mg.menu_group_name as header_menu_name  from menu m inner join menu_group mg  on m.menu_group_id  = mg.id  where mg.status = 1 and m.status =1;");
    const resp = result.rows;
    const modelsByParentId = {};
    for (const { menu_name, id, header_menu_name } of resp) {
      if (!modelsByParentId[header_menu_name]) {
        modelsByParentId[header_menu_name] = {"name":header_menu_name,  list: [] };
      }
      modelsByParentId[header_menu_name].list.push({ id, menu_name });
    }

    res.status(200).json({
      code: 200,
      message: "Success",
      data: Object.values(modelsByParentId),
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


exports.get_list_menu = async (req, res) => {
  // #swagger.tags = ['Menu']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Menu" }} */

  try {
    let result = await db_access.query("select m.id menu_id, m.menu_name, mr.id menu_role_id, menu_role_name from menu_role mr inner join menu m on mr.menu_id = m.id  where mr.status = 1 and m.status = 1 order by m.order_seq, mr.order_seq;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
