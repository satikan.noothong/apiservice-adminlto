const db_access = require("../db_access");

// Retrieve MenuGroup.
exports.get_menu_group = async (req, res) => {
  // #swagger.tags = ['MenuGroup']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/MenuGroup" }} */

  try {
    let result = await db_access.query("select * from menu_group where status = '1';");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
