const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');

// Retrieve NetworkMember.
exports.get_network_member = async (req, res) => {
  // #swagger.tags = ['NetworkMember']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/NetworkMember" }} */

  try {
    let result = await db_access.query("SELECT * FROM public.network_member;");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new NetworkMember.
exports.post_network_member = async (req, res) => {
  // #swagger.tags = ['NetworkMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/NetworkMember' }
  } */
  const data = req.body;
  try {
    await db_access.query("CALL public.spinsertupdatenetworkmember($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19);", [
      0,
      utilities.is_empty_string(data.order_no) ? null : data.order_no,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.agent_name) ? null : data.agent_name,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.credit) ? null : data.credit,
      utilities.is_empty_string(data.total_network) ? null : data.total_network,
      utilities.is_empty_string(data.deposit) ? null : data.deposit,
      utilities.is_empty_string(data.withdraw) ? null : data.withdraw,
      utilities.is_empty_string(data.totalbet_in_network) ? null : data.totalbet_in_network,
      utilities.is_empty_string(data.cumulative_income) ? null : data.cumulative_income,
      utilities.is_empty_string(data.current_income) ? null : data.current_income,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.last_accress) ? null : data.last_accress,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.created_by) ? null : data.created_by,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new NetworkMember.
exports.put_network_member = async (req, res) => {
  // #swagger.tags = ['NetworkMember']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/NetworkMember' }
  } */
  const data = req.body;
  const passwordHash = bcrypt.hashSync(data.password ? null : data.password , 25);
  try {
    await db_access.query("CALL public.spinsertupdatenetworkMember($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.order_no) ? null : data.order_no,
      utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
      utilities.is_empty_string(data.agent_name) ? null : data.agent_name,
      utilities.is_empty_string(data.phone) ? null : data.phone,
      utilities.is_empty_string(data.credit) ? null : data.credit,
      utilities.is_empty_string(data.total_network) ? null : data.total_network,
      utilities.is_empty_string(data.deposit) ? null : data.deposit,
      utilities.is_empty_string(data.withdraw) ? null : data.withdraw,
      utilities.is_empty_string(data.totalbet_in_network) ? null : data.totalbet_in_network,
      utilities.is_empty_string(data.cumulative_income) ? null : data.cumulative_income,
      utilities.is_empty_string(data.current_income) ? null : data.current_income,
      utilities.is_empty_string(data.ip_address) ? null : data.ip_address,
      utilities.is_empty_string(data.last_accress) ? null : data.last_accress,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.created_by) ? null : data.created_by,
      utilities.is_empty_string(data.created_date) ? null : data.created_date,
      utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
      utilities.is_empty_string(data.updated_date) ? null : data.updated_date
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete NetworkMember.
exports.delete_network_member = async (req, res) => {
  // #swagger.tags = ['NetworkMember']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletenetworkmember($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
