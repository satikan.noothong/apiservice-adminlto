const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve Outbox.
exports.get_outbox = async (req, res) => {
  // #swagger.tags = ['Outbox']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Outbox" }} */
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
      if (typeAgent === 0) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id=" + agentId + " order by i.created_date desc;");
      } else if (typeAgent === 1) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date  FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id=" + agentNo + " order by i.created_date desc;");
      } else if (typeAgent === 2) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date  FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id order by i.created_date desc;");
      } else if (typeAgent === 3) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date  FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id order by i.created_date desc;");
      }
    const resp = resu.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve Outbox.
exports.get_outbox_by_id = async (req, res) => {
  // #swagger.tags = ['Outbox']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Outbox" }} */
  const data = req.params.id;
  try {
    let result = await db_access.query("SELECT  * FROM public.outbox where outbox_id ="+data+";");
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new Outbox.
exports.post_outbox = async (req, res) => {
  // #swagger.tags = ['Outbox']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Outbox' }
  } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.credit, a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let agentNo = "";
    let agent = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;

    }
    if (typeUser === 1){
      agent = agentNo;
    } else {
      agent = agentId;
    }
    if (data.send_to === 0 || data.send_to === "0" || data.send_to == 0 || data.send_to == "0"){
      let agenttt = "select * from members m where m.status ='1' ";
      if (typeUser === 0) {
        agenttt += ` and  m.agent_id='${agentId}' `;
      } else  if (typeUser === 1) {
        agenttt += ` and m.agent_id='${agentNo}' `;
      }
      let repp = await db_access.query(agenttt);
      const response = repp.rows;
      for (let i = 0; i < response.length; i++) {
        await db_access.query("CALL public.spinsertupdateoutbox($1,$2,$3,$4,$5,$6);", [
          0,
          utilities.is_empty_string(response[i].id) ? 0 : response[i].id,
          utilities.is_empty_string(agent) ? 0 : agent,
          utilities.is_empty_string(data.title) ? null : data.title,
          utilities.is_empty_string(data.message) ? null : data.message,
          utilities.is_empty_string(req.user.ip_address) ? null : req.user.ip_address,
        ]);

        await db_access.query("CALL public.spinsertupdateinbox($1,$2,$3,$4,$5,$6);", [
          utilities.is_empty_string(agent) ? null : agent,
          utilities.is_empty_string(response[i].agent_id) ? null : response[i].agent_id,
          utilities.is_empty_string(data.title) ? null : data.title,
          utilities.is_empty_string(data.message) ? null : data.message,
          utilities.is_empty_string(req.user.ip_address) ? null : req.user.ip_address,
          utilities.is_empty_string(data.attachments) ? null : data.attachments
        ]);
      }

    } else {

      await db_access.query("CALL public.spinsertupdateoutbox($1,$2,$3,$4,$5,$6);", [
        0,
        utilities.is_empty_string(data.send_to) ? 0 : data.send_to,
        utilities.is_empty_string(agent) ? 0 : agent,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.message) ? null : data.message,
        utilities.is_empty_string(req.user.ip_address) ? null : req.user.ip_address,
      ]);

      await db_access.query("CALL public.spinsertupdateinbox($1,$2,$3,$4,$5,$6);", [
        utilities.is_empty_string(agent) ? null : agent,
        utilities.is_empty_string(data.send_to) ? null : data.send_to,
        utilities.is_empty_string(data.title) ? null : data.title,
        utilities.is_empty_string(data.message) ? null : data.message,
        utilities.is_empty_string(req.user.ip_address) ? null : req.user.ip_address,
        utilities.is_empty_string(data.attachments) ? null : data.attachments
      ]);
    }

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new Outbox.
exports.put_outbox = async (req, res) => {
  // #swagger.tags = ['Outbox']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Outbox' }
  } */
  const data = req.body;

  try {
    const agentId = req.user.agent_id;
    const type = "select a.credit, a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeUser = "";
    let agentNo = "";
    let agent = "";
    for (let i = 0; i < resps.length; i++) {
      typeUser = resps[i].type_user;
      agentNo = resps[i].agent;

    }
    if (typeUser === 1){
      agent = agentNo;
    } else {
      agent = agentId;
    }
    await db_access.query("CALL public.spinsertupdateoutbox($1,$2,$3,$4,$5,$6);", [
      utilities.is_empty_string(data.outbox_id) ? null : data.outbox_id,
      utilities.is_empty_string(data.send_to) ? 0 : data.send_to,
      utilities.is_empty_string(agent) ? 0 : agent,
      utilities.is_empty_string(data.title) ? null : data.title,
      utilities.is_empty_string(data.message) ? null : data.message,
      utilities.is_empty_string(req.user.ip_address) ? null : req.user.ip_address,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete Outbox.
exports.delete_outbox = async (req, res) => {
  // #swagger.tags = ['Outbox']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeleteoutbox($1);", [
      utilities.is_empty_string(data.outbox_id) ? null : data.outbox_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve Outbox.
exports.get_search_outbox = async (req, res) => {
  // #swagger.tags = ['Outbox']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Outbox" }} */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let resu = null;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (data.text !== null || data.text !== "") {
      if (typeAgent === 0) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id="+agentId+" and (m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(i.outbox_id as text) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%' or i.message ilike '%" + data.text + "%' or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%') ;");
      } else if (typeAgent === 1) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id="+agentNo+" and (m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(i.outbox_id as text) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%' or i.message ilike '%" + data.text + "%' or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%') ;");
      } else if (typeAgent === 2) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(i.outbox_id as text) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%' or i.message ilike '%" + data.text + "%' or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%' ;");
      } else if (typeAgent === 3) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where m.username ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%' or cast(i.outbox_id as text) ilike '%" + data.text + "%' or i.title ilike '%" + data.text + "%' or i.message ilike '%" + data.text + "%' or i.ip_address ilike '%" + data.text + "%' or cast(i.created_date as TEXT) ilike '%" + data.text + "%' ;");
      }
    } else{
      if (typeAgent === 0) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id=" + agentId + " order by i.created_date desc;");
      } else if (typeAgent === 1) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id where a.agent_id=" + agentNo + " order by i.created_date desc;");
      } else if (typeAgent === 2) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id order by i.created_date desc;");
      } else if (typeAgent === 3) {
        resu = await db_access.query("SELECT m.username as sent_to, a.username as send_from, i.outbox_id, i.title, i.message, i.ip_address, i.created_date + INTERVAL '7 hours' as created_date FROM public.outbox i inner join members m on i.sent_to = m.id inner join agent a on i.send_from= a.agent_id order by i.created_date desc;");
      }
    }
    const resp = resu.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
