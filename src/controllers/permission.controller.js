const db_access = require("../db_access");


exports.get_permission = async (req, res) => {
  // #swagger.tags = ['Permission']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Permission" }} */

  try {
    const first = "SELECT mam.menu_id as id,mg.menu_group_name as header_menu_name, m.menu_name as menu_name, a.type_user as type_user FROM public.mapping_agent_menu mam inner join agent a on a.agent_id =mam.agent_id inner join menu m on m.id = mam.menu_id inner join menu_group mg on m.menu_group_id  = mg.id  where mam.agent_id = ";
    const last = " and m.status =1 and mg.status =1;";
    let result = await db_access.query(first + req.user.agent_id + last);
    const resp = result.rows;
    const modelsByParentId = {};
    for (const { menu_name, id, header_menu_name, type_user } of resp) {
      if (!modelsByParentId[header_menu_name]) {
        modelsByParentId[header_menu_name] = {"name":header_menu_name,  list: [] };
      }
      modelsByParentId[header_menu_name].list.push({ id, menu_name, type_user });
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: Object.values(modelsByParentId),
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Retrieve agent.
exports.get_permission_user = async (req, res) => {
  // #swagger.tags = ['Permission']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Permission" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user,a.username from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    const response = Object.values(resps).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};