const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const path = require("path");
const moment = require("moment");

// Retrieve Popup.
exports.get_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/Popup" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = null;
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo =  resps[i].agent;
    }
    if (typeAgent === 0) {
      result = await db_access.query("SELECT * FROM public.popup where agent_id ="+agentId+"  order by created_date desc;");
    } else if (typeAgent === 1) {
      result = await db_access.query("SELECT * FROM public.popup where agent_id ="+agentNo+"  order by created_date desc;");
    } else if (typeAgent === 2) {
      result = await db_access.query("SELECT * FROM public.popup  order by i.created_date desc;");
    } else if (typeAgent === 3) {
      result = await db_access.query("SELECT * FROM public.popup  order by i.created_date desc;");
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve Popup.
exports.get_popup_by_id = async (req, res) => {
  // #swagger.tags = ['Popup']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Popup" }} */
  const agentId = req.user.agent_id;
  try {
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId+ ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    let one = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo =  resps[i].agent;
    }
    if (typeAgent === 1){
      one = "SELECT * FROM public.popup where agent_id ="+agentNo+";";
    } else{
      one = "SELECT * FROM public.popup where agent_id ="+agentId+";";
    }
    let result = await db_access.query(one);
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new Popup.
exports.post_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Popup' }
  } */
  try {
    let agentId = req.user.agent_id
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId+ ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo =  resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag = agentId;
    }
    const message_file = req.file;
    const data = req.body;
    let res_upload_s3 = null;

    if (message_file !== undefined) {
      const extension = path.extname(message_file.originalname);
      const file_name = `banner/${data.popup_id}_${moment().format("YYYYMMDD_HHmmss")}${extension}`;
      const res_upload_file = await utilities.upload_file_s3(message_file, file_name);
      res_upload_s3 = res_upload_file.Location;
    }

    await db_access.query("CALL public.spinsertupdatepopup($1,$2,$3,$4,$5);", [
      0,
      utilities.is_empty_string(data.description) ? null : data.description,
      utilities.is_empty_string(res_upload_s3) ? null : res_upload_s3,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(ag) ? null : ag,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new Popup.
exports.put_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  // #swagger.consumes = ['multipart/form-data']
  /* #swagger.parameters['file'] = {
          in: 'formData',
          type: 'file'
  } */
  /*  #swagger.parameters['id'] = {
           in: 'formData',
           type: 'number'
   } */
  /*  #swagger.parameters['description'] = {
           in: 'formData',
           type: 'string'
   } */
  /*  #swagger.parameters['status'] = {
           in: 'formData',
           type: 'number'
   } */
  try {
    const data = req.body;
    let agentId = req.user.agent_id
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId+ ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo =  resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag = agentId;
    }
    await db_access.query("CALL public.spinsertupdatepopup($1,$2,$3,$4);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(ag) ? null : ag,
      utilities.is_empty_string(data.description) ? null : data.description,
      utilities.is_empty_string(data.status) ? 2 : data.status
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
    } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new Popup.
exports.put_picture_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  // #swagger.consumes = ['multipart/form-data']
  /* #swagger.parameters['file'] = {
          in: 'formData',
          type: 'file'
  } */
  /*  #swagger.parameters['id'] = {
          in: 'formData',
          type: 'number'
  } */
  try {
    const message_file = req.file;
    const data = req.body;
    let res_upload_s3 = null;
    if (req.file) {
      const extension = path.extname(message_file.originalname);
      const file_name = `popup/${data.id}_${moment().format("YYYYMMDD_HHmmss")}${extension}`;
      const res_upload_file = await utilities.upload_file_s3(message_file, file_name);
      res_upload_s3 = res_upload_file.Location;
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: res_upload_s3
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete Popup.
exports.delete_popup = async (req, res) => {
  // #swagger.tags = ['Popup']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletepopup($1);", [
      utilities.is_empty_string(data.popup_id) ? null : data.popup_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
