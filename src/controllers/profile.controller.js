const db_access = require("../db_access");
const utilities = require("../utils/utilities");
const bcrypt = require('bcrypt');


exports.get_profile = async (req, res) => {
  // #swagger.tags = ['Profile']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/Profile" }} */
  const agentId = req.user.agent_id;
  try {
    let result = await db_access.query("SELECT a.agent_id as id, a.username, a.reference , a.email , a.phone , a.password FROM agent a where a.agent_id ="+agentId+";");
    const resp = result.rows;
    const response = Object.values(resp).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Update a new Agent.
exports.put_profile = async (req, res) => {
  // #swagger.tags = ['Profile']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/Profile' }
  } */
  const data = req.body;
  try {
    let result = await db_access.query("SELECT a.password  FROM agent a where a.agent_id ="+data.id+";");
    const resp = result.rows;
    for (let i = 0; i < resp.length; i++) {
      let password = "";
      if ((data.password !== '') && (data.confirm_password !== '')) {
        if (data.password === data.confirm_password) {
          password = await bcrypt.hash(data.password, 10);
          bcrypt.compare(data.old_password, resp[i].password, async function (err, result) {
            if (err) {
              res.status(400).json({
                code: 400,
                message: "Old Password Not Match ",
              });
            } else {
              if (result) {
                await db_access.query("CALL public.spupdateprofileuser($1,$2,$3,$4,$5,$6);", [
                  utilities.is_empty_string(data.id) ? null : data.id,
                  utilities.is_empty_string(data.reference) ? null : data.reference,
                  utilities.is_empty_string(data.email) ? null : data.email,
                  utilities.is_empty_string(data.phone) ? null : data.phone,
                  utilities.is_empty_string(password) ? null : password,
                  utilities.is_empty_string(password) ? null : password
                ]);

                res.status(200).json({
                  code: 200,
                  message: "Success",
                });
              } else {
                res.status(400).json({
                  code: 400,
                  message: "Old Password is invalid ",
                });
              }
            }
          });
        } else {
          res.status(400).json({
            code: 400,
            message: "Password Not Match ",
          });
        }
      } else if ((data.password === '') && (data.confirm_password === '')){
        await db_access.query("CALL public.spupdateprofilenopass($1,$2,$3,$4);", [
          utilities.is_empty_string(data.id) ? null : data.id,
          utilities.is_empty_string(data.reference) ? null : data.reference,
          utilities.is_empty_string(data.email) ? null : data.email,
          utilities.is_empty_string(data.phone) ? null : data.phone
        ]);
        res.status(200).json({
          code: 200,
          message: "Success",
        });
      } else{
        res.status(400).json({
          code: 400,
          message: "Sent password is incomplete ",
        });
      }
    }
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};