const db_access = require("../db_access");
const {
  response_error_builder,
  response_success_builder,
} = require("../utils/response-builder/response-utils");
const utilities = require("../utils/utilities");
const moment = require("moment/moment");

// Retrieve ReportCredit.
exports.get_reportCredit = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    let result = [];
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }

    let sqlQuery = `
  SELECT m.username as dealWith, a.username as actor, an.username as sub_actor, dw.amount as total, 
    m.credit as current_credit, dw.remark, dw.comment, dw.created_date + INTERVAL '7 hours' as created_date , dw.id, tt.transaction_type
  FROM deposit_withdraw dw
  INNER JOIN members m ON m.id = dw.member_id
  INNER JOIN agent an ON an.agent_id = dw.actor_id
  INNER JOIN transaction_type tt ON tt.id = dw.trans_type_id
  INNER JOIN agent a ON a.agent_id = dw.actor_id `;
    if (typeAgent=== 0){
      sqlQuery += ` where m.agent_id=`+ agentId ;
    } else if(typeAgent=== 1){
      sqlQuery += ` where m.agent_id=`+ agentNo ;
    }
    sqlQuery += `
  ORDER BY dw.created_date DESC;
`;

    const ress = await db_access.query(sqlQuery);
      result.push(ress.rows);

    // const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: result[0].sort(
        (a, b) => new Date(b.created_date) - new Date(a.created_date)
      ),
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ReportCredit.
exports.get_search_reportCredit = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let conditions = [];
// Construct conditions based on data
    if (data.text) {
      conditions.push(
          `(m.username ilike '%${data.text}%' OR a.username ilike '%${data.text}%' OR 
      cast(dw.amount as text) ilike '%${data.text}%' OR cast(m.credit as text) ilike '%${data.text}%' OR 
      dw.remark ilike '%${data.text}%' OR dw.comment ilike '%${data.text}%' OR 
      cast(dw.created_date as text) ilike '%${data.text}%' OR cast(dw.id as text) ilike '%${data.text}%' OR 
      tt.transaction_type ilike '%${data.text}%' OR cast(dw.amount as text) ilike '%${data.text}%' OR 
      cast(dw.amount as text) ilike '%${data.text}%' )`
      );
    }

    if (data.type) {
      conditions.push(data.type === 'ฝาก' ? 'dw.trans_type_id = 1 ': data.type === 'ถอน' ? 'dw.trans_type_id = 2 ' : data.type === 'ฝากตรง' ? 'dw.trans_type_id = 3 ' : 'dw.trans_type_id = 4 ');
    }

    if (data.more_than_amount) {
      conditions.push(`dw.amount >= ${data.more_than_amount}`);
    }

    if (data.less_amount) {
      conditions.push(`dw.amount <= ${data.less_amount}`);
    }

    if (data.typeName) {
      conditions.push(data.typeName === 'จำนวนค่าบวก' ? 'dw.trans_type_id = 2 ' : 'dw.trans_type_id = 1 ');
    }

    if (data.date) {
      if (data.date === '1') {
        conditions.push('DATE(dw.created_date  + INTERVAL \'7 hours\' ) = current_date');
      } else if (data.date === '0') {
        conditions.push('DATE(dw.created_date  + INTERVAL \'7 hours\' )   > (NOW() - INTERVAL \'7 DAY\')');
      }
    }

    if (typeAgent=== 0){
      conditions.push('dw.agent_id='+ agentId);
    } else if(typeAgent=== 1){
      conditions.push('dw.agent_id='+ agentNo);
    }

    let whereClause = conditions.length > 0 ? 'WHERE ' + conditions.join(' AND ') : '';

    const sqlQuery = `
  SELECT m.username as dealWith, an.username as actor, a.username as sub_actor, dw.amount as total, 
    m.credit as current_credit, dw.remark, dw.comment, dw.created_date + INTERVAL '7 hours' as created_date, dw.id, tt.transaction_type
  FROM deposit_withdraw dw
  LEFT JOIN members m ON m.id = dw.member_id
  LEFT JOIN agent an ON an.agent_id = dw.actor_id
  LEFT JOIN transaction_type tt ON tt.id = dw.trans_type_id
  LEFT JOIN agent a ON an.agent_id = a.agent
  ${whereClause}
  ORDER BY dw.created_date DESC;
`;

    const result = await db_access.query(sqlQuery);
    const response = result.rows;

    // const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve AskCredit.
exports.get_reportCreditMaster = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const agentId = req.user.agent_id;
    const type =
      "select a.type_user, a.agent from agent a where a.agent_id =" +
      agentId +
      ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let first =
      "select ac.id , ac.created_date + INTERVAL '7 hours' as created_date ,ac.total as credit, ac.description,";
    if (typeAgent === 0) {
      first += "COALESCE((select username  from agent a2  where a2.agent_id  = ac.agent_id ),null) as deal_with,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') as actor,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac  where ac.agent_id = " + agentId + " and ac.status=4  order by ac.created_date desc;";
    }
    if (typeAgent === 1) {
      first += "COALESCE((select username  from agent a2  where a2.agent_id  = ac.agent_id  ),null) as deal_with,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') as actor,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac where ac.agent_id = " + agentNo + " and ac.status=4 order by ac.created_date desc;";
    } else if (typeAgent === 2 || typeAgent === 3) {
      first += "COALESCE((select username  from agent a2  where a2.agent_id  = ac.agent_id ),null) as deal_with,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') as actor,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac where ac.status=4 order by ac.created_date desc;";
    }
    let respon = await db_access.query(first);
    const response = respon.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve AskCredit.
exports.get_reportCreditMasterByMaster = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const agentId = req.user.agent_id;
    const type =
        "select a.type_user, a.agent from agent a where a.agent_id =" +
        agentId +
        ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let first =
        "select ac.id , ac.created_date + INTERVAL '7 hours' as created_date ,ac.total as credit, ac.description,";
    if (typeAgent === 0) {
      first += "COALESCE((select MAX(a.username)  from agent a2 inner join agent a on a.agent_id = a2.agent_id where a2.agent_id  = ac.agent_id )) as agent,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS actor,";
      first += "COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id )) as deal_with,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac  where ac.agent_id = " + agentId + " and ac.status=4 order by ac.created_date desc;";
    }
    if (typeAgent === 1) {
      first += "COALESCE((select MAX(a.username)  from agent a2 inner join agent a on a.agent_id = a2.agent_id where a2.agent_id  = ac.agent_id )) as agent,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS actor,";
      first += "COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id )) as deal_with,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac where ac.agent_id = " + agentNo + " and ac.status=4 order by ac.created_date desc;";
    } else if (typeAgent === 2 || typeAgent === 3) {
      first += "COALESCE((select MAX(a.username)  from agent a2 inner join agent a on a.agent_id = a2.agent_id where a2.agent_id  = ac.agent_id )) as agent,";
      first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS actor,";
      first += "COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id )) as deal_with,";
      first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
      first += "COALESCE((select an.username from agent a2 inner join agent a on a.agent_id = a2.agent_id inner join agent aa on aa.agent_id  = a.agent inner join agent an on aa.agent  = an.agent_id  where a2.agent_id  = ac.agent_id ), null) as sub_actor,";
      first += "ac.current_credit as current_credit ";
      first += "from ask_credit ac  where ac.status=4 order by ac.created_date desc;";
    }
    let respon = await db_access.query(first);
    const response = respon.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

function isNumeric(value) {
  return /^-?\d+$/.test(value);
}

// Retrieve AskCredit.
exports.get_search_reportCreditMaster = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type =
        "select a.type_user, a.agent from agent a where a.agent_id =" +
        agentId +
        ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let text = "";
    let date = "";
    let types = "";
    let less_amount = "";
    let more_than_amount = "";
    let agentNo = "";
    let typeName = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0 || typeAgent === 1) {
      if (data.text === "" || data.text === null) {

        text += "";
      } else {
        text += " and (cast(ac.id as text) ilike '%" + data.text + "%' or cast(ac.created_date as text) ilike '%" +
            data.text + "%' or cast(ac.total as text) ilike '%" + data.text + "%' or ac.description ilike '%" + data.text + "%' ";
        text += " or CAST(COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " or CAST(COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id ))  AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT an.username FROM agent a2 INNER JOIN agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent  INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), NULL) AS TEXT) ilike '%" +
            data.text + "%') ";
      }
    } else {
      if (data.text === "" || data.text === null) {
        text += "";
      } else {
        text += " where (cast(ac.id as text) ilike '%" + data.text + "%' or cast(ac.created_date as text) ilike '%" +
            data.text + "%' or cast(ac.total as text) ilike '%" + data.text + "%' or ac.description ilike '%" + data.text + "%' ";
        text += " or CAST(COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " or CAST(COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id ))  AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT an.username FROM agent a2 INNER JOIN agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent  INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), NULL) AS TEXT) ilike '%" +
            data.text + "%') ";
      }
    }
    if (data.type === "" || data.type === null || data.type === undefined) {
      types += "";
    } else {
      types += " having CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
          data.type + "%' ";
    }

    if (
        data.more_than_amount === "" ||
        data.more_than_amount === null ||
        data.more_than_amount === undefined
    ) {
      more_than_amount += "";
    } else {
      if (data.type === "" || data.type === null || data.type === undefined) {
        more_than_amount += " having ac.current_credit >=" + data.more_than_amount + " and ac.total  >=" + data.more_than_amount + " ";
      } else {
        more_than_amount += " and ac.current_credit >=" + data.more_than_amount + " and ac.total >=" + data.more_than_amount + " ";
      }
    }

    if (data.less_amount === "" || data.less_amount === null) {
      less_amount += "";
    } else {
      if (
          (data.more_than_amount === "" ||
              data.more_than_amount === null ||
              data.more_than_amount === undefined) &&
          (data.type === "" || data.type === null || data.type === undefined)
      ) {
        less_amount += " having ac.current_credit <=" + data.less_amount + " or ac.total  <=" + data.less_amount + " ";
      } else {
        less_amount += " and ac.current_credit <=" + data.less_amount + " or ac.total <=" + data.less_amount + " ";
      }
    }

    if (data.typeName === "" || data.typeName === null || data.typeName === undefined) {
      typeName += "";
    } else {
      if (data.text === "" || data.text === null || data.text === undefined) {
        if (data.typeName === "จำนวนค่าบวก") {
          typeName += "where  ac.type_id = 2";
        } else {
          typeName += "where ac.type_id = 1";
        }
      } else {
        if (data.typeName === "จำนวนค่าบวก") {
          typeName += " and ac.type_id = 2";
        } else {
          typeName += " and ac.type_id = 1";
        }
      }
    }

    if (data.date === "" || data.date === null) {
      date = "";
    } else {
      if (
          (data.text === "" || data.text === null) &&
          (data.typeName === "" ||
              data.typeName === null ||
              data.typeName === undefined)
      ) {
        if (data.date === "1") {
          if (typeAgent === 0 || typeAgent === 1) {
            date = " and  DATE(ac.created_date) = current_date ";
          } else {
            date = " where  DATE(ac.created_date) = current_date ";
          }
        } else if (data.date === "0") {
          if (typeAgent === 0 || typeAgent === 1) {
            date = "  and  DATE(ac.created_date) > current_date - INTERVAL '8 DAY' and DATE(ac.created_date) != current_date ";
          } else {
            date = " where  DATE(ac.created_date) > current_date - INTERVAL '8 DAY' and DATE(ac.created_date) != current_date ";
          }
        }
      } else {
        if (data.date === "1") {
          date = " and DATE(ac.created_date) = current_date ";
        } else if (data.date === "0") {
          date = " and DATE(ac.created_date) > current_date - INTERVAL '8 DAY' and DATE(ac.created_date) != current_date  ";
        }
      }
    }

    let first = "select ac.id , ac.created_date + INTERVAL '7 hours' as created_date,ac.total as credit, ac.description,";
    first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS actor,";
    first += "COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id )) as deal_with,";
    first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
    first += "COALESCE((select an.username from agent a2 inner join agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), null) as sub_actor,";
    first += "ac.current_credit as current_credit ";

    if (typeAgent === 0) {
      first += "from ask_credit ac where ac.agent_id = " + agentId + text + typeName + date + " and ac.status=4  group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    } else if (typeAgent === 1) {
      first += "from ask_credit ac where ac.agent_id = " + agentNo + text + typeName + date + " and ac.status=4 group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    } else if (typeAgent === 2 || typeAgent === 3) {
      first += "from ask_credit ac " + text + typeName + date + " and ac.status=4 group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    }

    let respon = await db_access.query(first);

    const response = respon.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve AskCredit.
exports.get_search_reportCreditMasterByMaster = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type =
        "select a.type_user, a.agent from agent a where a.agent_id =" +
        agentId +
        ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let text = "";
    let date = "";
    let types = "";
    let less_amount = "";
    let more_than_amount = "";
    let agentNo = "";
    let typeName = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0 || typeAgent === 1) {
      if (data.text === "" || data.text === null) {

        text += "";
      } else {
        text += " and (cast(ac.id as text) ilike '%" + data.text + "%' or cast(ac.created_date as text) ilike '%" +
            data.text + "%' or cast(ac.total as text) ilike '%" + data.text + "%' or ac.description ilike '%" + data.text + "%' ";
        text += " or CAST(COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " or CAST(COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id ))  AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT an.username FROM agent a2 INNER JOIN agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent  INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), NULL) AS TEXT) ilike '%" +
            data.text + "%') ";
      }
    } else {
      if (data.text === "" || data.text === null) {
        text += "";
      } else {
        text += " where (cast(ac.id as text) ilike '%" + data.text + "%' or cast(ac.created_date as text) ilike '%" +
            data.text + "%' or cast(ac.total as text) ilike '%" + data.text + "%' or ac.description ilike '%" + data.text + "%' ";
        text += " or CAST(COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " or CAST(COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id ))  AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
            data.text + "%' ";
        text += " OR CAST(COALESCE((SELECT an.username FROM agent a2 INNER JOIN agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent  INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), NULL) AS TEXT) ilike '%" +
            data.text + "%') ";
      }
    }
    if (data.type === "" || data.type === null || data.type === undefined) {
      types += "";
    } else {
      types += " having CAST(COALESCE((SELECT transaction_type FROM transaction_type tt WHERE tt.id = ac.type_id), NULL) AS TEXT) ilike '%" +
          data.type + "%' ";
    }

    if (
        data.more_than_amount === "" ||
        data.more_than_amount === null ||
        data.more_than_amount === undefined
    ) {
      more_than_amount += "";
    } else {
      if (data.type === "" || data.type === null || data.type === undefined) {
        more_than_amount += " having ac.current_credit >=" + data.more_than_amount + " and ac.total  >=" + data.more_than_amount + " ";
      } else {
        more_than_amount += " and ac.current_credit >=" + data.more_than_amount + " and ac.total >=" + data.more_than_amount + " ";
      }
    }

    if (data.less_amount === "" || data.less_amount === null) {
      less_amount += "";
    } else {
      if (
          (data.more_than_amount === "" ||
              data.more_than_amount === null ||
              data.more_than_amount === undefined) &&
          (data.type === "" || data.type === null || data.type === undefined)
      ) {
        less_amount += " having ac.current_credit <=" + data.less_amount + " or ac.total  <=" + data.less_amount + " ";
      } else {
        less_amount += " and ac.current_credit <=" + data.less_amount + " or ac.total <=" + data.less_amount + " ";
      }
    }

    if (data.typeName === "" || data.typeName === null || data.typeName === undefined) {
      typeName += "";
    } else {
      if (data.text === "" || data.text === null || data.text === undefined) {
        if (data.typeName === "จำนวนค่าบวก") {
          typeName += "where  ac.type_id = 2";
        } else {
          typeName += "where ac.type_id = 1";
        }
      } else {
        if (data.typeName === "จำนวนค่าบวก") {
          typeName += " and ac.type_id = 2";
        } else {
          typeName += " and ac.type_id = 1";
        }
      }
    }

    if (data.date === "" || data.date === null) {
      date = "";
    } else {
      if (
          (data.text === "" || data.text === null) &&
          (data.typeName === "" ||
              data.typeName === null ||
              data.typeName === undefined)
      ) {
        if (data.date === "1") {
          if (typeAgent === 0 || typeAgent === 1) {
            date = " and  DATE(ac.created_date + INTERVAL '7 hours') = current_date ";
          } else {
            date = " where DATE(ac.created_date + INTERVAL '7 hours') = current_date ";
          }
        } else if (data.date === "0") {
          if (typeAgent === 0 || typeAgent === 1) {
            date = "  and  DATE(ac.created_date + INTERVAL '7 hours') > current_date - INTERVAL '8 DAY' and DATE(ac.created_date + INTERVAL '7 hours') != current_date ";
          } else {
            date = " where DATE(ac.created_date + INTERVAL '7 hours') > current_date - INTERVAL '8 DAY' and DATE(ac.created_date + INTERVAL '7 hours') != current_date ";
          }
        }
      } else {
        if (data.date === "1") {
          date = " and DATE(ac.created_date) = current_date ";
        } else if (data.date === "0") {
          date = " and DATE(ac.created_date) > current_date - INTERVAL '8 DAY' and DATE(ac.created_date) != current_date  ";
        }
      }
    }

    let first = "select ac.id , ac.created_date + INTERVAL '7 hours' as created_date ,ac.total as credit, ac.description,";
    first += "COALESCE((select MAX(a.username)  from agent a2 inner join agent a on a.agent_id = a2.agent_id where a2.agent_id  = ac.agent_id )) as agent,";
    first += "COALESCE((SELECT a.username FROM agent a2 INNER JOIN agent a ON a2.agent = a.agent_id WHERE a2.agent_id = ac.agent_id), 'master') AS actor,";
    first += "COALESCE((select a2.username from agent a2 where a2.agent_id  = ac.agent_id )) as deal_with,";
    first += "COALESCE((select transaction_type  from transaction_type tt  where tt.id  = ac.type_id  ),null) as transaction_type,";
    first += "COALESCE((select an.username from agent a2 inner join agent a ON a.agent_id = a2.agent_id INNER JOIN agent aa ON aa.agent_id = a.agent INNER JOIN agent an ON aa.agent = an.agent_id WHERE a2.agent_id = ac.agent_id), null) as sub_actor,";
    first += "ac.current_credit as current_credit ";

    if (typeAgent === 0) {
      first += "from ask_credit ac where ac.agent_id = " + agentId + text + typeName + date + " and ac.status=4  group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    } else if (typeAgent === 1) {
      first += "from ask_credit ac where ac.agent_id = " + agentNo + text + typeName + date + " and ac.status=4 group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    } else if (typeAgent === 2 || typeAgent === 3) {
      first += "from ask_credit ac " + text + typeName + date + " and ac.status=4 group by ac.id , ac.created_date ,ac.total , ac.description, ac.agent_id, ac.type_id, ac.current_credit " + types + more_than_amount + less_amount + " order by ac.created_date desc;";
    }

    let respon = await db_access.query(first);
    const response = respon.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new ReportCredit.
exports.post_reportCredit = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ReportCredit' }
  } */
  const data = req.body;

  try {
    await db_access.query(
      "CALL public.spinsertupdatereportcredit($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);",
      [
        0,
        utilities.is_empty_string(data.report_no) ? null : data.report_no,
        utilities.is_empty_string(data.detail) ? null : data.detail,
        utilities.is_empty_string(data.doer) ? null : data.doer,
        utilities.is_empty_string(data.deal_with) ? null : data.deal_with,
        utilities.is_empty_string(data.credit) ? null : data.credit,
        utilities.is_empty_string(data.date_time) ? null : data.date_time,
        utilities.is_empty_string(data.note) ? null : data.note,
        utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
        utilities.is_empty_string(data.created_by) ? null : data.created_by,
        utilities.is_empty_string(data.created_date) ? null : data.created_date,
        utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
        utilities.is_empty_string(data.updated_date) ? null : data.updated_date,
      ]
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new ReportCredit.
exports.put_reportCredit = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/ReportCredit' }
  } */
  const data = req.body;

  try {
    await db_access.query(
      "CALL public.spinsertupdatereportcredit($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13);",
      [
        utilities.is_empty_string(data.report_credit_id)
          ? null
          : data.report_credit_id,
        utilities.is_empty_string(data.report_no) ? null : data.report_no,
        utilities.is_empty_string(data.detail) ? null : data.detail,
        utilities.is_empty_string(data.doer) ? null : data.doer,
        utilities.is_empty_string(data.deal_with) ? null : data.deal_with,
        utilities.is_empty_string(data.credit) ? null : data.credit,
        utilities.is_empty_string(data.date_time) ? null : data.date_time,
        utilities.is_empty_string(data.note) ? null : data.note,
        utilities.is_empty_string(data.agent_id) ? null : data.agent_id,
        utilities.is_empty_string(data.created_by) ? null : data.created_by,
        utilities.is_empty_string(data.created_date) ? null : data.created_date,
        utilities.is_empty_string(data.updated_by) ? null : data.updated_by,
        utilities.is_empty_string(data.updated_date) ? null : data.updated_date,
      ]
    );

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete ReportCredit.
exports.delete_reportCredit = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletereportcredit($1);", [
      utilities.is_empty_string(data.report_credit_id)
        ? null
        : data.report_credit_id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

//report summary budget
exports.get_summary_budget = async (req, res) => {
  try {
    let week = "";
    let member_id = "";
    let agent_id = ""
    let pagination = "";
    let whereClause = "";
    const body = req.query;

    if (body.week) {
      week = body.week;
      whereClause += ` AND rs.date = '${week}'`;
    }

    if (body.member_id) {
      member_id = body.member_id;
      whereClause += ` AND rs.agent_id = '${member_id}'`;
    }

    if(body.agent_id){
      agent_id = body.agent_id;
      whereClause += ` AND rs.agent_id = '${agent_id}'`
    }

    if (body.limit && body.limit > 0 && body.page && body.page > 0) {
      const offset = (parseInt(body.page) - 1) * parseInt(body.limit);
      pagination += ` LIMIT ${body.limit} OFFSET ${offset}`;
    }

    // const sql = `select * from (select wa.date as "date" ,
    // wa.agent_id,
    // wa.username,
    // sum(wa.amount) as direct_amount,
    // coalesce(sum(vip.vip_amount), 0) as vip_amount,
    // coalesce(sum(sc.shootnumber_cost), 0) as shootnumber_cost
    // from (
    // select CONCAT(DATE_PART('week', wa.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', wa.created_date AT TIME ZONE 'UTC+7')) as "date" ,
    // a.agent_id,
    // a.username,
    // sum(wa.amount)* -1 as amount
    // from withdraw_affiliate wa 
    // inner join members m on m.id = wa.member_id
    // inner join agent a on a.agent_id = m.agent_id
    // group by CONCAT(DATE_PART('week', wa.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', wa.created_date AT TIME ZONE 'UTC+7')), a.agent_id, a.username
    // ) wa 
    // inner join agent a on a.agent_id = wa.agent_id
    // left join (
    //   select CONCAT(DATE_PART('week', va.accepted_date AT TIME ZONE 'UTC+7'), '/', date_part('year', va.accepted_date AT TIME ZONE 'UTC+7')) as "date" ,
    //  a.agent_id,
    //  a.username,
    //  sum(va.amount) as vip_amount
    //  from vip_accepted va
    //  inner join members m on m.id = va.member_id
    //  inner join agent a on a.agent_id = m.agent_id
    //  group by CONCAT(DATE_PART('week', va.accepted_date AT TIME ZONE 'UTC+7'), '/', date_part('year', va.accepted_date AT TIME ZONE 'UTC+7')), a.agent_id, a.username
    // ) vip on vip.agent_id = a.agent_id  and vip.date = wa.date
    // left join (
    //   select CONCAT(DATE_PART('week', sc.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', sc.created_date AT TIME ZONE 'UTC+7')) as "date" ,
    //  a.agent_id,
    //  a.username ,
    //  sum(sc.amount) as shootnumber_cost
    //  from shootnumber_cost sc
    //  inner join members m on m.id = sc.member_id
    //  inner join agent a on a.agent_id = m.agent_id
    //  group by CONCAT(DATE_PART('week', sc.created_date AT TIME ZONE 'UTC+7'), '/', date_part('year', sc.created_date AT TIME ZONE 'UTC+7')), a.agent_id, a.username
    // ) sc on sc.agent_id = wa.agent_id and sc.date = wa.date
    // group by wa.date ,
    // wa.agent_id,
    // wa.username
    // ) rs 

    const sql = `select * from (select * from (
	    select c.date, c.agent_id, c.username, 
	    coalesce(wa.amount, 0) as direct_amount,
	    coalesce(vip.vip_amount, 0) as vip_amount,
	    coalesce(sc.shootnumber_cost, 0) as shootnumber_cost from (
		    select * from agent a2
		    cross join (
		    	select CONCAT(EXTRACT(WEEK FROM start), '/', EXTRACT(ISOYEAR FROM start)) as "date" from (
				    select greatest(date_trunc('week', dates.d), date_trunc('month',dates.d)) as start
					from generate_series(date_trunc('month', current_date - interval '1' month)::date, current_date, '1 day') as dates(d)
					group by 1
					order by 1
				) as a 
				order by start desc
			limit 5) as b
		) as c
	    left join (
		    -- select CONCAT(EXTRACT(WEEK FROM wa.created_date), '/', EXTRACT(ISOYEAR FROM wa.created_date)) as "date" ,
        select CONCAT(EXTRACT(WEEK FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date  + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		    a.agent_id,
		    a.username,
		    sum(wa.amount)* -1 as amount
		    from withdraw_affiliate wa 
		    inner join members m on m.id = wa.member_id
		    inner join agent a on a.agent_id = m.agent_id
        group by CONCAT(EXTRACT(WEEK FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date  + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
		    -- group by CONCAT(EXTRACT(WEEK FROM wa.created_date), '/', EXTRACT(ISOYEAR FROM wa.created_date)), a.agent_id, a.username
	    ) wa on c.agent_id = wa.agent_id and c.date = wa.date
	    left join (
		     -- select CONCAT(EXTRACT(WEEK FROM va.accepted_date), '/', EXTRACT(ISOYEAR FROM va.accepted_date)) as "date" ,
         select CONCAT(EXTRACT(WEEK FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		     a.agent_id,
		     a.username,
		     sum(va.amount) as vip_amount
		     from vip_accepted va
		     inner join members m on m.id = va.member_id
		     inner join agent a on a.agent_id = m.agent_id
         group by CONCAT(EXTRACT(WEEK FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
		     -- group by CONCAT(EXTRACT(WEEK FROM va.accepted_date), '/', EXTRACT(ISOYEAR FROM va.accepted_date)), a.agent_id, a.username
	    ) vip on c.agent_id = vip.agent_id and c.date = vip.date
	    left join (
		     -- select CONCAT(EXTRACT(WEEK FROM sc.created_date), '/', EXTRACT(ISOYEAR FROM sc.created_date)) as "date" ,
         select CONCAT(EXTRACT(WEEK FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		     a.agent_id,
		     a.username ,
		     sum(sc.amount) as shootnumber_cost
		     from shootnumber_cost sc
		     inner join members m on m.id = sc.member_id
		     inner join agent a on a.agent_id = m.agent_id
         group by CONCAT(EXTRACT(WEEK FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
		     -- group by CONCAT(EXTRACT(WEEK FROM sc.created_date), '/', EXTRACT(ISOYEAR FROM sc.created_date)), a.agent_id, a.username
	    ) sc on c.agent_id = sc.agent_id and c.date = sc.date
	    group by c.date, c.agent_id, c.username, wa.amount, vip.vip_amount, sc.shootnumber_cost
    ) as d
    where direct_amount <> 0 or vip_amount <> 0 or shootnumber_cost <> 0
    order by date asc
    ) rs 
    where 1=1 ${whereClause}`;

    const result = await db_access.query(
      `select * from (${sql}) rs ${pagination}`
    );

    const total_sql = `select coalesce(sum(rs.direct_amount),0) as total_direct_amount, 
    coalesce(sum(rs.vip_amount), 0) as total_vip_amount, 
    coalesce(sum(rs.shootnumber_cost), 0) as total_shootnumber_cost
    from (${sql}) rs`;
    const data = result.rows.map((item) => ({
      ...item,
      direct_amount: parseFloat(item.direct_amount),
      vip_amount: parseFloat(item.vip_amount),
      shootnumber_cost: parseFloat(item.shootnumber_cost),
      total:
        parseFloat(item.direct_amount) +
        parseFloat(item.vip_amount) +
        parseFloat(item.shootnumber_cost),
    }));

    let total = {
      total_direct_amount: 0,
      total_vip_amount: 0,
      total_shootnumber_cost: 0,
      all: 0,
    };
    const result_sum = await db_access.query(total_sql);
    if (result_sum.rowCount > 0) {
      const item = result_sum.rows[0];
      total = {
        total_direct_amount: parseFloat(item.total_direct_amount),
        total_vip_amount: parseFloat(item.total_vip_amount),
        total_shootnumber_cost: parseFloat(item.total_shootnumber_cost),
        all: parseFloat(item.total_direct_amount) + parseFloat(item.total_vip_amount) + parseFloat(item.total_shootnumber_cost),
      };
    }
    let total_record = 0;
    const result_total = await db_access.query(
      `select count(*) as total_record from (${sql}) rs`
    );
    if (result_total.rowCount > 0) {
      total_record = parseInt(result_total.rows[0].total_record);
    }
    console.log(data)
    return await response_success_builder(res, {
      rows: data,
      total: total,
      total_record: total_record,
    });
  } catch (error) {
    console.log(error)
    return await response_error_builder(res, error);
  }
};

// Retrieve ReportCredit.
exports.get_expenseSummary = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const agentId = req.user.agent_id;
    const type =
      "select a.type_user, a.agent from agent a where a.agent_id =" +
      agentId +
      ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    let one = "";
    let two = "";
    let list = [];
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    one += "select ";
    one += "    created_date, username,";
    one +=
      "    SUM(total_recommended) AS total_recommended, SUM(rank_vip) AS rank_vip,";
    one +=
      "    SUM(firing_fee) AS firing_fee, SUM(total_recommended + rank_vip + firing_fee) AS total_sum ";
    one += "FROM (";
    one += "    SELECT";
    one += "        m.username,";
    one +=
      "        COALESCE(COALESCE(DATE(wa.created_date), DATE(va.accepted_date)), DATE(sc.created_date)) AS created_date,";
    one +=
      "        COALESCE(ABS(wa.amount), 0) AS total_recommended, COALESCE(va.amount, 0) AS rank_vip,";
    one += "        COALESCE(sc.amount, 0) AS firing_fee";
    one += "    FROM";
    one += "        members m";
    one += "    LEFT JOIN withdraw_affiliate wa ON wa.member_id = m.id";
    one += "    LEFT JOIN vip_accepted va ON va.member_id = m.id";
    one += "    LEFT JOIN shootnumber_cost sc ON sc.member_id = m.id";
    two += ") AS subquery ";
    two += "GROUP BY username, created_date ";
    two += "HAVING created_date IS NOT null ";
    two += "order by created_date desc ;";
    if (typeAgent === 0) {
      one += "where m.id=" + agentId + " ";
    } else if (typeAgent === 1) {
      one += "where m.id=" + agentNo + " ";
    }
    let resu = await db_access.query(one + two);
    const resp = resu.rows;
    if (resp != null) {
      for (let i = 0; i < resp.length; i++) {
        const data = {
          created_date: {},
          username: {},
          total_recommended: {},
          rank_vip: {},
          firing_fee: {},
          total: {},
        };
        const date = resp[i].created_date;
        const username = resp[i].username;
        const total_recommended = resp[i].total_recommended;
        const rank_vip = resp[i].rank_vip;
        const firing_fee = resp[i].firing_fee;
        const total = resp[i].total;
        let weekNumber = moment(date).isoWeek();
        let year = moment(date).locale("th").format("YYYY");
        let createDate = "สัปดาห์ที่ " + weekNumber + "/" + year;
        data.created_date = createDate;
        data.username = username;
        data.total_recommended = total_recommended;
        data.rank_vip = rank_vip;
        data.firing_fee = firing_fee;
        data.total = total;
        list.push(data);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: list,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve ReportCredit.
exports.get_expenseSummaryByWeek = async (req, res) => {
  // #swagger.tags = ['ReportCredit']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/ReportCredit" }} */
  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const type =
      "select a.type_user, a.agent from agent a where a.agent_id =" +
      agentId +
      ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = "";
    let one = "";
    let two = "";
    let username = "";
    let day = "";
    let list = [];
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (
      (data.year !== null || data.year !== "" || data.year !== undefined) &&
      (data.week !== null || data.week !== "" || data.week !== undefined)
    ) {
      let year = "'" + data.year + "'";

      let d = moment(year, "YYYY").week(data.week);
      const startMonth = d.clone().startOf("week");
      const endMonth = d.clone().endOf("week");
      let start = moment(startMonth).locale("th").format("YYYY-MM-DD");
      let end = moment(endMonth).locale("th").format("YYYY-MM-DD");
      let startDay = moment(start, "YYYY-MM-DD")
        .add(1, "days")
        .format("YYYY-MM-DD");
      let endDay = moment(end, "YYYY-MM-DD")
        .add(1, "days")
        .format("YYYY-MM-DD");

      if (typeAgent === 0 || typeAgent === 1) {
        day =
          "  and ((DATE(wa.created_date) >= '" +
          startDay +
          "' and DATE(wa.created_date) <= '" +
          endDay +
          "') or (DATE(va.accepted_date) >= '" +
          startDay +
          "' and DATE(va.accepted_date) <= '" +
          endDay +
          "') or (DATE(sc.created_date) >= '" +
          startDay +
          "' and DATE(sc.created_date) <= '" +
          endDay +
          "'))";
      } else {
        day =
          " where ((DATE(wa.created_date) >= '" +
          startDay +
          "' and DATE(wa.created_date) <= '" +
          endDay +
          "') or (DATE(va.accepted_date) >= '" +
          startDay +
          "' and DATE(va.accepted_date) <= '" +
          endDay +
          "') or (DATE(sc.created_date) >= '" +
          startDay +
          "' and DATE(sc.created_date) <= '" +
          endDay +
          "'))";
      }
    }
    if (
      data.username === null ||
      data.username === "" ||
      data.username === undefined
    ) {
      username += "";
    } else {
      username += " and m.username = '" + data.username + "' ";
    }

    one += "select ";
    one += "    created_date, username,";
    one +=
      "    SUM(total_recommended) AS total_recommended, SUM(rank_vip) AS rank_vip,";
    one +=
      "    SUM(firing_fee) AS firing_fee, SUM(total_recommended + rank_vip + firing_fee) AS total_sum ";
    one += "FROM (";
    one += "    SELECT";
    one += "        m.username,";
    one +=
      "        COALESCE(COALESCE(DATE(wa.created_date), DATE(va.accepted_date)), DATE(sc.created_date)) AS created_date,";
    one +=
      "        COALESCE(ABS(wa.amount), 0) AS total_recommended, COALESCE(va.amount, 0) AS rank_vip,";
    one += "        COALESCE(sc.amount, 0) AS firing_fee";
    one += "    FROM";
    one += "        members m";
    one += "    LEFT JOIN withdraw_affiliate wa ON wa.member_id = m.id";
    one += "    LEFT JOIN vip_accepted va ON va.member_id = m.id";
    one += "    LEFT JOIN shootnumber_cost sc ON sc.member_id = m.id";
    two += ") AS subquery ";
    two += "GROUP BY username, created_date ";
    two += "HAVING created_date IS NOT null ";
    two += "order by created_date desc ;";
    if (typeAgent === 0) {
      one += " where m.id=" + agentId + " ";
    } else if (typeAgent === 1) {
      one += " where m.id=" + agentNo + " ";
    }
    let resu = await db_access.query(one + day + username + two);
    const resp = resu.rows;
    if (resp != null) {
      for (let i = 0; i < resp.length; i++) {
        const data = {
          created_date: {},
          username: {},
          total_recommended: {},
          rank_vip: {},
          firing_fee: {},
          total: {},
        };
        const date = resp[i].created_date;
        const username = resp[i].username;
        const total_recommended = resp[i].total_recommended;
        const rank_vip = resp[i].rank_vip;
        const firing_fee = resp[i].firing_fee;
        const total = resp[i].total;
        let weekNumber = moment(date).isoWeek();
        let year = moment(date).locale("th").format("YYYY");
        let createDate = "สัปดาห์ที่ " + weekNumber + "/" + year;
        data.created_date = createDate;
        data.username = username;
        data.total_recommended = total_recommended;
        data.rank_vip = rank_vip;
        data.firing_fee = firing_fee;
        data.total = total;
        list.push(data);
      }
    }
    res.status(200).json({
      code: 200,
      message: "Success",
      data: list,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

exports.get_summary_budget_by_week = async (req, res) => {
  try {
    let month = "";
    let member_id = "";
    let pagination = "";
    let whereClause = "";
    const body = req.query;

    if (body.month) {
      month = body.month;
      whereClause += ` AND rs.month = '${month}'`;
    }

    if (body.member_id) {
      member_id = body.member_id;
      whereClause += ` AND rs.agent_id = '${member_id}'`;
    }


    if (body.limit && body.limit > 0 && body.page && body.page > 0) {
      const offset = (parseInt(body.page) - 1) * parseInt(body.limit);
      pagination += ` LIMIT ${body.limit} OFFSET ${offset}`;
    }
    const sql = `select * from (select * from (
	    select c.date, TO_CHAR(TO_DATE(c.month, 'MM/YYYY'), 'MM/YYYY')  as month,
	    coalesce(wa.amount, 0) as direct_amount,
	    coalesce(vip.vip_amount, 0) as vip_amount,
	    coalesce(sc.shootnumber_cost, 0) as shootnumber_cost,
	    coalesce(nt.net_income, 0) as net_income from (
		    select * from agent a2
		    cross join (
		    	select CONCAT(EXTRACT(WEEK FROM start), '/', EXTRACT(ISOYEAR FROM start)) as "date", CONCAT(EXTRACT(MONTH FROM start), '/', EXTRACT(ISOYEAR FROM start)) as "month" from (
				    select greatest(date_trunc('week', dates.d), date_trunc('month',dates.d)) as start
					from generate_series(date_trunc('month', current_date - interval '1' month)::date, current_date, '1 day') as dates(d)
					group by 1
					order by 1
				) as a 
				order by start desc
			limit 5) as b
		) as c
	    left join (
		    -- select CONCAT(EXTRACT(WEEK FROM wa.created_date), '/', EXTRACT(ISOYEAR FROM wa.created_date)) as "date" ,
        select CONCAT(EXTRACT(WEEK FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date  + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		    sum(wa.amount)* -1 as amount
		    from withdraw_affiliate wa 
		    inner join members m on m.id = wa.member_id
		    inner join agent a on a.agent_id = m.agent_id
        group by CONCAT(EXTRACT(WEEK FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date  + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end))
		    -- group by CONCAT(EXTRACT(WEEK FROM wa.created_date), '/', EXTRACT(ISOYEAR FROM wa.created_date)), a.agent_id, a.username
	    ) wa on c.date = wa.date
	    left join (
		     -- select CONCAT(EXTRACT(WEEK FROM va.accepted_date), '/', EXTRACT(ISOYEAR FROM va.accepted_date)) as "date" ,
         select CONCAT(EXTRACT(WEEK FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		     sum(va.amount) as vip_amount
		     from vip_accepted va
		     inner join members m on m.id = va.member_id
		     inner join agent a on a.agent_id = m.agent_id
         group by CONCAT(EXTRACT(WEEK FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end))
		     -- group by CONCAT(EXTRACT(WEEK FROM va.accepted_date), '/', EXTRACT(ISOYEAR FROM va.accepted_date)), a.agent_id, a.username
	    ) vip on c.date = vip.date
	    left join (
		     -- select CONCAT(EXTRACT(WEEK FROM sc.created_date), '/', EXTRACT(ISOYEAR FROM sc.created_date)) as "date" ,
         select CONCAT(EXTRACT(WEEK FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date" ,
		     sum(sc.amount) as shootnumber_cost
		     from shootnumber_cost sc
		     inner join members m on m.id = sc.member_id
		     inner join agent a on a.agent_id = m.agent_id
         group by CONCAT(EXTRACT(WEEK FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end))
		     -- group by CONCAT(EXTRACT(WEEK FROM sc.created_date), '/', EXTRACT(ISOYEAR FROM sc.created_date)), a.agent_id, a.username
	    ) sc on c.date = sc.date
	      left join (
	         -- select CONCAT(EXTRACT(WEEK FROM lh.created_date), '/', EXTRACT(ISOYEAR FROM lh.created_date)) as "date" ,
                select CONCAT(EXTRACT(WEEK FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date",
                sum(lh.total_price) - sum(lh.total_win) as net_income
                from lotto_header lh 
                inner join members m on m.id = lh.member_id
                inner join agent a on a.agent_id = m.agent_id
                where lh.status in (8,9,10) 
                group by CONCAT(EXTRACT(WEEK FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end))
             -- group by CONCAT(EXTRACT(WEEK FROM lh.created_date), '/', EXTRACT(ISOYEAR FROM lh.created_date)), a.agent_id, a.username
            ) nt on c.date = nt.date
	    group by c.date, wa.amount, vip.vip_amount, sc.shootnumber_cost, nt.net_income, c.month
    ) as d
    where direct_amount <> 0 or vip_amount <> 0 or shootnumber_cost <> 0  or net_income <> 0
    order by TO_CHAR(TO_DATE(month, 'MM/YYYY'), 'MM/YYYY') = '${month}' asc ,  date asc
    ) rs 
    where 1=1 ${whereClause}`;

    const result = await db_access.query(
        `select * from (${sql}) rs ${pagination}`
    );

    const total_sql = `select coalesce(sum(rs.direct_amount),0) as total_direct_amount, 
    coalesce(sum(rs.vip_amount), 0) as total_vip_amount, 
    coalesce(sum(rs.shootnumber_cost), 0) as total_shootnumber_cost,
    coalesce(sum(rs.net_income), 0) as net_income
    from (${sql}) rs`;
    const data = result.rows.map((item) => ({
      ...item,
      direct_amount: parseFloat(item.direct_amount),
      vip_amount: parseFloat(item.vip_amount),
      shootnumber_cost: parseFloat(item.shootnumber_cost),
      net_income: parseFloat(item.net_income),
      total:
          -(parseFloat(item.direct_amount) +
          parseFloat(item.vip_amount) +
          parseFloat(item.shootnumber_cost)),
    }));

    let total = {
      net_income: 0,
      all: 0,
    };
    const result_sum = await db_access.query(total_sql);
    if (result_sum.rowCount > 0) {
      const item = result_sum.rows[0];
      total = {
        net_income: parseFloat(item.net_income),
        all: -(parseFloat(item.total_direct_amount) + parseFloat(item.total_vip_amount) + parseFloat(item.total_shootnumber_cost)),
      };
    }
    let total_record = 0;
    const result_total = await db_access.query(
        `select count(*) as total_record from (${sql}) rs`
    );
    if (result_total.rowCount > 0) {
      total_record = parseInt(result_total.rows[0].total_record);
    }
    return await response_success_builder(res, {
      rows: data,
      total: total,
      total_record: total_record,
    });
  } catch (error) {
    console.log(error)
    return await response_error_builder(res, error);
  }
};


exports.get_summary_budget_by_month = async (req, res) => {
  try {
    let month = "";
    let member_id = "";
    let agent_id = "";
    let pagination = "";
    let whereClause = "";
    const body = req.query;

    if (body.month) {
      month = body.month;
      whereClause += ` AND rs.date=  CASE 
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '01' THEN CONCAT('มกราคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '02' THEN CONCAT('กุมภาพันธ์ ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '03' THEN CONCAT('มีนาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '04' THEN CONCAT('เมษายน ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '05' THEN CONCAT('พฤษภาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '06' THEN CONCAT('มิถุนายน ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '07' THEN CONCAT('กรกฎาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '08' THEN CONCAT('สิงหาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '09' THEN CONCAT('กันยายน ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '10' THEN CONCAT('ตุลาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '11' THEN CONCAT('พฤศจิกายน ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'MM') = '12' THEN CONCAT('ธันวาคม ', TO_CHAR(TO_DATE('${month}', 'MM/YYYY'), 'YYYY'))
      ELSE 'ไม่ระบุ'
    END `;
    }

    if (body.member_id) {
      member_id = body.member_id;
      whereClause += ` AND rs.agent_id = '${member_id}'`;
    }

    if (body.agent_id) {
      agent_id = body.agent_id;
      whereClause += ` AND rs.agent_id = '${agent_id}'`;
    }

    if (body.limit && body.limit > 0 && body.page && body.page > 0) {
      const offset = (parseInt(body.page) - 1) * parseInt(body.limit);
      pagination += ` LIMIT ${body.limit} OFFSET ${offset}`;
    }

    const sql = `select * from (select * from (
        select  CASE 
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '01' THEN CONCAT('มกราคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '02' THEN CONCAT('กุมภาพันธ์ ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '03' THEN CONCAT('มีนาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '04' THEN CONCAT('เมษายน ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '05' THEN CONCAT('พฤษภาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '06' THEN CONCAT('มิถุนายน ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '07' THEN CONCAT('กรกฎาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '08' THEN CONCAT('สิงหาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '09' THEN CONCAT('กันยายน ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '10' THEN CONCAT('ตุลาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '11' THEN CONCAT('พฤศจิกายน ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      WHEN TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'MM') = '12' THEN CONCAT('ธันวาคม ', TO_CHAR(TO_DATE(c.date, 'MM/YYYY'), 'YYYY'))
      ELSE 'ไม่ระบุ'
    END  as date, c.agent_id, c.username,
        coalesce(wa.amount, 0) as direct_amount,
        coalesce(vip.vip_amount, 0) as vip_amount,
        coalesce(sc.shootnumber_cost, 0) as shootnumber_cost,
        coalesce(nt.net_income, 0) as net_income from (
            select * from agent a2
            cross join (
                select CONCAT(EXTRACT(MONTH FROM start), '/', EXTRACT(ISOYEAR FROM start)) as "date" from (
                    select greatest(date_trunc('week', dates.d), date_trunc('month', dates.d)) as start
                    from generate_series(date_trunc('month', current_date - interval '1' month)::date, current_date, '1 day') as dates(d)
                    group by 1
                    order by 1
                ) as a
                order by start desc
                limit 5) as b
        ) as c
        left join (
            select CONCAT(EXTRACT(MONTH FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date",
            a.agent_id,
            a.username,
            sum(wa.amount) * -1 as amount
            from withdraw_affiliate wa
            inner join members m on m.id = wa.member_id
            inner join agent a on a.agent_id = m.agent_id
            group by CONCAT(EXTRACT(MONTH FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (wa.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (wa.created_date + INTERVAL '7 hours')::date else (wa.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
        ) wa on c.agent_id = wa.agent_id and c.date = wa.date
        left join (
            select CONCAT(EXTRACT(MONTH FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date",
            a.agent_id,
            a.username,
            sum(va.amount) as vip_amount
            from vip_accepted va
            inner join members m on m.id = va.member_id
            inner join agent a on a.agent_id = m.agent_id
            group by CONCAT(EXTRACT(MONTH FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (va.accepted_date + INTERVAL '7 hours')::time >= '04:00:00' then (va.accepted_date + INTERVAL '7 hours')::date else (va.accepted_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
        ) vip on c.agent_id = vip.agent_id and c.date = vip.date
        left join (
            select CONCAT(EXTRACT(MONTH FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date",
            a.agent_id,
            a.username,
            sum(sc.amount) as shootnumber_cost
            from shootnumber_cost sc
            inner join members m on m.id = sc.member_id
            inner join agent a on a.agent_id = m.agent_id
            group by CONCAT(EXTRACT(MONTH FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (sc.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (sc.created_date + INTERVAL '7 hours')::date else (sc.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
        ) sc on c.agent_id = sc.agent_id and c.date = sc.date
        left join (
            select CONCAT(EXTRACT(MONTH FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)) as "date",
            a.agent_id,
            a.username,
            sum(lh.total_price) - sum(lh.total_win) as net_income
            from lotto_header lh
            inner join members m on m.id = lh.member_id
            inner join agent a on a.agent_id = m.agent_id
            where lh.status in (8,9,10) 
            group by CONCAT(EXTRACT(MONTH FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end), '/', EXTRACT(ISOYEAR FROM case when (lh.created_date + INTERVAL '7 hours')::time >= '04:00:00' then (lh.created_date + INTERVAL '7 hours')::date else (lh.created_date + INTERVAL '7 hours')::date - INTERVAL '1 days' end)), a.agent_id, a.username
        ) nt on c.agent_id = nt.agent_id and c.date = nt.date
        group by c.date, c.agent_id, c.username, wa.amount, vip.vip_amount, sc.shootnumber_cost, nt.net_income
    ) as d
    where direct_amount <> 0 or vip_amount <> 0 or shootnumber_cost <> 0 or net_income <> 0
    order by date desc
    ) rs
    where 1=1 ${whereClause}`;

    const result = await db_access.query(
        `select * from (${sql}) rs ${pagination}`
    );

    const total_sql = `select coalesce(sum(rs.direct_amount),0) as total_direct_amount, 
    coalesce(sum(rs.vip_amount), 0) as total_vip_amount, 
    coalesce(sum(rs.shootnumber_cost), 0) as total_shootnumber_cost,
    coalesce(sum(rs.net_income), 0) as net_income
    from (${sql}) rs`;
    const data = result.rows.map((item) => ({
      ...item,
      direct_amount: parseFloat(item.direct_amount),
      vip_amount: parseFloat(item.vip_amount),
      shootnumber_cost: parseFloat(item.shootnumber_cost),
      net_income: parseFloat(item.net_income),
      total:
          -(parseFloat(item.direct_amount) +
          parseFloat(item.vip_amount) +
          parseFloat(item.shootnumber_cost)),
    }));

    let total = {
      net_income: 0,
      all: 0,
    };
    const result_sum = await db_access.query(total_sql);
    if (result_sum.rowCount > 0) {
      const item = result_sum.rows[0];
      total = {
        net_income: parseFloat(item.net_income),
        all:
            -(parseFloat(item.total_direct_amount) +
            parseFloat(item.total_vip_amount) +
            parseFloat(item.total_shootnumber_cost)),
      };
    }
    let total_record = 0;
    const result_total = await db_access.query(
        `select count(*) as total_record from (${sql}) rs`
    );
    if (result_total.rowCount > 0) {
      total_record = parseInt(result_total.rows[0].total_record);
    }
    return await response_success_builder(res, {
      rows: data,
      total: total,
      total_record: total_record,
    });
  } catch (error) {
    console.log(error);
    return await response_error_builder(res, error);
  }
};