const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve SettingDepositWithdraw.
exports.get_settingDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['SettingDepositWithdraw']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/SettingDepositWithdraw" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    let one = "SELECT dw.id, dw.agent_id, dw.withdraw_quota, dw.minimum_deposit, dw.minimum_withdraw, dw.maximum_withdraw, dw.turnover_percentage, dw.not_allow_no_source, dw.active_auto_approve, dw.max_amount_approve, dw.created_date, dw.updated_date, a.username_secret, a.username FROM public.setting_deposit_withdraw dw inner join agent a on a.agent_id = dw.agent_id ";
    if (typeAgent === 0) {
       one += "where dw.agent_id ="+agentId+";";
    }  else if (typeAgent === 1) {
      one += "where dw.agent_id ="+agentNo+";";
    }
    let result = await db_access.query(one);
    const resp = result.rows;
    const response = Object.values(resp).values().next().value;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: response,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new SettingDepositWithdraw.
exports.post_settingDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['SettingDepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/SettingDepositWithdraw' }
  } */
  const data = req.body;

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let agentNo = null;
    let ag = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 1){
      ag = agentNo;
    } else{
      ag = agentId;
    }
    await db_access.query("CALL public.spinsertupdatesettingdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);", [
      0,
      utilities.is_empty_string(ag) ? null : ag,
      utilities.is_empty_string(data.withdraw_quota) ? null : data.withdraw_quota,
      utilities.is_empty_string(data.minimum_deposit) ? null : data.minimum_deposit,
      utilities.is_empty_string(data.minimum_withdraw) ? null : data.minimum_withdraw,
      utilities.is_empty_string(data.maximum_withdraw) ? null : data.maximum_withdraw,
      utilities.is_empty_string(data.turnover_percentage) ? null : data.turnover_percentage,
      utilities.is_empty_string(data.not_allow_no_source) ? null : data.not_allow_no_source,
      utilities.is_empty_string(data.active_auto_approve) ? null : data.active_auto_approve,
      utilities.is_empty_string(data.max_amount_approve) ? null : data.max_amount_approve
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new SettingDepositWithdraw.
// ติดอยู่แต่หน้าไม่มีใช้
exports.put_settingDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['SettingDepositWithdraw']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/SettingDepositWithdraw' }
  } */
  const data = req.body;
  try {
    const agentId = req.user.agent_id;
    let age = "";
    let agen = "";
    let typeUser = "";
    let tt = "SELECT a.type_user,a.agent FROM agent a where a.agent_id ="+agentId+";";
    let resss = await db_access.query(tt);
    const responseTypeUser = resss.rows;
    for (let i = 0; i < responseTypeUser.length; i++){
      typeUser = responseTypeUser[i].type_user;
      age = responseTypeUser[i].agent;
    }
    if (typeUser === 1){
      agen= age;
    } else{
      agen = agentId;
    }
    let result = await db_access.query("SELECT * FROM public.setting_deposit_withdraw where agent_id ="+agen+";");
    const resp = result.rows;
    let agent = "";
    if (resp.length > 0) {
      for (let i = 0; i < resp.length; i++) {
        agent = resp[i].agent_id;
      }
      if (agent === agentId) {
        await db_access.query("CALL public.spinsertupdatesettingdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);", [
          utilities.is_empty_string(data.id) ? null : data.id,
          utilities.is_empty_string(agen) ? null : agen,
          utilities.is_empty_string(data.withdraw_quota) ? 0 : data.withdraw_quota,
          utilities.is_empty_string(data.minimum_deposit) ? 0 : data.minimum_deposit,
          utilities.is_empty_string(data.minimum_withdraw) ? 0 : data.minimum_withdraw,
          utilities.is_empty_string(data.maximum_withdraw) ? 0 : data.maximum_withdraw,
          utilities.is_empty_string(data.turnover_percentage) ? 0 : data.turnover_percentage,
          utilities.is_empty_string(data.not_allow_no_source) ? null : data.not_allow_no_source,
          utilities.is_empty_string(data.active_auto_approve) ? null : data.active_auto_approve,
          utilities.is_empty_string(data.max_amount_approve) ? 0 : data.max_amount_approve
        ]);
      } else {
        await db_access.query("CALL public.spinsertupdatesettingdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);", [
          0,
          utilities.is_empty_string(agen) ? null : agen,
          utilities.is_empty_string(data.withdraw_quota) ? 0 : data.withdraw_quota,
          utilities.is_empty_string(data.minimum_deposit) ? 0 : data.minimum_deposit,
          utilities.is_empty_string(data.minimum_withdraw) ? 0 : data.minimum_withdraw,
          utilities.is_empty_string(data.maximum_withdraw) ? 0 : data.maximum_withdraw,
          utilities.is_empty_string(data.turnover_percentage) ? null : data.turnover_percentage,
          utilities.is_empty_string(data.not_allow_no_source) ? null : data.not_allow_no_source,
          utilities.is_empty_string(data.active_auto_approve) ? 0 : data.active_auto_approve,
          utilities.is_empty_string(data.max_amount_approve) ? 0 : data.max_amount_approve
        ]);
      }
      res.status(200).json({
        code: 200,
        message: "Success",
      });
    } else{
      await db_access.query("CALL public.spinsertupdatesettingdepositwithdraw($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);", [
        0,
        utilities.is_empty_string(agen) ? null : agen,
        utilities.is_empty_string(data.withdraw_quota) ? 0 : data.withdraw_quota,
        utilities.is_empty_string(data.minimum_deposit) ? 0 : data.minimum_deposit,
        utilities.is_empty_string(data.minimum_withdraw) ? 0 : data.minimum_withdraw,
        utilities.is_empty_string(data.maximum_withdraw) ? 0 : data.maximum_withdraw,
        utilities.is_empty_string(data.turnover_percentage) ? 0 : data.turnover_percentage,
        utilities.is_empty_string(data.not_allow_no_source) ? null : data.not_allow_no_source,
        utilities.is_empty_string(data.active_auto_approve) ? 0 : data.active_auto_approve,
        utilities.is_empty_string(data.max_amount_approve) ? 0 : data.max_amount_approve
      ]);
      res.status(200).json({
        code: 200,
        message: "Success",
      });
    }
  } catch (error) {
      console.log("error : ", error);
      res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete SettingDepositWithdraw.
exports.delete_settingDepositWithdraw = async (req, res) => {
  // #swagger.tags = ['SettingDepositWithdraw']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletesettingdepositwithdraw($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
