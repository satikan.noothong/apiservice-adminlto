const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve smsGeneral.
exports.get_sms_general = async (req, res) => {
  // #swagger.tags = ['SmsGeneral']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/smsGeneral" }} */
  let type ;
  let result ;
  let type_user = "";
  let one = ""
  try {
    const agentId = req.user.agent_id;
    const typeUser = "select a.type_user from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(typeUser);
    const resps = resupp.rows;
    for (let i = 0; i < resps.length; i++) {
      type_user = resps[i].type_user;
    }
    if (type_user === 1){
      one = "SELECT * FROM sms_general sg inner join agent a on sg.agent_id = a.agent_id inner join agent a1 on a.agent = a1.agent_id where sg.agent_id="+agentId+" or a.agent = a1.agent_id order by sg.created_date desc;";
    } else{
      one = "SELECT * FROM sms_general sg where sg.agent_id="+agentId+" order by sg.created_date desc;";
    }
    result = await db_access.query(one);
    result.rows.filter((t) => {
      try {
         type =  t.type.includes('unknown');
      } catch {
         return false
      }
    });
    if (!type){
      result = await db_access.query("SELECT tt.transaction_type, sg.from, sg.to, sg.message, sg.created_date, a.username FROM sms_general sg inner join transaction_type tt on sg.type = tt.id inner join agent a on a.agent_id = sg.agent_id where sg.agent_id ="+agentId+"  order by sg.created_date desc;");
    }

    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Create a new smsGeneral.
exports.post_sms_general = async (req, res) => {
  // #swagger.tags = ['SmsGeneral']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/smsGeneral' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatesmsgeneral($1,$2,$3,$4,$5,$6);", [
      0,
      utilities.is_empty_string(data.from) ? null : data.from,
      utilities.is_empty_string(data.to) ? null : data.to,
      utilities.is_empty_string(data.type) ? null : data.type,
      utilities.is_empty_string(data.message) ? null : data.message
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new smsGeneral.
exports.put_sms_general = async (req, res) => {
  // #swagger.tags = ['SmsGeneral']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/smsGeneral' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatesmsgeneral($1,$2,$3,$4,$5,$6);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.from) ? null : data.from,
      utilities.is_empty_string(data.to) ? null : data.to,
      utilities.is_empty_string(data.type) ? null : data.type,
      utilities.is_empty_string(data.message) ? null : data.message,
      utilities.is_empty_string(data.date_time) ? null : data.date_time
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete smsGeneral.
exports.delete_sms_general = async (req, res) => {
  // #swagger.tags = ['SmsGeneral']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletesmsGeneral($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve smsGeneral.
exports.get_search_sms_general = async (req, res) => {
  // #swagger.tags = ['SmsGeneral']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/smsGeneral" }} */
  let type ;
  let result ;
  let type_user = "";
  let one = "";
  let two = "";
  let three = "";
  let four = "";
  try {
    const agentId = req.user.agent_id;
    const data = req.body;
    const typeUser = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(typeUser);
    const resps = resupp.rows;
    let agentNo = null;
    for (let i = 0; i < resps.length; i++) {
      type_user = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (type_user === 1) {
      one = "SELECT * FROM sms_general sg where sg.agent_id="+agentNo+" order by sg.created_date desc;";
      two = "SELECT tt.transaction_type, sg.from, sg.to, sg.message, sg.created_date, a.username FROM sms_general sg inner join transaction_type tt on sg.type = tt.id inner join agent a on a.agent_id = sg.agent_id where sg.agent_id ="+agentNo+"  order by sg.created_date desc;";
      three = "SELECT tt.transaction_type, sg.from, sg.to, sg.message, sg.created_date, a.username FROM sms_general sg inner join transaction_type tt on sg.type = tt.id inner join agent a on a.agent_id = sg.agent_id where sg.agent_id =" + agentNo + " and (cast(sg.id as text) ilike '%" + data.text + "%' or tt.transaction_type ilike '%" + data.text + "%' or sg.from ilike '%" + data.text + "%' or sg.to ilike '%" + data.text + "%' or sg.message ilike '%" + data.text + "%' or cast(sg.created_date as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%') order by sg.created_date desc;";
      four = "SELECT * FROM sms_general sg where sg.agent_id=" + agentNo + " and (cast(sg.id as text) ilike '%" + data.text + "%' or sg.from ilike '%" + data.text + "%' or sg.to ilike '%" + data.text + "%' or sg.type ilike '%" + data.text + "%' or sg.message ilike '%" + data.text + "%' or cast(sg.created_date as text) ilike '%" + data.text + "%')  order by sg.created_date desc;";
    } else{
      one = "SELECT * FROM sms_general sg where sg.agent_id="+agentId+" order by sg.created_date desc;";
      two = "SELECT tt.transaction_type, sg.from, sg.to, sg.message, sg.created_date, a.username FROM sms_general sg inner join transaction_type tt on sg.type = tt.id inner join agent a on a.agent_id = sg.agent_id where sg.agent_id ="+agentId+"  order by sg.created_date desc;";
      three = "SELECT tt.transaction_type, sg.from, sg.to, sg.message, sg.created_date, a.username FROM sms_general sg inner join transaction_type tt on sg.type = tt.id inner join agent a on a.agent_id = sg.agent_id where sg.agent_id =" + agentId + " and (cast(sg.id as text) ilike '%" + data.text + "%' or tt.transaction_type ilike '%" + data.text + "%' or sg.from ilike '%" + data.text + "%' or sg.to ilike '%" + data.text + "%' or sg.message ilike '%" + data.text + "%' or cast(sg.created_date as text) ilike '%" + data.text + "%' or a.username ilike '%" + data.text + "%') order by sg.created_date desc;";
      four = "SELECT * FROM sms_general sg where sg.agent_id=" + agentId + " and (cast(sg.id as text) ilike '%" + data.text + "%' or sg.from ilike '%" + data.text + "%' or sg.to ilike '%" + data.text + "%' or sg.type ilike '%" + data.text + "%' or sg.message ilike '%" + data.text + "%' or cast(sg.created_date as text) ilike '%" + data.text + "%')  order by sg.created_date desc;";
    }
    if (data.text === null || data.text === "") {
      result = await db_access.query(one);
      result.rows.filter((t) => {
        try {
          type =  t.type.includes('unknown');
        } catch {
          return false
        }
      });
      if (!type){
        result = await db_access.query(two);
      }
    } else{
      result = await db_access.query(one);
      result.rows.filter((t) => {
        try {
          type = t.type.includes('unknown');
        } catch {
          return false
        }
      });
      if (!type) {
        result = await db_access.query(three);
      } else{
        result = await db_access.query(four);

      }
    }
    const resp = result.rows;

    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
