const db_access = require("../db_access");
const utilities = require("../utils/utilities");

// Retrieve smsTransfer.
exports.get_sms_transfer = async (req, res) => {
  // #swagger.tags = ['SmsTransfer']
  /* #swagger.responses[200] = { 
      schema: { "$ref": "#/definitions/smsTransfer" }} */

  try {
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let result = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if (typeAgent === 0) {
      result = await db_access.query("select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id where st.agent_id="+agentId+" order by st.created_date desc;");
    } else if (typeAgent === 1) {
      result = await db_access.query("select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, a1.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join agent a1 on ag.agent = a1.agent_id inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id where st.agent_id="+agentNo+" or ag.agent = a1.agent_id order by st.created_date desc;");
    } else if (typeAgent === 2) {
      result = await db_access.query("select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id order by st.created_date desc;");
    } else if (typeAgent === 3) {
      result = await db_access.query("select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id order by st.created_date desc;");
    }
     const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Retrieve smsTransfer.
exports.get_search_sms_transfer = async (req, res) => {
  // #swagger.tags = ['SmsTransfer']
  /* #swagger.responses[200] = {
      schema: { "$ref": "#/definitions/smsTransfer" }} */

  try {
    const data = req.body;
    const agentId = req.user.agent_id;
    const type = "select a.type_user, a.agent from agent a where a.agent_id =" + agentId + ";";
    let resupp = await db_access.query(type);
    const resps = resupp.rows;
    let typeAgent = null;
    let bankName = "";
    let statusName = "";
    let dates = "";
    let times = "";
    let amount = "";
    let one = "";
    let agentNo = "";
    for (let i = 0; i < resps.length; i++) {
      typeAgent = resps[i].type_user;
      agentNo = resps[i].agent;
    }
    if ((data.bank_name === "" || data.bank_name === null) && (data.status_name === "" || data.status_name === null) &&
    (data.date === "" || data.date === null) && (data.time === "" || data.time === null) && (data.amount === "" || data.amount === null)) {
      if (typeAgent === 0) {
        one = "select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id where st.agent_id="+agentId+" order by st.created_date desc;";
      } else if (typeAgent === 1) {
        one = "select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, a1.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join agent a1 on ag.agent = a1.agent_id inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id where st.agent_id="+agentNo+" or ag.agent = a1.agent_id order by st.created_date desc;";
      } else if (typeAgent === 2) {
        one = "select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id order by st.created_date desc;";
      } else if (typeAgent === 3) {
        one = "select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id order by st.created_date desc;";
      }
    } else{
      if (data.bank_name === "" || data.bank_name === null){
        bankName =  "";
      } else{
        bankName =  " b.bank_name ilike '%"+data.bank_name+"%'";
      }
      if (data.bank_name === "" || data.bank_name === null) {
        if (data.status_name === "" || data.status_name === null) {
          statusName = "";
        } else {
          statusName = " s.status_name_th ilike '%" + data.status_name + "%'";
        }
      } else{
        if (data.status_name === "" || data.status_name === null) {
          statusName = "";
        } else {
          statusName = " and s.status_name_th ilike '%" + data.status_name + "%'";
        }
      }
      if (data.date === "" || data.date === null || data.date === ''){
        dates =  "";
      } else {
        let day =  data.date.substring(0,2)
        let month = data.date.substring(3,5)
        let switchDate = month+"-"+ day;
        if ((data.status_name === "" || data.status_name === null) && (data.bank_name === "" || data.bank_name === null)) {
          dates = " CAST(st.created_date + INTERVAL '7 hours'  as text) ilike '%" + switchDate + "%'";
        } else{
          dates = " and CAST(st.created_date + INTERVAL '7 hours' as text) ilike '%" + switchDate + "%'";
        }
      }
      if (data.time === "" || data.time === null) {
        times = "";
      } else {
        if((data.status_name === "" || data.status_name === null) && (data.bank_name === "" || data.bank_name === null) &&
            (data.date === "" || data.date === null || data.date === '')) {
          // times = "CAST(st.created_date as text) ilike '%" + data.time + "%'";
          times = "TO_CHAR(st.created_date + INTERVAL '7 hours', 'HH24:MI') ilike '%" + data.time + "%'";
        } else{
          times = " and TO_CHAR(st.created_date + INTERVAL '7 hours', 'HH24:MI') ilike '%" + data.time + "%'";
        }
      }
      if (data.amount === "" || data.amount === null){
        amount =  "";
      } else{
        if((data.status_name === "" || data.status_name === null) && (data.bank_name === "" || data.bank_name === null) &&
            (data.date === "" || data.date === null || data.date === '') && (data.time === "" || data.time === null)) {
          amount = " cast(st.amount as text) ilike '%" + data.amount + "%'";
        } else{
          amount = " and cast(st.amount as text) ilike '%" + data.amount + "%'";
        }
      }
      one += "select  st.id, st.amount, st.response, st.message, st.created_date, b.bank_name, s.status_name_th, ag.username from sms_transfer st inner join agent ag on st.agent_id = ag.agent_id  inner join bank b on st.bank_id = b.id inner join status s on st.status = s.id ";
      if (typeAgent === 0) {
        one += " where st.agent_id=" + agentId + " and ("+ bankName + statusName + dates + times + amount + ") order by st.created_date desc;;";
      }else if (typeAgent === 1 ) {
        one += " where st.agent_id=" + agentNo + " and ("+ bankName + statusName + dates + times + amount + ") order by st.created_date desc;;";
      } else if (typeAgent === 2 ) {
        one += " where " + bankName + statusName + dates + times + amount + " order by st.created_date desc;";
      } else if ( typeAgent === 3) {
        one += " where " +bankName + statusName + dates + times + amount + " order by st.created_date desc;" ;
      }
    }
    let result = await db_access.query(one);
    const resp = result.rows;
    res.status(200).json({
      code: 200,
      message: "Success",
      data: resp,
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};


// Create a new smsTransfer.
exports.post_sms_transfer = async (req, res) => {
  // #swagger.tags = ['SmsTransfer']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/smsTransfer' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatesmstransfer($1,$2,$3,$4,$5,$6);", [
      0,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.message) ? null : data.message,
      utilities.is_empty_string(data.response) ? null : data.response,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Update a new smsTransfer.
exports.put_sms_transfer = async (req, res) => {
  // #swagger.tags = ['SmsTransfer']
  /*  #swagger.parameters['obj'] = {
          in: 'body',
          schema: { $ref: '#/definitions/smsTransfer' }
  } */
  const data = req.body;

  try {
    await db_access.query("CALL public.spinsertupdatesmstransfer($1,$2,$3,$4,$5,$6);", [
      utilities.is_empty_string(data.id) ? null : data.id,
      utilities.is_empty_string(data.bank_id) ? null : data.bank_id,
      utilities.is_empty_string(req.user.agent_id) ? null : req.user.agent_id,
      utilities.is_empty_string(data.status) ? null : data.status,
      utilities.is_empty_string(data.message) ? null : data.message,
      utilities.is_empty_string(data.response) ? null : data.response,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};

// Delete smsTransfer.
exports.delete_sms_transfer = async (req, res) => {
  // #swagger.tags = ['SmsTransfer']
  const data = req.query;

  try {
    await db_access.query("CALL public.spdeletesmsTransfer($1);", [
      utilities.is_empty_string(data.id) ? null : data.id,
    ]);

    res.status(200).json({
      code: 200,
      message: "Success",
    });
  } catch (error) {
    console.log("error : ", error);
    res.status(500).send({
      code: 500,
      message: error,
    });
  }
};
