const { Pool } = require('pg')
const config = require("./config/config");
const pool_config = () => (config.pg_connection);

const query = async (command, parameters) => {
    const pool = new Pool(pool_config())
    const result = await pool.query(command, parameters);
    await pool.end();
    return result;
};

module.exports = {
    query
};