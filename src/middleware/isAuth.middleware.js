const jwt = require('jsonwebtoken');
const config = require('../config/config');
const db_access = require("../db_access");
const utilities = require("../utils/utilities");

exports.isAuth = (req, res, next) => {
  let authHeader  = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1]

  if (!token) {
    return res.status(403).send({
      code: 403,
      message: 'No token provided!',
    });
  }
  
  jwt.verify(token, config.jwt_secret, (err, username) => {
    if (err) {
      return res.status(401).send({
        code: 401,
        message: 'Unauthorized!',
      });
    }
    req.username = username;
    next();
  });
};

exports.isAccessAuth = (req, res, next) => {
  let authHeader  = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).send({
      code: 403,
      message: 'No token provided!',
    });
  }

  jwt.verify(token, config.access_secret, (err, decoded) => {
    global.io.sockets.emit('action_admin_logout', {
      agent_id: decoded?.agent_id,
      uuid: decoded?.uuid
    });

    if (err) {
      return res.status(401).send({
        code: 401,
        message: 'Unauthorized!',
      });
    }
    req.user = decoded;
    next();
  });
};

exports.jwtRefreshTokenValidate = (req, res, next) => {
  let authHeader  = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).send({
      code: 403,
      message: 'No token provided!',
    });
  }

  jwt.verify(token, config.refresh_secret, async (err, decoded) => {
    let result = await db_access.query(
      "SELECT uuid FROM public.agent WHERE agent_id=$1;",
      [utilities.is_empty_string(decoded?.agent_id) ? 0 : decoded?.agent_id]
    );

    if (err || result.rows[0].uuid != decoded?.uuid) {
      return res.status(401).send({
        code: 401,
        message: 'Unauthorized!',
      });
    }
    req.user = decoded;
    delete req.user.exp;
    delete req.user.iat;
    next();
  });
}