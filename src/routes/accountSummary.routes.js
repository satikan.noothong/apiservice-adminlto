const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const accountSummary = require("../controllers/accountSummary.controller");

router.get("/getToday", isAccessAuth, accountSummary.get_by_today);
router.get("/getBySixMonth", isAccessAuth, accountSummary.get_by_six_month);
router.post("/getByStartDateEndDate", isAccessAuth, accountSummary.get_by_date);
module.exports = router;
