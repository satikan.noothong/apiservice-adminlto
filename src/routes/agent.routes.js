const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const agent = require("../controllers/agent.controller");


router.get("/get", isAccessAuth, agent.get_agent);
router.get("/getMemberNetwork/:id", isAccessAuth, agent.get_listMemberNetwork);
router.get("/getCredit", isAccessAuth, agent.get_agent_credit);
router.get("/getAgentId/:id", isAccessAuth,agent.get_agent_id);
router.get("/get_agent_by_id", isAccessAuth,agent.get_agent_by_login);
router.get("/getDetailAgent/:id", isAccessAuth,agent.get_agent_by_id);
router.get("/getBank/:id", isAccessAuth,agent.get_bankByAgentId);
router.get("/getDirectDepositWithdrawById/:id", isAccessAuth,agent.get_listDirectDepositWithdrawById);
router.get("/getReportCreditById/:id", isAccessAuth,agent.get_reportCreditById);
router.get("/listAgent", isAccessAuth, agent.get_list_agent);
router.post("/search", isAccessAuth,agent.get_search_list_agent);
router.post("/createDepositWithdraw", isAccessAuth,agent.post_direct_withdraw);
router.post("/create", isAccessAuth, agent.post_agent);
router.put("/update", isAccessAuth, agent.put_agent);
router.put("/updateStatus", isAccessAuth, agent.put_status_agent);
router.post("/createNote", isAccessAuth, agent.post_note_agent);
router.delete("/delete/:id", isAccessAuth, agent.delete_agent);
router.delete("/deleteNote/:id", isAccessAuth , agent.delete_note_agent);
router.get("/getNoteAgent/:id", isAccessAuth, agent.get_note_agent);

module.exports = router;
