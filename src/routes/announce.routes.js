const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const announce = require("../controllers/announce.controller");


router.get("/get", isAccessAuth, announce.get_announce);
router.get("/getById/:id", isAccessAuth, announce.get_announce_by_id);
router.post("/search", isAccessAuth, announce.get_search_announce);
router.post("/create", isAccessAuth, announce.post_announce);
router.put("/update", isAccessAuth, announce.put_announce);
router.delete("/delete/:id", isAccessAuth, announce.delete_announce);

module.exports = router;
