const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const api = require("../controllers/api.controller");

router.get("/get", isAccessAuth, api.get_type);
router.get("/getAgent", isAccessAuth, api.get_agent);
router.get("/getListAgent", isAccessAuth, api.get_list_agent);
router.get("/getBank", isAccessAuth, api.get_bank_agent);
router.get("/getType", isAccessAuth, api.get_type);
router.get("/getTitleName", isAccessAuth, api.get_title);
router.post("/getSendOtp", isAccessAuth, api.get_send_otp);

module.exports = router;
