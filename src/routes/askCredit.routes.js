const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const askCredit = require("../controllers/askCredit.controller");
const askCredit_validator = require("../validator/askCredit.validator");

router.get("/get", isAccessAuth, askCredit.get_askCredit);
router.post("/search", isAccessAuth, askCredit.get_search_askCredit);
router.post("/create", isAccessAuth, askCredit.post_askCredit);
router.put("/update", isAccessAuth, askCredit.put_askCredit);
router.put("/updateStatus", isAccessAuth, askCredit.put_updateStatusAskCredit);
router.delete("/delete/:id", isAccessAuth,  askCredit.delete_askCredit);

module.exports = router;
