const express = require('express');
const router = express.Router();
const { jwtRefreshTokenValidate } = require("../middleware/isAuth.middleware");
const authentication = require("../controllers/authentication.controller");

router.post("/", authentication.authentication);
router.post("/refresh", jwtRefreshTokenValidate, authentication.refresh_token);

module.exports = router;