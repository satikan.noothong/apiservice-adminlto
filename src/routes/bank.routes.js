const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const bank = require("../controllers/bank.controller");

router.get("/get", isAccessAuth, bank.get_bank);

module.exports = router;
