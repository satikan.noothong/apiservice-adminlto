const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const bank_agent = require("../controllers/bankAgent.controller");
const bank_agent_validator = require("../validator/bankAgent.validator");
const agent = require("../controllers/agent.controller");

router.get("/get", isAccessAuth, bank_agent.get_bank_agent);
router.get("/getBankById/:id", isAccessAuth,bank_agent.get_bankById);
router.post("/search", isAccessAuth, bank_agent.get_search_bank_agent);
router.post("/create", isAccessAuth, bank_agent.post_bank_agent);
router.put("/update", isAccessAuth, bank_agent.put_bank_agent);
router.delete("/delete", isAccessAuth, bank_agent_validator.delete_bank_agent, bank_agent.delete_bank_agent);

module.exports = router;
