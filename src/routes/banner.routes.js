const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const banner = require("../controllers/banner.controller");
const multer = require('multer')
const storageEngine = multer.diskStorage({
    destination: "./banner",
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}--${file.originalname}`);
    },
});

const upload = multer({
    storage: storageEngine,
    limits: { fileSize: 8*1024*1024 },
});

router.get("/get", isAccessAuth, banner.get_banner);
router.get("/getById/:id", isAccessAuth, banner.get_banner_by_id);
router.post("/create", isAccessAuth, upload.single("file"), banner.post_banner);
router.put("/update", isAccessAuth, upload.single("file"), banner.put_banner);
router.put("/updateData", isAccessAuth, upload.single("file"), banner.put_banner_data);
router.delete("/delete/:id", isAccessAuth, banner.delete_banner);

module.exports = router;
