const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const board = require("../controllers/board.controller");

router.get("/get", isAccessAuth, board.get_board);
router.get("/getRef", isAccessAuth, board.get_ref_board);

module.exports = router;
