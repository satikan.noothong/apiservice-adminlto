const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const commission = require("../controllers/commission.controller");

router.get("/get", isAccessAuth, commission.get_commission);
router.post("/getByWeek", isAccessAuth, commission.get_commission_by_week);

module.exports = router;
