const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const contact = require("../controllers/contact.controller");
const contact_validator = require("../validator/contact.validator");

router.get("/get", isAccessAuth, contact.get_contact);
router.get("/getById/:id", isAccessAuth, contact.get_contact_by_id);
router.post("/create", isAccessAuth, contact.post_contact);
router.put("/update", isAccessAuth, contact.put_contact);
router.delete("/delete", isAccessAuth, contact_validator.delete_contact, contact.delete_contact);

module.exports = router;
