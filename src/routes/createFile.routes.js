const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const createFile = require("../controllers/createFile.controller");
const multer = require('multer')
const storageEngine = multer.diskStorage({
    destination: "./banner",
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}--${file.originalname}`);
    },
});

const upload = multer({
    storage: storageEngine,
    limits: { fileSize: 1000000 },
});

router.post("/createFile", upload.single("file"),isAccessAuth, createFile.post_create_filep);

module.exports = router;
