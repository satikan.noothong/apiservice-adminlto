const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const dailySummary = require("../controllers/dailySummary.controller");

router.post("/get", isAccessAuth, dailySummary.get_daily_summary);
router.post("/getSumYiki", isAccessAuth, dailySummary.get_daily_summary_by_yiki);
router.post("/getYiki", isAccessAuth, dailySummary.get_daily_summary_yiki);
router.post("/getByHuay", isAccessAuth, dailySummary.get_daily_summary_by_huay);
router.post("/getSumAlldayYiki", isAccessAuth, dailySummary.get_daily_summary_huay_all_day_by_yiki);

module.exports = router;
