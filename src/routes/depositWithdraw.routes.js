const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const depositwithdraw = require("../controllers/depositWithdraw.controller");
const depositwithdraw_validator = require("../validator/depositWithdraw.validator");

router.get("/get", isAccessAuth, depositwithdraw.get_depositwithdraw);
router.post("/create", isAccessAuth, depositwithdraw.post_depositwithdraw);
router.put("/update", isAccessAuth, depositwithdraw.put_depositwithdraw);
router.delete("/delete", isAccessAuth, depositwithdraw_validator.delete_depositwithdraw, depositwithdraw.delete_depositwithdraw);

module.exports = router;
