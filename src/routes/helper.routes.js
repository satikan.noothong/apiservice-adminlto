const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const helper = require("../controllers/helper.controller");
const agent = require("../controllers/agent.controller");

router.get("/get", isAccessAuth, helper.get_helper);
router.get("/getMemberNetwork/:id", isAccessAuth, agent.get_listMemberNetwork);
router.get("/listHelper", isAccessAuth, helper.get_list_helper);
router.get("/getDirectDepositWithdrawById/:id", isAccessAuth,agent.get_listDirectDepositWithdrawById);
router.get("/getReportCreditById/:id", isAccessAuth,agent.get_reportCreditById);
router.get("/getHelperId/:id", isAccessAuth, agent.get_agent_id)
router.get("/getBank/:id", isAccessAuth,agent.get_bankByAgentId);
router.get("/getDetailHelper/:id", isAccessAuth,agent.get_agent_by_id);
router.post("/search", isAccessAuth,helper.get_search_list_helper);
router.post("/createDepositWithdraw", isAccessAuth,agent.post_direct_withdraw);
router.post("/create", isAccessAuth, helper.post_helper);
router.put("/update", isAccessAuth, agent.put_agent);
router.put("/updateStatus", isAccessAuth, agent.put_status_agent);
router.delete("/delete/:id", isAccessAuth , agent.delete_agent);
router.delete("/deleteNote/:id", isAccessAuth, agent.delete_note_agent);
router.post("/createNote", isAccessAuth, agent.post_note_agent);
router.get("/getNoteHelper/:id", isAccessAuth, agent.get_note_agent);

module.exports = router;
