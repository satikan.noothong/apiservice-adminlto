const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const inbox = require("../controllers/inbox.controller");
const inbox_validator = require("../validator/inbox.validator");

router.get("/get", isAccessAuth, inbox.get_inbox);
router.post("/search", isAccessAuth, inbox.get_search_inbox);
router.post("/create", isAccessAuth, inbox.post_inbox);
router.put("/update", isAccessAuth, inbox.put_inbox);
router.delete("/delete", isAccessAuth, inbox_validator.delete_inbox, inbox.delete_inbox);

module.exports = router;
