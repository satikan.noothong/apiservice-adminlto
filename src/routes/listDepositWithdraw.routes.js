const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const listDepositWithdraw = require("../controllers/listDepositWithdraw.controller");
const listDepositWithdraw_validator = require("../validator/listDepositWithdraw.validator");
const askCredit = require("../controllers/askCredit.controller");
const multer = require("multer");
const listHuay = require("../controllers/listHuay.controller");


router.get("/get", isAccessAuth, listDepositWithdraw.get_listDepositWithdraw);
router.post("/search", isAccessAuth, listDepositWithdraw.get_search_listDepositWithdraw);
router.post("/getSlip", isAccessAuth, listDepositWithdraw.get_slipDepositWithdraw);
router.post("/create", isAccessAuth, listDepositWithdraw.post_listDepositWithdraw);
router.post("/getDetailHuay", isAccessAuth, listDepositWithdraw.post_listDetailHuay);
router.put("/update", isAccessAuth, listDepositWithdraw.put_listDepositWithdraw);
router.put("/updateStatus", isAccessAuth, listDepositWithdraw.put_updateStatusDepositWithdraw);
router.delete("/delete", isAccessAuth, listDepositWithdraw_validator.delete_listDepositWithdraw, listDepositWithdraw.delete_listDepositWithdraw);

module.exports = router;
