const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const listDirectDepositWithdraw = require("../controllers/listDirectDepositWithdraw.controller");
const listDirectDepositWithdraw_validator = require("../validator/listDirectDepositWithdraw.validator");

router.get("/get", isAccessAuth, listDirectDepositWithdraw.get_listDirectDepositWithdraw);
router.post("/search", isAccessAuth, listDirectDepositWithdraw.get_search_listDirectDepositWithdraw);

module.exports = router;
