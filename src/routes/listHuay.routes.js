const express = require('express');
const router = express.Router();
const { isAccessAuth } = require('../middleware/isAuth.middleware');
const listHuay = require('../controllers/listHuay.controller');
const { request_validator } = require('../validator/request.validator');

router.get('/get', isAccessAuth, listHuay.get_listHuay);

router.get('/getNumberSetFalse', isAccessAuth, listHuay.get_numbersetfalse);
router.get('/getNumberSetTrue', isAccessAuth, listHuay.get_numbersettrue);

router.post('/getRateByNumber', isAccessAuth, listHuay.get_ratebynumber);

router.get('/getMenuHuay', isAccessAuth, listHuay.get_listMenuHuay);

router.post('/search', isAccessAuth, listHuay.get_search_listHuay);

router.post('/getDetailHuay', isAccessAuth, listHuay.post_listDetailHuay);

router.get('/getHeaderHuay', isAccessAuth, listHuay.get_list_header_huay);
//huay setting
router.get('/setting/type/:id', isAccessAuth, listHuay.get_config_huay);
router.get('/setting/type/:id/info', isAccessAuth, listHuay.get_config_huay_info);
router.put('/setting/lotto/:id/update', isAccessAuth, listHuay.update_huay_config);
router.get('/week/:id/result', isAccessAuth, listHuay.get_huay_week);
router.get('/week/:id/result/info', isAccessAuth, listHuay.get_huay_week_info);
router.put('/week/:id/result/update', isAccessAuth, listHuay.update_huay_week);
router.get('/rate-limit/:id/list', isAccessAuth, listHuay.list_huay_rate_limit);
router.put('/rate-limit/:id/update', isAccessAuth, listHuay.update_huay_rate_limit);
router.post('/rate-limit/:id/add', isAccessAuth, listHuay.add_huay_rate_limit);
router.delete('/rate-limit/:id/delete', isAccessAuth, listHuay.delete_huay_rate_limit);
router.get('/number-limit/:id/list', isAccessAuth, listHuay.list_number_limit);
router.post('/number-limit/:id', isAccessAuth, listHuay.update_number_limit);
router.delete('/number-limit/:id/delete', isAccessAuth, listHuay.delete_number_limit);
router.get('/user-limit/:id/list', isAccessAuth, listHuay.list_user_limit);
router.post('/user-limit/:id', isAccessAuth, listHuay.upsert_user_limit);
router.delete('/user-limit/:id/delete', isAccessAuth, listHuay.delete_user_limit);
router.get('/result/:id/summary', isAccessAuth, listHuay.get_result_huay);
// router.get('/lotto/:id/lists', isAccessAuth, listHuay.get)

module.exports = router;
