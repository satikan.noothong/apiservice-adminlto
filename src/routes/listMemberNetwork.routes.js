const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const listMemberNetwork = require("../controllers/listMemberNetwork.controller");
const listMember = require("../controllers/listMember.controller");

router.get("/get", isAccessAuth, listMemberNetwork.get_listMemberNetwork);
router.get("/getDetailMemberNetwork/:id", isAccessAuth,listMember.get_member_by_id);
router.post("/search", isAccessAuth, listMemberNetwork.get_search_listMemberNetwork);

module.exports = router;
