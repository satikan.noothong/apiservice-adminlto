const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const login = require("../controllers/login.controller");
const master_validator = require("../validator/master.validator");

router.post('/login', login.post_login);
router.get("/get", isAccessAuth, login.get_user);
router.post("/create", isAccessAuth, login.post_user);
router.put("/update", isAccessAuth, login.put_user);
router.delete("/delete", isAccessAuth, master_validator.delete_user, login.delete_user);

module.exports = router;
