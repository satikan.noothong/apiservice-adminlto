const express = require('express');
const router = express.Router();
const { isAccessAuth } = require('../middleware/isAuth.middleware');
const master = require('../controllers/master.controller');
const master_validator = require('../validator/master.validator');

router.get('/user', isAccessAuth, master.get_user);
router.post('/user', isAccessAuth, master.post_user);
router.put('/user', isAccessAuth, master.put_user);
router.delete('/user', isAccessAuth, master_validator.delete_user, master.delete_user);

router.get('/commission', isAccessAuth, master.get_commission);
router.get('/news/flash-news', master.get_flash_news);
router.post('/news/flash-news', isAccessAuth, master.update_flash_news);

router.get('/number-types', isAccessAuth, master.get_number_types)

router.get('/credit/report', isAccessAuth, master.get_report_credit)
router.post("/cancel_stake_category", isAccessAuth, master_validator.post_cancel_stake_category, master.post_cancel_stake_category);
router.post("/reward_result_huay", isAccessAuth, master_validator.post_reward_result_huay, master.post_reward_result_huay);

router.get('/config_result_tong', isAccessAuth, master.get_config_result_tong);
router.put('/config_result_tong', isAccessAuth, master.put_config_result_tong);

module.exports = router;
