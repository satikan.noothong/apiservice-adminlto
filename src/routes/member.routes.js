const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const member = require("../controllers/member.controller");

router.get("/get", isAccessAuth, member.get_member);

module.exports = router;
