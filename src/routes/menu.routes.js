const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const menu = require("../controllers/menu.controller");

router.get("/get", isAccessAuth, menu.get_menu);

router.get("/get/list_menu", isAccessAuth, menu.get_list_menu);

module.exports = router;
