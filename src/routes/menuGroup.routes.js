const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const menuGroup = require("../controllers/menuGroup.controller");

router.get("/get", isAccessAuth, menuGroup.get_menu_group);

module.exports = router;
