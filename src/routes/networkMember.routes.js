const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const network_member = require("../controllers/networkMember.controller");
const network_member_validator = require("../validator/networkMember.validator");

router.get("/get", isAccessAuth, network_member.get_network_member);
router.post("/create", isAccessAuth, network_member.post_network_member);
router.put("/update", isAccessAuth, network_member.put_network_member);
router.delete("/delete", isAccessAuth, network_member_validator.delete_network_member, network_member.delete_network_member);

module.exports = router;
