const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const outbox = require("../controllers/outbox.controller");
const outbox_validator = require("../validator/outbox.validator");
const inbox = require("../controllers/inbox.controller");

router.get("/get", isAccessAuth, outbox.get_outbox);
router.post("/search", isAccessAuth, outbox.get_search_outbox);
router.get("/getById/:id", isAccessAuth, outbox.get_outbox_by_id);
router.post("/create", isAccessAuth, outbox.post_outbox);
router.put("/update", isAccessAuth, outbox.put_outbox);
router.delete("/delete", isAccessAuth, outbox_validator.delete_outbox, outbox.delete_outbox);

module.exports = router;
