const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const permission = require("../controllers/permission.controller");

router.get("/get", isAccessAuth, permission.get_permission);
router.get("/getPermissonUser", isAccessAuth, permission.get_permission_user);

module.exports = router;
