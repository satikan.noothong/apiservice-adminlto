const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const popup = require("../controllers/popup.controller");
const popup_validator = require("../validator/popup.validator");
const multer = require('multer')
const storageEngine = multer.diskStorage({
    destination: "./banner",
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}--${file.originalname}`);
    },
});

const upload = multer({
    storage: storageEngine,
    limits: { fileSize: 1000000 },
});

router.get("/get", isAccessAuth, popup.get_popup);
router.get("/getById", isAccessAuth, popup.get_popup_by_id);
router.post("/create", isAccessAuth, popup.post_popup);
router.put("/update",isAccessAuth, popup.put_popup);
router.post("/createFile", upload.single("file"),isAccessAuth, popup.put_picture_popup);
router.delete("/delete", isAccessAuth, popup_validator.delete_popup, popup.delete_popup);

module.exports = router;
