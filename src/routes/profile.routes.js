const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const profile = require("../controllers/profile.controller");

router.get("/get", isAccessAuth, profile.get_profile);
router.put("/update", isAccessAuth, profile.put_profile);

module.exports = router;
