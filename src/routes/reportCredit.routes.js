const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const reportCredit = require("../controllers/reportCredit.controller");
const reportCredit_validator = require("../validator/reportCredit.validator");

router.get("/get", isAccessAuth, reportCredit.get_reportCredit);
router.get("/getExpenseSummary", isAccessAuth, reportCredit.get_expenseSummary);
router.post("/getExpenseSummary/search", isAccessAuth, reportCredit.get_expenseSummaryByWeek);
router.post("/searchReportCredit", isAccessAuth, reportCredit.get_search_reportCredit);
router.get("/getReportCreditMaster", isAccessAuth, reportCredit.get_reportCreditMaster);
router.post("/getSearchReportCreditMaster", isAccessAuth, reportCredit.get_search_reportCreditMaster);
router.get("/getReportCreditMasterByMaster", isAccessAuth, reportCredit.get_reportCreditMasterByMaster);
router.post("/getSearchReportCreditMasterByMaster", isAccessAuth, reportCredit.get_search_reportCreditMasterByMaster);
router.post("/create", isAccessAuth, reportCredit.post_reportCredit);
router.put("/update", isAccessAuth, reportCredit.put_reportCredit);
router.delete("/delete", isAccessAuth, reportCredit_validator.delete_reportCredit, reportCredit.delete_reportCredit);
router.get("/summary-budget", isAccessAuth, reportCredit.get_summary_budget);
router.get("/summary-budget-month", isAccessAuth, reportCredit.get_summary_budget_by_month);
router.get("/summary-budget-week", isAccessAuth, reportCredit.get_summary_budget_by_week);

module.exports = router;
