const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const settingDepositWithdraw = require("../controllers/settingDepositWithdraw.controller");
const settingDepositWithdraw_validator = require("../validator/settingDepositWithdraw.validator");

router.get("/get", isAccessAuth, settingDepositWithdraw.get_settingDepositWithdraw);
router.post("/create", isAccessAuth, settingDepositWithdraw.post_settingDepositWithdraw);
router.put("/update", isAccessAuth, settingDepositWithdraw.put_settingDepositWithdraw);
router.delete("/delete", isAccessAuth, settingDepositWithdraw_validator.delete_settingDepositWithdraw, settingDepositWithdraw.delete_settingDepositWithdraw);

module.exports = router;
