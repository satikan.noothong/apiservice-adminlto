const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const smsGeneral = require("../controllers/smsGeneral.controller");
const smsGeneral_validator = require("../validator/smsGeneral.validator");

router.get("/get", isAccessAuth, smsGeneral.get_sms_general);
router.post("/search", isAccessAuth, smsGeneral.get_search_sms_general);
router.post("/create", isAccessAuth, smsGeneral.post_sms_general);
router.put("/update", isAccessAuth, smsGeneral.put_sms_general);
router.delete("/delete", isAccessAuth, smsGeneral_validator.delete_sms_general, smsGeneral.delete_sms_general);

module.exports = router;
