const express = require('express');
const router = express.Router();
const { isAccessAuth } = require("../middleware/isAuth.middleware");
const smsTransfer = require("../controllers/smsTransfer.controller");
const smsTransfer_validator = require("../validator/smsTransfer.validator");

router.get("/get", isAccessAuth, smsTransfer.get_sms_transfer);
router.post("/search", isAccessAuth, smsTransfer.get_search_sms_transfer);
router.post("/create", isAccessAuth, smsTransfer.post_sms_transfer);
router.put("/update", isAccessAuth, smsTransfer.put_sms_transfer);
router.delete("/delete", isAccessAuth, smsTransfer_validator.delete_sms_transfer, smsTransfer.delete_sms_transfer);

module.exports = router;
