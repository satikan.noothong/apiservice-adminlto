const utilities = require('./utilities');
const model = require('../commons/constants/validator.constants');

exports.get_error_by_code = async (field) => {
    let error_desc = model.msg_validate[field.msg_validate];
    if (field) {
        Object.keys(field).forEach(function (item) {
            error_desc = utilities.replace_all(error_desc, '@' + item, field[item]);
        });
        return error_desc;
    }
    else
        return `Error not found`;
};