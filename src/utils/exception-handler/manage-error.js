const { INTERNAL_SERVER_ERROR } = require("../../commons/errors/error-message");
const ApplicationError = require("../../commons/exceptions/application-error")

exports.manage_error = (error) => {
    try {
        if(error instanceof ApplicationError){
            throw error;
        }
        else{
            throw new ApplicationError(INTERNAL_SERVER_ERROR, error);
        }
    } catch (error) {
        throw error
    }
}