const { SUCCESS, INTERNAL_SERVER_ERROR } = require("../../commons/errors/error-message")
const ApplicationError = require("../../commons/exceptions/application-error")

exports.response_success_builder = (res, data=undefined) => {
    return res.status(SUCCESS.code).send({
        ...SUCCESS,
        data: data
    })
}

exports.response_error_builder = (res, error) => {
    if(error instanceof ApplicationError){
        return res.status(error.code).send({
            code: error.code,
            message: error.message,
            errors: error.error
        })
    }else {
        return res.status(500).send({
            ...INTERNAL_SERVER_ERROR,
            errors: error
        })
    }
}