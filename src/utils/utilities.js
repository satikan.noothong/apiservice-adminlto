
const fs = require("fs");
const axios = require('axios').default;
const AWS  = require('aws-sdk');
const config = require("../config/config");
const db_access = require('../db_access');
const httpContext = require('express-http-context')

const s3 = new AWS.S3({
    accessKeyId: config.aws_s3.access_key,
    secretAccessKey: config.aws_s3.access_secret,
    region: config.aws_s3.region
});

exports.is_empty_string = (str) => {
    return is_empty_string(str);
}

exports.replace_all = (target, search, replacement) => {
    if (target)
        return target.replace(search, replacement);
    else
        return target;
};

exports.gen_log = (function_name, data) => {
    console.log("---------------------------------------------------------------------");
    console.log(`${function_name} : `)
    console.log(JSON.stringify(data))
    console.log("---------------------------------------------------------------------");
}

exports.auth_osp = async (url, username, password) => {
    try {
        const resp = await axios.post(url, {
            username: username,
            password: password
        });
        return resp.data.data;
    } catch (error) {
        throw error;
    }
}

exports.validate_value_require = (value)=>{
    try{
        return !( value === undefined || value === '' || value === null);
    }catch(error){
        return false;
    }
}


// uploads a file to s3
exports.upload_file_s3 = async (file, filename) => {
    try {
        let files = file.path.replace(/\\/,'/');
        const fileStream = fs.createReadStream(files);
        const uploadParams = {
            Bucket: config.aws_s3.bucket_name,
            // Bucket: "ltobet",
            Body: fileStream,
            Key: filename
        };

        return s3.upload(uploadParams).promise();
    } catch (error) {
        throw error;
    }
};
const is_empty_string = (str) => {
    if (String(str).trim() == '0')
        return false;
    else
        return (!str || 0 === str.length || String(str).trim() === "" || str === null);
}

exports.defaultValue = (value, defaultValue) => {
    try {
        if(value() === undefined){
            return defaultValue
        }
        return value()
    } catch (error) {
        return defaultValue === undefined ? '' : defaultValue
    }
}

exports.getDataLotto = async () => {
    try {
        const sql = `select lt.id as lotto_type_id, lt.lotto_type_name, l.id as lotto_id, l.lotto_name 
        from lotto_type lt 
        inner join lotto l on l.lotto_type_id = lt.id
        order by lt.id asc, l.id asc`;

        const lotto_type = {}
        const result = await db_access.query(sql);
        for await (const item of result.rows){
            if(lotto_type[item.lotto_type_id]){
                lotto_type[item.lotto_type_id].push({
                    lotto_id: item.lotto_id,
                    lotto_name: item.lotto_name
                })
            }else{
                lotto_type[item.lotto_type_id] = [{
                    lotto_id: item.lotto_id,
                    lotto_name: item.lotto_name
                }];
            }
        }

        // console.log("lotto_type====>", lotto_type)
        await httpContext.set('lotto_type', lotto_type)
    } catch (error) {
        console.error(error)
    }
}