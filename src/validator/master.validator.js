const moment = require('moment');
const err_handler = require('../utils/error_handler');

exports.delete_user = async (req, res, next) => {
    req.check('user_id', 'user_id is required').notEmpty();
    const errors = await req.validationErrors();

    if (errors) {
        const err = errors.map(error => {
            let obj_err = {};
            const split_err = error.msg.split("|");
            obj_err.code = split_err[0];
            obj_err.message = split_err[1];
            return obj_err;
        });
        
        return res.send({
            code: 200,
            message: 'Success',
            errors: err
        })
    }
    next();
};

exports.post_cancel_stake_category = async (req, res, next) => {
    req.check('category_id').notEmpty().withMessage(await err_handler.get_error_by_code({fieldname: 'category_id', msg_validate: 'msg_required'})).isInt().withMessage(await err_handler.get_error_by_code({fieldname: 'category_id', msg_validate: 'msg_not_number'}));
    req.check('date_id').notEmpty().withMessage(await err_handler.get_error_by_code({fieldname: 'date_id', msg_validate: 'msg_required'})).custom(value => {
        return value ? moment(value, 'YYYY-MM-DD', true).isValid() : true
    }).withMessage(await err_handler.get_error_by_code({fieldname: 'date_id', formatdate: 'yyyy-MM-dd', msg_validate: 'msg_date_format'}));
    const errors = await req.validationErrors();

    if (errors) {
        const err = errors.map(error => {
            const split_err = error.msg;
            return split_err;
        });
        
        return res.status(500).send({
            code: 500,
            message: err
        });
    }
    next();
};

exports.post_reward_result_huay = async (req, res, next) => {
    req.check('category_id').notEmpty().withMessage(await err_handler.get_error_by_code({fieldname: 'category_id', msg_validate: 'msg_required'})).isInt().withMessage(await err_handler.get_error_by_code({fieldname: 'category_id', msg_validate: 'msg_not_number'}));
    req.check('date_id').notEmpty().withMessage(await err_handler.get_error_by_code({fieldname: 'date_id', msg_validate: 'msg_required'})).custom(value => {
        return value ? moment(value, 'YYYY-MM-DD', true).isValid() : true
    }).withMessage(await err_handler.get_error_by_code({fieldname: 'date_id', formatdate: 'yyyy-MM-dd', msg_validate: 'msg_date_format'}));
    const errors = await req.validationErrors();

    if (errors) {
        const err = errors.map(error => {
            const split_err = error.msg;
            return split_err;
        });
        
        return res.status(500).send({
            code: 500,
            message: err
        });
    }
    next();
};