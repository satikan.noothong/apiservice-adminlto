exports.delete_popup = async (req, res, next) => {
    req.check('popup_id', 'popup_id is required').notEmpty();
    const errors = await req.validationErrors();

    if (errors) {
        const err = errors.map(error => {
            let obj_err = {};
            const split_err = error.msg.split("|");
            obj_err.code = split_err[0];
            obj_err.message = split_err[1];
            return obj_err;
        });
        
        return res.send({
            code: 200,
            message: 'Success',
            errors: err
        })
    }
    next();
};
