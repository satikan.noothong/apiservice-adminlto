exports.request_validator = async (res, data, keys = []) => {
  try {
    for await (const key of keys) {
      if (data[key] === "" || data[key] === null || data[key] === undefined) {
        return await res.status(400).send({
          code: 400,
          message: `${key} is required`,
        });
      }
    }
  } catch (error) {
    return await res.status(500).send({
      code: 500,
      message: "error",
      errors: error,
    });
  }
};

exports.check_validator_basic = (data, keys = []) => {
  try {
    for (const key of keys) {
      if (data[key] === "" || data[key] === null || data[key] === undefined) {
        return false;
      }
    }
    return true;
  } catch (error) {
    return false;
  }
};
