const swaggerAutogen = require("swagger-autogen")();
const config = require("./src/config/config");
const utilities = require("./src/utils/utilities");

const doc = {
  info: {
    version: "1.0.0",
    title: "AdminLTO API",
    description: "Spec API The AdminLTO module.",
  },
  host: `${config.swagger_host}`,
  basePath: "/",
  schemes: ["http", "https"],
  consumes: ["application/json"],
  produces: ["application/json"],
  tags: [
    {
      name: "Authentication",
      description: "Endpoints",
    },
  ],
  securityDefinitions: {
    bearerAuth: {
      type: "apiKey",
      in: "header",
      name: "Authorization",
      description:
        "JWT Authorization header using the Bearer scheme. Enter 'Bearer' [space] and then your token in the text input below. Example: 'Bearer 12345abcdef'",
    },
  },
  security: [{ bearerAuth: [] }],
  definitions: {
    User: {
      user_id: "number",
      firstname: "string",
      lastname: "string"
    },
    Login: {
      username: "string",
      password: "string",
      ip_address: "string"
    },
    Contact: {
      agent_id: "number",
      phone: "string",
      phone1: "string",
      phone2: "string",
      phone3: "string",
      phone4: "string",
      phone5: "string",
      phone6: "string",
      phone7: "string",
      phone8: "string",
      phone9: "string",
      email: "string",
      line: "string",
      facebook: "string",
      group_facebook: "string",
      twitter: "string",
      website: "string",
      description: "string",
      status: "number"
    },
    BankAgent: {
      bank_id: "number",
      bank_name: "string",
      bank_account: "string",
      weight: "number",
      remark: "string",
      status: "number"
    },
    SmsGeneral: {
      from: "string",
      to: "string",
      type: "string",
      message: "string",
      date_time: "date"
    },
    SmsTransfer: {
      from: "string",
      to: "string",
      type: "string",
      message: "string",
      date_time: "date"
    },
    Outbox: {
      outbox_id: "number",
      send_to: "number",
      send_from: "number",
      title: "string",
      message: "string"
    },
    Announce: {
      id:"number",
      agent_id: "string",
      title: "string",
      detail: "string",
      status: "number"
    },
    SettingDepositWithdraw: {
      id: "number",
      agent_id: "number",
      withdraw_quota: "number",
      minimum_deposit: "number",
      minimum_withdraw: "number",
      maximum_withdraw: "number",
      turnover_percentage: "number",
      not_allow_no_source: "number",
      active_auto_approve: "number",
      max_amount_approve: "number",

    },
    Agent: {
      agent_id: "number",
      username: "string",
      password: "string",
      confirm_password: "string",
      status: "number",
      reference: "string",
      phone: "string",
      email: "string",
      ipAddress: "string",
      menu: "list"
    },
    AskCredit: {
      id: "number",
      agent_id: "number",
      type_id: "number",
      bank_id: "number",
      total: "number",
      description: "string",
      status: "number"
    },
    Helper: {
      helper_id: "number",
      username: "string",
      password: "string",
      confirm_password: "string",
      status: "number",
      reference: "number",
      phone: "string",
      email: "string",
      ipAddress: "string",
      menu: "list"
    },
    listMember: {
      id: "number",
      username: "string",
      ref_username: "string",
      email: "string",
      mobile_no: "string",
      birthday: "string",
      password: "string",
      confirm_password: "string",
    },
    PutConfigResultTong: {
      lotto_id: "number",
      start_date: "string format date : yyyy-MM-dd HH:mm:ss",
      end_date: "string format date : yyyy-MM-dd HH:mm:ss",
      max_tong: "number",
      max_double: "number",
      list_huay: "string",
      status: "number"
    },
    PostCancelStakeCategory: {
      category_id: "number",
      date_id: "string format date : yyyy-MM-dd"
    },
    PostRewardResultHuay: {
      category_id: "number",
      date_id: "string format date : yyyy-MM-dd",
      six_top: "string",
      four_top: "string",
      three_top: "string",
      three_front1: "string",
      three_front2: "string",
      three_back1: "string",
      three_back2: "string",
      two_top: "string",
      two_under: "string"
    },
    ResultLogin: {
      code: 200,
      message: "Success",
      data: {
        access_token: "string",
        refresh_token: "string",
      },
    },
  },
};

const outputFile = "./swagger-output.json";
const endpointsFiles = ["./app.js"];

swaggerAutogen(outputFile, endpointsFiles, doc);
// swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
//     require('./app.js'); // Your project's root file
// });
